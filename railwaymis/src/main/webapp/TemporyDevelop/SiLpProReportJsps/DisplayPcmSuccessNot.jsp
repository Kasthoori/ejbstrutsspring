<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sri Lanka Railways</title>
<link rel="stylesheet" type="text/css" href="http://localhost:8082/RLMSProject/TemporyDevelop/ReportsSiPosition/css_files/design_page.css" />
        <script type="text/javascript" src="http://localhost:8082/RLMSProject/JqueryJqplot_plugin/jquery.min.js"></script>
        <script type="text/javascript" src="http://localhost:8082/RLMSProject/JqueryJqplot_plugin/jquery-ui-1.10.4.custom.js"></script>
        <link type="text/css" rel="stylesheet" href="http://localhost:8082/RLMSProject/JqueryJqplot_plugin/jquery-ui-1.10.4.custom.css" />
        <link type="text/css" href="http://localhost:8082/RLMSProject/JqueryJqplot_plugin/jquery.jqplot.min.css" />
<style type="text/css" media="print">
    @media print
    {
    #non-printable { display: none; }
    #printable {
    display: block;
    width: 100%;
    height: 100%;
    }
   }
   
   @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
    
  @media print  { #notprint  { display: none; } }
/*   @media print  { #tmpPrintAlbum  { display: none; } } */
/*   @media print  { #tmpAddAlbum  { display: none; } } */
/*   @media print  { #tmpAlbum  { display: none; } } */
/*   @media print  { #sameitem { display: none; } } */
/*   @media print  { #storenum { display: none; } } */
/*   @media print  { #imagedisplay { display: none; } } */
/*   @media print  {#tmpSpecAlbum {display:none;}} */
 
                  
  
</style>
<style type="text/css">

.content {
    font-family:sans-serif;
    font-size:small;
    margin-left:100px;
    
}

.userTable {
        border-width:1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        margin-left:10px;
        width : 80%;
        cellpadding:5px;
    
}

.userTable  td {
    
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
       
}

.userTable th {
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        background-color: rgb(255, 255, 255);
}

.odd {
    
        background-color: #fffff0;
        
}

.even {
        background-color: #faf0e6;
}

.textInput {

        border: 1px solid #123B5F;
        color:#123B5F;
}

.submit {
 color: #000;
 font-size: 14px;
 width: 100px;
 height: 30px;
 border: 1px;
 margin: 1px;
 padding: 1px;
 background:#BEC0C0;
 margin-left:110px;
 
}


</style>
<s:head/>
</head>
<body>
 <div class="container_1">
        <div class="logo">
            <img src="http://localhost:8082/RLMSProject/TemporyDevelop/ReportsSiPosition/images/Ceylon_Government_Railways_logo.gif" width="60px" height="60px" />
        </div>
      <div class="heading_1">
            <h2>Sri Lanka Railway</h2>
        </div>
    </div>

<br/> <br />


<s:if test="condList.size() > 0">
            <div id="printable" class="content">
            
            
                    <table class="userTable">
                    <caption>Details of LP-SI Items Status (PCM-NOTHELD)</caption>
                       
                            <tr class="even">
                                <th>Item No</th>
                                <th>Section</th>
                                <th>File Number</th>
                                <th>File Date</th>
                                <th>TEC Date</th>
                                <th id="noprint">More....</th>
                                
                                
                            </tr>
                                <s:iterator value="condList" status="condListStatus" step="1">
                        
                                        <tr class="<s:if test="#condListStatus.odd == true">odd</s:if><s:else>even</s:else>">
                                        
                                        <td>
                                           <s:property value="#condListStatus.count" />
                                        </td>
                                        
                                        <td colspan="1">
                                        <s:property value="section"/>
                                        </td>
                                       
                                        <td colspan="1">  
                                         
                                            <s:property value="filenum" />
                                         
                                        </td>
                                        
                       
                                         <td colspan="1">
                                            
                                            <s:property value="filedate" />
                                        </td>
                                        
                                        <td colspan="1">
                                        
                                        	<s:property value="tecdate" />
                                        </td>
                                        
                                         <td id="notprint">
                					        
                					         <s:url id="getStockItemDetails" action="StockIemDetInFiles">
                					              <s:set var="filenumber" value="filenum" />
				                                  <s:param name="filenumber" value="%{filenumber}" />
				                             </s:url>
				                            
					                              <s:a href="%{getStockItemDetails}"><img width="20px" height="20px" alt="search" src="http://localhost:8082/RLMSProject/TemporyDevelop/ReportsSiPosition/images/search-icon.png">
					                         </s:a>  
					
                					    
                					    	
                					    </td>
                                        
                                        <%-- <td>
                                        
                                        
                                            <s:url id="verifybysrs" action="VerifyToGrantAppBySRS">
                                                   <s:param  name="storenumber" value="%{storenumber}" />
                                                   <s:param name="quantityreq" value="%{quantityreq}" />
                                                   <s:param name="unitreq" value="%{unitreq}" />
                                                   <s:param name="approvedby" value="%{approvedby}" />
                                                   <s:param name="appdate" value="%{appdate}" />
                                                   <s:param name="exdate" value="%{exdate}" />
                                            </s:url>
                                        <s:a href="%{verifybysrs}">Verify for Approval</s:a>  
                                          
                                            
                                    
                                        </td>
                 --%>
                                    </tr>
                                   
                                
                                </s:iterator>
                        
                    </table>
             
            </div>
        
        </s:if>
        <input id="notprint" type="button" id='tmpPrintAlbum' onclick="JavaScript:window.print();" value="print" class="submit" /> 

</body>
</html>