<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sd" uri="/struts-dojo-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<title>Insert title here</title>
<script type="text/javascript" language="javascript" src="JsFiles/EnterSILpDetailsSame.js"></script>
<script src="../../trimFunctions.js" language="JavaScript" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="../../jquery-1.4.2.js"></script>
<script>
function gen_siorder_num(Obj_order_type)
{button;
    var silpnumbers = Obj_order_type.value;
    
  
    
    $.post("GetSiLpDetails.jsp", { typeorder: "" + silpnumbers + "" },
               function(data){
         $('#detailDisplay').html(data);
        });
    
  
}
  
    function gen_store_num(){

        var totStoreNum = "";  

            var x = document.siform.storeNum.value;
            var y = document.siform.storeNumDigit1.value;
            var z = document.siform.storeNumDigit2.value;
            var i = document.siform.storeNumDigit3.value;
            var m = document.siform.storeNumDigit4.value;


           

          if((x != "") && (y == "") && (z == "") && (i == "") && (m == "")){

                     totStoreNum = x;
                    
              }else if((x != "") && (y != "") && (z == "") && (i == "") && (m == "")){

                         totStoreNum = x + " " + y;

                  }else if((x != "") && (y != "") && (z != "") && (i == "") && (m == "")){

                             totStoreNum = x + " " + y + "/" + z;

                      }else if((x != "") && (y != "") && (z != "") && (i != "") && (m == "")){


                                 totStoreNum = x + " " + y + "/" + z + "/" + i;
                          }else{

                                    totStoreNum = x + " " + y + "/" + z + "/" + i + "/"  +  m;

                              }

            
            

            $.post("CheckStoreNum.jsp",{storenum : "" + totStoreNum + ""},

                        function(data){
                            $('#imagedisplay').html(data)

                });

            
        }
    
    
    
    
    
</script>

<script type="text/javascript">

$(function() {
	  $('#storeNum1').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});

$(function() {
	  $('#storeNumDigit11').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});


$(function() {
	  $('#storeNumDigit22').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});

$(function() {
	  $('#storeNumDigit33').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});

$(function() {
	  $('#storeNumDigit44').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});


</script>

<style>
.tableoiltankfrm {
    
        font:12px verdana, arial, helvetica, sans-serif;
        border-width: 2px solid;
        border-color:#067bb6;
        border-style:solid;
        color:#0d157f;
        background-color: #88d5fc;
        width:95%;
        margin-left:5%;
        margin-top:5%;
}

.details {

        margin-left:5%;
        height:100px;
        overflow:auto;
}

.tablemessage {
    
        font:12px verdana, arial, helvetica, sans-serif;
        border-width: 2px solid;
        border-color:#067bb6;
        border-style:solid;
        color:#0d157f;
        background-color: #88d5fc;
        width:90%;
        margin-left:5%;
        margin-top:5%;
}


.tableoiltankfrm  tr td {
    
        background-color:#e2f3fc;
        margin: 5px;
        padding: 5px;
}

.tablemessage  tr td {
    
        background-color:#e2f3fc;
        margin: 5px;
        padding: 5px;
}

.tdLabel {
    
        font-weight: bold;
        align: top;
        
}

.label {} 

 .errorMessage {
    
        color:red;
        font-size: 12px;
}

.textInput {

        border: 1px solid #3998c7;
        color:#0d157f;
}

</style>
<sj:head jqueryui="true" />
</head>
<body>

<%

                                                                    request.getSession();

                                                                    Object isLogedIn = session.getAttribute("isLogedIn");
                                                                    
                                                                    if(isLogedIn == null){
                                                                        
                                                                        response.sendRedirect("../../Message.jsp");
                                                                    }
                                                                    
                                                                    session.setMaxInactiveInterval(1000);

                                                                    String  subDept = (String)session.getAttribute("subDept");
                                                                    String  shop  = (String)session.getAttribute("str3");
                                                                    String user = (String)session.getAttribute("str");
                                                                    String role = (String)session.getAttribute("strRole");
%>

<s:iterator value="getDetailList" status="getDetailListStatus">
<s:form name="siform" method="post" action="lpdetailregisterfromsamefile" onSubmit="javascript:return ValidateSIEnterDetailsSame();" >
        <table class="tableoiltankfrm">
        
            <tr>
                   <th colspan="8" align="center"><font size="3px">Register SI/LPO into Same File</font></th>
             </tr>
             <tr>
                <td colspan="2">
                    <s:label key="si-order.section" required="true"/>
                </td>
                    <td colspan="6">
                        <b><s:property value="section" /></b>
                        <s:set var="section" value="%{section}" />
                        <s:hidden name="type" value="%{section}" />
                    </td>
             </tr>
             <tr>
                <td colspan="2">
                        <s:label key="si-order.storenumber" required="true"/>
                </td>
                <td colspan="6">
                <s:textfield cssClass="textInput" id="storeNum1" name="storeNum" size="5" required="true"/>
                <s:textfield cssClass="textInput" id="storeNumDigit11" name="storeNumDigit1" size="5" required="true"/>
                <s:textfield cssClass="textInput" id="storeNumDigit22" name="storeNumDigit2" size="5"/>
                <s:textfield cssClass="textInput" id="storeNumDigit33" name="storeNumDigit3" size="5"/>
                <s:textfield cssClass="textInput" id="storeNumDigit44" name="storeNumDigit4" size="5"/>
                <div id="imagedisplay"></div>
                </td>
               </tr>
               
               <tr>
               
               <td colspan="2">
                   <s:label key="si-order.typeorder" />
                </td>
                <td colspan="6">
                     <b><s:property value="ordertype" /></b>
                        <s:set var="ordertype" value="%{ordertype}" />
                        <s:hidden name="typeorder" value="%{ordertype}" />
                        
                      </td>
               </tr>
               
               
               <tr> 
               
               <td colspan="2">
                   <s:label key="si-order.lponum" />
                </td>
                <td colspan="2">
                    
                    <b><s:property value="silpnum" /></b>
                        <s:set var="silpnum" value="%{silpnum}" />
                        <s:hidden name="lponum" value="%{silpnum}" />
                    <input type="hidden" value="<%= shop %>" name="shop" />
                    <input type="hidden" value="<%= subDept %>" name="subdept" />
                    <input type="hidden" value="<%= user  %>"  name="depuser" />
                    <input type="hidden" value="<%= role  %>"  name="role" />
                </td> 
                
                <td colspan="2">
                   <s:label key="si-order.lpdate" />
                </td>
                <td colspan="2">
                     <b><s:property value="lpsidate" /></b>
                        <s:set var="lpsidate" value="%{lpsidate}" />
                        <s:hidden name="date1" value="%{lpsidate}" />
                </td> 
                
             </tr>
             
             <tr>          
               <td colspan="1">
                   <s:label key="si-order.filenum" />
                </td>
                <td colspan="2">
                    <b><s:property value="filenum" /></b>
                        <s:set var="filenum" value="%{filenum}" />
                        <s:hidden name="filenumsame" value="%{filenum}" />
                    
                </td> 
                
                <td colspan="2">
                   <s:label key="si-order.filedate" />
                </td>
                <td colspan="1">
                     <b><s:property value="filedate" /></b>
                        <s:set var="filedate" value="%{filedate}" />
                        <s:hidden name="datefile" value="%{filedate}" />
                </td> 
                <td colspan="1">
                   <s:label key="si-order.newfiledate" />
                </td>
                <td colspan="1">
                     <b><s:property value="fileregdate" /></b>
                        <s:set var="fileregdate" value="%{fileregdate}" />
                        <s:hidden name="newfiledate" value="%{fileregdate}" />
                </td> 
                
             </tr>
             
            
            <tr>
                    <td colspan="8">
                            <s:submit cssClass="textInput" key="si-order.sub" />
                            <s:reset cssClass="textInput" value="Reset" />
                            
                    </td>
             </tr>
            
        </table>        
        
 </s:form>

<%--<div class="details" id="detailDisplay"></div>--%>

<br>
<br>

<s:property value="storenumber"/> 
<s:property value="ordertype" />
 
</s:iterator>
</body>
</html>