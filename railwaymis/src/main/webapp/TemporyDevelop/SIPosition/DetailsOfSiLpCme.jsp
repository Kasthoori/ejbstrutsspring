<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<s:head/>
<style type="text/css">

.content {
	font-family:sans-serif;
	font-size:small;
	
}

.userTable {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:10px;
		width : 1000px;
	
}

.userTable  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTable th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.odd {
	
		background-color: #fffff0;
		
}

.even {
		background-color: #faf0e6;
}

.textInput {

		border: 1px solid #123B5F;
		color:#123B5F;
}

</style>
</head>
<body>
<h1 align="center"><font color="#050750">Sri-Lanka Railway Department</font></h1>
<h2 align="center"><font color="#050750">CME's Sub-Department :: Ratmalana</font></h2>

<s:if test="getFilteredList.size() > 0">
			<div class="content">
			
			
					<table class="userTable" cellpadding="5px">
				       
							<tr class="even">
								<th>File Number</th>
								<th>Store Number</th>
								<th>Section</th>
								
								
							</tr>
								<s:iterator value="getFilteredList" status="getFilteredListStatus">
						
										<tr class="<s:if test="#getFilteredListStatus.odd == true">odd</s:if><s:else>even</s:else>">
										
						 
									    
										<td colspan="1">
										<b><s:property value="filenum"/></b>
										</td>
										
										<td colspan="1">
										
											<s:property value="storenumber"/>
											
										</td>
									
										<td colspan="1">
											
											<s:property value="section" />
										</td>
									
										
										
										<%-- <td>
										
										
											<s:url id="verifybysrs" action="VerifyToGrantAppBySRS">
									               <s:param  name="storenumber" value="%{storenumber}" />
									               <s:param name="quantityreq" value="%{quantityreq}" />
									               <s:param name="unitreq" value="%{unitreq}" />
									               <s:param name="approvedby" value="%{approvedby}" />
									               <s:param name="appdate" value="%{appdate}" />
									               <s:param name="exdate" value="%{exdate}" />
											</s:url>
										<s:a href="%{verifybysrs}">Verify for Approval</s:a>  
										  
											
									
										</td>
				 --%>
									</tr>
								
								</s:iterator>
				        
					</table>
			 
			</div>
		
		</s:if>
			

</body>
</html>