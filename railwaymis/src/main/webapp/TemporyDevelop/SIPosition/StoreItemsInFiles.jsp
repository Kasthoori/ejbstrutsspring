<%-- 
    Document   : CallReport
    Created on : Dec 14, 2010, 2:34:53 AM
    Author     : Admin
--%>
 
<%@page import="java.io.IOException"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>
<%@page import="net.sf.jasperreports.engine.export.JRCsvExporter"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporter"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporter"%>
<%@page import="net.sf.jasperreports.engine.export.JRRtfExporter"%>
<%@page import="net.sf.jasperreports.engine.JRExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRPdfExporter"%>
<%@page import="net.sf.jasperreports.engine.JRExporter"%>
<%@page import="java.io.OutputStream"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.JasperFillManager"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="db" scope="request" class="pk1.DbBean" />
<jsp:setProperty property="*" name="db"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
<head>
<title>report generation in  jsp</title>
</head>
<body>
<%
/*
String filename =  request.getParameter("filename");
String reporttype =  request.getParameter("reporttype");
String Paramtype =  request.getParameter("paramtype");;
*/

                      String startdate = request.getParameter("startdate");
                      String enddate = request.getParameter("enddate");
                      String reporttype =  request.getParameter("reporttype");

                      String filename =  "/TemporyDevelop/SIPosition/JasReport/storeitem_infiles.jasper";
                      //String reporttype =  "html";
                      String Paramtype =  "";
                      String report = "StoreItemsInFiles";
 
                      Connection  conn = db.connect();
 
                      Map<String, Object>  jasperParameter = new  HashMap<String, Object>();
                      jasperParameter.put("startdate",startdate);
                      jasperParameter.put("enddate", enddate);
 
                       String path =  application.getRealPath("/");
                       JasperPrint jasperPrint =  JasperFillManager.fillReport(path +"/"+filename, jasperParameter,  conn);
                       //JasperPrint jasperPrint =  JasperFillManager.fillReport(filename, jasperParameter, conn);
//System.out.println("Report Created... in  "+reporttype +" Format");
                        
                       

                        OutputStream ouputStream =  response.getOutputStream();
                        JRExporter exporter = null;
 
                         if( "pdf".equalsIgnoreCase(reporttype)  )
                       {
                                 response.setContentType("application/pdf");
                                 response.setHeader("content-disposition","attachment;filename=" + report + ".pdf");
 
                                 exporter = new  JRPdfExporter();
                                 exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrint);
                                 exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,  ouputStream);
                        }
 
                              else if(  "rtf".equalsIgnoreCase(reporttype)  )
                        {
                                   response.setContentType("application/rtf");
                                   response.setHeader("content-disposition","attachment;filename=" + report + ".rtf");
 
                                   exporter = new  JRRtfExporter();
                                   exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrint);
                                   exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,  ouputStream);
                         }
 
                                   else if(  "html".equalsIgnoreCase(reporttype)  )
                         {
                                   response.setContentType("application/html");
                                   response.setHeader("content-disposition","attachment;filename=" + report + ".html");
 
                                   exporter = new  JRHtmlExporter();
                                   exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrint);
                                   exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,  ouputStream);
                                   exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI,"image?image=");
                         }
 
                                   else if( "xls".equalsIgnoreCase(reporttype)  )
                         {
                                   response.setContentType("application/xls");
                                   response.setHeader("content-disposition","attachment;filename=" + report + ".xls");
                                   exporter = new  JRXlsExporter();
                                   exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrint);
                                   exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,  ouputStream);
                          }
 
 
                                   else if(  "csv".equalsIgnoreCase(reporttype)  )
                          {
                                   response.setContentType("application/csv");
                                   response.setHeader("content-disposition","attachment;filename=" + report + ".csv");
 
                                   exporter = new  JRCsvExporter();
                                   exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrint);
                                   exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,  ouputStream);
                          }
 
                                try
                          {
                                   exporter.exportReport();
                          }
                                   catch  (JRException e)
                          {
                                   throw new  ServletException(e);
                          }
                                finally
                          {
                                   if (ouputStream !=  null)
                          {
                                try
                          {
                                  ouputStream.close();
                          }
                                catch (IOException  ex){}
                          }
                          }
%>
</body>
</html>