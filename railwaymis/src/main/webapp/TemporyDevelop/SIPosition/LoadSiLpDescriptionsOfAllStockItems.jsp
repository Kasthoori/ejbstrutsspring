<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.content {
	font-family:sans-serif;
	font-size:small;
	
}

.userTable {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:20px;
		width : 300px;
		font-size: 14px;
		 
}

.userTable  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTable th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.odd {
	
		background-color: #fffff0;
		
}

.even {
		background-color: #faf0e6;
}

.textInput {

		border: 1px solid #123B5F;
		color:#123B5F;
}

</style>



</head>
<body>


<table class="userTable">
	<tr>
		<th colspan="2" align="center"><s:label key="silp-details.title" /></th>
	</tr>
       <s:iterator value="getDes" status="getDesStatus">
       
            <tr>
               <td><s:label key="silp-details.stockitemname" /></td>
               <td><font color="red"><b><s:property value="storenumber" /></b></font></td>
  			</tr>
  			
  			<tr>
  			    <th colspan="2"><s:label key="silp-details.descript" /></th>
  			</tr>
  			<tr>
  				<td colspan="2">
  					<s:property value="descriptions" />
               </td>
  			</tr> 
  			<tr>
  			    <td><s:label key="silp-details.partnum" /></td>
  				<td><s:property value="itemname" /></td>
  			</tr>
               
               
  </s:iterator> 
</table>

   
    
   <%--  <s:iterator value="getDes" status="getDesStatus">
      <s:property value="storenumber" />
      <s:property value="itemname" />
    </s:iterator> --%>
</body>
</html>