
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ page import="java.io.*" %>
<%@ page import="java.sql.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sd" uri="/struts-dojo-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="db" scope="request" class="pk1.DbBean" />
<jsp:setProperty name="db" property="*" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<title>Items Issue for Consumption</title>
<s:head/>
<sj:head jqueryui="true" />
<script language="javascript" type="text/javascript" src="../../jquery-1.4.2.js"></script>
<script type="text/javascript" language="javascript" src="JsFiles/EnterTecRec.js"></script>
<script src="../../trimFunctions.js" language="JavaScript" type="text/javascript"></script>
<script type="text/javascript" language="javascript">

$(document).ready(function(){
    $("#quantity").keydown(function(event) {
       if(event.shiftKey)
       {
            event.preventDefault();
       }

       if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190)    {
       }
       else {
            if (event.keyCode < 95) {
              if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
              }
            }
            else {
                  if (event.keyCode < 96 || event.keyCode > 105) {
                      event.preventDefault();
                  }
            }
          }
       });
    });
    
$(document).ready(function(){
    $("#orderedquantity").keydown(function(event) {
       if(event.shiftKey)
       {
            event.preventDefault();
       }

       if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190)    {
       }
       else {
            if (event.keyCode < 95) {
              if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
              }
            }
            else {
                  if (event.keyCode < 96 || event.keyCode > 105) {
                      event.preventDefault();
                  }
            }
          }
       });
    });


$(document).ready(function(){
    $("#uprice").keydown(function(event) {
       if(event.shiftKey)
       {
            event.preventDefault();
       }

       if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190)    {
       }
       else {
            if (event.keyCode < 95) {
              if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
              }
            }
            else {
                  if (event.keyCode < 96 || event.keyCode > 105) {
                      event.preventDefault();
                  }
            }
          }
       });
    });


/* function newPopupSendFuelRep(url1){
	
	$('.child1 td').hide();

    var val2 = url1.value;

    if(val2 == 'Recommended With Third Party Inspection' || val2 == 'Recommended Without Third Party Inspection'){
    	
    	$('.child1 td').show('slow');

    //popupWindow = window.open('http://www.cbnrms.net/RLMSProject/TemporyDevelop/SIPosition/SendNameOfSuccBider.jsp','popUpWindow','Height=550px,width=1000px,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=yes,status=yes');

    }else if(val2 == "Remarks" || val2 == "Deleted" || val2 == "Clarification" || val2 == "Fresh quatation"){
    	
    //popupWindow = window.open('http://www.cbnrms.net/RLMSProject/TemporyDevelop/SIPosition/Remarks.jsp','popUpWindow','Height=550px,width=1000px,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=yes,status=yes');	
    	
    }
}
 */
  
    function gen_store_num(){

        var totStoreNum = "";  

            var x = document.tecform.storeNum.value;
            var y = document.tecform.storeNumDigit1.value;
            var z = document.tecform.storeNumDigit2.value;
            var i = document.tecform.storeNumDigit3.value;
            var m = document.tecform.storeNumDigit4.value;


           

          if((x != "") && (y == "") && (z == "") && (i == "") && (m == "")){

                     totStoreNum = x;
                    
              }else if((x != "") && (y != "") && (z == "") && (i == "") && (m == "")){

                         totStoreNum = x + " " + y;

                  }else if((x != "") && (y != "") && (z != "") && (i == "") && (m == "")){

                             totStoreNum = x + " " + y + "/" + z;

                      }else if((x != "") && (y != "") && (z != "") && (i != "") && (m == "")){


                                 totStoreNum = x + " " + y + "/" + z + "/" + i;
                          }else{

                                    totStoreNum = x + " " + y + "/" + z + "/" + i + "/"  +  m;

                              }

            
            

            $.post("CheckSILpSnumber.jsp",{storenum : "" + totStoreNum + ""},

                        function(data){
                            $('#imagedisplay').html(data)

                });

            
        }
    
 $(document).ready(function(){
        
        $('.child1 td').hide();
        $('.child11 td').hide();
        $('.child2 td').hide();
        $('.child3 td').hide();
        $('.child4 td').hide();
        $('.child5 td').hide();
       
        
        $('#selectcon').change(function(){
            
            var val = $(this).val();
            
            if(val == "Recommended With Third Party Inspection" || val == "Recommended"){
                
                $('.child1 td').show('slow');
                $('.child11 td').show('slow');
               // $('.child2 td').show();
                //$('.child3 td').show();
                
               
            }else{
                
                $('.child1 td').hide('slow');
                $('.child11 td').show('slow');
                $('.child2 td').show('slow');
                $('.child3 td').hide();
                $('.child4 td').hide();
                $('.child5 td').hide();
                
            }
        });
    });
 
 $(document).ready(function () { 
     $('#selectcon1').change(function () {
    	 
    	 var val1 = $(this).val();
    	 
    	 if(val1 == 'SELBYSRS'){
    		 
    		    $('.child2 td').hide('slow');
    	        $('.child3 td').hide('slow');
    	        $('.child4 td').hide('slow');
    	        $('.child5 td').hide('slow');
    		 
    	 }
        //do something here
     });
     $('#selectcon2').change(function () {
    	 
    	 var val2 = $(this).val();
    	 
    	 if(val2 == 'SELBYCME'){
    		 
    		 $('.child2 td').show('slow');
 	         $('.child3 td').show('slow');
 	         $('.child4 td').show('slow');
 	         $('.child5 td').show('slow');
    		 
    	 }
       //do some more stuff here
     });
 });
 
 
 
 $(function() {
	  $('#storeNum').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});

$(function() {
	  $('#storeNumDigit1').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});


$(function() {
	  $('#storeNumDigit2').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});

$(function() {
	  $('#storeNumDigit3').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});

$(function() {
	  $('#storeNumDigit4').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});

$(function() {
	  $('#filenum').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});

$(function() {
	  $('#filenum1').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});

$(function() {
	  $('#filenum2').keydown(function(e) {
	    if (e.keyCode == 32) // 32 is the ASCII value for a space
	      e.preventDefault();
	  });
	});

</script>

<style>
.tableoiltankfrm {
    
        font:12px verdana, arial, helvetica, sans-serif;
        border-width: 2px solid;
        border-color:#067bb6;
        border-style:solid;
        background-color: #88d5fc;
        width:90%;
        margin-left:5%;
        margin-top:5%;
}

.tableoiltankfrm  tr td {
    
        background-color:#e2f3fc;
        margin: 5px;
        padding: 5px;
}

.tdLabel {
    
        font-weight: bold;
        align: top;
        
}

.label {} 

 .errorMessage {
    
        color:red;
        font-size: 12px;
}

.textInput {

        border: 1px solid #3998c7;
        color:#0d157f;
}

</style>
<sj:head jqueryui="true" />
</head>
<body>

<noscript> 

      
	 <meta http-equiv="Refresh" content="0;URL=../../disabled.html">  
</noscript>




<%

                                                                    request.getSession();

                                                                    Object isLogedIn = session.getAttribute("isLogedIn");
                                                                    
                                                                    if(isLogedIn == null){
                                                                        
                                                                        response.sendRedirect("../../Message.jsp");
                                                                    }
                                                                    
                                                                    session.setMaxInactiveInterval(1000);

                                                                    String  subDept = (String)session.getAttribute("subDept");
                                                                    String  shop  = (String)session.getAttribute("str3");
                                                                    String user = (String)session.getAttribute("str");
                                                                    String role = (String)session.getAttribute("strRole");
%>

<s:iterator value="getList" status="getListStatus">
      
      
      
        <s:set var="storenumber" value="storenumber" />  
        <s:set var="filenumber" value="filenum" />
        <s:set var="clarify" value="clarificbysrs"  />
        
        
        
        
 
<s:form name="tecform" action="updateSrsClarification" method="post" onSubmit="javascript:return ValidateTecRec();">
        <table class="tableoiltankfrm">
        
            <tr>
                   <th colspan="8" align="center"><font size="3px" style="color:#0d157f;"><s:property value="storenumber" />&nbsp;and&nbsp;<s:property value="filenum"/>TEC Recommendation Entering Form after Clarification</font></th>
             </tr>
             
             <tr>
             	<td colspan="4"><font style="color:red;"><s:label key="clarifyform.srsstatus" /></font></td><td colspan="4"><font style="color:red;"><s:if test="%{#clarify == 'Not processed'}">Not Responded</s:if><s:else><s:property value="clarificbysrs" /></s:else></font></td>
             </tr>
            
           <tr class="parent" id="2478">
                <td colspan="2">
                    <font style="color:#0d157f;"><s:label key="si-order.tecreccond" required="true"/></font>
                </td>
                    <td colspan="3">
                    <s:select cssClass="textInput" id="selectcon" name="tecrecommend" 
            list="{'Recommended With Third Party Inspection','Recommended','Deleted','Clarification','Fresh quatation'}" 
            headerValue="----Please select----" headerKey="" />
            
            <s:hidden name="storenumber" value="%{storenumber}" />
            <s:hidden name="filenumber" value="%{filenumber}" />
            <input type="hidden" value="<%= shop %>" name="shop" />
            <input type="hidden" value="<%= subDept %>" name="subdept" />
            <input type="hidden" value="<%= user  %>"  name="depuser" />
            <input type="hidden" value="<%= role  %>"  name="role" />
                    
                 </td>
             <td colspan="2"><s:label key="si-order.tecround" /></td>
             <td colspan="1"><s:textfield cssClass="textInput" name="tecround" size="2"  /></td>
             </tr>
             
 <!-- #######################################Added Part###################################################################################################### -->
 
             <tr class="child1">
                <td colspan="2">
                	<font style="color:#0d157f;"><s:label key="si-order.selsrsname"></s:label></font>
                </td>
             	 <td colspan="6">
             		<input type="radio" id="selectcon1" name="selection" value="SELBYSRS"/>Selected by SRS
             		<input type="radio" id="selectcon2" name="selection" value="SELBYCME"/>Selected by CME
             	</td> 
             </tr>
             
             <tr class="child11">
                <td colspan="2">
                    <font style="color:#0d157f;"><s:label key="si-order.reasons" required="true"/></font>
                </td>
                    <td colspan="6">
                     <s:textarea cssClass="textInput" cols="5" rows="3" name="remarks"></s:textarea>
                    </td>
             </tr>
               
               <tr class="child2">
                <td colspan="2">
                    <font style="color:#0d157f;"><s:label key="si-order.sucbidder" required="true"/></font>
                </td>
                    <td colspan="6">
                     <s:textfield cssClass="textInput" name="succBidder" size="80px" onmouseover="gen_store_num()" />
                    </td>
             </tr>
                        
 
 
 <!-- #######################################End of Added Part############################################################################################### -->
             
             
              <tr class="child3">
               
               <td colspan="2">
                   <font style="color:#0d157f;"><s:label key="si-order.quantity" /></font>
                </td>
                <td colspan="2">
                    <s:textfield cssClass="textInput" id="quantity" name="quantity" size="20" />
                    
                </td> 
               
               <td colspan="2">
                   <font style="color:#0d157f;"><s:label key="si-order.units" /></font>
                </td>
                <td colspan="2">
                     <s:select cssClass="textInput" name="units"
                           list="{'Kg','L','m','Nos','Gross','Dozen','Set','Rolls','Barrels','MT','Pounds','Reels','Pounds'}" 
                           headerValue="----Please select----" headerKey="" onmouseover='gen_store_num()'/>
                    
                      </td>
               </tr>
               <tr class="child4">
                <td colspan="2">
                   <font color="red"><s:label key="si-order.orderedquantity" /></font>
                </td>
                <td colspan="2">
                    <s:textfield cssClass="textInput" id="orderedquantity" name="orderedQuantity" size="20" />
                    
                </td> 
               
                <td colspan="2">
                   <font color="red"><s:label key="si-order.orderedunits" /></font>
                </td>
                <td colspan="2">
                     <s:select cssClass="textInput" name="orderedUnit"
                           list="{'Kg','L','m','Nos','Gross','Dozen','Set','Rolls','Barrels','MT','Pounds','Reels','Pounds'}" 
                           headerValue="----Please select----" headerKey="" onmouseover='gen_store_num()'/>
                    
                      </td>
               </tr>
               
               <tr class="child5">
               
               <td colspan="2">
                   <font style="color:#0d157f;"><s:label key="si-order.unitprice" /></font>
                </td>
                <td colspan="2">
                    <s:textfield cssClass="textInput" id="uprice" name="uprice" size="20" />
                    
                </td> 
               
               <td colspan="2">
                   <font style="color:#0d157f;"><s:label key="si-order.currency" /></font>
                </td>
                <td colspan="2">
                     <s:select cssClass="textInput" name="currency"
                           list="{'AUD','CAD','CNY','EUR','GBP','INR','IDR','JPY','KRW','LKR','USD'}" 
                           headerValue="----Please select----" headerKey="" onmouseover='gen_store_num()'/>
                    
                      </td>
               </tr>
               
             
             
               
             <tr>
               <td colspan="2">
                   <font style="color:#0d157f;"> <s:label key="si-order.tecrecdate" /></font>
                </td>
                <td colspan="6">
                   <sj:datepicker id="tecrecdate" cssClass="textInput" maxDate="0" onmouseover='gen_store_num()'
                                                                     name="tecrecdate" size="14" 
                                                                     buttonImageOnly="true" showOn="button"
                                                                      displayFormat="dd-M-yy" changeYear="true" />
                </td>
             </tr>
             
             
                       
            
            <tr>
                    <td colspan="8">
                            <s:submit cssClass="textInput" key="si-order.tecrec" />
                            <s:reset cssClass="textInput" value="Reset" />
                    </td>
             </tr>
            
        </table>        
        
 </s:form>
 </s:iterator>
 
</body>
</html>
