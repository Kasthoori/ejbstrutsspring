/**
 * A.P.Kasthoori
 */
function ValidatesDates(){
	
	var type = document.tecform.type.value;
	var storenumPart1 = document.tecform.storeNum.value;
	var trimstorenumPart1 = trimAll(storenumPart1);
	var trimedstornumPart1 = trimstorenumPart1.length;


	var storenumPart2 = document.tecform.storeNumDigit1.value;
	var trimstorenumPart2 = trimAll(storenumPart2);
	var trimedstorenumPart2 = trimstorenumPart2.length;


	var storenumPart3 = document.tecform.storeNumDigit2.value;
	var trimstorenumPart3 = trimAll(storenumPart3);
	var trimedstorenumPart3 = trimstorenumPart3.length;
	
	var filenum = document.tecform.filenum.value;
	var trimfilenum = trimAll(filenum);
	var trimedfilenum = trimfilenum.length;
	
	var filenum1 = document.tecform.filenum1.value;
	var trimfilenum1 = trimAll(filenum1);
	var trimedfilenum1 = trimfilenum1.length;
	
	var filenum2 = document.tecform.filenum2.value;
	var trimfilenum2 = trimAll(filenum2);
	var trimedfilenum2 = trimfilenum2.length;
	
	var seleDates = document.tecform.selecteddate.value;
	
	var eventdates = document.tecform.datesforevent.value;
	
	
	
if(type == ""){
		
		window.alert("Please select type");
		return false;
	}

	if(trimedstornumPart1 == 0){
		
		window.alert("Please enter Lexical Part1 of Store number");
		
		return false;
	}

	if(trimedstorenumPart2 == 0){
		
		window.alert("Please enter Numerical Part2 of Store number");
		return false;
	}

	if(trimedstorenumPart3 == 0){
		
		window.alert("Please enter Numerical Part3 of Store number");
		return false;
	}
	
	if(trimedfilenum == 0){
		
		window.alert("Please enter lexical part of file number");
		return false;
	}
	
if(trimedfilenum1 == 0){
		
		window.alert("Please enter first numerical part of file number");
		return false;
	}

if(trimedfilenum2 == 0){
	
	window.alert("Please enter second numerical part of file number");
	return false;
}
	
	if(seleDates == ""){
		
		window.alert("Please select dates of event");
		return false;
	}
	
	if(seleDates == "Item Recived date"){
		
		var invoice = document.tecform.invoicenum.value;
		var triminvoice = trimAll(invoice);
		var trimedinvoice = triminvoice.length;
		
		if(trimedinvoice == 0){
			
			window.alert("Please enter invoice number");
			return false;
		}
		
	}
	
	if(eventdates == ""){
		
		window.alert("Please select date for event");
		return false;
	}
	
	return true;
	
}