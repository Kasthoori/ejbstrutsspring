/**
 * A.P.Kasthoori
 */



function ValateSuitability(){

    var type = document.tecform.type.value;
	var storenumPart1 = document.tecform.storeNum.value;
	var trimstorenumPart1 = trimAll(storenumPart1);
	var trimedstornumPart1 = trimstorenumPart1.length;


	var storenumPart2 = document.tecform.storeNumDigit1.value;
	var trimstorenumPart2 = trimAll(storenumPart2);
	var trimedstorenumPart2 = trimstorenumPart2.length;


	var storenumPart3 = document.tecform.storeNumDigit2.value;
	var trimstorenumPart3 = trimAll(storenumPart3);
	var trimedstorenumPart3 = trimstorenumPart3.length;
	
	var filenum = document.tecform.filenum.value;
	var trimfilenum = trimAll(filenum);
	var trimedfilenum = trimfilenum.length;
	
	var filenum1 = document.tecform.filenum1.value;
	var trimfilenum1 = trimAll(filenum1);
	var trimedfilenum1 = trimfilenum1.length;
	
	var filenum2 = document.tecform.filenum2.value;
	var trimfilenum2 = trimAll(filenum2);
	var trimedfilenum2 = trimfilenum2.length;
	
	var suitCond = document.tecform.suitabilitycond.value;
	
	var date = document.tecform.suitgivendate.value;
	
	
	
	
if(type == ""){
		
		window.alert("Please select type");
		return false;
	}

	if(trimedstornumPart1 == 0){
		
		window.alert("Please enter Lexical Part1 of Store number");
		
		return false;
	}

	if(trimedstorenumPart2 == 0){
		
		window.alert("Please enter Numerical Part2 of Store number");
		return false;
	}

	if(trimedstorenumPart3 == 0){
		
		window.alert("Please enter Numerical Part3 of Store number");
		return false;
	}
	
	if(trimedfilenum == 0){
		
		window.alert("Please enter lexical part of file number");
		return false;
	}
	
if(trimedfilenum1 == 0){
		
		window.alert("Please enter first numerical part of file number");
		return false;
	}

if(trimedfilenum2 == 0){
	
	window.alert("Please enter Second numerical part of file number");
	return false;
}
	
	
	if(suitCond == ""){
		
		window.alert("Please select suitability condition");
		return false;
	}
	
	if(suitCond == "Not Suitable"){
		
		var reason = document.tecform.reasons.value;
		var trimreason = trimAll(reason);
		var trimedreason = trimreason.length;
		
		if(trimedreason == 0){
			
			window.alert("Please enter reason");
			return false;
		}
		
	}else if(suitCond == "Short Fall"){
		
		var shortfall = document.tecform.amount.value;
		var trimshortfall = trimAll(shortfall);
		var trimedshortfall = trimshortfall.length;
		
		if(trimedshortfall == 0){
			
			window.alert("Please enter short fall amount");
			return false;
		}
	}else if(suitCond == "Damaged"){
		
		var damage = document.tecform.damamount.value;
		var trimdamage = trimAll(damage);
		var trimeddamage = trimdamage.length;
		
		if(trimeddamage == 0){
			
			window.alert("Please enter damage amount");
			return false;
		}
	}
	
	if(date == ""){
		
		window.alert("Please select date");
		return false;
	}
	
	return true;
}