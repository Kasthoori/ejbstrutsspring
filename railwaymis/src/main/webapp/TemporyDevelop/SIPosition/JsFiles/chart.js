$(document).ready(function (){
	
	
    
    $.ajax({
    	
    	
    	url: 'dbGetData.jsp',
    	
    	type: 'get',
    	
    	dataType: 'json',
    	
    	success: function(data){
    		
    		
    			
    	var line3 = data[0];
    	
    	
    	
    	 
    	 //$("#result").html(line3);
    	
 
    var plot3 = $.jqplot('chart1', [line3], {
       
       
    animate: !$.jqplot.use_excanvas,
      series:[{
    	    renderer:$.jqplot.BarRenderer,
    	    label:'Inflight', 
    	    pointLabels: {ypadding: 1},
    	    rendererOptions: {
                fillToZero: true,
                barPadding: 0,
                barMargin: 0,
                barWidth: 51,
                groups: 1,
                varyBarColor: true
            },
      	    showMarker:true,
      	    lineWidth:1,

      	}],
      	

        title: {
            text: 'Quontitative Analysis of Stock Items',   // title for the plot,
            show: true,
            fontSize: '18pt',
        },
    	
      	
      
       grid: {
        drawGridLines: true,       
        gridLineColor: '#cccccc'  
             
                               
    },
    
    axesDefaults: {
    	
    	numberTicks: undefined,
    	
    },
       

      axes: {
      	
        xaxis: {
          pad: 1, 
          renderer:$.jqplot.BarRenderer,
          renderer: $.jqplot.CategoryAxisRenderer, 
          //ticks: ["FOT","WEL","MLV","RML","MRT","PND","WDA","KTN","KTS","PGS","BRL","ALT","BNT","IDA","KDA","BPT","ABA","KWE","HKD","DNA","BSH","GNT","GLE","KUG","TLP","KOG","ANM","WLM","KMG","MTR"],
          
          label: 'Stock Items',
         
          
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
          tickRenderer: $.jqplot.CanvasAxisTickRenderer,
          tickOptions: {
              angle: -30,
              fontFamily: 'Courier New',
              fontSize: '9pt',
             
              showGridline: true,
              category: true
              
            	
          }
           
        },
        yaxis: {
        	
                 label:'Item Count',
                 min : 0,
                 tickInterval : 350,
                  max:3500,
                  pad:1,
                  autoscale:true,
                  labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                  labelOptions:{
                     fontFamily:'Helvetica',
                     fontSize: '14pt'
                  },
                 
        }
        
       
      },  
    
      highlighter: { 
    	  show: true,
    	  tooltipAxes: 'y',
    	  
      }
       
       });
    
   /* $('#chart1').bind('jqplotDataClick',
            function (data) {
                $('#info1').html('data: '+data);
    });*/
       
       
    }
    
    
    });
							
	
								
});

