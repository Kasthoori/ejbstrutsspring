/**
 * A.P.Kasthoori
 */

function ValidateSIEnterDetails(){
	
var type = document.siform.type.value;

var storenumPart1 = document.siform.storeNum.value;
var trimstorenumPart1 = trimAll(storenumPart1);
var trimedstornumPart1 = trimstorenumPart1.length;


var storenumPart2 = document.siform.storeNumDigit1.value;
var trimstorenumPart2 = trimAll(storenumPart2);
var trimedstorenumPart2 = trimstorenumPart2.length;


var storenumPart3 = document.siform.storeNumDigit2.value;
var trimstorenumPart3 = trimAll(storenumPart3);
var trimedstorenumPart3 = trimstorenumPart3.length;



var typeOrder = document.siform.typeorder.value;

var lpnum = document.siform.lponum.value;
var trimlpnum = trimAll(lpnum);
var trimedlpnum = trimlpnum.length;


var date = document.siform.date1.value;

var filenum = document.siform.filenum.value;
var trimfilenum = trimAll(filenum);
var trimedfilenum = trimfilenum.length;


var filenum1 = document.siform.filenum1.value;

var trimfilenum1 = trimAll(filenum1);
var trimedfilenum1 = trimfilenum1.length;


var filenum2 = document.siform.filenum2.value;

var trimfilenum2 = trimAll(filenum2);
var trimedfilenum2 = trimfilenum2.length;


var datefile = document.siform.datefile.value;
var newfiledate = document.siform.newfiledate.value;



if(type == ""){
	
	window.alert("Please select type");
	return false;
}

if(trimedstornumPart1 == 0){
	
	window.alert("Please enter Lexical Part1 of Store number correctly");
	
	return false;
}

if(trimedstorenumPart2 == 0){
	
	window.alert("Please enter Numerical Part2 of Store number");
	return false;
}

if(trimedstorenumPart3 == 0){
	
	window.alert("Please enter Numerical Part3 of Store number");
	return false;
}

if(typeOrder == ""){
	
	window.alert("Please select type of order");
	return false;
}

if(typeOrder == "NORTEN" || typeOrder == "EMGTEN"){
	
	if(trimedfilenum == 0){
		
		window.alert("Please enter lexical part of file number");
		return false;
	}
	
	if(trimedfilenum1 == 0){
		
		window.alert("Please enter numerical part 1 of file num");
		return false;
	}

	if(trimedfilenum2 == 0){
		
		window.alert("Please enter numerical part 2 of file num");
		return false;
	}

	if(datefile == ""){
		
		window.alert("Please enter Registered date");
		return false;
	}

	if(newfiledate == ""){
		
		window.alert("Please enter date of file");
		return false;
	 }
	
}else{

if(trimedlpnum == 0){
	
	window.alert("Please enter LP/SI number");
	return false;
}

if(date == ""){
	
	window.alert("Please enter date of LP/SI");
	return false;
}

if(trimedfilenum == 0){
	
	window.alert("Please enter lexical part of file number");
	return false;
}

if(trimedfilenum1 == 0){
	
	window.alert("Please enter numerical part 1 of file num");
	return false;
}

if(trimedfilenum2 == 0){
	
	window.alert("Please enter numerical part 2 of file num");
	return false;
}

if(datefile == ""){
	
	window.alert("Please enter Registered date");
	return false;
}

if(newfiledate == ""){
	
	window.alert("Please enter date of file");
	return false;
 }
}
return true;

}