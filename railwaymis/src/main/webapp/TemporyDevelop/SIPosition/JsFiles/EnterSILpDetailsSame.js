function ValidateSIEnterDetailsSame(){

var storenumPart1 = document.siform.storeNum.value;
var trimstorenumPart1 = trimAll(storenumPart1);
var trimedstornumPart1 = trimstorenumPart1.length;


var storenumPart2 = document.siform.storeNumDigit1.value;
var trimstorenumPart2 = trimAll(storenumPart2);
var trimedstorenumPart2 = trimstorenumPart2.length;


var storenumPart3 = document.siform.storeNumDigit2.value;
var trimstorenumPart3 = trimAll(storenumPart3);
var trimedstorenumPart3 = trimstorenumPart3.length;

if(trimedstornumPart1 == 0){
	
	window.alert("Please enter Lexical Part1 of Store number correctly");
	
	return false;
}

if(trimedstorenumPart2 == 0){
	
	window.alert("Please enter Numerical Part2 of Store number");
	return false;
}

if(trimedstorenumPart3 == 0){
	
	window.alert("Please enter Numerical Part3 of Store number");
	return false;
}
return true;

}