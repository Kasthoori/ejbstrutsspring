<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <%@ page import="java.io.*" %>
<%@ page import="java.sql.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sd" uri="/struts-dojo-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>   
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../ReportsSiPosition/css_files/design_page.css" />
		<script type="text/javascript" src="../../JqueryJqplot_plugin/jquery.min.js"></script>
		<script type="text/javascript" src="../../JqueryJqplot_plugin/jquery-ui-1.10.4.custom.js"></script>
		<link type="text/css" rel="stylesheet" href="../../JqueryJqplot_plugin/jquery-ui-1.10.4.custom.css" />
		<link type="text/css" href="../../JqueryJqplot_plugin/jquery.jqplot.min.css" />
		
		

<title>Insert title here</title>
<script type="text/javascript" src="../../jquery-1.4.2.js"></script> 
<script type="text/javascript">
/* function get_details(){
	
	var storeNum = document.detailfrm.storenumber.value;
	var fileNum = document.detailfrm.filenumber.value;
	
	$.post("getAllDetails",{storenumber: "" + storeNum + "",filenumber:"" + fileNum + ""},
			
			function(data){
		
		
		          $('#imagedisplay').html(data);
		
	      }
	   );

} */


</script>
		
<style type="text/css">

.content {
	font-family:sans-serif;
	font-size:small;
	
}

.userTable {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:20px;
		width : 200px;
	
}

.userTable  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTable th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.odd {
	
		background-color: #fffff0;
		
}

.even {
		background-color: #faf0e6;
}

.textInput {

		border: 1px solid #123B5F;
		color:#123B5F;
}

</style>

<s:head/>
</head>
<body>
    
<s:iterator value="getStockItemDetailsOfFiles" begin="0" end="0">
   
   			<h2 align="center"><s:property value="filenum" /></h2>
   </s:iterator>
 
 
 
 
  <s:if test="getStockItemDetailsOfFiles.size() > 0">
  
 
   <table class="userTable">	

    <tr>
    	<th colspan="2">Stock Items Details</th>
    </tr>
   
   <s:iterator value="getStockItemDetailsOfFiles" status="getStockItemDetailsOfFilesStatus">
       <tr class="<s:if test="#getStockItemDetailsOfFilesStatus.odd == true">odd</s:if><s:else>even</s:else>">
       <td>        
               <s:property value="storenumber" />
              
       </td>
       <td>
        
         
           <s:form name="detailfrm" method="post" action="getAllDetails">
           
                 <s:set var="storenum" value="storenumber" />
                 <s:set var="filenumber" value="filenum" />
                 <s:set var="type" value="section" />
                 <s:hidden name="type" value="%{type}" />
           		 <s:hidden name="storenumber" value="%{storenum}" />  
           		 <s:hidden name="filenumber" value="%{filenumber}" />                          
		         <!-- <img width="20px" height="20px" alt="search" src="http://www.rlmsproject.com/RLMSProject/Images/Search.jpeg" onmouseover='get_details();' /> -->
				 <s:submit type="image" height="20px" width="20px" value="Submit" src="http://www.rlmsproject.com/RLMSProject/Images/Search.jpeg" />  
				 <%-- <s:submit />   --%>                   
			</s:form>
					
       </td>
       </tr>
   </s:iterator>
 </table>
 </s:if> 
 <div id="imagedisplay"></div>
</body>
</html>