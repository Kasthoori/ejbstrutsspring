<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.*" %>
<%@ page import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="db" scope="request" class="pk1.DbBean" />
<jsp:setProperty name="db" property="*" />
<%@ taglib prefix="s" uri="/struts-tags" %>
<link rel="stylesheet" href="RLMS.css" type="text/css" />
<style type="text/css">

.content {
    font-family:sans-serif;
    font-size:small;
    
}

.userTable {
        border-width:1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        margin-left:10px;
        width:90%;
    
}

.userTable  td {
    
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
}

.userTable th {
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        background-color: rgb(255, 255, 255);
}

.odd {
    
        background-color: #fffff0;
        
}

.even {
        background-color: #faf0e6;
}

.textInput {

        border: 1px solid #123B5F;
        color:#123B5F;
}

</style>
<% 

          db.connect();

          String  s = request.getParameter("typeorder");
          String trancond = "true";
          
          ResultSet rsCheck = null;
          ResultSet rs = null;
          String checkStoreNum = "";
          
          
          
          
          if(s.matches("FRESHQUO")){
        	  
        	   String tecRecommond = "Fresh quatation";
          
        	   rsCheck = db.execSql("SELECT store_num FROM details_lp_si_table WHERE tec_recommond='" + tecRecommond + "' AND transac_cond='" + trancond + "'");
        	   
        	  while(rsCheck.next()){
        		  
        		  checkStoreNum = rsCheck.getString("store_num");
        		  
        	  }
        	  
       if(!checkStoreNum.isEmpty()){
          
          rs = db.execSql("SELECT section, store_num,si_lpo_num, file_num FROM details_lp_si_table WHERE tec_recommond='" + tecRecommond + "' AND transac_cond='" + trancond + "'");

          
                 	%>
                    	<h2 align="center">Previous Details of SI/LP</h2>
                    	<table class="userTable ">
                    	<tr>
                    	  <th>Section</th>
                    	  <th>Store Number</th>
                    	  <th>SI/LP Number</th>
                    	  <th>Previous File Number</th>
                    	</tr>
                    	<% 
              
           
        	  
        	  
             while(rs.next()){
              
              String  s1 = rs.getString("section");
              String  s2 = rs.getString("store_num");
              String  s3 = rs.getString("si_lpo_num");
              String  s4 = rs.getString("file_num");
              
             
              
              
              %>
              <tr class="<s:if test="#Status.odd == true">odd</s:if><s:else>even</s:else>">
              <td><%= s1 %></td>
              <td><%= s2 %></td>
              <td><%= s3 %></td>
              <td><%= s4 %></td>
              
              </tr>
              
              
  <%

             
              
             }
                    	
                    	
        }else{
                   
               %>
               
               <p></p>
               
               <%    
                   
               }
          
          db.close();
          
               
          
%>            


</table>

<%

          }else{
        	  
        	  %>
        	  
        	  <p></p>
        	  
        	  <%
        
          }

%>
