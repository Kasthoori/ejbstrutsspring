<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*" %>
<%@ page import="java.sql.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sd" uri="/struts-dojo-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="db" scope="request" class="pk1.DbBean" />
<jsp:setProperty name="db" property="*" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<title>Items Issue for Consumption</title>
<s:head/>
<sj:head jqueryui="true" />
<script language="javascript" type="text/javascript" src="../../jquery-1.4.2.js"></script>
<script type="text/javascript" language="javascript" src="JsFiles/EnterDates.js"></script>
<script src="../../trimFunctions.js" language="JavaScript" type="text/javascript"></script>
<script type="text/javascript" language="javascript">

      function gen_store_num(){

        var totStoreNum = "";  

            var x = document.tecform.storeNum.value;
            var y = document.tecform.storeNumDigit1.value;
            var z = document.tecform.storeNumDigit2.value;
            var i = document.tecform.storeNumDigit3.value;
            var m = document.tecform.storeNumDigit4.value;


           

          if((x != "") && (y == "") && (z == "") && (i == "") && (m == "")){

                     totStoreNum = x;
                    
              }else if((x != "") && (y != "") && (z == "") && (i == "") && (m == "")){

                         totStoreNum = x + " " + y;

                  }else if((x != "") && (y != "") && (z != "") && (i == "") && (m == "")){

                             totStoreNum = x + " " + y + "/" + z;

                      }else if((x != "") && (y != "") && (z != "") && (i != "") && (m == "")){


                                 totStoreNum = x + " " + y + "/" + z + "/" + i;
                          }else{

                                    totStoreNum = x + " " + y + "/" + z + "/" + i + "/"  +  m;

                              }

            
            

            $.post("CheckSILpSnumber.jsp",{storenum : "" + totStoreNum + ""},

                        function(data){
                            $('#imagedisplay').html(data)

            });

            
      }
      
      
 
	 
	 
     
   $(document).ready(function(){
         
    	 $('.child td').hide();
    	 
   
         
         $('#selectcon').change(function(){
        	 
        	 var val = $(this).val();
        	 
        	 if(val == "Item Recived date"){
        		 
        		 $('.child td').show();
        	 }else{
        		 
        		 $('.child td').hide();
        	 }
         
         });
   
     });
   
   $(function() {
		  $('#storeNum').keydown(function(e) {
		    if (e.keyCode == 32) // 32 is the ASCII value for a space
		      e.preventDefault();
		  });
		});

	$(function() {
		  $('#storeNumDigit1').keydown(function(e) {
		    if (e.keyCode == 32) // 32 is the ASCII value for a space
		      e.preventDefault();
		  });
		});


	$(function() {
		  $('#storeNumDigit2').keydown(function(e) {
		    if (e.keyCode == 32) // 32 is the ASCII value for a space
		      e.preventDefault();
		  });
		});

	$(function() {
		  $('#storeNumDigit3').keydown(function(e) {
		    if (e.keyCode == 32) // 32 is the ASCII value for a space
		      e.preventDefault();
		  });
		});

	$(function() {
		  $('#storeNumDigit4').keydown(function(e) {
		    if (e.keyCode == 32) // 32 is the ASCII value for a space
		      e.preventDefault();
		  });
		});

	$(function() {
		  $('#filenum').keydown(function(e) {
		    if (e.keyCode == 32) // 32 is the ASCII value for a space
		      e.preventDefault();
		  });
		});

	$(function() {
		  $('#filenum1').keydown(function(e) {
		    if (e.keyCode == 32) // 32 is the ASCII value for a space
		      e.preventDefault();
		  });
		});

	$(function() {
		  $('#filenum2').keydown(function(e) {
		    if (e.keyCode == 32) // 32 is the ASCII value for a space
		      e.preventDefault();
		  });
		});

 
</script>

<style>
.tableoiltankfrm {
    
        font:12px verdana, arial, helvetica, sans-serif;
        border-width: 2px solid;
        border-color:#067bb6;
        border-style:solid;
        color:#0d157f;
        background-color: #88d5fc;
        width:90%;
        margin-left:5%;
        margin-top:5%;
}

.tableoiltankfrm  tr td {
    
        background-color:#e2f3fc;
        margin: 5px;
        padding: 5px;
}

.tdLabel {
    
        font-weight: bold;
        align: top;
        
}

.label {} 

 .errorMessage {
    
        color:red;
        font-size: 12px;
}

.textInput {

        border: 1px solid #3998c7;
        color:#0d157f;
}


</style>
<sj:head jqueryui="true" />
</head>
<body>


<noscript> 
	<meta http-equiv="Refresh" content="0;URL=../../disabled.html"> 
</noscript>

<%


                                                                    request.getSession();

                                                                    Object isLogedIn = session.getAttribute("isLogedIn");
                                                                    
                                                                    if(isLogedIn == null){
                                                                        
                                                                        response.sendRedirect("../../Message.jsp");
                                                                    }
                                                                    
                                                                    session.setMaxInactiveInterval(1000);

                                                                    String  subDept = (String)session.getAttribute("subDept");
                                                                    String  shop  = (String)session.getAttribute("str3");
                                                                    String user = (String)session.getAttribute("str");
                                                                    String role = (String)session.getAttribute("strRole");
                                                                    
%>

<h2 align="center">Sri Lanka Railway Department</h2>
<h3 align="center">CME's Sub Department :: SI/LPO Update Form</h3>


<s:form name="tecform" action="upDatesEvents" method="post" onSubmit="javascript:return ValidatesDates();">
        <table class="tableoiltankfrm">
        
            <tr class="parent" id="2478">
                   <th colspan="8" align="center"><font size="3px">PCM Date Entering Form</font></th>
             </tr>
             <tr class="parent" id="2478">
                <td colspan="2">
                    <s:label key="si-order.section" required="true"/>
                </td>
                    <td colspan="6">
                    <s:select cssClass="textInput" name="type" 
            list="{'DLE','DC','DE','DLH','DP','DS'}" 
            headerValue="----Please select----" headerKey="" />
                    
                    </td>
             </tr>
             <tr class="parent" id="2478">
                <td colspan="2">
                        <s:label key="si-order.storenumber" required="true"/>
                </td>
                <td colspan="6">
                <s:textfield cssClass="textInput" id="storeNum" name="storeNum" size="5" required="true"/>
                <s:textfield cssClass="textInput" id="storeNumDigit1" name="storeNumDigit1" size="5" required="true"/>
                <s:textfield cssClass="textInput" id="storeNumDigit2" name="storeNumDigit2" size="5"/>
                <s:textfield cssClass="textInput" id="storeNumDigit3" name="storeNumDigit3" size="5"/>
                <s:textfield cssClass="textInput" id="storeNumDigit4" name="storeNumDigit4" size="5"/>
                <div id="imagedisplay"></div>
                </td>
               </tr> 
               
               <tr>          
               <td colspan="2">
                   <s:label key="si-order.filenum" />
                </td>
                <td colspan="6">
                    <s:textfield cssClass="textInput" id="filenum" name="filenum" size="5" />
                    <s:textfield cssClass="textInput" id="filenum1" name="filenum1" size="5" />
                    <s:textfield cssClass="textInput" id="filenum2" name="filenum2" size="5" />
                    <input type="hidden" value="<%= shop %>" name="shop" />
                    <input type="hidden" value="<%= subDept %>" name="subdept" />
                    <input type="hidden" value="<%= user  %>"  name="depuser" />
                    <input type="hidden" value="<%= role  %>"  name="role" />
                </td> 
                
             </tr>
               
               <tr class="parent" id="2478">
                <td colspan="2">
                    <s:label key="si-order.dateselect" required="true"/>
                </td>
                    <td colspan="6">
                    <s:select cssClass="textInput" id="selectcon" name="selecteddate" 
            list="{'PCM date','Inspection Report date','Order Place date','LC Opening date','Item Recived date','Suitablility Report date'}" 
            headerValue="----Please select----" headerKey="" />
                    
                    </td>
             </tr>
               
             <tr class="child"> 
               
               <td colspan="2">
                   <s:label key="si-order.invoicenum" />
                </td>
                <td colspan="6">
                    <s:textfield cssClass="textInput" name="invoicenum" size="20" />
                    
                </td> 
                
             </tr>
                        
             <tr class="parent" id="2478">
               <td colspan="2">
                    <s:label key="si-order.pcmdate" />
                </td>
                <td colspan="6">
                   <sj:datepicker id="datesforevent" cssClass="textInput" maxDate="" onmouseover='gen_store_num()'
                                                                     name="datesforevent" size="14" 
                                                                     buttonImageOnly="true" showOn="button"
                                                                     displayFormat="dd-M-yy" changeYear="true" />
                </td>
             </tr>    
             <tr class="parent" id="2478">
                    <td colspan="8">
                            <s:submit cssClass="textInput" key="si-order.tecrec" />
                            <s:reset cssClass="textInput" value="Reset" />
                    </td>
             </tr>            
        </table>                
 </s:form>
  
  
  
  

</body>
</html>