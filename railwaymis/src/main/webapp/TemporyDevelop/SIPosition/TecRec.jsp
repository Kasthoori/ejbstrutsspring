<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ page import="java.io.*" %>
<%@ page import="java.sql.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sd" uri="/struts-dojo-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="db" scope="request" class="pk1.DbBean" />
<jsp:setProperty name="db" property="*" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Items Issue for Consumption</title>
<s:head/>
<sj:head jqueryui="true" />
<script language="javascript" type="text/javascript" src="../../jquery-1.4.2.js"></script>
<script type="text/javascript" language="javascript" src="JsFiles/EnterTecSelect.js"></script>
<script src="../../trimFunctions.js" language="JavaScript" type="text/javascript"></script>
<script type="text/javascript" language="javascript">

/* $(document).ready(function(){
    $("#quantity").keydown(function(event) {
       if(event.shiftKey)
       {
            event.preventDefault();
       }

       if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190)    {
       }
       else {
            if (event.keyCode < 95) {
              if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
              }
            }
            else {
                  if (event.keyCode < 96 || event.keyCode > 105) {
                      event.preventDefault();
                  }
            }
          }
       });
    });
    
$(document).ready(function(){
    $("#orderedquantity").keydown(function(event) {
       if(event.shiftKey)
       {
            event.preventDefault();
       }

       if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190)    {
       }
       else {
            if (event.keyCode < 95) {
              if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
              }
            }
            else {
                  if (event.keyCode < 96 || event.keyCode > 105) {
                      event.preventDefault();
                  }
            }
          }
       });
    });


$(document).ready(function(){
    $("#uprice").keydown(function(event) {
       if(event.shiftKey)
       {
            event.preventDefault();
       }

       if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190)    {
       }
       else {
            if (event.keyCode < 95) {
              if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
              }
            }
            else {
                  if (event.keyCode < 96 || event.keyCode > 105) {
                      event.preventDefault();
                  }
            }
          }
       });
    });


function newPopupSendFuelRep(url1){

    var val2 = url1.value;

    if(val2 == 'Recommended With Third Party Inspection' || val2 == 'Recommended Without Third Party Inspection'){

    popupWindow = window.open('http://www.cbnrms.net/RLMSProject/TemporyDevelop/SIPosition/SendNameOfSuccBider.jsp','popUpWindow','Height=550px,width=1000px,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=yes,status=yes');

    }else if(val2 == "Remarks" || val2 == "Deleted" || val2 == "Clarification" || val2 == "Fresh quatation"){
    	
    popupWindow = window.open('http://www.cbnrms.net/RLMSProject/TemporyDevelop/SIPosition/Remarks.jsp','popUpWindow','Height=550px,width=1000px,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=yes,status=yes');	
    	
    }
}
*/
  
    function gen_store_num(){

        var totStoreNum = "";  

            var x = document.tecform.storeNum.value;
            var y = document.tecform.storeNumDigit1.value;
            var z = document.tecform.storeNumDigit2.value;
            var i = document.tecform.storeNumDigit3.value;
            var m = document.tecform.storeNumDigit4.value;


           

          if((x != "") && (y == "") && (z == "") && (i == "") && (m == "")){

                     totStoreNum = x;
                    
              }else if((x != "") && (y != "") && (z == "") && (i == "") && (m == "")){

                         totStoreNum = x + " " + y;

                  }else if((x != "") && (y != "") && (z != "") && (i == "") && (m == "")){

                             totStoreNum = x + " " + y + "/" + z;

                      }else if((x != "") && (y != "") && (z != "") && (i != "") && (m == "")){


                                 totStoreNum = x + " " + y + "/" + z + "/" + i;
                          }else{

                                    totStoreNum = x + " " + y + "/" + z + "/" + i + "/"  +  m;

                              }

            
            

            $.post("CheckSILpSnumber.jsp",{storenum : "" + totStoreNum + ""},

                        function(data){
                            $('#imagedisplay').html(data)

                });

            
        }
    
/* $(document).ready(function(){
        
        $('.child1 td').hide();
        $('.child2 td').hide();
        $('.child3 td').hide();
       
        
  
        
        $('#selectcon').change(function(){
            
            var val = $(this).val();
            
            if(val == "Recommended With Third Party Inspection" || val == "Recommended Without Third Party Inspection"){
                
                $('.child1 td').show();
                $('.child2 td').show();
                $('.child3 td').show();
                
            }else{
                
                $('.child1 td').hide();
                $('.child2 td').hide();
                $('.child3 td').hide();
                
            }
        });
        
    }); */
</script>

<style>
.tableoiltankfrm {
    
        font:12px verdana, arial, helvetica, sans-serif;
        border-width: 2px solid;
        border-color:#067bb6;
        border-style:solid;
        color:#0d157f;
        background-color: #88d5fc;
        width:90%;
        margin-left:5%;
        margin-top:5%;
}

.tableoiltankfrm  tr td {
    
        background-color:#e2f3fc;
        margin: 5px;
        padding: 5px;
}

.tdLabel {
    
        font-weight: bold;
        align: top;
        
}

.label {} 

 .errorMessage {
    
        color:red;
        font-size: 12px;
}

.textInput {

        border: 1px solid #3998c7;
        color:#0d157f;
}

</style>
<sj:head jqueryui="true" />
</head>
<body>
<noscript>
    <meta http-equiv="Refresh" content="0;URL=disabled.html">
</noscript>




<%

                                                                    request.getSession();

                                                                    Object isLogedIn = session.getAttribute("isLogedIn");
                                                                    
                                                                    if(isLogedIn == null){
                                                                        
                                                                        response.sendRedirect("../../Message.jsp");
                                                                    }
                                                                    
                                                                    session.setMaxInactiveInterval(1000);

                                                                    String  subDept = (String)session.getAttribute("subDept");
                                                                    String  shop  = (String)session.getAttribute("str3");
                                                                    String user = (String)session.getAttribute("str");
                                                                    String role = (String)session.getAttribute("strRole");
%>

<h2 align="center">Sri Lanka Railway Department</h2>
<h3 align="center">CME's Sub Department :: SI/LPO Update Form</h3>


<s:form name="tecform" action="selectTecRecommendType" method="post" onSubmit="javascript:return ValidateTecSelection();">
        <table class="tableoiltankfrm">
        
            <tr>
                   <th colspan="8" align="center"><font size="3px">TEC Recommendation Entering Form TEST</font></th>
             </tr>
             <tr>
                <td colspan="2">
                    <s:label key="si-order.section" required="true"/>
                </td>
                    <td colspan="6">
                    <s:select cssClass="textInput" name="type" 
            list="{'DLE','DC','DE','DLH','DP','DS'}" 
            headerValue="----Please select----" headerKey="" />
                    
                    </td>
             </tr>
             <tr>
                <td colspan="2">
                        <s:label key="si-order.storenumber" required="true"/>
                </td>
                <td colspan="6">
                <s:textfield cssClass="textInput" name="storeNum" size="5" required="true"/>
                <s:textfield cssClass="textInput" name="storeNumDigit1" size="5" required="true"/>
                <s:textfield cssClass="textInput" name="storeNumDigit2" size="5"/>
                <s:textfield cssClass="textInput" name="storeNumDigit3" size="5"/>
                <s:textfield cssClass="textInput" name="storeNumDigit4" size="5"/>
                <div id="imagedisplay"></div>
                </td>
               </tr>
               
               <tr>          
               <td colspan="2">
                   <s:label key="si-order.filenum" />
                </td>
                <td colspan="6">
                    <s:textfield cssClass="textInput" name="filenum" size="5" />
                    <s:textfield cssClass="textInput" name="filenum1" size="5" />
                    <s:textfield cssClass="textInput" name="filenum2" size="5" />
                    <input type="hidden" value="<%= shop %>" name="shop" />
                    <input type="hidden" value="<%= subDept %>" name="subdept" />
                    <input type="hidden" value="<%= user  %>"  name="depuser" />
                    <input type="hidden" value="<%= role  %>"  name="role" />
                </td> 
               
                
             </tr>
             
             
                
            <tr>
                    <td colspan="8">
                            <s:submit cssClass="textInput" key="si-order.tecrec" />
                            <s:reset cssClass="textInput" value="Reset" />
                    </td>
             </tr>
            
        </table>        
        
 </s:form>
 
 
</body>
</html>
