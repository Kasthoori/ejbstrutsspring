<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="db" scope="request" class="pk1.DbBean" />
<jsp:setProperty property="*" name="db"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language="javascript" type="text/javascript" src="../../jquery-1.4.2.js"></script>
<link rel="stylesheet" href="../../RLMS.css" type="text/css">
<title>SI Opening control Page</title>

<style type="text/css">
.userTable {
        border-width:1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        margin-left:20px;
        width : 550px;
        
    
}

.userTable  td {
    
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
}

.userTable th {
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        background-color: rgb(255, 255, 255);
}

.setTable{

       position:float;
       margin-top:100px;
       margin-left:50px;

}

.notify_table{

	position:float;
	margin-top:-120px;
	margin-left:650px;

}

.userTableNotify {
        border-width:1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        margin-left:20px;
        width : 100px;
        
    
}

.userTableNotify  td {
    
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
}

.userTableNotify th {
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        background-color: rgb(255, 255, 255);
}


</style>

<script type="text/javascript">

function newPopup(url){

	popupWindow = window.open('https://www.rlmsproject.com/RLMSProject/TemporyDevelop/PopupWindows/DetailsPages/DetailsSectionDle.jsp','popUpWindow','Height=250,width=600,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=yes,status=yes')
}

function newPopupOne(url){

	popupWindow = window.open('https://www.rlmsproject.com/RLMSProject/TemporyDevelop/PopupWindows/DetailsPages/DetailsSectionDlh.jsp','popUpWindow','Height=250,width=600,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=yes,status=yes')
}

function newPopupTwo(url){

	popupWindow = window.open('https://www.rlmsproject.com/RLMSProject/TemporyDevelop/PopupWindows/DetailsPages/DetailsSectionDc.jsp','popUpWindow','Height=250,width=600,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=yes,status=yes')
}

function newPopupThree(url){

	popupWindow = window.open('https://www.rlmsproject.com/RLMSProject/TemporyDevelop/PopupWindows/DetailsPages/DetailsSectionDe.jsp','popUpWindow','Height=250,width=600,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=yes,status=yes')
}

function newPopupFour(url){

	popupWindow = window.open('https://www.rlmsproject.com/RLMSProject/TemporyDevelop/PopupWindows/DetailsPages/DetailsSectionDp.jsp','popUpWindow','Height=250,width=600,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=yes,status=yes')
}

</script>
</head>
<body>
<h1 align="center"><font color="#030834">Sri-Lanka Railway Department</font></h1>
<h2 align="center"><font color="#030834">CME's Sub Department :: Ratmalana</font></h2>


<%

String dlecount = "";
String dlhcount = "";
String dccount = "";
String decount = "";
String dpcount = "";

String secDle = "DLE";
String secDlh = "DLH";
String secDe = "DE";
String secDc = "DC";
String secDp = "DP";
		
		try{
			
			
			db.connect();
			
			ResultSet  rsdle = db.execSql("SELECT COUNT(*) as dlenum FROM details_lp_si_table WHERE section='DLE' AND acknow_cme='F'");
			
			while(rsdle.next()){
				
				dlecount = rsdle.getString("dlenum");
				
			}
			
           ResultSet  rsdlh = db.execSql("SELECT COUNT(*) as dlhnum FROM details_lp_si_table WHERE section='DLH' AND acknow_cme='F'");
			
			while(rsdlh.next()){
				
				dlhcount = rsdlh.getString("dlhnum");
				
			}
			
            ResultSet  rsdc = db.execSql("SELECT COUNT(*) as dcnum FROM details_lp_si_table WHERE section='DC' AND acknow_cme='F'");
			
			while(rsdc.next()){
				
				dccount = rsdc.getString("dcnum");
				
			}
			
          ResultSet  rsde = db.execSql("SELECT COUNT(*) as denum FROM details_lp_si_table WHERE section='DE' AND acknow_cme='F'");
			
			while(rsde.next()){
				
				decount = rsde.getString("denum");
				
			}
			
          ResultSet  rsdp = db.execSql("SELECT COUNT(*) as dpnum FROM details_lp_si_table WHERE section='DP' AND acknow_cme='F'");
			
			while(rsdp.next()){
				
				dpcount = rsdp.getString("dpnum");
				
			} 
			 
			db.close();
			
		}catch(Exception   exe){
			
			exe.printStackTrace();
		}


%>

<div class="setTable">
<table class="userTable">
<tr height="50px">
    <th colspan="7">
       Update Data Regarding SI And LP Orders
    </th>
</tr>
<tr height="70px">
<%--
    <td align="center" width="70px">
     <a href="EnterSIPositionDetails.jsp"><img src="../../Images/File.png" alt="Open SI File"></a>
    </td>
--%>
    <td align="center" width="70px">
     <a href=""><img width="60px" height="60px" src="../../Images/acknowledge.jpg" alt="Acknowledge"></a>
    </td> 
    <td align="center" width="70px">
     <a href="TecRec.jsp"><img src="../../Images/TecRec.png" alt="TEC Recommandation"></a>
    </td>
    <td align="center" width="70px">
     <a href="EnterDates.jsp"><img src="../../Images/PCMDate.png" alt="PCM Date"></a>
    </td>
    <td align="center" width="70px">
     <a href="PcmApproval.jsp"><img src="../../Images/PCMApp.png" alt="PCM Approval"></a>
    </td>
    <td align="center" width="70px">
     <a href="SuitabilityEnteringForm.jsp"><img src="../../Images/Suit.png" alt="Suitabiltiy"></a>
    </td>
    <td align="center" width="70px">
     <a href=""><img width="60px" height="60px" src="../../Images/postimage.jpg" alt="Send to SRS"></a>
    </td>
    <td align="center" width="70px">
     <a href=""><img width="50px" height="50px" src="../../Images/tickComplete.jpg" alt="Complete"></a>
    </td>
</tr>
</table>
</div>
<div class="notify_table">
	<table class="userTableNotify">
		<tr>
			<th>Section</th><th>Registered Files</th><th>Clarification Received</th>
		</tr>
		<tr>
			<td bgcolor="red">DLE</td>
			<td>
				
				<a href="JavaScript:newPopup('DetailsSectionDle.jsp');"><font color="darkblue"><b><%= dlecount %></b></font>&nbsp;&nbsp;Details</a>	
			</td>
			<td></td>
		</tr>
		<tr>
			<td bgcolor="lightgreen">DLH</td>
			<td><a href="JavaScript:newPopupOne('DetailsSectionDlh.jsp');"><font color="darkblue"><b><%= dlhcount %></b></font>&nbsp;&nbsp;Details</a>
			<td></td>
		</tr>
		<tr>
			<td bgcolor="yellow">DC</td>
			<td><a href="JavaScript:newPopupTwo('DetailsSectionDc.jsp');"><font color="darkblue"><b><%= dccount %></b></font>&nbsp;&nbsp;Details</a>
			<td></td>
		</tr>
		<tr>
			<td bgcolor="violet">DE</td>
			<td><a href="JavaScript:newPopupThree('DetailsSectionDe.jsp');"><font color="darkblue"><b><%= decount %></b></font>&nbsp;&nbsp;Details</a>
			</td><td></td>
		</tr>
		<tr>
			<td bgcolor="pink">DP</td>
			<td><a href="JavaScript:newPopupFour('DetailsSectionDp.jsp');"><font color="darkblue"><b><%= dpcount %></b></font>&nbsp;&nbsp;Details</a>
			<td></td>
		</tr>
	
	</table>
</div>
<dir id="imagedisplay"></dir>
 
</body>
</html>
