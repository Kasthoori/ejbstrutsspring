<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="/struts-jquery-tags" prefix="sj" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css" media="print">
    @media print
    {
    #non-printable { display: none; }
    #printable {
    display: block;
    width: 100%;
    height: 200%;
    }
   }
   
   #acro{
    display: block;
    width: 100%;
    height: 200%;
   
   }
   #itemdescript{
    display: block;
    width: 100%;
    height: 100%;
   
   }
   
   @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    } 
    
  @media print  { #notprint  { display: none; } }
/*   @media print  { #tmpPrintAlbum  { display: none; } } */
/*   @media print  { #tmpAddAlbum  { display: none; } } */
/*   @media print  { #tmpAlbum  { display: none; } } */
/*   @media print  { #sameitem { display: none; } } */
/*   @media print  { #storenum { display: none; } } */
/*   @media print  { #imagedisplay { display: none; } } */
/*   @media print  {#tmpSpecAlbum {display:none;}} */
 
                  
  
</style>
<style type="text/css">

.content {
	font-family:sans-serif;
	font-size:small;
	margin-top:45px;
	
}

.userTable {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:20px;
		width : 90%;
		font-size: 16px;
		//height:126px;
		//overflow-y:scroll; 
		//display:block; 
}

.userTableDes {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:20px;
		width : 60%;
		font-size: 16px;
		//height:126px;
		//overflow-y:scroll; 
		//display:block; 
}

.userTableAcro {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:10px;
		width : 45%;
		font-size: 14px;
		//height:126px;
		//overflow-y:scroll; 
		//display:block; 
}

.userTable  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTableDes  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTableAcro  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}


.userTable th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.userTableDes th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.userTableAcro th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.odd {
	
		background-color: #fffff0;
		
}

.even {
		background-color: #faf0e6;
}

.textInput {

		border: 1px solid #123B5F;
		color:#123B5F;
}

#itemdescript{
    
       margin-left:500px;
       margin-top:-265px;

}

#acro {
    margin-top:15px;
	margin-left:20px;
}
.submit {
 color: #000;
 font-size: 14px;
 width: 100px;
 height: 30px;
 border: 1px;
 margin: 1px;
 padding: 1px;
 background:#BEC0C0;
 margin-left:110px;
 
}

</style>
</head>
<body>
<div id="acro">
<table class="userTableAcro">
<tr>
   	 <th colspan="2">Acronyms</th>
   </tr>
   <tr>
      <th align="left" colspan="1">TEC</th>
      <td align="left" colspan="1"><i>Technical Evaluation Committee</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">T/Rec</th>
   	  <td align="left" colspan="1"><i>TEC Recommended</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">T/RTI</th>
   	  <td align="left" colspan="1"><i>TEC Recommended with Third Party Inspection</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">T/CL</th>
   	  <td align="left" colspan="1"><i>TEC Clarification</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">T/FQ</th>
   	  <td align="left" colspan="1"><i>TEC Fresh Quotation</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">T/DEL</th>
   	  <td align="left" colspan="1"><i>TEC Deleted</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">PCM</th>
   	  <td align="left" colspan="1"><i>Procurement Committee Meeting</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">PCM/APP</th>
   	  <td align="left" colspan="1"><i>PCM Approval</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">Suit</th>
   	  <td align="left" colspan="1"><i>Suitability</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">Suit/Con</th>
   	  <td align="left" colspan="1"><i>Suitability Condition</i></td>
   </tr>
</table>
</div>
<br /><br />
<div id="itemdescript">
 <table class="userTableDes">
 <s:if test="%{getDes.isEmpty()}">
 
 
                <tr height="220px">
                	<td colspan="2">
                	   <font size="22px">Item Not Registered</font>
                	</td>
                
                </tr>
                
            </s:if>
            
            <s:else>
 
            <s:iterator value="getDes" status="getDesStatus">
       
            <tr>
               <td><font size="18px"><s:label key="silp-details.stockitemname" /></font></td>
               <td><font color="red"><font size="18px"><b><s:property value="storenumber" /></b></font></font></td>
  			</tr>
  			
  			<tr>
  			    <th colspan="2"><s:label key="silp-details.descript" /></th>
  			</tr>
  			<tr>
  				<td colspan="2">
  					<s:property value="descriptions" />
               </td>
  			</tr> 
  			<tr>
  			    <td><b><i><s:label key="silp-details.partnum" /></i></b></td>
  				<td><b><i><s:property value="itemname" /></i></b></td>
  			</tr>
               
               
  </s:iterator> 
 	</s:else>
 </table>

</div>

<s:if test="getFilesDetails.size() > 0">

 <div id="printable" class="content"> 
			<table class="userTable" cellpadding="5px">	
			
			
			       <tr>
			            <th colspan="2">Store Number</th>
			       		<th colspan="2" >File Number</th>
			       		<th colspan="2">SI/LP Number</th>
			       		<th colspan="2">Section</th>
			       		<th colspan="2">File Date</th>
			       		
			       </tr>		
							             
					<s:iterator value="getFilesDetails" status="getFilesDetailsStatus">
                                  <tr class="<s:if test="#getFilesDetailsStatus.odd == true">odd</s:if><s:else>even</s:else>">
               							<td align="center" colspan="2">
               								<s:property value="storenumber" />
               							</td>
               							
               							<td align="center" colspan="2">
               							 <s:property value="filenum" />
               							 
                					    </td>
                					    
                					    <td align="center" colspan="2">
                					    	<s:property value="silpnum" />
                					    </td>
                					    <td align="center" colspan="2">
                					    	<s:property value="section" />
                					    </td>
                					    <td align="center" colspan="2">
                					    	<s:property value="filedate" />
                					    </td>
                					    <%-- <td>
                					        
                					         <s:url id="getdetails" action="StockIemDetInFiles">
                					              <s:set name="filenumber" value="filenum" />
				                                  <s:param name="filenumber" value="%{filenumber}" />
				                             </s:url>
				                            
					                              <s:a href="%{getdetails}"><img width="20px" height="20px" alt="search" src="../ReportsSiPosition/images/search-icon.png">
					                         </s:a>  
					
                					    
                					    	
                					    </td> --%>
                				 </tr>
                				 
                				 <tr>
                				    <s:set var="teccond" value="tecrecommend" />
   									<s:set var="pcmcond" value="pcmapp" />
   									<s:set var="suit" value="suitability" />
   									<s:set var="bidselect" value="bidselect" />
                                    <s:set var="sucbidder" value="namesuccbider" />
                                    <s:set var="ordquantity" value="orderedquantity" />
		                            <s:set var="quan" value="quantity" />
		                            <s:set var="pcmapp" value="pcmapp" />
           
   								
     							   <th colspan="1">
     							   		TEC
     							   </th>	
     							   <td colspan="1" bgcolor="yellow">
                                         <i>
    									 <s:if test="#teccond == 'Not processed'">
       											 <b> No</b>
    									 </s:if>
     								<s:else>
     									<b> Yes,</b> number of TEC <font color="blue"><s:set var="tecround" value="tecround" />
                   							<s:if test="#tecround == null">0</s:if>
                  								 <s:else><s:property value="tecround" /></s:else>
                  						 </font>
     								</s:else>
     								</i>
     							</td> 
     							<th colspan="1">
     							   		T/Rec
     							   </th>
     							<td colspan="2">
     							        <i>
     									<s:if test="#teccond == 'Recommended'">
     											Yes
     									</s:if>
    									 <s:else>
       											 No
     									</s:else>
     									</i>
     							</td> 
     							<th colspan="1">
     							   		T/RTI
     							   </th>  
     							<td colspan="2">
     							        <i>
     									<s:if test="#teccond == 'Recommended With Third Party Inspection'">
     											Yes
     									</s:if>
    									 <s:else>
        										No
     									</s:else>
     									</i>
     							</td>  
     							<th colspan="1">
     							   		T/CL
     							   </th> 
     							<td colspan="1">
     							        <i>
     									<s:if test="#teccond == 'Clarification'">
     											Yes
     									</s:if>
     									<s:else>
        										No
     									</s:else>
     									</i>
     							</td>  
     							</tr>
     							<tr>
     							<th colspan="1">
     							   		T/FQ
     							   </th> 
     							<td colspan="1">
     							        <i>
     									<s:if test="#teccond == 'Fresh quatation'">
     											Yes
     									</s:if>
     									<s:else>
       											 No
     									</s:else>
     									</i>
    							 </td> 
    							 <th colspan="1">
     							   		T/DEL
     							   </th>  
     							 <td colspan="1">
     							        <i>
     										<s:if test="#teccond == 'Deleted'">
     											Yes
     										</s:if>
     										<s:else>
        										No
     										</s:else>
     										</i>
     							</td> 
     							<th colspan="1">
     							   		PCM
     							   </th>   
     							<td colspan="2" bgcolor="yellow">
     							            <i>
    										 <s:if test="#pcmcond == 'Not processed'">
     												<b>No</b>
     										</s:if>
     										<s:else>
       												<b> Yes </b>
     										</s:else>
     										</i>
     							</td>
     							<th colspan="1">
     							   		PCM/APP
     							   </th>  
    							 <td colspan="2">
    							           <i>
     										<s:if test="#pcmcond == 'Approved'">
     														App
     										</s:if>
    										 <s:else>
       														 NoApp
     										</s:else>
     										</i>
    							 </td> 
    							 </tr>
    							 <tr>
    							 <th colspan="2">
     							   		Suit
     							   </th> 
     							<td colspan="3" bgcolor="yellow">
     							
     							               <i>
     											<s:if test="#suit == 'Not processed'">
     													<b>No</b>
     											</s:if>
    											 <s:else>
        													<b>Yes</b>
     											</s:else>
     										</i>
    							 </td> 
    							 <th colspan="2">
     							   		Suit/Con
     							   </th> 
     							 <td colspan="3">
     							              <i>
     											<s:if test="#suit == 'Suitable'">
     													S
     											</s:if>
    										    <s:elseif test="#suit == 'Short Fall'">
       													 SF
     											</s:elseif>
    										    <s:elseif test="#suit == 'Damaged'">
       													 D
    											 </s:elseif>
    											 <s:elseif test="#suit == 'Not Suitable'">
      											         NS
    											 </s:elseif>
    											 <s:else>
      													  No
     											 </s:else>
     											 
     											 </i>
     										</td> 
                				 
                				 		</tr>
                				 		
                				 		
                				   <s:if test="#sucbidder == 'Selected the most economical supplier by SRS'">
           
                
                                      <tr>
                      		             <th bgcolor="ash" colspan="10">Name of Successful Bidder</th>
                      
                                       </tr>
                                     <tr>
                      		                <td colspan="10"><font color="#14820a"><i>Bidder is selected by the SRS in the most economical way</i></font></td>
                      
                                     </tr>
           
                                 </s:if>
                                        <s:elseif test="#sucbidder != 'null' ||  ''">
                  
                                       <tr>
                      		                   <th bgcolor="ash" colspan="10">Name of Successful Bidder</th>
                      
                                       </tr>
                                       <tr>
                      		               <td colspan="5"><s:label key="silp-details-alldescription.biddername" /></td>
                      		               <td colspan="5"><font color="#14820a"><i><s:property value="namesuccbider" /></i></font></td>
                      
                                      </tr>
                
           
               </s:elseif>
               
               <s:if test="#ordquantity != #quan"> 
          
              
             	<tr>
             		<th bgcolor="ash" colspan="10">Descriptions of Quantity</th>
             	</tr>
               <tr>
                    <td colspan="2"><s:label key="silp-details-alldescription.quantity" /></td>
                	<td colspan="1"><s:property value="quantity" /><s:property value="units" /></td>
                    <td colspan="2"><s:label key="silp-details-alldescription.ordquan" /></td>
                	<td colspan="1"><s:property value="orderedquantity" /><s:property value="orderedunit" /></td>
                	
                </tr>
                <tr>
                   <td colspan="3">
                          <font color="blue"><s:label key="silp-details-alldescription.uprice"  /></font>
                   </td>
                   <td colspan="2"><s:property value="unitprice" />&nbsp;<s:property value="currency" /></td>
                   <td colspan="3">
                          <font color="brown"><s:label key="silp-details-alldescription.totalvalu" /></font>
                   </td>
                   <td colspan="2"><s:property value="totalvalue" />&nbsp;<s:property value="currency" /></td>
                </tr>
                
             
          
          </s:if>  
          <s:elseif test="#ordquantity != '0.0'">
               
             	<tr>
             		<th bgcolor="ash" colspan="10">Descriptions of Quantity</th>
             	</tr>
                <tr>
                    <td colspan="5"><s:label key="silp-details-alldescription.ordquan" /></td>
                	<td colspan="5"><s:property value="orderedquantity" /><s:property value="orderedunit" /></td>
                </tr>
                <tr>
                   <td colspan="3">
                          <font color="blue"><s:label key="silp-details-alldescription.uprice"  /></font>
                   </td>
                   <td colspan="2"><s:property value="unitprice" />&nbsp;<s:property value="currency" /></td>
                   <td colspan="3">
                          <font color="brown"><s:label key="silp-details-alldescription.totalvalu" /></font>
                   </td>
                   <td colspan="2"><s:property value="totalvalue" />&nbsp;<s:property value="currency" /></td>
                </tr>
            
          
          </s:elseif>
          <s:else>
          
          </s:else> 
          
          
          <!-- Details of PCM -->
          
          
          
           <s:if test="#pcmapp != 'Not processed'">
                <s:set var="guidelines" value="pcmguidelines" />
                	<s:if test="#guidelines != 'Not processed'">
                	
                	
            						<tr>
            							<th bgcolor="#cccccc" colspan="10">PCM Details</th>
            						</tr>
            						<tr>
            						
            							<td colspan="5"><s:label key="silp-details-alldescription.pcmcond" /></td>
            							<td colspan="5"><s:property value="pcmapp" /></td>
            						</tr>
            						<tr>
            						
            							<td colspan="5"><s:label key="silp-details-alldescription.guideline" /></td>
            							<td colspan="5"><s:property value="pcmguidelines" /></td>
            						</tr>
            	   
            			
                	
                	</s:if>
            	    <s:else>
            	    		
            						<tr>
            							<th colspan="10">PCM Details</th>
            						</tr>
            						<tr>
            						
            							<td colspan="5"><s:label key="silp-details-alldescription.pcmcond" /></td>
            							<td colspan="5"><s:property value="pcmapp" /></td>
            						</tr>
            						<tr>
            						
            							<td colspan="5"><s:label key="silp-details-alldescription.guideline" /></td>
            							<td colspan="5">Guide Lines have not been provided</td>
            						</tr>
            	   
            			
                	
            	    
            	    </s:else>
            
            </s:if>
            <s:else>
            
           </s:else>
                 
                
               
   			 </s:iterator>
   			</table> 
   			<input id="notprint" type="button" id='tmpPrintAlbum' onclick="JavaScript:window.print();" value="print" class="submit" />
    </div>
    
</s:if>
<s:else>
    <h2>No Files</h2>

</s:else>

</body>
</html>