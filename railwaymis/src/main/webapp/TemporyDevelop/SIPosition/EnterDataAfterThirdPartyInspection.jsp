<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.tableoiltankfrm {
    
        font:12px verdana, arial, helvetica, sans-serif;
        border-width: 2px solid;
        border-color:#067bb6;
        border-style:solid;
        background-color: #88d5fc;
        width:90%;
        margin-left:5%;
        margin-top:5%;
}

.tableoiltankfrm  tr td {
    
        background-color:#e2f3fc;
        margin: 5px;
        padding: 5px;
}

.tdLabel {
    
        font-weight: bold;
        align: top;
        
}

.label {} 

 .errorMessage {
    
        color:red;
        font-size: 12px;
}

.textInput {

        border: 1px solid #3998c7;
        color:#0d157f;
}

</style>

</head>
<body>

<%

                                                                    request.getSession();

                                                                    Object isLogedIn = session.getAttribute("isLogedIn");
                                                                    
                                                                    if(isLogedIn == null){
                                                                        
                                                                        response.sendRedirect("../../Message.jsp");
                                                                    }
                                                                    
                                                                    session.setMaxInactiveInterval(1000);

                                                                    String  subDept = (String)session.getAttribute("subDept");
                                                                    String  shop  = (String)session.getAttribute("str3");
                                                                    String user = (String)session.getAttribute("str");
                                                                    String role = (String)session.getAttribute("strRole");
%>


<h2>Third Party Inspection successful</h2>

<s:iterator value="getList" status="getListStatus">   
      
        <s:set var="storenumber" value="storenumber" />  
        <s:set var="filenumber" value="filenum" />
        <s:set var="clarify" value="clarificbysrs"  />
        <s:property value="storenumber" />
        
        
        <s:form name="tecform" action="thirdPartyRepUpdate" method="post" onSubmit="javascript:return ValidateTecRec();">
        <table class="tableoiltankfrm">
        
            <tr>
                   <th colspan="8" align="center"><font size="3px" style="color:#0d157f;"><s:property value="storenumber" />&nbsp;and&nbsp;<s:property value="filenum"/>&nbsp;TEC Recommendation after Third Party Inspection</font></th>
             </tr>
            
            <tr>
                <td colspan="2">
                    <font style="color:#0d157f;"><s:label key="si-order.tecreccond" required="true"/></font>
                </td>
                    <td colspan="3">
                    <s:select cssClass="textInput" id="selectcon" name="tecrecommend" 
            list="{'Recommended With Third Party Inspection','Recommended','Deleted','Clarification','Fresh quatation'}" 
            headerValue="----Please select----" headerKey="" />
            
            <s:hidden name="storenumber" value="%{storenumber}" />
            <s:hidden name="filenumber" value="%{filenumber}" />
            <input type="hidden" value="<%= shop %>" name="shop" />
            <input type="hidden" value="<%= subDept %>" name="subdept" />
            <input type="hidden" value="<%= user  %>"  name="depuser" />
            <input type="hidden" value="<%= role  %>"  name="role" />
                    
                 </td>
             <td colspan="2"><s:label key="si-order.tecround" /></td>
             <td colspan="1"><s:textfield cssClass="textInput" name="tecround" size="2"  /></td>
             </tr>
              <tr>
                <td colspan="2">
                    <s:label key="si-order.reasons" required="true"/>
                </td>
                    <td colspan="6">
                     <s:textarea cssClass="textInput" cols="5" rows="3" name="thirdpartyinspectrep"></s:textarea>
                    </td>
             </tr>
             
              <tr>
                    <td colspan="8">
                            <s:submit cssClass="textInput" key="si-order.tecrec" />
                            <s:reset cssClass="textInput" value="Reset" />
                    </td>
             </tr>
             
     </table>
    </s:form>    
        
        
        
 </s:iterator>

</body>
</html>