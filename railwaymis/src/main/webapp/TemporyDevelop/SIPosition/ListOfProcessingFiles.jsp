<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<s:head/>
<style type="text/css">

.content {
    font-family:sans-serif;
    font-size:small;
    
}

.userTable {
        border-width:1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        margin-left:10px;
        width : 130px;
        height:126px;
		overflow-y:scroll; 
		display:block; 
    
}

.userTable  td {
    
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
}

.userTable th {
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        background-color: rgb(255, 255, 255);
}

.odd {
    
        background-color: #fffff0;
        
}

.even {
        background-color: #faf0e6;
}

.textInput {

        border: 1px solid #123B5F;
        color:#123B5F;
}

</style>
</head>
<body>


<s:if test="getFilesList.size() > 0">
            <div class="content">
            
            
                    <table class="userTable" cellpadding="5px">
                       
                            <tr class="even">
                               
                                <th>File Number</th>
                                
                                
                                
                            </tr>
                                <s:iterator value="getFilesList" status="getFilesListStatus">
                        
                                        <tr class="<s:if test="#getFilesListStatus.odd == true">odd</s:if><s:else>even</s:else>">
                                    
                                        
                                        <td>
                                        <b><s:property value="filenum"/></b>
                                        </td>
                                        
                                       
                                    </tr>
                                
                                </s:iterator>
                        
                    </table>
             
            </div>
        
        </s:if>

</body>
</html>