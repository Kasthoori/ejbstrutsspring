<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css" media="print">
    @media print
    {
    #non-printable { display: none; }
    #printable {
    display: block;
    width: 100%;
    height: 100%;
    }
   }
   
   @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
    
  @media print  { #notprint  { display: none; } }
/*   @media print  { #tmpPrintAlbum  { display: none; } } */
/*   @media print  { #tmpAddAlbum  { display: none; } } */
/*   @media print  { #tmpAlbum  { display: none; } } */
/*   @media print  { #sameitem { display: none; } } */
/*   @media print  { #storenum { display: none; } } */
/*   @media print  { #imagedisplay { display: none; } } */
/*   @media print  {#tmpSpecAlbum {display:none;}} */
 
                  
  
</style>
<style type="text/css">

.content {
	font-family:sans-serif;
	font-size:small;
	
}

.userTable {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:20px;
		width : 80%;
		font-size: 12px;
		//height:126px;
		//overflow-y:scroll; 
		//display:block; 
}

.userTable  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTable th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.odd {
	
		background-color: #fffff0;
		
}

.even {
		background-color: #faf0e6;
}

.textInput {

		border: 1px solid #123B5F;
		color:#123B5F;
}

.submit {
 color: #000;
 font-size: 14px;
 width: 100px;
 height: 30px;
 border: 1px;
 margin: 1px;
 padding: 1px;
 background:#BEC0C0;
 margin-left:20px;
 
}

</style>

</head>
<body>
<h2 align="center">Outstanding Files</h2>

<div id="printable" align="justify">  
  <s:if test="getOutstandsFrom.size() > 0">
   <table class="userTable">
     <caption>Outstanding Files</caption>
        <tr>
          <th>File Number</th>
          <th>Section</th>
          <th>SI/LP Number</th>
          <th>SI/LP Date</th>
          <th>File Date</th>
          <th>File Registered Date</th>
          <th>Search</th>
        </tr>
    <s:iterator value="getOutstandsFrom" status="getOutstandsFromStatus">
        <tr class="<s:if test="#getOutstandsFromStatus.odd == true">odd</s:if><s:else>even</s:else>">
          <td>
          <s:property value="filenum" />
          </td>
          <td>
          <s:property value="section" />
          </td>
          <td>
           <s:property value="silpnum" />
          </td>
          <td>
          <s:property value="silpdate" />
          </td>
          <td>
          <s:set var="filed" value="filedate" />
          <s:if test="#filed == 'Not processed'">
               Date not Entered
          </s:if>
          <s:else>
          <s:property value="filedate" />
          </s:else>
          
          </td>
          <td>
          <s:property value="newfiledate" />
          </td>
          <td id="notprint">
                					        
                					         <s:url id="getStockItemDetails" action="StockIemDetInFiles">
                					              <s:set var="filenumber" value="filenum" />
				                                  <s:param name="filenumber" value="%{filenumber}" />
				                             </s:url>
				                            
					                              <s:a href="%{getStockItemDetails}"><img width="20px" height="20px" alt="search" src="http://localhost:8082/RLMSProject/TemporyDevelop/ReportsSiPosition/images/search-icon.png">
					                         </s:a>  
					
                					    
                					    	
                					    </td>
        </tr>
    </s:iterator>
    
  </table>
  <input id="notprint" type="button" id='tmpPrintAlbum' onclick="JavaScript:window.print();" value="print" class="submit" /> 
 </s:if>
 <s:else>
 
     <h2>Not Files</h2>
 </s:else>
 </div>
</body>
</html>