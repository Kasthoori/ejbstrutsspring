<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sri Lanka Railways</title>
<style type="text/css" media="print">
    @media print
    {
    #non-printable { display: none; }
    #printable {
    display: block;
    width: 100%;
    height: 100%;
    }
   }
   
   @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
    
  @media print  { #notprint  { display: none; } }
/*   @media print  { #tmpPrintAlbum  { display: none; } } */
/*   @media print  { #tmpAddAlbum  { display: none; } } */
/*   @media print  { #tmpAlbum  { display: none; } } */
/*   @media print  { #sameitem { display: none; } } */
/*   @media print  { #storenum { display: none; } } */
/*   @media print  { #imagedisplay { display: none; } } */
/*   @media print  {#tmpSpecAlbum {display:none;}} */
 
                  
  
</style>
<style type="text/css">

.content {
	font-family:sans-serif;
	font-size:small;
	
}

.userTable {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:20px;
		width : 90%;
		font-size: 12px;
		//height:126px;
		//overflow-y:scroll; 
		//display:block; 
}

.userTable  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTable th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.odd {
	
		background-color: #fffff0;
		
}

.even {
		background-color: #faf0e6;
}

.textInput {

		border: 1px solid #123B5F;
		color:#123B5F;
}

.submit {
 color: #000;
 font-size: 14px;
 width: 100px;
 height: 30px;
 border: 1px;
 margin: 1px;
 padding: 1px;
 background:#BEC0C0;
 margin-left:20px;
 
}


</style>

</head>
<body>
<div id="printable">
<table border="0">
   <tr>
   	 <th colspan="2">Acronyms</th>
   </tr>
   <tr>
      <th align="left" colspan="1">TEC</th>
      <td align="left" colspan="1"><i>Technical Evaluation Committee</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">T/Rec</th>
   	  <td align="left" colspan="1"><i>TEC Recommended</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">T/RTI</th>
   	  <td align="left" colspan="1"><i>TEC Recommended with Third Party Inspection</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">T/CL</th>
   	  <td align="left" colspan="1"><i>TEC Clarification</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">T/FQ</th>
   	  <td align="left" colspan="1"><i>TEC Fresh Quotation</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">T/DEL</th>
   	  <td align="left" colspan="1"><i>TEC Deleted</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">PCM</th>
   	  <td align="left" colspan="1"><i>Procurement Committee Meeting</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">PCM/APP</th>
   	  <td align="left" colspan="1"><i>PCM Approval</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">Suit</th>
   	  <td align="left" colspan="1"><i>Suitability</i></td>
   </tr>
   <tr>
   	  <th align="left" colspan="1">Suit/Con</th>
   	  <td align="left" colspan="1"><i>Suitability Condition</i></td>
   </tr>
</table>
</div>
   <s:iterator value="getDetOfStoIteInFil" begin="0" end="0">
   
   			<h2 align="center"><s:property value="filenum" /></h2>
   			
   </s:iterator>
 <div id="printable">
<table class="userTable">
   <caption>Current Summary of Stock Items in File</caption>
   <tr>
   	<th>Stock Number</th>
   	<th bgcolor="yellow">TEC</th>
   	<th>T/Rec</th>
   	<th>T/RTI</th>
   	<th>T/CL</th>
   	<th>T/FQ</th>
   	<th>T/DEL</th>
   	<th bgcolor="yellow">PCM</th>
   	<th>PCM/APP</th>
   	<th bgcolor="yellow">Suit</th>
   	<th colspan="2">Suit/Con</th>
   	<th>More...</th>
   </tr>

   <s:iterator value="getDetOfStoIteInFil" status="getDetOfStoIteInFilStatus">
   <s:set var="teccond" value="tecrecommend" />
   <s:set var="pcmcond" value="pcmapp" />
   <s:set var="suit" value="suitability" />
   <tr>
     <td>
          <s:property value="storenumber" />
     </td>
     <td bgcolor="yellow">
     
     <s:if test="#teccond == 'Not processed'">
        <b> No</b>
     </s:if>
     <s:else>
     	<b> Yes,</b> number of TEC <font color="blue"><s:set var="tecround" value="tecround" />
                   <s:if test="#tecround == null">0</s:if>
                   <s:else><s:property value="tecround" /></s:else>
                   </font>
     </s:else>
     </td> 
     <td>
     <s:if test="#teccond == 'Recommended'">
     	Yes
     </s:if>
     <s:else>
        No
     </s:else>
     </td>   
     <td>
     <s:if test="#teccond == 'Recommended With Third Party Inspection'">
     	Yes
     </s:if>
     <s:else>
        No
     </s:else>
     </td>  
     <td>
     <s:if test="#teccond == 'Clarification'">
     	Yes
     </s:if>
     <s:else>
        No
     </s:else>
     </td>  
     <td>
     <s:if test="#teccond == 'Fresh quatation'">
     	Yes
     </s:if>
     <s:else>
        No
     </s:else>
     </td>  
     <td>
     <s:if test="#teccond == 'Deleted'">
     	Yes
     </s:if>
     <s:else>
        No
     </s:else>
     </td>   
     <td bgcolor="yellow">
     <s:if test="#pcmcond == 'Not processed'">
     	<b>No</b>
     </s:if>
     <s:else>
       <b> Yes </b>
     </s:else>
     </td> 
     <td>
     <s:if test="#pcmcond == 'Approved'">
     	App
     </s:if>
     <s:else>
        NoApp
     </s:else>
     </td> 
     <td bgcolor="yellow">
     <s:if test="#suit == 'Not processed'">
     	<b>No</b>
     </s:if>
     <s:else>
        <b>Yes</b>
     </s:else>
     </td> 
     <td>
     <s:if test="#suit == 'Suitable'">
     	S
     </s:if>
     <s:elseif test="#suit == 'Short Fall'">
        SF
     </s:elseif>
     <s:elseif test="#suit == 'Damaged'">
        D
     </s:elseif>
     <s:elseif test="#suit == 'Not Suitable'">
        NS
     </s:elseif>
     <s:else>
        No
     </s:else>
     </td> 
     <td id="notprint" colspan="2">
         <s:form name="detailfrm" method="post" action="getAllDetails">
           
                 <s:set var="storenum" value="storenumber" />
                 <s:set var="filenumber" value="filenum" />
                 <s:set var="type" value="section" />
                 <s:hidden name="type" value="%{type}" />
           		 <s:hidden name="storenumber" value="%{storenum}" />  
           		 <s:hidden name="filenumber" value="%{filenumber}" />                          
		         <!-- <img width="20px" height="20px" alt="search" src="http://www.rlmsproject.com/RLMSProject/Images/Search.jpeg" onmouseover='get_details();' /> -->
				 <s:submit type="image" height="20px" width="20px"  src="http://localhost:8082/RLMSProject/Images/Search.jpeg" />  
				 <%-- <s:submit />   --%>                   
			</s:form>
     </td>       
   </tr>
   </s:iterator>
</table>
<input id="notprint" type="button" id='tmpPrintAlbum' onclick="JavaScript:window.print();" value="print" class="submit" /> 
</div>
</body>
</html>