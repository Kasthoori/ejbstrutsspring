<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="/struts-jquery-tags" prefix="sj" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Insert title here</title>

<style type="text/css">

.content {
	font-family:sans-serif;
	font-size:small;
	
}

.userTable {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:20px;
		width : 60%;
		font-size: 12px;
		//height:126px;
		//overflow-y:scroll; 
		//display:block; 
}

.userTable  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTable th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.odd {
	
		background-color: #fffff0;
		
}

.even {
		background-color: #faf0e6;
}

.textInput {

		border: 1px solid #123B5F;
		color:#123B5F;
}

</style>

</head>
<body>

<s:if test="getFilesDetails.size() > 0">

 <div class="content"> 
			<table class="userTable" cellpadding="5px">	
			<caption>File Details</caption>
			
			       <tr>
			       		<th>File Number</th>
			       		<th>Section</th>
			       		<th>File Date</th>
			       		<th>Search</th>
			       </tr>		
							             
					<s:iterator value="getFilesDetails" status="getFilesDetailsStatus">
                                  <tr class="<s:if test="#getFilesDetailsStatus.odd == true">odd</s:if><s:else>even</s:else>">
               							<td>
               							 <s:property value="filenum" />
               							 
                					    </td>
                					    <td>
                					    	<s:property value="section" />
                					    </td>
                					    <td>
                					    	<s:property value="filedate" />
                					    </td>
                					    <td>
                					        
                					         <s:url id="getdetails" action="StockIemDetInFiles">
                					              <s:set var="filenumber" value="filenum" />
				                                  <s:param name="filenumber" value="%{filenumber}" />
				                             </s:url>
				                            
					                              <s:a href="%{getdetails}"><img width="20px" height="20px" alt="search" src="../ReportsSiPosition/images/search-icon.png">
					                         </s:a>  
					
                					    
                					    	
                					    </td>
                				 </tr>
               
   				     </s:iterator>
   			</table> 
    </div>
    
</s:if>
<s:else>
    <h2>No Files</h2>

</s:else>
</body>
</html>