<%-- 
    Document   : CallReport
    Created on : Dec 14, 2010, 2:34:53 AM
    Author     : Admin
--%>
 
<%@page import="java.io.IOException"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>
<%@page import="net.sf.jasperreports.engine.export.JRCsvExporter"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporter"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporter"%>
<%@page import="net.sf.jasperreports.engine.export.JRRtfExporter"%>
<%@page import="net.sf.jasperreports.engine.JRExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRPdfExporter"%>
<%@page import="net.sf.jasperreports.engine.JRExporter"%>
<%@page import="java.io.OutputStream"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.JasperFillManager"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="pk1.*" %>
<%@page import="java.sql.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="db" scope="request" class="pk1.DbBean" />
<jsp:setProperty property="*" name="db"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
<head>
<title>report generation in  jsp</title>
</head>
<body>
<%
/*
String filename =  request.getParameter("filename");
String reporttype =  request.getParameter("reporttype");
String Paramtype =  request.getParameter("paramtype");;
*/



					    request.getSession();
						Object   isLogedIn  =  session.getAttribute("isLogedIn");

							if(isLogedIn == null) {
	
									response.sendRedirect("Message.jsp");
								}

					  session.setMaxInactiveInterval(1500);


                     
                      

			           String sNumPart3Fil = "";
					   String sNumPart4Fil = "";
					   String sNumPart5Fil = "";
					
					
					   String sNumPart1 = request.getParameter("storeNum");
					   String fSl1 = " ";
					   String sNumPart2 = request.getParameter("storeNumDigit1");
					   String fSl2 = "";
					   String sNumPart3 = request.getParameter("storeNumDigit2");
					   String sNumPart5 =  request.getParameter("storeNumDigit4");
					   String fS14 = "";
					   
					   if(sNumPart3.isEmpty()){
						   
						   sNumPart3Fil = "";
					   }else{
						   
						   fSl2 = "/";
						   sNumPart3Fil = sNumPart3;
					   }
					   
					   String fSl3 = "";
					   
					   String sNumPart4 = request.getParameter("storeNumDigit3");
					   
					   if(sNumPart4.isEmpty()){
						   
						   sNumPart4Fil = "";
					   }else{
						   
						   fSl3 = "/";
						   sNumPart4Fil = sNumPart4;
					   }
					   
					   if(sNumPart5.isEmpty()){
						   
						   sNumPart5Fil = "";
						   
					   }else{
						   
						   fS14 = "/";
						   sNumPart5Fil = sNumPart5;
					   }		   
					   
			        String sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
			        
			        String  filenumber  =  request.getParameter("filenum");

                    String filename = "";
                    
                    
                    
                    String recommondation = "";
        	        
        	        DbBean  db1 = new DbBean();
        	      
        	        try {
        	        db1.connect();
        	        
        	        ResultSet  rs = null;
        	        
        	        rs = db1.execSql("SELECT tec_recommond FROM details_lp_si_table WHERE store_num='" + sStoreNumber + "' AND file_num='" + filenumber + "'");
        	        
        	        while(rs.next()) {
        	        	
        	        	recommondation = rs.getString("tec_recommond");
        	        }
        	        
        	        db1.close();
        			
        	        }catch(Exception  ex) {
        	        	
        	        	ex.printStackTrace();
        	        }
                      
        	         
                      
                      if((recommondation.matches("Recommended With Third Party Inspection")) || (recommondation.matches("Recommended"))){
                    	  
                    	  
                    	  filename="/TemporyDevelop/SIPosition/JasReport/lpsi_details_withquantity.jasper";
                    	  
                      }else{
                    	  
                    	  filename="/TemporyDevelop/SIPosition/JasReport/lpsi_details_without_quantity.jasper";
                    	  
                      }
                     
                      
                      //String reporttype =  "html";
                      String Paramtype =  "";
                      String report = "StockItemsFullDetailsinFiles";
 
                      Connection  conn = db.connect();
 
                      Map<String, Object>  jasperParameter = new  HashMap<String, Object>();
                      jasperParameter.put("fileNum",filenumber);
                      jasperParameter.put("storeNum", sStoreNumber);
                   
 
                       String path =  application.getRealPath("/");
                       JasperPrint jasperPrint =  JasperFillManager.fillReport(path +"/"+filename, jasperParameter,  conn);
                       //JasperPrint jasperPrint =  JasperFillManager.fillReport(filename, jasperParameter, conn);
//System.out.println("Report Created... in  "+reporttype +" Format");
                        
                       

                        OutputStream ouputStream =  response.getOutputStream();
                        JRExporter exporter = null;
 
                         
                                 response.setContentType("application/pdf");
                                 response.setHeader("content-disposition","attachment;filename=" + report + ".pdf");
 
                                 exporter = new  JRPdfExporter();
                                 exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrint);
                                 exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,  ouputStream);
                        
 
                         
 
                                try
                          {
                                   exporter.exportReport();
                          }
                                   catch  (JRException e)
                          {
                                   throw new  ServletException(e);
                          }
                                finally
                          {
                                   if (ouputStream !=  null)
                          {
                                try
                          {
                                  ouputStream.close();
                          }
                                catch (IOException  ex){}
                          }
                          }
%>
</body>
</html>