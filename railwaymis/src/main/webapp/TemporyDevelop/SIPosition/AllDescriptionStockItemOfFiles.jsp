<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.content {
	font-family:sans-serif;
	font-size:small;
	
}

.userTable1 {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:20px;
		width : 650px;
		font-size: 14px;
		 
}

.userTable2 {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:20px;
		width : 650px;
		font-size: 14px;
		 
}

.userTable1  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTable2  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTable1 th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.userTable2 th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.odd {
	
		background-color: #fffff0;
		
}

.even {
		background-color: #faf0e6;
}

.textInput {

		border: 1px solid #123B5F;
		color:#123B5F;
}

</style>

</head>
<body>

<br/>
<br/>
<br/>

 			   
<table class="userTable1">
	<tr>
		<th colspan="4" align="center"><s:label key="silp-details-alldescription.title" /></th>
	</tr>
       <s:iterator value="getAllDetailsOfStockItems" status="getAllDetailsOfStockItemsStatus">
            <tr>
                <td colspan="2"><s:label key="silp-details-alldescription.sec" /></td>
                <td colspan="2"><s:property value="section" /></td>
            </tr>
            <tr>
               <td colspan="1"><s:label key="silp-details-alldescription.filenum" /></td>
               <td colspan="3"><font color="red"><b><s:property value="filenum" /></b></font></td>
  			</tr>
  			
  			<tr>
  			    <td colspan="1"><s:label key="silp-details-alldescription.filedate" /></td>
  			
  			
  				<td colspan="1">
  					<s:property value="filedate" />
               </td>
               <td colspan="1"><s:label key="silp-details-alldescription.numtec" /></td>
               <td colspan="1">
                   <font color="blue"><s:set var="tecround" value="tecround" />
                   <s:if test="#tecround == null">0</s:if>
                   <s:else><s:property value="tecround" /></s:else>
                   </font>
               </td>
  			</tr> 
  			<%-- <tr>
  			    <td colspan="1"><s:label key="silp-details-alldescription.tecrec" /></td>
  				<td colspan="3">
  				   <s:set name="tecrec" value="tecrecommend" />
  				   <s:if test="#tecrec == 'Not processed'">Pending for Decision of TEC</s:if>
  				   <s:else><s:property value="tecrecommend" /></s:else>
  				
  				</td>
  			</tr> --%>
               
  </s:iterator> 
  <s:iterator value="getFilesList" status="getFilesListStatus">

  
     <s:set var="files" value="filenum" />
          <s:if test="#getFilesListStatus.count <= 0">
 			
 			    
 			
     </s:if>
     <s:elseif test="#files != 'null' || ''">
     
                 <tr>
 			     
 			     	 <td align="center"><i>Outstanding Files</i></td><td><s:property value="filenum" /></td>
 			     	
 			     </tr>
     
     
     </s:elseif>
     <s:else>
        
     </s:else>
    
</s:iterator>
</table>

<br/>
<br/>

<s:iterator value="getAllDetailsOfStockItems" status="getAllDetailsOfStockItemsStatus" >
    <s:set var="tecrec" value="tecrecommend" />
  				   <s:if test="#tecrec == 'Not processed'">
   <table class="userTable2">
             <tr>
             		<th colspan="4" align="center">TEC Details</th>
             
             </tr>
   
             <tr>
  			    <td colspan="1"><s:label key="silp-details-alldescription.tecrec" /></td>
  				<td colspan="3">
  				  
  				       Pending for Decision of TEC
  				       
  				  	</td>
  			</tr>
   </table>     
 </s:if>
  				   
 <s:elseif test="#tecrec == 'Recommended With Third Party Inspection'">
  		<table class="userTable2">
             <tr>
             		<th colspan="2" align="center">TEC Details</th>
             
             </tr>
             
             <tr>
  			     <td colspan="1"><s:label key="silp-details-alldescription.tecrec" /></td>
  			     <td colspan="1"><s:property value="tecrecommend" /></td>
  			</tr>
  			<tr>
  			     <td colspan="1"><s:label key="silp-details-alldescription.tecrecgivdate" /></td>
  			     <td colspan="1"><s:property value="tecrecgivendate" /></td>
  			</tr>
   
  			<tr>
  			    <th colspan="2"><s:label key="silp-details-alldescription.thirddecision" /></th>
  				
  			</tr>
  			
  			
  			
  			<tr>
  			   <td colspan="2">
  			        <s:set var="thirdpartydecision" value="condthirdpartyinspect" />
  			        <s:if test="#thirdpartydecision == 'Not processed' || 'null'">
  			        
  			        	Third party inspection report not received. Pending for Report.
  			        </s:if>
  			        <s:else>
  			              <s:property value="condthirdpartyinspect" />
  			        </s:else>
  			   </td>
  			</tr>
          </table>   
  				   
   </s:elseif>
  				   
  <s:elseif test="#tecrec == 'Clarification'">
  	<table class="userTable2">
             <tr>
             		<th colspan="2" align="center">TEC Details</th>
             
             </tr>
             
              <tr>
  			     <td colspan="1"><s:label key="silp-details-alldescription.tecrec" /></td>
  			     <td colspan="1"><s:property value="tecrecommend" /></td>
  			</tr>
  			<tr>
  			     <td colspan="1"><s:label key="silp-details-alldescription.tecrecgivdate" /></td>
  			     <td colspan="1"><s:property value="tecrecgivendate" /></td>
  			</tr>
   
  			<tr>
  			    <th colspan="2"><s:label key="silp-details-alldescription.clarification" /></th>
  				
  			</tr>
  			<tr>
  			   <td colspan="2">
  			        <s:set var="clarification" value="clarificbysrs" />
  			        <s:if test="#clarification == 'Not processed' || 'null'">
  			        
  			        	Clarification Report of SRS still not received. Pending for Clarification.
  			        	
  			        </s:if>
  			        <s:else>
  			              <s:property value="clarificbysrs" />
  			        </s:else>
  			   </td>
  			</tr>
          </table>   
  				      
  	</s:elseif>
 <s:elseif test="#tecrec == 'Fresh quatation'">
       
       <table class="userTable2">
             <tr>
             		<th colspan="2" align="center">TEC Details</th>
             
             </tr>
             
              <tr>
  			     <td colspan="1"><s:label key="silp-details-alldescription.tecrec" /></td>
  			     <td colspan="1"><s:property value="tecrecommend" /></td>
  			</tr>
  			<tr>
  			     <td colspan="1"><s:label key="silp-details-alldescription.tecrecgivdate" /></td>
  			     <td colspan="1"><s:property value="tecrecgivendate" /></td>
  			</tr>
   
  			<tr>
  			    <th colspan="2"><s:label key="silp-details-alldescription.freshquotation" /></th>
  				
  			</tr>
  			<tr>
  			   <td colspan="2">
  			        <s:set var="freshquo" value="remarks" />
  			        <s:if test="#freshquo == 'Not processed' || 'null'">
  			        
  			        	Reasons not privided.
  			        	
  			        </s:if>
  			        <s:else>
  			              <s:property value="remarks" />
  			        </s:else>
  			   </td>
  			</tr>
          </table>   
  				      
 			
 
 </s:elseif>
  				   
     <s:else>
  				   
  				   <table class="userTable2">
             <tr>
             		<th colspan="4" align="center">TEC Details</th>
             
             </tr>
             
             <tr>
  			    <td colspan="1"><s:label key="silp-details-alldescription.tecrec" /></td>
  				<td colspan="3">
  				  
  				       <s:property value="tecrecommend" />
  				       
  				  	</td>
  			</tr>
            <tr>
  			     <td colspan="1"><s:label key="silp-details-alldescription.tecrecgivdate" /></td>
  			     <td colspan="1"><s:property value="tecrecgivendate" /></td>
  			</tr>
  				   		
  				   		
  			</table>  		
  				   		
  	 </s:else>
  				
  			

</s:iterator>

<br/>
<br/>
<s:iterator value="getAllDetailsOfStockItems" status="getAllDetailsOfStockItemsStatus" >
           <s:set var="bidselect" value="bidselect" />
           <s:set var="sucbidder" value="namesuccbider" />
           
           <s:if test="#sucbidder == 'Selected the most economical supplier by SRS'">
           
                 <table class="userTable2">
                      <tr>
                      		<th>Name of Successful Bidder</th>
                      
                      </tr>
                      <tr>
                      		<td><font color="#14820a"><i>Bidder is selected by the SRS in the most economical way</i></font></td>
                      
                      </tr>
                 
                 </table>
           
           </s:if>
           <s:elseif test="#sucbidder != 'null' ||  ''">
                   <table class="userTable2">
                      <tr>
                      		<th colspan="2">Name of Successful Bidder</th>
                      
                      </tr>
                      <tr>
                      		<td colspan="1"><s:label key="silp-details-alldescription.biddername" /></td>
                      		<td colspan="1"><font color="#14820a"><i><s:property value="namesuccbider" /></i></font></td>
                      
                      </tr>
                 
                 </table>
           
           </s:elseif>
           
           <s:else>
           
           </s:else>

</s:iterator>


<br/>
<br/>
<s:iterator value="getAllDetailsOfStockItems" status="getAllDetailsOfStockItemsStatus" >
		  <s:set var="ordquantity" value="orderedquantity" />
		  <s:set var="quan" value="quantity" />
          <s:if test="#ordquantity != #quan">
          
              <table class="userTable2">
             	<tr>
             		<th colspan="4">Descriptions of Quantity</th>
             	</tr>
                <tr>
                    <td colspan="1"><s:label key="silp-details-alldescription.quantity" /></td>
                	<td colspan="1"><s:property value="quantity" /><s:property value="units" /></td>
                    <td colspan="1"><s:label key="silp-details-alldescription.ordquan" /></td>
                	<td colspan="1"><s:property value="orderedquantity" /><s:property value="orderedunit" /></td>
                	
                </tr>
                <tr>
                   <td colspan="1">
                          <font color="blue"><s:label key="silp-details-alldescription.uprice"  /></font>
                   </td>
                   <td colspan="1"><s:property value="unitprice" />&nbsp;<s:property value="currency" /></td>
                   <td colspan="1">
                          <font color="brown"><s:label key="silp-details-alldescription.totalvalu" /></font>
                   </td>
                   <td colspan="1"><s:property value="totalvalue" />&nbsp;<s:property value="currency" /></td>
                </tr>
                
             </table>
          
          </s:if> 
          <s:elseif test="#ordquantity != '0.0'">
               <table class="userTable2">
             	<tr>
             		<th colspan="4">Descriptions of Quantity</th>
             	</tr>
                <tr>
                    <td colspan="2"><s:label key="silp-details-alldescription.ordquan" /></td>
                	<td colspan="2"><s:property value="orderedquantity" /><s:property value="orderedunit" /></td>
                </tr>
                <tr>
                   <td colspan="1">
                          <font color="blue"><s:label key="silp-details-alldescription.uprice"  /></font>
                   </td>
                   <td colspan="1"><s:property value="unitprice" />&nbsp;<s:property value="currency" /></td>
                   <td colspan="1">
                          <font color="brown"><s:label key="silp-details-alldescription.totalvalu" /></font>
                   </td>
                   <td colspan="1"><s:property value="totalvalue" />&nbsp;<s:property value="currency" /></td>
                </tr>
             </table>
          
          </s:elseif>
          <s:else>
          </s:else>

</s:iterator>
<br/>
<br/>

<s:iterator value="getAllDetailsOfStockItems" status="getAllDetailsOfStockItemsStatus" >
            <s:set var="pcmapp" value="pcmapp" />
            <s:if test="#pcmapp != 'Not processed'">
                <s:set var="guidelines" value="pcmguidelines" />
                	<s:if test="#guidelines != 'Not processed'">
                	
                		<table class="userTable2">
            						<tr>
            							<th colspan="2">PCM Details</th>
            						</tr>
            						<tr>
            						
            							<td colspan="1"><s:label key="silp-details-alldescription.pcmcond" /></td>
            							<td colspan="1"><s:property value="pcmapp" /></td>
            						</tr>
            						<tr>
            						
            							<td colspan="1"><s:label key="silp-details-alldescription.guideline" /></td>
            							<td colspan="1"><s:property value="pcmguidelines" /></td>
            						</tr>
            	   
            			</table>
                	
                	</s:if>
            	    <s:else>
            	    			<table class="userTable2">
            						<tr>
            							<th colspan="2">PCM Details</th>
            						</tr>
            						<tr>
            						
            							<td colspan="1"><s:label key="silp-details-alldescription.pcmcond" /></td>
            							<td colspan="1"><s:property value="pcmapp" /></td>
            						</tr>
            						<tr>
            						
            							<td colspan="1"><s:label key="silp-details-alldescription.guideline" /></td>
            							<td colspan="1">Guide Lines have not been provided</td>
            						</tr>
            	   
            			</table>
                	
            	    
            	    </s:else>
            
            </s:if>
            <s:else>
            
           </s:else>
</s:iterator>

<br/>
<br/>

<s:iterator value="getAllDetailsOfStockItems" status="getAllDetailsOfStockItemsStatus">

              <s:set var="suitability" value="suitability" />
              <s:if test="#suitability == 'Suitable'">
                  <table class="userTable2">
                     <tr>
                     	<th colspan="2">Suitability Details</th>
                     </tr>
                     <tr>
                      <td colspan="1"><s:label key="silp-details-alldescription.suitability" /></td>
              		  <td colspan="1"><s:property value="suitability" /></td>
              			
              		</tr>
                  </table>
              </s:if>
              <s:elseif test="#suitability == 'Not Suitable'">
              
                   <s:set var="reason" value="reason" />
                   <s:if test="#reason != 'Not processed'">
                     <table class="userTable2">
                     <tr>
                     	<th colspan="4">Suitability Details</th>
                     </tr>
                     <tr>
                      <td colspan="1"><s:label key="silp-details-alldescription.suitability" /></td>
              		  <td colspan="1"><s:property value="suitability" /></td>
              		  <td colspan="1"><s:label key="silp-details-alldescription.suitabilityreason" /></td>
              		  <td colspan="1"><s:property value="reason" /></td>
              			
              		</tr>
                  </table>
              
                  </s:if>
                  <s:else>
                       <table class="userTable2">
                     <tr>
                     	<th colspan="4">Suitability Details</th>
                     </tr>
                     <tr>
                      <td colspan="1"><s:label key="silp-details-alldescription.suitability" /></td>
              		  <td colspan="1"><s:property value="suitability" /></td>
              		  <td colspan="1"><s:label key="silp-details-alldescription.suitabilityreason" /></td>
              		  <td colspan="1">Reasons have not been provided</td>
              			
              		</tr>
                  </table>
                  
                  </s:else>
                  
              </s:elseif>
              <s:elseif test="#suitability == 'Short Fall'">
                      <table class="userTable2">
                     <tr>
                     	<th colspan="4">Suitability Details</th>
                     </tr>
                     <tr>
                      <td colspan="1"><s:label key="silp-details-alldescription.suitability" /></td>
              		  <td colspan="1"><s:property value="suitability" /></td>
              		  <td colspan="1"><s:label key="silp-details-alldescription.suitabilityshort" /></td>
              		  <td colspan="1"><s:property value="shortfall" /></td>
              			
              		</tr>
                  </table>
                  
                  </s:elseif>
                  
                  <s:elseif test="#suitability == 'Damaged'">
                      <table class="userTable2">
                     <tr>
                     	<th colspan="4">Suitability Details</th>
                     </tr>
                     <tr>
                      <td colspan="1"><s:label key="silp-details-alldescription.suitability" /></td>
              		  <td colspan="1"><s:property value="suitability" /></td>
              		  <td colspan="1"><s:label key="silp-details-alldescription.suitabilitydamage" /></td>
              		  <td colspan="1"><s:property value="damaged" /></td>
              			
              		</tr>
                  </table>
                  
                  </s:elseif>
                  <s:else>
                  
                  </s:else>
			
</s:iterator>

<br/>
<br/>
<br/>

   <%-- <s:iterator value="getAllDetailsOfStockItems" status="getAllDetailsOfStockItemsStatus">
  			   <s:property value="storenumber" />
               <s:property value="filenum" />
  </s:iterator>  --%>
</body>
</html>