<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>

.tableoiltankfrm {
    
        font:12px verdana, arial, helvetica, sans-serif;
        border-width: 2px solid;
        border-color:#067bb6;
        border-style:solid;
        color:#0d157f;
        background-color: #88d5fc;
        width:90%;
        margin-left:5%;
        margin-top:5%;
}

.tableoiltankfrm  tr td {
    
        background-color:#e2f3fc;
        margin: 5px;
        padding: 5px;
}

.tdLabel {
    
        font-weight: bold;
        align: top;
        
}

.label {} 

 .errorMessage {
    
        color:red;
        font-size: 12px;
}

.textInput {

        border: 1px solid #3998c7;
        color:#0d157f;
}

.messtable{
            margin-top:150px;
            margin-left:400px;

}



</style>


</head>
<body>

<%


										request.getSession();
										Object  isLogedIn  =  session.getAttribute("isLogedIn");
											if(isLogedIn == null){

       												   response.sendRedirect("../../Message.jsp");
												}

  										session.setMaxInactiveInterval(1000);





						String user = (String)session.getAttribute("str");
						String role = (String)session.getAttribute("strRole");
						String subDept = (String)session.getAttribute("subDept");




%>




 <div class="gensheet">
    <form name="gensheetfrm" method="post" action="ItemSpecificationsReport.jsp">
        <table>
        <caption>Generate New Report<br><br></caption>
        
           <tr>
           	<td>
           	 Enter Item Code:
           	 <br><br>
           	</td>
           	<td>
           	<input type="text" name="itemtype" />
           	<input type="hidden" name="user" value="<%= user %>" />
           	<input type="hidden" name="subdept" value="<%= subDept %>" />
           	<br><br>
           	</td>
           </tr> 
           <tr>
           		<td>Enter SI/LP Number</td>
           		<td><input type="text" name="silpnum" /></td>
           </tr>
            <tr>
            	<td>Select Report Type:<br><br></td>
            	<td>
            	<select name="reptype">
            	    <option value="">Please Select</option>
            		<option value="SI">SI</option>
            		<option value="LP">LP</option>
            	</select>
            	<br>
            	<br>
            	</td>
            	
            	
            </tr>
            <tr>
            	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            	<td><input type="submit" value="Generate" /><input type="reset" /></td>
            </tr>
        </table>
        
    </form>
    </div>
    
</body>
</html>