<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>design_page</title>
		<meta name="description" content="">
		<meta name="author" content="kasthoori">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="stylesheet" type="text/css" href="css_files/design_page.css" />
		<script type="text/javascript" src="../../JqueryJqplot_plugin/jquery.min.js"></script>
		<script type="text/javascript" src="../../JqueryJqplot_plugin/jquery-ui-1.10.4.custom.js"></script>
		<link type="text/css" rel="stylesheet" href="../../JqueryJqplot_plugin/jquery-ui-1.10.4.custom.css" />
		<link type="text/css" href="../../JqueryJqplot_plugin/jquery.jqplot.min.css" />
		
<script>		
$(function() {
		$("#datepicker").datepicker({dateFormat: 'yy-mm-dd'});
});
</script>


<script type="text/javascript">

function get_item_list(){
	
	 var daterec = document.genrepfrm.datepicker.value;
	
	 $.post("siLpSheetGeneratedList",{date : "" + daterec + ""},

             function(data){
		 
                 $('#imagedisplay').html(data);

     });
	 
	
}


</script>
		
	</head>

<body>


<%


										request.getSession();
										Object  isLogedIn  =  session.getAttribute("isLogedIn");
											if(isLogedIn == null){

       												   response.sendRedirect("../../Message.jsp");
												}

  										session.setMaxInactiveInterval(1000);





						String user = (String)session.getAttribute("str");
						String role = (String)session.getAttribute("strRole");
						String subDept = (String)session.getAttribute("subDept");




%>
		
    <div class="container_1">
    	<div class="logo">
    		<img src="images/Ceylon_Government_Railways_logo.gif" width="60px" height="60px" />
    	</div>
      <div class="heading_1">
            <h2>Sri Lanka Railway</h2>
        </div>
    </div>
    
    <div class="gensheet">
    <form name="gensheetfrm" method="post" action="ItemSpecificationsReport.jsp">
        <table>
        <caption>Generate New Report<br><br></caption>
        
           <tr>
           	<td>
           	 Enter Item Code:
           	 <br><br>
           	</td>
           	<td>
           	<input type="text" name="itemtype" />
           	<input type="hidden" name="user" value="<%= user %>" />
           	<input type="hidden" name="subdept" value="<%= subDept %>" />
           	<br><br>
           	</td>
           </tr> 
            <tr>
            	<td>Select Report Type:<br><br></td>
            	<td>
            	<select name="reptype">
            	    <option value="">Please Select</option>
            		<option value="SI">SI</option>
            		<option value="LP">LP</option>
            	</select>
            	<br>
            	<br>
            	</td>
            	
            	
            </tr>
            <tr>
            	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            	<td><input type="submit" value="Generate" /><input type="reset" /></td>
            </tr>
        </table>
        
    </form>
    </div>
    
    
  <div class="genrep">
    <form name="genrepfrm" method="post" action="">
        <table>
        <caption>Print Past History<br><br></caption>
           <tr>
            <td align="center">
            	<img src="images/search-icon.png" width="80px" height="80px" />
            </td>
           </tr>
           <tr>
           	<td>
           	   Date:
           	 <br><br>
           	</td>
           	<td>
           	   <input type="text" name="datepicker" id="datepicker" onChange='get_item_list();' />
           	
           	<br><br>
           	</td>
           </tr> 
            
            <tr>
            	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            	<td><input type="reset" /></td>
            </tr>
        </table>
        
    </form>
    </div>
    
    <div class="reptable" id="imagedisplay"></div>

    
    <div class="container_3">
    
    	
           <footer>
            <div class="copyrigth">
				<p>
					&copy; R &amp; D SLRGTTC
				</p>
		     </div>
			</footer>
			
   </div>
    
    
   



	</body>
</html>