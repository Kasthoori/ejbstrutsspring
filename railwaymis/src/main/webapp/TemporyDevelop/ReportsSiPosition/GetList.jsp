<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<style type="text/css">

.userTable {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:20px;
		width : 250px;
		font-size: 12px;
		height:165px;
		overflow-y:scroll; 
		display:block; 
	
}

.userTable  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTable th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.odd {
	
		background-color: #fffff0;
		
}

.even {
		background-color: #faf0e6;
}

.textInput {

		border: 1px solid #123B5F;
		color:#123B5F;
}

</style>
</head>
<body>

 <s:if test="getItems.size() > 0">
    
      <table class="userTable" cellpadding="5px">			
		 <tr>
          <th colspan="2" align="center">LP And SI Sheets Numbers</th>
         </tr>
          <s:iterator value="getItems" status="getItemsStatus">
             <tr class="<s:if test="#getItemsStatus.odd == true">odd</s:if><s:else>even</s:else>">
             <td>
                 <s:property value="repsheetnum" />
                 <s:set var="date" value="timestamp" />
                 <s:set var="repsheetnum" value="repsheetnum" />
             </td>
             <td align="center">
             
                <s:url id="getdetails" action="getPastListItems">
				      <s:param name="repsheet" value="%{repsheetnum}" />
				</s:url>
					<s:a href="%{getdetails}"><img src="images/print_icon.jpeg" width="20px" height="20px" />
					</s:a>  
					
             </td>
             </tr>
          </s:iterator>
      
      </table>
      
</s:if>
</body>
</html>