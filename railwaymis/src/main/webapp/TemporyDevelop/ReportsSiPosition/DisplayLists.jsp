<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page import="pk1.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="http://www.rlmsproject.com/RLMSProject/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="../../RLMS.css" type="text/css">
<script type="text/javascript" src="http://www.rlmsproject.com/RLMSProject/TemporyDevelop/ReportsSiPosition/js/print_copies_lp_si_sheets.js"></script> 

<style type="text/css">

.content {
	font-family:sans-serif;
	font-size:medium;
	
}

.userTable {
		border-width:1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		margin-left:20px;
		width : 40%;
		font-size: 12px;
		
}

.userTable  td {
	
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
}

.userTable th {
		border-width: 1px 1px 1px 1px;
		border-spacing: 2px;
		border-style: outset outset outset outset;
		border-color: black black black black;
		border-collapse: collapse;
		background-color: rgb(255, 255, 255);
}

.odd {
	
		background-color: #fffff0;
		
}

.even {
		background-color: #faf0e6;
}

.textInput {

		border: 1px solid #123B5F;
		color:#123B5F;
}



   @media print
    {
    #non-printable { display: none; }
    #printable {
    display: block;
    width: 100%;
    height: 100%;
    }
    }
   
  @media print  { #destab {display: none;}}
  @media print  { #tmpDelAlbum  { display: none; } }
  @media print  { #tmpPrintAlbum  { display: none; } }
  @media print  { #tmpAddAlbum  { display: none; } }
  @media print  { #tmpAlbum  { display: none; } }
  @media print  { #sameitem { display: none; } }
  @media print  { #storenum { display: none; } }
  @media print  { #imagedisplay { display: none; } }
  @media print  {#tm<%@ page import="pk1.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>pSpecAlbum {display:none;}}
 

.des {
    font-size:14px;
    width: 1300px;
    background: lightblue;
    border-collapse: collapse;
    border: 1px solid darkblue;
}
.des th {
    background: blue;
    color: lightblue;
    border: 1px solid darkblue;
    height:10px;
}

.des td {
    border: 1px solid darkblue;

} 

.submit {
 color: #000;
 font-size: 14px;
 width: 100px;
 height: 20px;
 border: 1px;
 margin: 1px;
 padding: 1px;
 background:#BEC0C0;
 
}

.delete {
 color: #000;
 font-size: 14px;
 font-weight: bold;.
 width: 60px;
 height: 15px;
 border: 1px;
 margin: 1px;
 padding: 1px;
 background:lightblue;
 
 
 
}

@media print {
    .watermark {
      display: block;
    }
    
.prepare {

	position:relative;
	margin-top:10px;
	margin-left:50px;
}

.check {

	position:absolute;
	margin-top:10px;
	margin-left:150px
}	

</style>

</head>
<body>




 
<%


					    
                        

								request.getSession();
								Object  isLogedIn  =  session.getAttribute("isLogedIn");
								if(isLogedIn == null){

          									response.sendRedirect("../../Message.jsp");
									}

 								 session.setMaxInactiveInterval(1000);






						String user = (String)session.getAttribute("str");
					    String role = (String)session.getAttribute("strRole");
                        String subDept = (String)session.getAttribute("subDept");
                        
                        
                        
                        TimeAndDate  td = new TimeAndDate();
                        
                        String time = td.Time();
                        String date = td.Date();
                        
                        Calendar now = Calendar.getInstance();
                        int year = now.get(Calendar.YEAR);
                        String yearString = String.valueOf(year);
                        
                       
%>
<div id="printable" >
 <h2 align="center">Sri-Lanka Railway</h2>
 <h3 align="center">Order Specification</h3>
 
 <div id="destab">
 <table class="userTable">
    
			<s:iterator value="getLists">
      					  <tr>
  								<th colspan="1">Report Number:</th>
  									<td colspan="2"> <s:property value="repsheetnum" /></td>
  						  </tr>
			</s:iterator>
					<s:if test="getLists.size() > 0">
					
						<s:iterator value="getLists" status="getListsStatus">
								<s:iterator var="item" value="items.split(',')">
 									<tr class="<s:if test="#getLists.odd == true">odd</s:if><s:else>even</s:else>">	
        								<th>Stock Item</th>
        	                                   
                                               
        										<td colspan="1"><font style="color:red;">
        										        
        	   											 <s:property value="item" />
        	   									 </font></td>
        										</tr> 
        										
        						</s:iterator>
				</s:iterator>
				
		</s:if>
</table>
</div>
 <br />
 <br />
<div id="repNum"> 
 <s:iterator value="getLists" begin="0" end="0">
    <p>Order Number:&nbsp;<s:property value="repsheetnum" /></p>
 </s:iterator>
</div>
<br />

     <table class="des">
     <thead>
      
      </thead>
      <tbody id="spbody">
      	
      </tbody>
     </table>
  <br />  
     <table class="des">
     
       <thead>
       <tr>
       <th rowspan="2">List Num.
       <th rowspan="2">Stock Number
       <th rowspan="2">Part Number
       <th rowspan="2">Descriptions
       <th rowspan="2">Reference
       <th rowspan="2">Order Quantity.
       <th id="tmpDelAlbum" rowspan="2">Remove
       
      
       </thead>
       <tbody id="tbody">
       </tbody>
     </table>   
     <br />
     <div id="imagedisplay"></div>
     
     
<%-- <table class="userTable">
     
     <s:form name="detfrm" method="post" action="">
			<s:iterator value="getLists">
      					  <tr>
  								<th colspan="1">Report Number:</th>
  									<td colspan="2"> <s:property value="repsheetnum" /></td>
  						  </tr>
			</s:iterator>
					<s:if test="getLists.size() > 0">
					<tr>
					 <td colspan="3"><s:checkbox cssClass="textInput" name="same" id="sameitem">Same</s:checkbox></td> 
					 </tr>
						<s:iterator value="getLists" status="getListsStatus">
								<s:iterator var="item" value="items.split(',')">
 									<tr class="<s:if test="#getLists.odd == true">odd</s:if><s:else>even</s:else>">	
        								<th>Stock Item</th>
        	                                   
                                               
        										<td colspan="1"><font style="color:red;">
        										         <s:set name="item" value="item" />
        										         <s:textfield name="storenum" id="storenum" value="%{item}"/>
        	   											 <s:property value="item" />
        	   									 </font></td>
        	                                     <td colspan="1">
        	                                     	<s:submit cssClass="textInput" value='Add To Report' id='tmpAddAlbum' class="submit" />
        	                         
        	                                     </td>
        	
        										</tr> 
        										
        						</s:iterator>
				</s:iterator>
				<tr>
				   <td colspan="2">
					  <s:submit cssClass="textInput" id='tmpPrintAlbum' onclick="JavaScript:window.print();" value="print" class="submit"  />
				  </td>
				</tr>
		</s:if>
  </s:form>
</table>
      --%>
     
     
     
  <form name="detfrm" method="post" action="">
     <label id="tmpAlbum" for="same"><font color="red">Check To Add Specifications For Same Category:</font></label> 
     <input type="checkbox" name="same" id="sameitem" /> 
     <input type="text" name="storenum" id="storenum"/>
     <input type='submit' value='Add To Report' id='tmpAddAlbum' class="submit" />
     <input type="button" id='tmpPrintAlbum' onclick="JavaScript:window.print();" value="print" class="submit" /> 
      <a href="ReportSelection.jsp" target="frm3"><b>Report Selection</b></a> 
     <div id="imagedisplay"></div>
     </form>  
    <!-- <form action="" name="printfrm" method="get">
     	<input type="submit" id='tmpPrintAlbum' onclick="JavaScript:window.print();" value="print" class="submit" />
     </form>  -->
     <br>
     <table>
     <tr>
     <td colspan="4" align="left">
           Prepared by:<%= user %>&nbsp;<%= role %>&nbsp;<%= subDept %>&nbsp;TimeStamp:-<%= date %>&#124;<%=time %><br></td>
	 <td colspan="2" align="right">Entered By(S.K):...........................<br></td>
	 </tr>
	 <tr>
	 	<td colspan="3"><b>Sec.Eng:...............................&nbsp;&nbsp;</b><br><br></td>
	 	<td colspan="3" align="right"><br>Checked by:.........................<br><br><br></td>
	 </tr>
	 <tr>
	 <td colspan="1"><b>ASRS(R):....................................</b><br></td>
	 <td colspan="2"><b>CME:........................................</b><br></td>
	 <td colspan="1"><b>ASRS(I):....................................</b><br></td>
	 <td colspan="1"><b>DSRS(S&amp;A):..............................</b><br></td>
	 <td colspan="1"><b>SRS:........................................</b><br></td>
	 
	 </tr>
	 <tr>
	  <td colspan="6" align="justify"><font size="1px">&copy;&nbsp;Research &amp;Development Unit - SLRGTTC</font></td>
	 </tr>
    </table>
</div>


</body>
</html>