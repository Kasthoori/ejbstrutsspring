<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="pk1.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="javascript" type="text/javascript" src="http://localhost:8082/RLMSProject/JqueryJqplot_plugin/jquery-1.10.2.js"></script>
<script language="javascript" type="text/javascript" src="http://localhost:8082/RLMSProject/JqueryJqplot_plugin/jquery.min.js"></script>
<link rel="stylesheet" href="../../RLMS.css" type="text/css">
<title>Insert title here</title>
<script type="text/javascript" src="js/print_js.js"></script> 

<style type="text/css" media="print">
    @media print
    {
    #non-printable { display: none; }
    #printable {
    display: block;
    width: 100%;
    height: 100%;
    }
    }
    
  @media print  { #tmpDelAlbum  { display: none; } }
  @media print  { #tmpPrintAlbum  { display: none; } }
  @media print  { #tmpAddAlbum  { display: none; } }
  @media print  { #tmpAlbum  { display: none; } }
  @media print  { #sameitem { display: none; } }
  @media print  { #storenum { display: none; } }
  @media print  { #imagedisplay { display: none; } }
  @media print  {#tmpSpecAlbum {display:none;}}
 
                  
  
</style>
<style type='text/css'>

.des {
    font-size:14px;
    width: 1300px;
    background: lightblue;
    border-collapse: collapse;
    border: 1px solid darkblue;
}
.des th {
    background: blue;
    color: lightblue;
    border: 1px solid darkblue;
    height:10px;
}

.des td {
    border: 1px solid darkblue;

} 

.submit {
 color: #000;
 font-size: 14px;
 width: 100px;
 height: 20px;
 border: 1px;
 margin: 1px;
 padding: 1px;
 background:#BEC0C0;
 
}

.delete {
 color: #000;
 font-size: 14px;
 font-weight: bold;.
 width: 60px;
 height: 15px;
 border: 1px;
 margin: 1px;
 padding: 1px;
 background:lightblue;
 
 
 
}

@media print {
    .watermark {
      display: block;
    }
    
.prepare {

	position:relative;
	margin-top:10px;
	margin-left:50px;
}

.check {

	position:absolute;
	margin-top:10px;
	margin-left:150px
}	

</style>
</head>
<body>

<%


					    
                        

								request.getSession();
								Object  isLogedIn  =  session.getAttribute("isLogedIn");
								if(isLogedIn == null){

          									response.sendRedirect("../../Message.jsp");
									}

 								 session.setMaxInactiveInterval(1000);






						String user = (String)session.getAttribute("str");
					    String role = (String)session.getAttribute("strRole");
                        String subDept = (String)session.getAttribute("subDept");
                        
                        
                        TimeAndDate  td = new TimeAndDate();
                        
                        String time = td.Time();
                        String date = td.Date();
                        
                        Calendar now = Calendar.getInstance();
                        int year = now.get(Calendar.YEAR);
                        String yearString = String.valueOf(year);
                        
                        String itemtype = request.getParameter("itemtype");
                        String reptype = request.getParameter("reptype");
                        String userrec = request.getParameter("user");
                        String subdept = request.getParameter("subdept");
                        
                        int seqNum = 0;
                        
                        String sheetnum = "";
                        
                        DbBean  db = new DbBean();

                        db.connect();
                        
                        ResultSet rs = null;
                      
                        
                        rs = db.execSql("SELECT seq_id, year FROM si_lp_sheet_generated WHERE subdept='" + subdept + "' order by time_stamp desc limit 1");
                        
                        if(rs.next() == false){
                        	
                        	seqNum = 1;
                        	
                        	
                        }else{
                        	
                        	
                           rs = db.execSql("SELECT seq_id, year FROM si_lp_sheet_generated WHERE subdept='" + subdept + "' order by time_stamp desc limit 1");
                        
                           while(rs.next()){
                        	   
                        	   
                        	
                               int seqId = rs.getInt("seq_id");
                               String entYear = rs.getString("year");
                               
                              
                                     
                               if(!entYear.matches(yearString)){
                            	   
                            	     seqNum = 1;
                            	     
                               }else{
                            	   
                            	   seqNum = seqId + 1;
                            	   
                            	
                               }               
                        
                        
                        } 
                        
}
                        
                        sheetnum = subdept + "/" + itemtype + "/" + reptype + "/" +  yearString + "/" + seqNum;   
                        
                      
                        

%>
<div id="printable" >
 <h3 align="center"><%= reptype %> ** <%= subDept %> &nbsp; Sub Department ** Sri-Lanka Railway</h3>
     <table class="des">
     <thead>
      
      </thead>
      <tbody id="spbody">
      	
      </tbody>
     </table>
     <p>Sheet Number:&nbsp;<%= sheetnum %></p>
     <table class="des">
     
       <thead>
       <tr>
       <th rowspan="2">List Num.
       <th rowspan="2">Stock Number
       <th rowspan="2">Part Number
       <th rowspan="2">Descriptions
       <!-- <th rowspan="2">Specifications -->
       <th rowspan="2">ReferencestmpAddAlbum
       <th rowspan="2">Max/Min
       <th rowspan="2">Present Stock
       <th colspan="5">Consumption for last 5 years
       <th rowspan="2" width="100px">O/S Indents And Qty.
       <th rowspan="2">Order Quantity.
       <th id="tmpDelAlbum" rowspan="2">Remove
       <tr><th>Year 2.....<th>Year 2.....<th>Year 2.....<th>Year 2.....<th>Year 2.....
      
       </thead>
       <tbody id="tbody">
       </tbody>
     </table>
     
    <%-- <form action="EnterDatabase.jsp" method="post" name="detfrm">
     
     <label id="tmpAlbum" for="same"><font color="red">Check To Add Specifications For Same Category:</font></label> 
     <input type="checkbox" name="same" id="sameitem" /> 
     <input type="text" name="storenum" id="storenum"/>
     <input type="hidden" name="sheetid" id="sheetid" value="<%= sheetnum %>" />
     <input type="hidden" name="seqid" id="seqid" value="<%= seqNum %>" />
     <input type="hidden" name="user" id="user" value="<%= userrec %>" />
     <input type="hidden" name="subdept" id="subdept" value="<%= subdept %>" />
     <input type="hidden" name="year" id="year" value="<%= yearString %>" />
      <input type='submit' value='Add To Report' onmouseover='gen_store_num();' class="submit" />
     
     </form>  --%>
     <div id="imagedisplay"></div>
     <form name="detfrm" method="post" action="EnterDatabase.jsp">
     <label id="tmpAlbum" for="same"><font color="red">Check To Add Specifications For Same Category:</font></label> 
     <input type="checkbox" name="same" id="sameitem" /> 
     <input type="text" name="storenum" id="storenum"/>
     <input type="hidden" name="sheetid" id="sheetid" value="<%= sheetnum %>" />
     <input type="hidden" name="seqid" id="seqid" value="<%= seqNum %>" />
     <input type="hidden" name="user" id="user" value="<%= userrec %>" />
     <input type="hidden" name="subdept" id="subdept" value="<%= subdept %>" />
     <input type="hidden" name="year" id="year" value="<%= yearString %>" />
      <input type="hidden" name="itemtype" id="itemtype" value="<%= itemtype %>" />
     <input type='submit' value='Add To Report' id='tmpAddAlbum' onmouseover='gen_store_num();' class="submit" />
    <!-- <input type="submit" id='tmpPrintAlbum' onclick="JavaScript:window.print();" value="print" class="submit" /> --> 

     <input type="button" id='tmpPrintAlbum' onclick="JavaScript:window.print();" value="print" class="submit" />
     <a href="ReportSelection.jsp" target="frm3"><b>Report Selection</b></a>


     <div id="imagedisplay"></div>
     </form> 
    <!--  <form action="" name="printfrm" method="get">
     	<!--<input type="submit" id='tmpPrintAlbum' onclick="JavaScript:window.print();" value="print" class="submit" />-->        

     </form> -->
     <br>
     <table border="0">
     <tr>
     <td colspan="4" align="left">
     Prepared by:<%= user %>&nbsp;<%= role %>&nbsp;<%= subDept %>&nbsp;TimeStamp:-<%= date %><%=time %><br></td>
	 <td colspan="2" align="right">Entered By(S.K):...........................<br></td>
	 </tr>
	 <tr>
	 	<td colspan="3"><b>Sec.Eng:...............................&nbsp;&nbsp;</b><br><br></td>
	 	<td colspan="3" align="right"><br>Checked by:.........................<br><br><br></td>
	 </tr>
	 <tr>
	 <td colspan="1"><b>ASRS(R):....................................</b><br></td>
	 <td colspan="2"><b>CME:........................................</b><br></td>
	 <td colspan="1"><b>ASRS(I):....................................</b><br></td>
	 <td colspan="1"><b>DSRS(S&amp;A):..............................</b><br></td>
	 <td colspan="1"><b>SRS:........................................</b><br></td>
	 
	 </tr>
	 <tr>
	  <td colspan="6" align="justify"><font size="1px">&copy;&nbsp;Research &amp; Development Unit - SLRGTTC</font></td>
	 </tr>
    </table>
</div>

  
	 
</body>
</html>
