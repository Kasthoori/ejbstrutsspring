<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="db" scope="request" class="pk1.DbBean" />
<jsp:setProperty property="*" name="db"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
.userTable {
        border-width:1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        margin-left:20px;
        width : 800px;
        
    
}

.userTable  td {
    
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
}

.userTable th {
        border-width: 1px 1px 1px 1px;
        border-spacing: 2px;
        border-style: outset outset outset outset;
        border-color: black black black black;
        border-collapse: collapse;
        background-color: rgb(255, 255, 255);
}

.setTable{

       position:float;
       margin-top:100px;
       margin-left:50px;

}

.submit{

	border:0;
	width:190px;

	

}

</style>

</head>
<body>
<%


            String storeNum = "";
            String fileNum = "";
            String siLpoNum = "";
            String siLpDate = "";
            String fileDate = "";
            String sect = "DC";
            String subDept = "";

			db.connect();

		    ResultSet  rsdc = db.execSql("SELECT store_num, file_num,si_lpo_num,si_lp_subdept,lp_si_date,file_date FROM details_lp_si_table WHERE section='DC' AND acknow_cme='F'");
		    
		   %>
		    <div class="setTable">
		    <table class="userTable">
		        <tr>
		        	<th colspan="6">
		        		Details of Registered Files By SRS - DC
		        	</th>
		        </tr>
		    	<tr>
		    		<th>Store Number</th>
		    		<th>File Number</th>
		    		<th>SI/LPO Number</th>
		    		<th>LP/SI Date</th>
		    		<th>File Date</th>
		    		<th>Dept. Entered By</th>
		    		<th>Acknowledge</th>
		    		
		    	</tr>
           <%  
           
           
           		while(rsdc.next()){
           			
           			
           			storeNum = rsdc.getString("store_num");
           			fileNum = rsdc.getString("file_num");
           			siLpoNum = rsdc.getString("si_lpo_num");
           			subDept = rsdc.getString("si_lp_subdept");
           			siLpDate = rsdc.getString("lp_si_date");
           			fileDate = rsdc.getString("file_date");
           			
           			
           			%>
           			
           			<tr>
           			
           				<td><%= storeNum %></td>
           				<td><%= fileNum %></td>
           				<td><%= siLpoNum %></td>
           				<td><%= siLpDate %></td>
           				<td><%= fileDate %></td>
           				<td><%= subDept %></td>
           				<td>
           				
           					<form method="post" action="AcknowledgeLpSiFromSRS.jsp">
           					    
           						<input type="hidden" name="storenum" value="<%= storeNum %>" />
           						<input type="hidden" name="filenum" value="<%= fileNum %>" />
           						<input type="hidden" name="section" value="<%= sect %>" />
           						<input type="submit" class="submit" value="Acknowledge" />
           					</form>
           				</td>
           				
           		    </tr>
           			
           			<% 
           			
           		}

%>
           </table>
           </div>

</body>
</html>