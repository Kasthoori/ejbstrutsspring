<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="db" scope="request" class="pk1.DbBean" />
<jsp:setProperty name="db" property="*" />
<html>
<head>
<title>User and Group Page</title>
<link rel="stylesheet" href="RLMS.css" type="text/css">
<script src="trimFunctions.js" language="JavaScript"
	type="text/javascript"></script>


<script type=text/javascript>
<!--
function myValidate(useraddform) {
	var v1 = document.useraddform.UserName.value;
	var x = trimAll(v1);
	var v2 = x.length;

	if (v2==0) {
		alert("user name cannot be empty");
		return false;
	} else{
		var v3 = document.useraddform.Pwd.value;
		var v4 = v3.length;
		if (v4==0) {
			alert("password cannot be empty");
			return false;
		} else {
         var v5 = document.useraddform.Pwdr.value;
         var v6 = v5.length;
          if(v6==0){
             alert("Re-entered password cannot be empty");
                      return false;
                      
                  }else if( v5 != v3){

                            alert("Re entered password does not match");
                            return false;
                      }
          	
			return true;
		}
	}

}
//-->
</script>
<script type="text/javascript" language="javascript">
function newPopup(url){

	popupWindow = window.open('http://localhost:8082/railwaymis/VerifyAvailabilityUserName.html','popUpWindow','Height=250,width=600,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=yes,status=yes')
}

</script>

</head>
<body>
<p align="center"><b><font size="3px">User and Group</font></b></p>

<!-- form for add users  -->
<table border="1">
     <tr>
       <td>
   <form name="useraddform" action="AddUser.jsp"
			onSubmit='javascript:return myValidate();'>
          <table border="0">
			<tr>
				<td><label for="User name">Enter your User name:</label></td>
				<td><input type="text" name="UserName" maxlength="100" /><br>
				<a href="JavaScript:newPopup('VerifyAvailabilityUserName.html');" onMouseUp="Red"><font size="2px">Please Verify availability of User name:</font></a>
				</td>
				<tr>
					<td><label for="Password">Enter your Password:</label></td>
					<td><input type="password" name="Pwd" maxlength="150" /></td>
				</tr>
				<tr>
					<td><label for="Password">Re-enter your Password:</label></td>
					<td><input type="password" name="Pwdr" maxlength="150" onChange='myValidate(this.useraddform);' />
					<DIV ID="password_result">&nbsp;</DIV>
					</td>
				</tr>
				<tr>
					<td><label for="designation">Select your designation:</label></td>
					<td><select name="designation">
						<option value="" selected="selected">Please select</option>
						<option value="Train Controler">Train Controler</option>
						<option value="DIB">DIB</option>
						<option value="Loco Foreman(R)">Loco Foreman(R)</option>
						<option value="Loco Foreman(C)">Loco Foreman(C)</option>
						<option value="Loco Foreman(OUT)">Loco Foreman(OUT)</option>
						<option value="EM-RS">Engineer Mechanical-Running Shed</option>
						<option value="EM-ELS">Engineer Mechanical-ELS</option>
						<option value="EM-HLS">Engineer Mechanical-HLS</option>
						<option value="EM-EPCS">Engineer Mechanical-EPCS</option>
						<option value="EM-PCS">Engineer Mechanical-PCS</option>
						<option value="EM-OUT">Engineer Mechanical(Out)</option>
						<option value="Deputy Engineer(M)">DEM(Motive Power)</option>
						<option value="Chief Engineer(M)">Chief Engineer(Motive Power)</option>
						<option value="DME-I">Depu.Mechanical Engineer</option>
						<option value="DME-P&D">Depu.Mechanical Engineer Project And Developments</option>
						<option value="DME(E)">Depu.Mechanical Engineer Electrical</option>
						<option value="CME">Chief Mechanical Engineer</option>
						<option value="GMR">General Manager(R)</option>
						<option value="MEDP-I">Mechanical Engineer-I Planning and Drawing</option>
						<option value="MEDP-II">Mechanical Engineer-II Planning and Drawing</option>
						<option value="MELE-I">Mechanical Engineer-I Electrical Locomotives</option>
						<option value="MELE-II">Mechanical Engineer-II Electrical Locomotives</option>
						<option value="MELH-I">Mechanical Engineer Hydraulic Locomotives-I</option>
						<option value="MELH-II">Mechanical Engineer Hydraulic Locomotives-II</option>
						<option value="MELP-I">Mechanical Engineer-I Power Coaches</option>
						<option value="MELP-II">Mechanical Engineer-II Power Coaches</option>
						<option value="MEC">Mechanical Engineer Carriages</option>
						<option value="MEW">Mechanical Engineer Wagon</option>
						<option value="MEP-I">Mechanical Engineer-I Production</option>
						<option value="MEP-II">Mechanical Engineer-II Production</option>
						<option value="MEM">Mechanical Engineer Plants and Machinery</option>
						<option value="MES">Mechanical Engineer Road Vehicle</option>
						<option value="EET-I">Electrical Engineer-I Locomotive and Carriages(All Repairs)</option>
						<option value="EET-II">Electrical Engineer=-II Locomotive and Carriages(All Repairs)</option>
						<option value="EET-DMU">Electrical And Electronic Engineer DMU</option>
						<option value="EEP">Electrical Engineer Power</option>
						<option value="EEE">Electrical and Electronic Engineer</option>
						<option value="ASRSRML">Assistant Superintendent of R-Stores Ratmalana</option>
						<option value="MFSHOP15">Mechanical Foreman Shop 15</option>
						<option value="MFSHOP27">Mechanical Foreman Shop 27</option>
						<option value="MFSHOP42">Mechanical Foreman Shop 42</option>
						<option value="ASRS-REG">Assistant Superintendent Stores Registration</option>
						<option value="SRS">Superintendent of Railway Stores</option>
						<option value="WORKSHOP-OFFICER">Work Shop Officer</option>
						<option value="DATA-ENTRY-OP">Date Entry Operator 1</option>
						<option value="PLANNER-STORES">Planner Stores</option>
						<option value="PLANNER-CME">Planner CME</option>
						<option value="DRAWING-OFFICE-CLERK">Drawing Office Clerk</option>
						<option value="DLE">Draftsman Locomotive Electrical</option>
						<option value="DLE-I">Draftsman Locomotive Electrical I</option>
						<option value="DLE-II">Draftsman Locomotive Electrical II</option>
						<option value="DLE-III">Draftsman Locomotive Electrical III</option>
						<option value="DRAWING-OFFICE-ASST">Drawing Office Assistant</option>
						<option value="DRAFTSMAN-PRODUCTION">Draftsman Production</option>
						<option value="DRAFTSMAN-PRODUCT-I">Draftsman Production I</option>
						<option value="DRAFTSMAN-CARRIAGES">Draftsman Carriages</option>
						<option value="DRAFTSMAN-CARRIAGES-I">Draftsman Carriages I</option>
						<option value="DRAFTSMAN-LOCO-HYD">Draftsman Locomotive Hydraulic</option>
						<option value="DRAFTSMAN-LOCO-HYD-I">Draftsman Locomotive Hydraulic I</option>
						<option value="DRAFTSMAN-ELECT">Draftsman Electrical</option>
						<option value="DRAFTSMAN-ELECT-I">Draftsman Electrical I</option>
						<option value="DRAFTSMAN-ELECT-II">Draftsman Electrical II</option>
						<option value="ASST-SUPDT-IND-ELECT">Assistant Superintendent(Indent Electrical)</option>
						<option value="ASST-SUPDT-IND-HYDROL">Assistant Superintendent(Indent Hydraulic)</option>
						<option value="ASST-SUPDT-IND-GEN">Assistant Superintendent(Indent General)</option>
						<option value="ASST-SUPDT-IND-DOC">Assistant Superintendent(Indent Documentation)</option>
						<option value="ASST-SUPDT-IND-TEN-I">Assistant Superintendent(Tender I)</option>
						<option value="ASST-SUPDT-IND-TEN-II">Assistant Superintendent(Tender II)</option>
						<option value="ASST-SUPDT-IND-II">Assistant Superintendent(Indent II)</option>
						<option vlaue="ASST-SUPDT-WARF">Assistant Superintendent Warf</option>
						<option value="ASST-SUPDT-LP-I">Assistant Superintendent Loco Purchasing I</option>
						<option value="ASST-SUPDT-LP-I">Assistant Superintendent Loco Purchasing II</option>
						<option value="CLERCK-SRS">Clerck SRS</option>
						<option value="PMA-SRS">Personal Management Assistant SRS</option>
						<option value="DO-SRS">Development Officer SRS</option>
						<option value="SRS-BO">SRS Branch Officer</option>
						<option value="SRS-INDENT-CLERK">SRS Indent Clerk</option>
						<option value="SRS-HR-ASST">SRS HR Asst</option>
						<option value="SRS-PUB-MANAGT-ASST">SRS Public Management Asst</option>
						<option value="SRS-DEVELOP-OFFICER">SRS Development Officer</option>
						<option value="SRS-RML-MANAGT-ASST">SRS-RML Public Management Asst</option>
						<option value="SRS-MSR-PUB-MANAGT-ASST">SRS Main Stores Pub Management Asst</option>
						<option value="SRS-MSR-CHIEF-STORE-KEEPER">SRS Main Stores Chief Stores Keeper</option>
						<option value="">------CAR SUB-DEPARTMENT---------</option>
						<option value="CAR">Chief Accountant Railway</option>
						<option value="CAR-R">Chief Accountant Railway - R</option>
						<option value="ARR-II">Accountant Railway Revenue II</option>
						<option value="IT-ASSIST-DU">IT Assistant Data Unit</option>
						
						
						
						
					</select>
					<br>
					</td>
				</tr>
				
				<tr>
					<td><br>
					<label for="role">Select your role:</label></td>
					<td><select name="role">
						<option value="" selected="selected">Please select</option>
						<option value="lfr_rs">LF(R)-R-shed</option>
						<option value="lfr_els">LF(R)-ELS</option>
						<option value="lfr_hls">LF(R)-HLS</option>
						<option value="lfr_epcs">LF(R)-EPCS</option>
						<option value="lfr_pcs">LF(R)-PCS</option>
						<option value="lfc_rs">LF(C)-R-shed</option>
						<option value="lfc_els">LF(C)-ELS</option>
						<option value="lfc_hls">LF(C)-HLS</option>
						<option value="lfc_epcs">LF(C)-EPCS</option>
						<option value="lfc_pcs">LF(C)-PCS</option>
						<option value="lf_gle">LF-GLE</option>
						<option value="lf_kdt">LF-KDT</option>
						<option value="lf_mho">LF-MHO</option>
						<option value="lf_anp">LF-ANP</option>
						<option value="lf_tco">LF-TCO</option>
						<option value="lf_bco">LF-BCO</option>
						<option value="lf_bad">LF-BAD</option>
						<option value="lf_nvp">LF-NVP</option>
						<option value="lf_ptm">LF-PTM</option>
						<option value="dib_rs">DIB-RS</option>
						<option value="dib_els">DIB-ELS</option>
						<option value="dib_hls">DIB-HLS</option>
						<option value="dib_epcs">DIB-EPCS</option>
						<option value="dib_pcs">DIB-PCS</option>
						<option value="dib_gle">DIB-GLE</option>
						<option value="dib_anp">DIB-ANP</option>
						<option value="dib_kdt">DIB-KDT</option>
						<option value="dib_nvp">DIB-NVP</option>
						<option value="dib_ptm">DIB-PTM</option>
						<option value="cem">CEM</option>
						<option value="dem">DEM</option>
						<option value="cme">CME</option>
						<option value="dme_1">DME-I</option>
						<option value="dme_2">DME-P And D</option>
						<option value="dmee">Depu.Mechanical Engineer Electrical</option>
						<option value="gmr">GMR</option>
						<option value="em_rs">EM-Rshed</option>
						<option value="em_els">EM-ELS</option>
						<option value="em_hls">EM-HLS</option>
						<option value="em_epcs">EM-EPCS</option>
						<option value="em_pcs">EM-PCS</option>
						<option value="em_out">EM-OUT</option>
						<option value="ctc_col">CTC-COL</option>
						<option value="ctc_anp">CTC-ANP</option>
						<option value="ctc_nvp">CTC-NVP</option>
						<option value="lms_admin">Administrator</option>
						<option value="em_drawings">Mechanical Engineer-I Planning and Drawing</option>
						<option value="em_drawings_ii">Mechanical Engineer-II Planning and Drawing</option>
						<option value="mele_i">Mechanical Engineer-I Electrical Locomotives</option>
						<option value="mele_ii">Mechanical Engineer-II Electrical Locomotives</option>
						<option value="melh_i">Mechanical Engineer Hydraulic Locomotives-I</option>
						<option value="melh_ii">Mechanical Engineer Hydraulic Locomotives-II</option>
						<option value="melp_i">Mechanical Engineer-I Power Coaches</option>
						<option value="melp_ii">Mechanical Engineer-II Power Coaches</option>
						<option value="mec">Mechanical Engineer Carriages</option>
						<option value="mew">Mechanical Engineer Wagon</option>
						<option value="mep_i">Mechanical Engineer-I Production</option>
						<option value="mep_ii">Mechanical Engineer-II Production</option>
						<option value="mem">Mechanical Engineer Plant and Machinery</option>
						<option value="mes">Mechanical Engineer Road Vehicle</option>
						<option value="eet_i">Electrical Engineer-I Locomotive and Carriages(All Repairs)</option>
						<option value="eet_ii">Electrical Engineer-II Locomotive and Carriages(All Repairs)</option>
						<option value="eet_dmu">Electrical And Electronic Engineer DMU</option>
						<option value="eep">Electrical Engineer Power</option>
						<option value="eee">Electrical and Electronic Engineer</option>
						<option value="asrsrml">Assistant Superintend of Railway Stores Ratmalana</option>
						<option value="mf_shop_15">Mechanical Foreman Shop 15</option>
						<option value="mf_shop_27">Mechanical Foreman Shop 27</option>
						<option value="mf_shop_42">Mechanical Foreman Shop 42</option>
						<option value="asrs_reg_srs">Assistant Superintendent Stores Registration</option>
						<option value="srs">Superintend of Railway Stores</option>
						<option value="wshop_officer">Work Shop Officer</option>
						<option value="planner_stores">Planner Stores</option>
						<option value="cme_planner">Planner CME</option>
						<option value="data_entry_op1">Data Entry Operator 1</option>
						<option value="draw_office_clerk">Drawing Office Clerk</option>
						<option value="dle">Draftsman Locomotive Electrical</option>
						<option value="dle_i">Draftsman Locomotive Electrical I</option>
						<option value="dle_ii">Draftsman Locomotive Electrical II</option>
						<option value="dle_iii">Draftsman Locomotive Electrical III</option>
						<option value="doa">Drawing Office Assistant</option>
						<option value="dp">Draftsman Production</option>
						<option value="dp_i">Draftsman Production I</option>
						<option value="dc">Draftsman Carriages</option>
						<option value="dc_i">Draftsman Carriages I</option>
						<option value="dlh">Draftsman Locomotive Hydraulic</option>
						<option value="dlh_i">Draftsman Locomotive Hydraulic I</option>
						<option value="de">Draftsman Electrical</option>
						<option value="de_i">Draftsman Electrical I</option>
						<option value="de_ii">Draftsman Electrical II</option>
						<option value="asst_sup_ie">Assistant Superintendent(Indent Electrical)</option>
						<option value="asst_sup_ih">Assistant Superintendent(Indent Hydraulic)</option>
						<option value="asst_sup_ig">Assistant Superintendent(Indent General)</option>
						<option value="asst_sup_id">Assistant Superintendent(Indent Documentation)</option>
						<option value="asst_sup_teni">Assistant Superintendent(Tender I)</option>
						<option value="asst_sup_tenii">Assistant Superintendent(Tender II)</option>
						<option value="asst_sup_inii">Assistant Superintendent(Indent II)</option>
						<option vlaue="asst_sup_warf">Assistant Superintendent Warf</option>
						<option value="asst_sup_lpi">Assistant Superintendent Loco Purchasing I</option>
						<option value="asst_sup_lpii">Assistant Superintendent Loco Purchasing II</option>
						<option value="clerck_srs">Clerk SRS</option>
						<option value="pma_srs">Personal Management Assistant SRS</option>
						<option value="do_srs">Development Officer SRS</option>
						<option value="srs_bo_indt_a">SRS Branch Officer</option>
						<option value="srs_indt_clerck_a">SRS Indent Clerk</option>
						<option value="srs_human_r_asst">SRS HR Asst</option>
						<option value="srs_pub_mang_asst">SRS Public Management Asst</option>
						<option value="srs_develop_officer">SRS Development Officer</option>
						<option value="srs_rml_pub_man_asst">SRS RML Pub Management Asst</option>
						<option value="srs_msr_pub_mang_asst">SRS Main Stores Pub Management Asst</option>
						<option value="srs_msr_chief_store_keeper">SRS Main Stores Chief Stores Keeper</option>
						<option value="">----------CAR SUB DEPARTMENT----------</option>
						<option value="car">Chief Accountant Railway</option>
						<option value="car_r">Chief Accountant Railway - R</option>
						<option value="crr_ii">Accountant Railway Revenue II</option>
						<option value="it_assit_du">IT Assistant Data Unit</option>
						
						
						
						
					</select>
					<br>
					</td>
				</tr>
				
				<tr>
					<td><label for="shed">Select your work place:</label></td>
					
					<td><select name="shed">
						<option value="" selected="selected">Please select</option>
						<option value="R-SHED">R-Shed</option>
						<option value="ELS">Els</option>
						<option value="HLS">Hls</option>
						<option value="EPCS">Epcs</option>
						<option value="PCS">Pcs</option>
						<option value="GM-OFFICE">Gm-Office</option>
						<option value="CEM-OFFICE">CEM-OFFICE</option>
						<option value="CME-RML">CME-RML</option>
						<option value="TC-OFFICE-MDA">TC-OFFICE-MDA</option>
						<option value="TC-OFFICE-NVP">TC-OFFICE-NVP</option>
						<option value="TC-OFFICE-ANP">TC-OFFICE-ANP</option>
						<option value="RS-GLE">Rs-GLE</option>
						<option value="RS-KDT">Rs-KDT</option>
						<option value="RS-NVP">Rs-NVP</option>
						<option value="RS-NOA">Rs-NOA</option>
						<option value="RS-BDA">Rs-BDA</option>
						<option value="RS-ANP">Rs-ANP</option>
						<option value="RS-TCO">Rs-TCO</option>
						<option value="RS-BCO">Rs-BCO</option>
						<option value="RS-MHO">Rs-MHO</option>
						<option value="RS-PTM">Rs-PTM</option>
						<option value="MW-RML">MW-RML</option>
						<option value="SRS-RML">SRS-RML</option>
						<option value="SRS">SRS</option>
						<option value="SH15">Shop 15</option>
						<option value="SH27">Shop 27</option>
						<option value="SH42">Shop 42</option>
						<option value="RS">RS</option>
						<option value="SRS">SRS-MDA</option>
						<option value="SRS-RGM">SRS-RGM</option>
						<option value="">--------CAR SUB DEPARTMENT--------</option>
						<option value="CAR">CAR</option>
						<option value="CAR-R">CAR(R)</option>
						<option value="CRR-II">CRR-II</option>
						<option value="IT-UNIT">IT-UNIT</option>
										
					</select></td>
				</tr>
				
				<tr>
                    <td><label for="subdept">Select your sub department</label></td>
                    
                    <td><select name="subdept">
                        <option value="" selected="selected">Please select</option>
                        <option value="GM-OFFICE">GM-Office</option>
                        <option value="CME">CME-Ratmalana</option>
                        <option value="CEM">CEM-Dematagoda</option>
                        <option value="AOT">AOT-Maradana</option>
                        <option value="CEW">CEW-Dematagoda</option>
                        <option value="SRS">SRS-MDA</option>
                        <option value="CAR">CAR</option>
                        <option value="IT">IT-Unit</option>
                                              
                    </select></td>
                </tr>
                <tr>
                	<td>Enter NIC No</td>
                	<td>
                		<input type="text" name="nic" />
                		<br>
				        <br>
                	</td>
                </tr>
				
				
				
				<tr>
					<td align="right"><input class="button1" type="submit" value="Enter" /></td>
					<td align="left"><input class="button1" type="reset" value="Reset" /><br></td>
					
				</tr>
				
				
		</table>
		</form>
		</tr>
</table>
</body>
</html>