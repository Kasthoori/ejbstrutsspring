/**
 * 
 */
package hibernate.service.srs;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.srs.CommonInventItemsLedger;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class CommonInventItemsLedgerDAOImpl implements CommonInventItemsLedgerDAO {

    public void saveCommonInventItems(
	    CommonInventItemsLedger commonInventItemsLedger) {
	
		SessionFactory    session  =   HibernateUtil.getSessionFactory();
		Session   sess =  session.getCurrentSession();
		
		Transaction  tx  =  sess.beginTransaction();
		
		sess.save(commonInventItemsLedger);
		
		tx.commit();
		session.close();
	
    }

    @SuppressWarnings({ "unchecked" })
    public List<CommonInventItemsLedger> getListCommonStoreNum(
	    String commStoreNum, String locomotive) {
	
	List<CommonInventItemsLedger>   getCommonItemList = new ArrayList<CommonInventItemsLedger>();
	
	SessionFactory    session  =  HibernateUtil.getSessionFactory();
	


	Session   sess =  session.getCurrentSession();
	
	
	Transaction   tx  =  sess.beginTransaction();
	
	
	getCommonItemList  =  sess.createQuery("from CommonInventItemsLedger  C  where C.commstorenum like '%" + commStoreNum + "%' and commapp like '%" + locomotive  + "%'").list();

	tx.commit();
	session.close();
	
 
	
	
	
	return getCommonItemList;
    }

    @SuppressWarnings("unchecked")
    public List<CommonInventItemsLedger> getMappStoreNum(String mappStoreNum) {
	
	List<CommonInventItemsLedger>   getMappItemList = new ArrayList<CommonInventItemsLedger>();
	
		SessionFactory  session  = HibernateUtil.getSessionFactory();
		Session  sess  =  session.getCurrentSession();
		
		Transaction  tx  =  sess.beginTransaction();
		
		try{
		
		getMappItemList  =  sess.createQuery("from CommonInventItemsLedger  C where C.mappstorenum = '" +  mappStoreNum +  "'").list();
		
		}catch(Exception   ex){
		    
		    ex.printStackTrace();
		}
		tx.commit();
		session.close();
		
	return getMappItemList;
    }

}
