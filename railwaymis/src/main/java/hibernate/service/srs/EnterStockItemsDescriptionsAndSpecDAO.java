/**
 * 
 */
package hibernate.service.srs;

import java.util.List;

import hibernate.domain.srs.EnterStockItemsDescriptionsAndSpec;

/**
 * @author A.P.Kasthoori
 *
 */
public interface EnterStockItemsDescriptionsAndSpecDAO {
	
	public void saveStockItemsDescriptions(EnterStockItemsDescriptionsAndSpec  enterStockItemsDescriptionsAndSpec);
	List<EnterStockItemsDescriptionsAndSpec>  getDescriptionList(String stockNumber);
	public void updateStockItemsDescriptions(String stockNum, String descript, String specification, String userEdit, String roleEdit, String subDeptEdit, String dateEdit);

}
