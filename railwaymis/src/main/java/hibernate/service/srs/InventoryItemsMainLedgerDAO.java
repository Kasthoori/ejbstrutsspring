/**
 * 
 */
package hibernate.service.srs;

import java.util.List;

import hibernate.domain.srs.InventoryItemsMainLedger;

/**
 * @author A.P.Kasthoori
 *
 */
public interface InventoryItemsMainLedgerDAO {
	
	public void saveInventoryItemsMainLedgerDAO(InventoryItemsMainLedger   inventoryItemsMainLedger);
	public List<InventoryItemsMainLedger>  getListInventoryItemsMainLedger(String storeNumber);
	public void updateBranch(String storeNum, String branch);
	
	//This is load to inventory items procurement checking for si/lp details
	public List<InventoryItemsMainLedger> getList();

}
