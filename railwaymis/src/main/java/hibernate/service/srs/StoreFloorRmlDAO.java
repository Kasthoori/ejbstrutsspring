/**
 * 
 */
package hibernate.service.srs;

import java.util.List;

import hibernate.domain.InventoryItems;
import hibernate.domain.srs.StoreFloorRgm;
import hibernate.domain.srs.StoreFloorRml;

/**
 * @author A.P.Kasthoori
 *
 */
public interface StoreFloorRmlDAO {
	
	public void saveStoreFloorRmlDAO(StoreFloorRml  storeFloorRml);
	public void updateStoreFloorRmlDAO(String storeNum, double quantity);
	public List<StoreFloorRml>   getListStoreFloorRml(StoreFloorRml storeFloorRml);
	public List<StoreFloorRml>   getFilteredListOfInventItemsFloorRml(String storeNum, String description);
	public List<StoreFloorRml>   listItemsForSrs(String storeNum);

}
