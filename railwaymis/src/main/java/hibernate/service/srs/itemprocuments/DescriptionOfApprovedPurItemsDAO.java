/**
 * 
 */
package hibernate.service.srs.itemprocuments;

import hibernate.domain.srs.itemprocuments.DescriptionOfApprovedPurItems;

/**
 * @author A.P.Kasthoori
 *
 */
public interface DescriptionOfApprovedPurItemsDAO {
	
	public void saveDescriptionOfApprovedPurItems(DescriptionOfApprovedPurItems  descriptionOfApprovedPurItems);
    public void updateSeqNumCond(String purchaseId, String seqNumCond);

}
