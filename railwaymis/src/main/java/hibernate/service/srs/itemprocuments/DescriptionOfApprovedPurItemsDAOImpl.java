/**
 * 
 */
package hibernate.service.srs.itemprocuments;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.srs.itemprocuments.DescriptionOfApprovedPurItems;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class DescriptionOfApprovedPurItemsDAOImpl implements DescriptionOfApprovedPurItemsDAO {

	public void saveDescriptionOfApprovedPurItems(
			DescriptionOfApprovedPurItems descriptionOfApprovedPurItems) {
		
		    SessionFactory   session  =  HibernateUtil.getSessionFactory();
		    Session   sess =  session.getCurrentSession();
		    
		    Transaction  tx = sess.beginTransaction();
		    
		    sess.save(descriptionOfApprovedPurItems);
		    
		    tx.commit();
		    session.close();
		
	}

	public void updateSeqNumCond(String purchaseId, String seqNumCond) {
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess =  session.getCurrentSession();
		
		Transaction  tx  =  sess.beginTransaction();
		
		DescriptionOfApprovedPurItems  doa =  (DescriptionOfApprovedPurItems)sess.get(DescriptionOfApprovedPurItems.class, purchaseId);
		doa.setSeqnumcond(seqNumCond);
		
		tx.commit();
		session.close();
		
	}

}
