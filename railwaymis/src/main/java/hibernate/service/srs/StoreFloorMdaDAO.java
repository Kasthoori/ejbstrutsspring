/**
 * 
 */
package hibernate.service.srs;

import java.util.List;

import hibernate.domain.srs.StoreFloorMda;

/**
 * @author A.P.Kasthoori
 *
 */
public interface StoreFloorMdaDAO {
	
	public void saveStoreFloorMdaDAO(StoreFloorMda   storeFloorMda);
	public List<StoreFloorMda>  getListStoreFloorMda(StoreFloorMda   storeFloorMda);

}
