/**
 * 
 */
package hibernate.service.srs;

import hibernate.domain.srs.CheckPurchaseOrdersFromSubDept;

import java.util.List;

/**
 * @author A.P.Kasthoori
 *
 */
public interface CheckPurchaseOrderFromSubDeptDAO {
    
    public List<CheckPurchaseOrdersFromSubDept>   listOrders(CheckPurchaseOrdersFromSubDept  checkPurchaseOrdersFromSubDept);

}
