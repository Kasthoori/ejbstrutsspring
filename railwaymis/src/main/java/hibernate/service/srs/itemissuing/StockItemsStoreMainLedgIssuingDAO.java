/**
 * 
 */
package hibernate.service.srs.itemissuing;

import java.util.List;

import hibernate.domain.srs.itemissuing.StockItemsStoresMainLedgIssuing;

/**
 * @author Anura P. Kasthoori
 *
 */

public interface StockItemsStoreMainLedgIssuingDAO {
	
	public void addStockItem(StockItemsStoresMainLedgIssuing stockItemsStoresMainLedgIssuing);
	public List<StockItemsStoresMainLedgIssuing>  getItemList(String storeNum, String date, String subdept);

}
