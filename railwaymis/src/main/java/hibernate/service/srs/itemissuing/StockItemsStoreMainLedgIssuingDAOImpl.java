/**
 * 
 */
package hibernate.service.srs.itemissuing;

import hibernate.domain.srs.itemissuing.StockItemsStoresMainLedgIssuing;
import hibernate.utility.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Anura P. Kasthoori
 *
 */


public class StockItemsStoreMainLedgIssuingDAOImpl implements StockItemsStoreMainLedgIssuingDAO{
	
	
	public void addStockItem(
			StockItemsStoresMainLedgIssuing stockItemsStoresMainLedgIssuing) {
		// TODO Auto-generated method stub
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		
		Session  sess = session.getCurrentSession();
		
		Transaction tx = sess.beginTransaction();
		
		sess.save(stockItemsStoresMainLedgIssuing);
		
		
		tx.commit();
		session.close();
	
		
		
		
		
	}

	@SuppressWarnings("unchecked")
	public List<StockItemsStoresMainLedgIssuing> getItemList(String storeNum, String date,String subdept) {
		// TODO Auto-generated method stub
		
		List<StockItemsStoresMainLedgIssuing> getList = new ArrayList<StockItemsStoresMainLedgIssuing>();
		
        SessionFactory  session = HibernateUtil.getSessionFactory();
		
		Session  sess = session.getCurrentSession();
		
		Transaction tx = sess.beginTransaction();
		
		
		String hsql = "FROM StockItemsStoresMainLedgIssuing WHERE storenumber LIKE :storeNumber OR issueddate LIKE :issuedDate OR issuedto LIKE :subdepart";
		
		Query  query = sess.createQuery(hsql);
		
		query.setParameter("storeNumber", "%" + storeNum + "%");
		query.setParameter("issuedDate", date);
		query.setParameter("subdepart", subdept);
		
		getList = query.list();
		
		tx.commit();
		session.close();
		
		/*String hsql = "FROM DetailsOfLpAndSiOrders WHERE newfiledate BETWEEN :startdate AND :enddate AND section=:section AND transcondition=:tranCond";
		
		Query  query = sess.createQuery(hsql);
		query.setParameter("section", section);
		
		query.setParameter("startdate", startdate);
		query.setParameter("enddate", enddate);
		query.setParameter("tranCond", tranCond);
		getFilteredList = query.list();
		*/
		
		
		return getList;
	}

}
