package hibernate.service.srs;

import hibernate.domain.srs.InItemSMainLeIssuing;
import hibernate.utility.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;



public class InItemSMainLeIssuingDAOImpl implements IntItemSMainLeIssuingDAO {

	@SuppressWarnings("unchecked")
	public List<InItemSMainLeIssuing>  listIntItemSMainLeIssuingDAO(String issuedTo) {
		
				List<InItemSMainLeIssuing>   returnListItemsIssuing = null;
				
				SessionFactory   session = HibernateUtil.getSessionFactory();
				Session   sess  =  session.getCurrentSession();
				
				Transaction  tx  =  sess.beginTransaction();
				
				String check = "false";
				
				try{
					
						returnListItemsIssuing  =  sess.createQuery("from InItemSMainLeIssuing where issuedto='" + issuedTo  + "' and outbuffer='" + check + "'").list();
					
				}catch(Exception    ex){
					
					ex.printStackTrace();
				}finally{
					tx.commit();
					session.close();
					
				}
				
		return returnListItemsIssuing;
	}

	public void updateIntItemSMainLeIssuingDAO(String storeNum, String outBuffer){
		// TODO Auto-generated method stub
		SessionFactory   session =  HibernateUtil.getSessionFactory();
		Session   sess  =  session.getCurrentSession();
		
		Transaction    tx  =  sess.beginTransaction();
		
		try{
		InItemSMainLeIssuing   ism = (InItemSMainLeIssuing) sess.get(InItemSMainLeIssuing.class, storeNum);
		ism.setOutbuffer(outBuffer);
		
		}catch(Exception   ex ){
			
			ex.printStackTrace();
		}finally{
			
			tx.commit();
			session.close();
		}
		
	}


	@SuppressWarnings("unchecked")
	public List<InItemSMainLeIssuing> listSelecIntItemSMainLeIssuingDAO(String issuedTo, String storeNum) {
		List<InItemSMainLeIssuing>   returnListItemsIssuing = null;
		
		SessionFactory   session = HibernateUtil.getSessionFactory();
		Session   sess  =  session.getCurrentSession();
		
		Transaction  tx  =  sess.beginTransaction();
		
		String check = "false";
		
		try{
			
				returnListItemsIssuing  =  sess.createQuery("from InItemSMainLeIssuing where issuedto='" + issuedTo  + "' and outbuffer='" + check + "' and storenumber='" + storeNum + "'").list();
			
		}catch(Exception    ex){
			
			ex.printStackTrace();
		}finally{
			tx.commit();
			session.close();
			
		}
		
             return returnListItemsIssuing;
	
	}

}
