package hibernate.service.srs;

import java.util.List;

import hibernate.domain.srs.InItemSMainLeIssuing;

public interface IntItemSMainLeIssuingDAO {
	
	public List<InItemSMainLeIssuing>  listIntItemSMainLeIssuingDAO(String  issuedTo);
	public List<InItemSMainLeIssuing>  listSelecIntItemSMainLeIssuingDAO(String issuedTo, String storeNum);
	public void updateIntItemSMainLeIssuingDAO(String storeNum, String outBuffer);

}
