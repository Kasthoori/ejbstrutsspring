/**
 * 
 */
package hibernate.service.srs;

import java.util.List;

import hibernate.domain.srs.CommonInventItemsLedger;

/**
 * @author A.P.Kasthoori
 *
 */
public interface CommonInventItemsLedgerDAO {
    
       public void saveCommonInventItems(CommonInventItemsLedger   commonInventItemsLedger);
       List<CommonInventItemsLedger>   getListCommonStoreNum(String commStoreNum, String locomotive);
       List<CommonInventItemsLedger>  getMappStoreNum(String mappStoreNum);

}
