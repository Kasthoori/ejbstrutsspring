/**
 * 
 */
package hibernate.service.srs;

import hibernate.domain.InventoryItems;
import hibernate.domain.srs.StoreFloorRgm;
import hibernate.utility.HibernateUtil;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * @author A.P.Kasthoori
 *
 */
public class StoreFloorRgmDAOImpl implements StoreFloorRgmDAO{

	public void saveStoreFloorRgmDAO(StoreFloorRgm storeFloorRgm) {
		
			SessionFactory   session   =   HibernateUtil.getSessionFactory();
			Session  sess  =  session.getCurrentSession();
			
			Transaction   tx  =  sess.beginTransaction();
			
			try{
					sess.save(storeFloorRgm);
				
			}catch(Exception   ex){
				
				ex.printStackTrace();
			}
		tx.commit();
		session.close();
	}

	public List<StoreFloorRgm> getListStoreFloorRgm(StoreFloorRgm storeFloorRgm) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<StoreFloorRgm> getListStoreFloorRgm(
			StoreFloorRgm storeFloorRgm, InventoryItems inventoryItems) {
		// TODO Auto-generated method stub
		return null;
	}

}
