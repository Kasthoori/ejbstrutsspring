/**
 * 
 */
package hibernate.service.srs;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.srs.EnterStockItemsDescriptionsAndSpec;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class EnterStockItemsDescriptionsAndSpecDAOImpl implements  EnterStockItemsDescriptionsAndSpecDAO{

	public void saveStockItemsDescriptions(
			EnterStockItemsDescriptionsAndSpec enterStockItemsDescriptionsAndSpec) {
	
		
		SessionFactory   session  =  HibernateUtil.getSessionFactory();
		Session   sess =  session.getCurrentSession();
		
		Transaction  tx  =  sess.beginTransaction();
		
		sess.save(enterStockItemsDescriptionsAndSpec);
		
		tx.commit();
		session.close();
		
	}

	@SuppressWarnings("unchecked")
	public List<EnterStockItemsDescriptionsAndSpec> getDescriptionList(
			String stockNumber) {
		// TODO Auto-generated method stub
		List<EnterStockItemsDescriptionsAndSpec>  getStockDesList = new ArrayList<EnterStockItemsDescriptionsAndSpec>();
		
		SessionFactory  session  = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction  tx =  sess.beginTransaction();
		
		getStockDesList =  sess.createQuery("from EnterStockItemsDescriptionsAndSpec E where E.stocknum='" + stockNumber + "'").list();
		
		tx.commit();
		session.close();
		
		
		return getStockDesList;
		
		
		
	}
	
	/*
	 * 
	 * This is update Stock Item Main Component and sub component if it incorrect.
	 * */

	public void updateStockItemsDescriptions(String stockNum, String descript,
			String specification, String userEdit, String roleEdit,
			String subDeptEdit, String dateEdit) {
		// TODO Auto-generated method stub
		
		  SessionFactory  session  = HibernateUtil.getSessionFactory();
		  Session  sess = session.getCurrentSession();
		  
		  Transaction tx = sess.beginTransaction();
		  
		  String hql = "UPDATE EnterStockItemsDescriptionsAndSpec SET description=:descript,specification=:spec,userEdit=:userEdited," +
		  		       "roleEdit=:roleEdited,subdeptEdit=:subDeptEdited,dateEdit=:dateEdited WHERE stocknum=:stockNum";
		  
		  
		  Query  query = sess.createQuery(hql);
		  
		  query.setParameter("descript", descript);
		  query.setParameter("spec",specification);
		  query.setParameter("userEdited",userEdit);
		  query.setParameter("roleEdited", roleEdit);
		  query.setParameter("subDeptEdited", subDeptEdit);
		  query.setParameter("dateEdited", dateEdit);
		  query.setParameter("stockNum", stockNum);
		  
		  query.executeUpdate();
		  
		  sess.flush();
		  sess.clear();
		  
		  tx.commit();
		  session.close();
		  
		  
		
	}

}
