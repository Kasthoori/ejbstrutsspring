/**
 * 
 */
package hibernate.service.srs;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import com.opensymphony.xwork2.ActionContext;

import hibernate.domain.srs.EnterCommonStockItemsDescriptionsAndSpec;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class EnterCommonStockItemsDescriptionsAndSpecDAOImpl implements EnterCommonStockItemsDescriptionsAndSpecDAO{
	
	 HttpServletResponse    response =  (HttpServletResponse) ActionContext.getContext().get(ServletActionContext.HTTP_RESPONSE);

	public void saveCommonStockItemsDescript(
			EnterCommonStockItemsDescriptionsAndSpec enterCommonStockItemsDescriptionsAndSpec) {
		
		SessionFactory  session  =  HibernateUtil.getSessionFactory();
		Session   sess =  session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		

			
		sess.save(enterCommonStockItemsDescriptionsAndSpec);
	
		
		tx.commit();
		session.close();
		
	}

}
