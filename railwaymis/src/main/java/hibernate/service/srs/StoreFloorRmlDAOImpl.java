package hibernate.service.srs;

import hibernate.domain.InventoryItems;
import hibernate.domain.srs.StoreFloorRgm;
import hibernate.domain.srs.StoreFloorRml;
import hibernate.utility.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class StoreFloorRmlDAOImpl implements StoreFloorRmlDAO {

	public void saveStoreFloorRmlDAO(StoreFloorRml storeFloorRml) {
	
		SessionFactory  session  = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction  tx  =  sess.beginTransaction();
		
		try{
			
				sess.save(storeFloorRml);
			
		}catch(Exception  ex){
			
				ex.printStackTrace();
		}
		
		sess.flush();
		tx.commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
        public List<StoreFloorRml> getListStoreFloorRml(StoreFloorRml  storeFloorRml) {
		List<StoreFloorRml>    listStoreItemRml  =  new ArrayList<StoreFloorRml>();

		
        
		
		SessionFactory   session  =  HibernateUtil.getSessionFactory();
		Session   sess  =  session.getCurrentSession();
		
		Transaction   tx  =   sess.beginTransaction();
		
		try{
					
			listStoreItemRml  =  sess.createQuery("from StoreFloorRml").list();
			
			
		}catch(Exception   ex){
			
			ex.printStackTrace();
			
		}finally{
			
			sess.flush();
			tx.commit();
			session.close();
		}
                   return listStoreItemRml;
	}

	@SuppressWarnings("unchecked")
	public List<StoreFloorRml> getFilteredListOfInventItemsFloorRml(
			String storeNum , String description) {
		
					List<StoreFloorRml>    filteredItemsList  =  new ArrayList<StoreFloorRml>();
		
					SessionFactory   session  = HibernateUtil.getSessionFactory();
					Session   sess  =  session.getCurrentSession();
					
					Transaction    tx  =  sess.beginTransaction();
					
					try{
						        
						filteredItemsList  =  sess.createQuery("from StoreFloorRml S  where S.storenumber like '%" + storeNum + "%' and S.inventoryItemsMainLedger.descriptions like '%" + description + "%'").list();
						
						
						
						
					}catch(Exception   ex){
						
						ex.printStackTrace();
					}
					
					
					sess.flush();
					tx.commit();
					session.close();
					
		return filteredItemsList;
	}

	public void updateStoreFloorRmlDAO(String storeNum, double quantity) {
		
			SessionFactory  session  =  HibernateUtil.getSessionFactory();
			Session   sess  = session.getCurrentSession();
			
			Transaction   tx  =   sess.beginTransaction();
			
			try{
				
					StoreFloorRml   sfr =  (StoreFloorRml) sess.get(StoreFloorRml.class, storeNum);
					sfr.setQuantity(quantity);
				
			}catch(Exception    ex){
				
				ex.printStackTrace();
			}finally{
				
				sess.flush();
				tx.commit();
				session.close();
			}
		
	}

	@SuppressWarnings("unchecked")
	public List<StoreFloorRml> listItemsForSrs(String storeNum) {
	    
	       List<StoreFloorRml>   getListForSrs = new ArrayList<StoreFloorRml>();
	       
	       SessionFactory   session  =  HibernateUtil.getSessionFactory();
	       Session   sess =  session.getCurrentSession();
	       
	       Transaction   tx  =  sess.beginTransaction();
	       
	       
	       
	       try{
		       getListForSrs  =  sess.createQuery("from  StoreFloorRml  S where S.storenumber = '" + storeNum + "'").list();
		   
	       }catch(Exception   ex ){
		   
		   ex.printStackTrace();
	       }
	       
	       sess.flush();
	       tx.commit();
	       session.close();
	       
	    return getListForSrs;
	}

}
