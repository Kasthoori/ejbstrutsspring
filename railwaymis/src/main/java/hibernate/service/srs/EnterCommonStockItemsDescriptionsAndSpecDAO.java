/**
 * 
 */
package hibernate.service.srs;

import hibernate.domain.srs.EnterCommonStockItemsDescriptionsAndSpec;

/**
 * @author A.P.Kasthoori
 *
 */
public interface EnterCommonStockItemsDescriptionsAndSpecDAO {
	
	public void saveCommonStockItemsDescript(EnterCommonStockItemsDescriptionsAndSpec  enterCommonStockItemsDescriptionsAndSpec);

}