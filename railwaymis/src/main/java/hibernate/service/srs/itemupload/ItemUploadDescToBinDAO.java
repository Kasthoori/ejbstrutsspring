/**
 * 
 */
package hibernate.service.srs.itemupload;

import hibernate.domain.srs.itemupload.ItemUploadDescToBin;

import java.util.List;

/**
 * @author Anura P. Kasthoori
 *
 */

public interface ItemUploadDescToBinDAO {
	
	public List<ItemUploadDescToBin>  getUploadItemList(String storeNum, String date,String datelast, String ordertype);

}
