/**
 * 
 */
package hibernate.service.srs.itemupload;

import hibernate.domain.srs.itemupload.ItemUploadDescToBin;
import hibernate.utility.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * @author Anura P. Kasthoori
 *
 */


public class ItemUploadDescToBinDAOImpl implements ItemUploadDescToBinDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemUploadDescToBin> getUploadItemList(String storeNum,
			String date,String datelast, String ordertype) {
		
		List<ItemUploadDescToBin> getItems = new ArrayList<ItemUploadDescToBin>();
		
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		Transaction tx = sess.beginTransaction();
		
		
       String hsql = "FROM ItemUploadDescToBin WHERE storenumber LIKE :storeNumber OR  issueddate BETWEEN :issuedDate AND :lastdate OR ordtype=:ordType";
       
		
		Query  query = sess.createQuery(hsql);
		
		query.setParameter("storeNumber", "%" + storeNum + "%");
		query.setParameter("issuedDate", date);
		query.setParameter("lastdate",datelast);
		query.setParameter("ordType", ordertype);
		
		getItems = query.list();
		
		tx.commit();
		session.close();
		
		return getItems;
	}

}
