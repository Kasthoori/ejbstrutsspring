/**
 * This is save inventory items to main 
 * ledger
 */
package hibernate.service.srs;

import hibernate.domain.srs.InventoryItemsMainLedger;
import hibernate.utility.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * @author A.P.Kasthoori
 *
 */
public class InventoryItemsMainLedgerDAOImpl implements InventoryItemsMainLedgerDAO{

	public void saveInventoryItemsMainLedgerDAO(
			InventoryItemsMainLedger inventoryItemsMainLedger) {
		
			SessionFactory  session   =  HibernateUtil.getSessionFactory();
			Session   sess  =  session.getCurrentSession();
			
			Transaction   tx  =  sess.beginTransaction();
			
			try{
				
					sess.save(inventoryItemsMainLedger);
				
			}catch(Exception  ex){
				
				ex.printStackTrace();
			}
		
			tx.commit();
			session.close();
	}

	@SuppressWarnings("unchecked")
	public List<InventoryItemsMainLedger> getListInventoryItemsMainLedger(String storeNumber) {
	    
	       List<InventoryItemsMainLedger>    getItemList = new ArrayList<InventoryItemsMainLedger>();
		
	            SessionFactory    session  =  HibernateUtil.getSessionFactory();
	            Session    sess  =  session.getCurrentSession();
	            
	            Transaction   tx  = sess.beginTransaction();
	            
	            
	            
	            getItemList  =  sess.createQuery("from InventoryItemsMainLedger  I where I.storenumber = '" + storeNumber + "'").list();
	            
	           
	            
	            tx.commit();
	            session.close();
	    
		return getItemList;
	}

	public void updateBranch(String storeNum, String branch) {
	    
	    SessionFactory    session  =  HibernateUtil.getSessionFactory();
	    Session   sess  =  session.getCurrentSession();
	    
	    Transaction    tx  =   sess.beginTransaction();
	    
	    try{
		
		InventoryItemsMainLedger   iiml =  (InventoryItemsMainLedger) sess.get(InventoryItemsMainLedger.class, storeNum);
		iiml.setStore(branch);
		
		tx.commit();
		session.close();
		
	    }catch(Exception   ex){
		
		ex.printStackTrace();
	    }
	    
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InventoryItemsMainLedger> getList() {
		
		List<InventoryItemsMainLedger>    getListforProc = new ArrayList<InventoryItemsMainLedger>();
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction tx = sess.beginTransaction();
		
		getListforProc = sess.createQuery("select distinct i.storenumber from InventoryItemsMainLedger i").list();
		
		tx.commit();
		session.close();
		
		return getListforProc;
	}

}
