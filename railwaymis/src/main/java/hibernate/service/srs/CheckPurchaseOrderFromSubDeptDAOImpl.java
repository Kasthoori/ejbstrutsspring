/**
 * 
 */
package hibernate.service.srs;

import hibernate.domain.srs.CheckPurchaseOrdersFromSubDept;
import hibernate.utility.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * @author A.P.Kasthoori
 *
 */
public class CheckPurchaseOrderFromSubDeptDAOImpl implements CheckPurchaseOrderFromSubDeptDAO {

    @SuppressWarnings("unchecked")
    public List<CheckPurchaseOrdersFromSubDept> listOrders(
	    CheckPurchaseOrdersFromSubDept checkPurchaseOrdersFromSubDept) {
	
	     List<CheckPurchaseOrdersFromSubDept>  getListPurchaseOrder = new ArrayList<CheckPurchaseOrdersFromSubDept>();
	
	     SessionFactory    session  = HibernateUtil.getSessionFactory();
	     Session   sess =  session.getCurrentSession();
	     
	     Transaction   tx  =  sess.beginTransaction();
	     
	     getListPurchaseOrder  =  sess.createQuery("from CheckPurchaseOrdersFromSubDept").list();
	     
	     tx.commit();
	     session.close();
	     
	return getListPurchaseOrder;
    }

}
