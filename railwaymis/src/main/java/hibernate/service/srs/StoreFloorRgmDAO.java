package hibernate.service.srs;

import java.util.List;

import hibernate.domain.InventoryItems;
import hibernate.domain.srs.StoreFloorRgm;

/*
 * @author A.P.Kasthoori
 */

public interface StoreFloorRgmDAO {
	
	public void saveStoreFloorRgmDAO(StoreFloorRgm  storeFloorRgm);
	public List<StoreFloorRgm>   getListStoreFloorRgm(StoreFloorRgm   storeFloorRgm , InventoryItems inventoryItems);

}
