/**
 * 
 */
package hibernate.service.workshopsrml;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.workshopsrml.InventItemInsertToShopFloor;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class InItemInToSFloorDAOImpl implements InItemInToSFloorDAO {

	public void saveInItemInToSFloorDAO(InventItemInsertToShopFloor  inventItemInsertToShopFloor) {
		// TODO Auto-generated method stub
		
		SessionFactory  session  =  HibernateUtil.getSessionFactory();
		Session  sess =  session.getCurrentSession();
		 
		Transaction   tx  =  sess.beginTransaction();
		
		try{
			
			sess.save(inventItemInsertToShopFloor);
			
		}catch(Exception   ex){
			
			ex.printStackTrace();
		}finally{
			
			tx.commit();
			session.close();
			
		}
		
	}

	@SuppressWarnings("unchecked")
	public List<InventItemInsertToShopFloor> getListItemShopFloor(String storeNum, String shop, String subDept) {
		
		List<InventItemInsertToShopFloor>    listShopFloorItems = new ArrayList<InventItemInsertToShopFloor>();
		
		SessionFactory   session  =  HibernateUtil.getSessionFactory();
		Session   sess =  session.getCurrentSession();
		
		Transaction  tx =  sess.beginTransaction();
		
		try{
			
				listShopFloorItems  =  sess.createQuery("from InventItemInsertToShopFloor where storenumber = '" + storeNum + "' and shop='" + shop + "' and subdept='" + subDept + "'").list();
			
		}catch(Exception    ex){
			
			ex.printStackTrace();
		}
		
		tx.commit();
		session.close();
		
		return listShopFloorItems;
	}

	public void updateInItemInToSFloorDAO(String storeNum, double quan) {
            
		     SessionFactory    session  =   HibernateUtil.getSessionFactory();
		     Session   sess  =  session.getCurrentSession();
		     
		     Transaction   tx  =  sess.beginTransaction();
		     
		     InventItemInsertToShopFloor     its  =   (InventItemInsertToShopFloor) sess.get(InventItemInsertToShopFloor.class, storeNum);
		     its.setQuantity(quan);
		     
		     tx.commit();
		     session.close();
		
	}

	
}
