/**
 * 
 */
package hibernate.service.workshopsrml;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.workshopsrml.InventoryItemSpecifications;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class InventoryItemSpecificationsDAOImpl implements InventoryItemSpecificationsDAO {

	public void saveSpecifications(
			InventoryItemSpecifications inventoryItemSpecifications) {
	   
		
		SessionFactory   session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		sess.save(inventoryItemSpecifications);
		
		tx.commit();
		session.close();
		
	}

}
