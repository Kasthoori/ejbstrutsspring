/**
 * 
 */
package hibernate.service.workshopsrml;

import hibernate.domain.workshopsrml.InventoryItemSpecifications;

/**
 * @author A.P.Kasthoori
 *
 */
public interface InventoryItemSpecificationsDAO {
	
	public void saveSpecifications(InventoryItemSpecifications   inventoryItemSpecifications);

}
