/**
 * 
 */
package hibernate.service.workshopsrml;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.workshopsrml.ShopsBinControl;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class ShopsBinsControlDAOImpl implements ShopsBinsControlDAO {

	public void saveShopsBinsControlDAO(ShopsBinControl shopsBinControl) {
		// TODO Auto-generated method stub
		
		SessionFactory   session  =  HibernateUtil.getSessionFactory();
		Session   sess  =  session.getCurrentSession();
		
		Transaction   tx  = sess.beginTransaction();
		
		try{
			
					sess.save(shopsBinControl);
			
		}catch(Exception   ex){
			
			ex.printStackTrace();
		}finally{
			
			tx.commit();
			session.close();
		}
		
	}

}
