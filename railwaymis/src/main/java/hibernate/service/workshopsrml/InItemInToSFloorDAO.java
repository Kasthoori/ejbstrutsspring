/**
 * 
 */
package hibernate.service.workshopsrml;

import java.util.List;

import hibernate.domain.workshopsrml.InventItemInsertToShopFloor;

/**
 * @author A.P.Kasthoori
 *
 */
public interface InItemInToSFloorDAO {

	public void saveInItemInToSFloorDAO(InventItemInsertToShopFloor  inventItemInsertToShopFloor);
	public List<InventItemInsertToShopFloor>  getListItemShopFloor(String storeNum, String shop, String subDept); 
	public void updateInItemInToSFloorDAO(String storeNum, double quan);


}
