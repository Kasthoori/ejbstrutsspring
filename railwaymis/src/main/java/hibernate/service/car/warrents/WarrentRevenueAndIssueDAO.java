/**
 * 
 */
package hibernate.service.car.warrents;

import java.util.List;

import hibernate.domain.car.warrents.WarrentRevenueAndIssue;

/**
 * @author A.P.Kasthoori
 *
 */
public interface WarrentRevenueAndIssueDAO {
	
	public void saveWarrentRevAndIssue(WarrentRevenueAndIssue  warrentRevenueAndIssue);
	public List<WarrentRevenueAndIssue>  getListWarrent(String warrnum);
	public void updateEdit(String dateissued,String depcode,String warrantnum,String newWarrNum,String fromstation
			               ,String tostation,String type,int fullnum, int halfnum, String seattype
			               ,String traveltypes,String transit,String tranpoint,double tranuprice, int numpassengers, double unitcharge, double totcost, int finturn);
	public List<WarrentRevenueAndIssue> getFilteredList(String warrNum,String depNum,String startDate,String endDate);

}
