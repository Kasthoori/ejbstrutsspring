/**
 * 
 */
package hibernate.service.car.warrents;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.car.warrents.WarrentRevenueAndIssue;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class WarrentRevenueAndIssueDAOImpl implements WarrentRevenueAndIssueDAO {

	@Override
	public void saveWarrentRevAndIssue(
			WarrentRevenueAndIssue warrentRevenueAndIssue) {
		// TODO Auto-generated method stub
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		sess.save(warrentRevenueAndIssue);
		
		tx.commit();
		session.close();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WarrentRevenueAndIssue> getListWarrent(String warrnum) {
		
		List<WarrentRevenueAndIssue> getList = new ArrayList<WarrentRevenueAndIssue>();
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction tx = sess.beginTransaction();
		
		
		
		getList = sess.createQuery("from WarrentRevenueAndIssue w where w.warrantnumber='" + warrnum + "'").list();
		
		
		tx.commit();
		session.close();
		
		
		return getList;
		
		
	    
	    
	}

	@Override
	public void updateEdit(String dateissued, String depcode,
			String warrantnum,String newWarrNum, String fromstation, String tostation,
			String type, int fullnum, int halfnum, String seattype,
			String traveltypes,String transit,String transpoint,double tranuprice, int numpassengers, double unitcharge,
			double totcost, int finturn) {
		
		
		 SessionFactory  session = HibernateUtil.getSessionFactory();
		 
		 Session sess = session.getCurrentSession();
		 
		 Transaction tx = sess.beginTransaction();
		 
		 String hql = "UPDATE WarrentRevenueAndIssue SET issuedate=:issuDate,depcode=:depcode,warrantnumber=:newwarrnum," +
		              "fromstation=:fromStation,tostation=:toStation,type=:type,fulticket=:fultic," +
				      "halfticket=:halftick,reservetype=:restype,traveltype=:travtype,transit=:trans,tranplace=:transitplace,transituprice=:tranuprice,numpassenger=:numpass," +
		              "unitprice=:unitch,totalprice=:totcost,editturn=:fintur WHERE warrantnumber=:warrnum";
				
		 Query query = sess.createQuery(hql);
		 
		 query.setParameter("issuDate", dateissued);
		 query.setParameter("depcode", depcode);
		 query.setParameter("newwarrnum", newWarrNum);
		 query.setParameter("fromStation", fromstation);
		 query.setParameter("toStation", tostation);
		 query.setParameter("type", type);
		 query.setParameter("fultic", fullnum);
		 query.setParameter("halftick", halfnum);
		 query.setParameter("restype", seattype);
		 query.setParameter("travtype", traveltypes);
		 query.setParameter("trans", transit);
		 query.setParameter("transitplace", transpoint);
		 query.setParameter("tranuprice", tranuprice);
		 query.setParameter("numpass", numpassengers);
		 query.setParameter("unitch", unitcharge);
		 query.setParameter("totcost", totcost);
		 query.setParameter("fintur", finturn);
		 query.setParameter("warrnum", warrantnum);
		 
		 
		 query.executeUpdate();
		 
		 tx.commit();
		 session.close();
		 
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WarrentRevenueAndIssue> getFilteredList(String warrNum,String depNum,
			String startDate, String endDate) {
		
		List<WarrentRevenueAndIssue>  getList = new ArrayList<WarrentRevenueAndIssue>();
		
        SessionFactory  session = HibernateUtil.getSessionFactory();
        Session  sess = session.getCurrentSession();
        
        Transaction tx = sess.beginTransaction();
        
        String hsql = "FROM WarrentRevenueAndIssue WHERE warrantnumber=:warrnum OR depcode=:depNumber OR issuedate BETWEEN :startdate AND :enddate";
		
        Query  query = sess.createQuery(hsql);
        
        query.setParameter("warrnum", warrNum);
        query.setParameter("depNumber", depNum);
        query.setParameter("startdate", startDate);
        query.setParameter("enddate", endDate);
        
        getList = query.list();
        
        
        tx.commit();
        session.close();
        
		return getList;
	}
	
	

}
