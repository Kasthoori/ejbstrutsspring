/**
 * 
 */
package hibernate.service.car.warrents;

import hibernate.domain.car.warrents.EnterDepCodesForWarrents;

/**
 * @author A.P.Kasthoori
 *
 */
public interface EnterDepCodesForWarrentsDAO {
	
	public void saveDepCodes(EnterDepCodesForWarrents enterDepCodesForWarrents);

}
