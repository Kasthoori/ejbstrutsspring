/**
 * 
 */
package hibernate.service.car.warrents;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.car.warrents.EnterDepCodesForWarrents;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class EnterDepCodesForWarrentsDAOImpl implements EnterDepCodesForWarrentsDAO {

	public void saveDepCodes(EnterDepCodesForWarrents enterDepCodesForWarrents) {
		// TODO Auto-generated method stub
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session sess = session.getCurrentSession();
		
		Transaction tx = sess.beginTransaction();
		
		sess.save(enterDepCodesForWarrents);
		
		tx.commit();
		session.close();
		
	}

}
