package hibernate.service.Rml;

import java.util.List;

import hibernate.domain.Rml.RegisterWagon;

public interface RegisterWagonDAO {
	
	public void saveRegisterWagonDAO(RegisterWagon  registerWagon);
	public List<RegisterWagon>  getRegisterWagonList(RegisterWagon  registerWagon);

}
