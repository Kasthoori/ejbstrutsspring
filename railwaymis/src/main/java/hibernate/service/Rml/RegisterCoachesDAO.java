package hibernate.service.Rml;

import java.util.ArrayList;
import java.util.List;

import hibernate.domain.Rml.RegisterCoaches;

public interface RegisterCoachesDAO {
	
	public void saveRegisterCoachesDAO(RegisterCoaches   registerCoaches);
	public  List<RegisterCoaches>  getRegisterCoachesList(RegisterCoaches  registerCoaches);

}
