/**
 * 
 */
package hibernate.service.Rml;

import java.util.List;

import hibernate.domain.Rml.JobOrder;



/**
 * @author A.P.Kasthoori
 *
 */
public interface JobOrderDAO {
    
    public void saveJobOrderDAO(JobOrder jobOrder);
    public List<JobOrder>   listJobOrder(String currInsert);
    public void updateJobOrder(String currInsertUpdate, String previousInsert);

}
