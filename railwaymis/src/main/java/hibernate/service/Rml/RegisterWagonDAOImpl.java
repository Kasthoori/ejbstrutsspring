package hibernate.service.Rml;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.Rml.RegisterWagon;
import hibernate.utility.HibernateUtil;

public class RegisterWagonDAOImpl implements RegisterWagonDAO {
	
	 public void saveRegisterWagonDAO(RegisterWagon  registerWagon){
		 
		 	SessionFactory   session  = HibernateUtil.getSessionFactory();
		 	Session  sess = session.getCurrentSession();
		 	
		 	Transaction   transaction = sess.beginTransaction();
		 	sess.save(registerWagon);
		 	transaction.commit();
		 	session.close();
	 }
	 
	 public List<RegisterWagon>  getRegisterWagonList(RegisterWagon  registerWagon){
		 return null;
	 }

}
