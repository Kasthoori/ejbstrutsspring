package hibernate.service.Rml;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.Rml.RegOilTanker;
import hibernate.utility.HibernateUtil;

public class RegOilTankDAOImpl implements RegOilTankerDAO {
	
	public void saveRegOilTankDAO(RegOilTanker  regOilTanker){
		
			SessionFactory  session = HibernateUtil.getSessionFactory();
			Session  sess = session.getCurrentSession();
			
			Transaction  tx = sess.beginTransaction();
			
			sess.save(regOilTanker);
			tx.commit();
			session.close();
	}

}
