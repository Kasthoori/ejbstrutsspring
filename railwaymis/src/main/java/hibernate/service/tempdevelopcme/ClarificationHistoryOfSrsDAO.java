/**
 * 
 */
package hibernate.service.tempdevelopcme;

import hibernate.domain.tempdevelopcme.ClarificationHistoryOfSrs;

/**
 * @author A.P.Kasthoori
 *
 */
public interface ClarificationHistoryOfSrsDAO {
	
	public void saveHIstoryData(ClarificationHistoryOfSrs clarificationHistoryOfSrs);

}
