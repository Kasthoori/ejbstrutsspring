/**
 * 
 */
package hibernate.service.tempdevelopcme;

import hibernate.domain.tempdevelopcme.TecRecommondHistory;

/**
 * @author A.P.Kasthoori
 *
 */
public interface TecRecommondHistoryDAO {
	
	public void saveTecRecommondHistory(TecRecommondHistory  tecRecHis);
	

}
