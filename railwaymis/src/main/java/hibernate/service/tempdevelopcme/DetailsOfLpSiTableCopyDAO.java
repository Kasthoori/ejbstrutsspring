/**
 * 
 */
package hibernate.service.tempdevelopcme;

import java.util.List;

import hibernate.domain.tempdevelopcme.DetailsOfLpSiTableCopy;

/**
 * @author A.P.Kasthoori
 *
 */
public interface DetailsOfLpSiTableCopyDAO {
	
	public void saveDetailCopy(DetailsOfLpSiTableCopy  detailsOfLpSiTableCopy);
	public void updateDetailCopy(String section, String storeNum, String ordType, String siLpNum, 
			String lpSiDate, String fileNum, String fileDate, String fileRegDate, String fileRegUser, String fileRegRole);
	
	public List<DetailsOfLpSiTableCopy>  getDetailList(String user, String role);

}
