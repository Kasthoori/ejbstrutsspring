/**
 * 
 */
package hibernate.service.tempdevelopcme;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.tempdevelopcme.ConditionOfThirdPartyDecissionHistory;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class ConditionOfThirdPartyDecissionHistoryDAOImpl implements ConditionOfThirdPartyDecissionHistoryDAO {

	@Override
	public void saveThirdpartyConditionsHistory(
			ConditionOfThirdPartyDecissionHistory conditionOfThirdPartyDecissionHistory) {
		// TODO Auto-generated method stub
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		Transaction tx = sess.beginTransaction();
		
	
		sess.save(conditionOfThirdPartyDecissionHistory);
		
		tx.commit();
		session.close();
		
		
	}

}
