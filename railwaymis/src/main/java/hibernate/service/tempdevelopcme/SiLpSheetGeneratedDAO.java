/**
 * 
 */
package hibernate.service.tempdevelopcme;

import hibernate.domain.tempdevelopcme.SiLpSheetGenerated;

import java.util.List;

/**
 * @author A.P.Kasthoori
 *
 */
public interface SiLpSheetGeneratedDAO {
	
	public List<SiLpSheetGenerated> getSiLpSheetItems(String date);
	public List<SiLpSheetGenerated> getDetails(String reportNum);
}
