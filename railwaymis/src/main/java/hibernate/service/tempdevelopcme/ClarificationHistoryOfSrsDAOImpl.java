/**
 * 
 */
package hibernate.service.tempdevelopcme;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.tempdevelopcme.ClarificationHistoryOfSrs;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class ClarificationHistoryOfSrsDAOImpl implements ClarificationHistoryOfSrsDAO {

	@Override
	public void saveHIstoryData(
			ClarificationHistoryOfSrs clarificationHistoryOfSrs) {
		// TODO Auto-generated method stub
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		Transaction tx = sess.beginTransaction();
		
		
		sess.save(clarificationHistoryOfSrs);
		
		tx.commit();
		session.close();
		
	}

}
