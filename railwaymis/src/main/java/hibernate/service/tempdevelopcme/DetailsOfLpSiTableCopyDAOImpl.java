/**
 * 
 */
package hibernate.service.tempdevelopcme;





import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.tempdevelopcme.DetailsOfLpSiTableCopy;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class DetailsOfLpSiTableCopyDAOImpl implements DetailsOfLpSiTableCopyDAO {

	public void saveDetailCopy(DetailsOfLpSiTableCopy detailsOfLpSiTableCopy) {
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		
		Session  sess = session.getCurrentSession();
		
		Transaction tx = sess.beginTransaction();
		
		sess.save(detailsOfLpSiTableCopy);
		
		sess.flush();
		sess.clear();
		tx.commit();
		session.close();
		
	}

	public void updateDetailCopy(String section, String storeNum,
			String ordType, String siLpNum, String lpSiDate, String fileNum,
			String fileDate, String fileRegDate, String fileRegUser,
			String fileRegRole) {
		
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		
		Session  sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		String hsql = "UPDATE DetailsOfLpSiTableCopy SET section=:section, storenumber=:storeNum," + 
		             "ordertype=:ordType, silpnum=:siLpNum, lpsidate=:lpSiDate," + 
				     "filenum=:fileNum,filedate=:fileDate,fileregdate=:fileRegDate WHERE filereguser=:fileRegUser AND fileregrole=:fileRegRole";
		
		Query  query = sess.createQuery(hsql);
		
		
		query.setParameter("section", section);
		query.setParameter("storeNum", storeNum);
		query.setParameter("ordType", ordType);
		query.setParameter("siLpNum", siLpNum);
		query.setParameter("lpSiDate", lpSiDate);
		query.setParameter("fileNum", fileNum);
		query.setParameter("fileDate", fileDate);
		query.setParameter("fileRegDate", fileRegDate);
		query.setParameter("fileRegUser", fileRegUser);
		query.setParameter("fileRegRole", fileRegRole);
		
		query.executeUpdate();
		
		sess.flush();
		sess.clear();
		tx.commit();
		session.close();
		
	}

	@SuppressWarnings("unchecked")
	public List<DetailsOfLpSiTableCopy> getDetailList(String user, String role) {
		
		List<DetailsOfLpSiTableCopy>  getList = new ArrayList<DetailsOfLpSiTableCopy>();
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		
		Session  sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		getList = sess.createQuery("from DetailsOfLpSiTableCopy where filereguser='" + user + "' and fileregrole='" + role + "'").list();
		
		sess.flush();
		sess.clear();
		tx.commit();
		session.close();
		
		
		return getList;
		
		
		
	}
	
	

}
