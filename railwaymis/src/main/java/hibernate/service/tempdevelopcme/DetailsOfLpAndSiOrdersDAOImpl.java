/**
 * 
 */
package hibernate.service.tempdevelopcme;



import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import hibernate.domain.srs.InventoryItemsMainLedger;
import hibernate.domain.srs.itemissuing.StockItemsStoresMainLedgIssuing;
import hibernate.domain.tempdevelopcme.DetailsOfLpAndSiOrders;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class DetailsOfLpAndSiOrdersDAOImpl implements DetailsOfLpAndSiOrdersDAO {

	public void registerLpAndSiOrders(
			DetailsOfLpAndSiOrders detailsOfLpAndSiOrders) {
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		
		Session sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		sess.save(detailsOfLpAndSiOrders);
	
		
		tx.commit();
		session.close();
		
	}

	public void updateRegisterFile(String fileNum, String sStoreNumber,
			String date, String user, String role, String dateTime) {
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction tx = sess.beginTransaction();
		
		String transCond = "false";
		
		String hql = "UPDATE DetailsOfLpAndSiOrders set tecdate=:tecDate,usertecdate=:tecUser,roletecdate=:tecRole,datetimetecdate=:dateTimeTec WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
		
		Query query = sess.createQuery(hql);
		query.setParameter("tecDate", date);
		query.setParameter("tecUser", user);
		query.setParameter("tecRole", role);
		query.setParameter("dateTimeTec", dateTime);
		query.setParameter("StoreNum", sStoreNumber);
		query.setParameter("fileNum", fileNum);
		query.setParameter("tranCond",transCond);
		
		query.executeUpdate();
		
	
		
		tx.commit();
		session.close();
		
		
	}

	public void updateTecRecommondation(String fileNum, String sStoreNumber,
			String tecRec,int tecRound,String teccond, String tecRecGivDate, String tecRecUser,
			String tecRecRole, String tecRecDaTime, String tranCond, String bidSelect,String nameSuccBidder,String upRemarks,double quantity, String units,double orderQuan,String orderUnit, double uPrice, String currency, double totalvalue) {
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction tx = sess.beginTransaction();
		
		String transCond = "false";
		
		
		String hql = "UPDATE DetailsOfLpAndSiOrders set tecrecommend=:tecRecommend,tecround=:tecround,teccond=:tecCon,tecrecgivendate=:givenDate," +
		"usertecrec=:userTecRec,roletecrec=:roleTecRec,datetimetecrec=:dateTimeTecRec, transcondition=:tranCond,bidselect=:bidSelect,namesuccbider=:nameSuccBidder,remarks=:upRemarks," + 
				"quantity=:Quan, units=:units,orderedquantity=:orderQuan,orderedunit=:orderUnit, unitprice=:uPrice, currency=:Curr, totalvalue=:toValue WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:transCond";
		
		Query query = sess.createQuery(hql);
		
		
		query.setParameter("tecRecommend", tecRec);
		query.setParameter("tecround", tecRound);
		query.setParameter("tecCon", teccond);
		query.setParameter("givenDate", tecRecGivDate);
		query.setParameter("userTecRec",tecRecUser);
		query.setParameter("roleTecRec", tecRecRole);
		query.setParameter("dateTimeTecRec", tecRecDaTime);
		query.setParameter("StoreNum", sStoreNumber);
		query.setParameter("fileNum", fileNum);
		query.setParameter("transCond",transCond);
		query.setParameter("tranCond", tranCond);
		query.setParameter("bidSelect", bidSelect);
		query.setParameter("nameSuccBidder", nameSuccBidder);
		query.setParameter("upRemarks", upRemarks);
		query.setParameter("Quan",quantity);
		query.setParameter("units",units);
		query.setParameter("orderQuan", orderQuan);
		query.setParameter("orderUnit", orderUnit);
		query.setParameter("uPrice", uPrice);
		query.setParameter("Curr",currency);
		query.setParameter("toValue", totalvalue);
		
		query.executeUpdate();
		
	
		
		tx.commit();
		session.close();
	}

	public void updateNameSuccBidder(String fileNum, String sStoreNumber,
			String nameSuccBidder, String rem) {
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		String transCond = "false";
		String upRem = "Items are successfull";
		
		String hql = "UPDATE DetailsOfLpAndSiOrders set namesuccbider=:nameSuccBidder, remarks=:upRemark WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
		
		Query query = sess.createQuery(hql);
		
		query.setParameter("nameSuccBidder", nameSuccBidder);
		query.setParameter("upRemark", rem);
		query.setParameter("StoreNum", sStoreNumber);
		query.setParameter("fileNum", fileNum);
		query.setParameter("tranCond",transCond);
		
		
		query.executeUpdate();
		

	
		tx.commit();
		session.close();
		
	}

	public void updateRemarks(String fileNum, String sStoreNumber,
			String upRemarks) {
		
		SessionFactory session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction tx = sess.beginTransaction();
		
		String transCond = "false";
		
		String hql = "UPDATE DetailsOfLpAndSiOrders set remarks=:upRem WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
		
		Query query = sess.createQuery(hql);
		
		query.setParameter("upRem", upRemarks);
		query.setParameter("StoreNum", sStoreNumber);
		query.setParameter("fileNum", fileNum);
		query.setParameter("tranCond",transCond);
		
		
		query.executeUpdate();
		
		
	
		tx.commit();
		session.close();
		
	}

	public void updateDates(String fileNum, String sStoreNumber,
			String selectDate,String dateForEvent, String invoiceNum, String userLupDate,
			String roleLuDate, String datTimLupDate) {
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		String transCond = "false";
		
		String invoiceNumber = "";
		
		/*if(invoiceNum.isEmpty()) {
			
			invoiceNumber = "Not processed";
			
		}else {
			
			invoiceNumber = invoiceNum;
		}
		*/
		
		if(selectDate.matches("PCM date")) {
			
			
			
			
			String hql1 = "UPDATE DetailsOfLpAndSiOrders set pcmdate=:pcmDate,userdatelupda=:userLaUp," + 
			"roledatelupda=:roleLaUp, datetimelupda=:dateTimeLaUp WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
			
			Query query = sess.createQuery(hql1);
			
			
			
			query.setParameter("pcmDate",dateForEvent);
			//query.setParameter("invoiceNum", invoiceNumber);
			query.setParameter("userLaUp", userLupDate);
			query.setParameter("roleLaUp", roleLuDate);
			query.setParameter("dateTimeLaUp",datTimLupDate);
			query.setParameter("StoreNum", sStoreNumber);
			query.setParameter("fileNum", fileNum);
			query.setParameter("tranCond",transCond);
			
			query.executeUpdate();
			
		}else if(selectDate.matches("Inspection Report date")) {
			
            String hql2 = "UPDATE DetailsOfLpAndSiOrders set inspectrepdate=:insRepDate, userdatelupda=:userLaUp," +  
            		       "roledatelupda=:roleLaUp, datetimelupda=:dateTimeLaUp WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
			
			Query query = sess.createQuery(hql2);
			
			query.setParameter("insRepDate",dateForEvent);
			//query.setParameter("invoiceNum", invoiceNumber);
			query.setParameter("userLaUp", userLupDate);
			query.setParameter("roleLaUp", roleLuDate);
			query.setParameter("dateTimeLaUp",datTimLupDate);
			query.setParameter("StoreNum", sStoreNumber);
			query.setParameter("fileNum", fileNum);
			query.setParameter("tranCond",transCond);
			
			query.executeUpdate();
			
		}else if(selectDate.matches("Order Place date")) {
			
			    String hql3 = "UPDATE DetailsOfLpAndSiOrders set ordplacedate=:orderPlace, userdatelupda=:userLaUp," +  
			    				"roledatelupda=:roleLaUp, datetimelupda=:dateTimeLaUp WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
				
				Query query = sess.createQuery(hql3);
				
				query.setParameter("orderPlace",dateForEvent);
				//query.setParameter("invoiceNum", invoiceNumber);
				query.setParameter("userLaUp", userLupDate);
				query.setParameter("roleLaUp", roleLuDate);
				query.setParameter("dateTimeLaUp",datTimLupDate);
				query.setParameter("StoreNum", sStoreNumber);
				query.setParameter("fileNum", fileNum);
				query.setParameter("tranCond",transCond);
				
				query.executeUpdate();
				
		}else if(selectDate.matches("LC Opening date")) {
			
			 String hql4 = "UPDATE DetailsOfLpAndSiOrders set lcopendate=:lcOpenDate, userdatelupda=:userLaUp," +  
			 					"roledatelupda=:roleLaUp, datetimelupda=:dateTimeLaUp WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
				
				Query query = sess.createQuery(hql4);
				
				query.setParameter("lcOpenDate",dateForEvent);
				//query.setParameter("invoiceNum", invoiceNumber);
				query.setParameter("userLaUp", userLupDate);
				query.setParameter("roleLaUp", roleLuDate);
				query.setParameter("dateTimeLaUp",datTimLupDate);
				query.setParameter("StoreNum", sStoreNumber);
				query.setParameter("fileNum", fileNum);
				query.setParameter("tranCond",transCond);
				
				query.executeUpdate();
				
		}else if(selectDate.matches("Item Recived date")) {
			
			String hql5 = "UPDATE DetailsOfLpAndSiOrders set itemrecievdate=:itemRecievDate, invoicenum=:invoiceNum, userdatelupda=:userLaUp," +  
								"roledatelupda=:roleLaUp, datetimelupda=:dateTimeLaUp WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
			
			Query query = sess.createQuery(hql5);
			
			query.setParameter("itemRecievDate",dateForEvent);
			query.setParameter("invoiceNum", invoiceNum);
			query.setParameter("userLaUp", userLupDate);
			query.setParameter("roleLaUp", roleLuDate);
			query.setParameter("dateTimeLaUp",datTimLupDate);
			query.setParameter("StoreNum", sStoreNumber);
			query.setParameter("fileNum", fileNum);
			query.setParameter("tranCond",transCond);
			
			query.executeUpdate();
			
		}else {
			
          String hql6 = "UPDATE DetailsOfLpAndSiOrders set suitrepdate=:suitRepDate, userdatelupda=:userLaUp," + 
          					"roledatelupda=:roleLaUp, datetimelupda=:dateTimeLaUp WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
			
			Query query = sess.createQuery(hql6);
			
			query.setParameter("suitRepDate",dateForEvent);
			//query.setParameter("invoiceNum", invoiceNumber);
			query.setParameter("userLaUp", userLupDate);
			query.setParameter("roleLaUp", roleLuDate);
			query.setParameter("dateTimeLaUp",datTimLupDate);
			query.setParameter("StoreNum", sStoreNumber);
			query.setParameter("fileNum", fileNum);
			query.setParameter("tranCond",transCond);
			
			query.executeUpdate();
		}
		
	
		
		tx.commit();
		session.close();
	}

	public void updatePcmApproval(String fileNum, String sStoreNumber,
			String pcmApp,String pcmGuide, String userPcmApp, String rolePcmApp,
			String dateTimPcmApp) {
         
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		String transCond = "false";
		
		String hql = "UPDATE DetailsOfLpAndSiOrders set pcmapp=:pcmApp,pcmguidelines=:pcmGuideLine,userpcmapp=:userPcmApp,rolepcmapp=:rolePcmApp," +
		"datetimepcmapp=:dateTimePcmApp WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
		
		Query query = sess.createQuery(hql);
		
		
		query.setParameter("pcmApp", pcmApp);
		query.setParameter("userPcmApp", userPcmApp);
		query.setParameter("pcmGuideLine", pcmGuide);
		query.setParameter("rolePcmApp",rolePcmApp);
		query.setParameter("dateTimePcmApp",dateTimPcmApp);
		query.setParameter("StoreNum", sStoreNumber);
		query.setParameter("fileNum", fileNum);
		query.setParameter("tranCond",transCond);
		
		
		query.executeUpdate();
		
	
		
		tx.commit();
		session.close();
		
	}

public void updateSuitability(String fileNum, String sStoreNumber,
			String suit, String reason, String amount, String damamount,
			String suitGivDate, String userSuit, String roleSuit,
			String dateTimeSuit) {
		
		SessionFactory  session  = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		String transCond = "false";
		
		if(suit.matches("Suitable")) {
			
			String hql = "UPDATE DetailsOfLpAndSiOrders set suitability=:suit," +
			"suitgivendate=:suitGivDate, usersuit=:userSuit,rolesuit=:roleSuit,datetimesuit=:dateTimeSuit WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
			
			Query query = sess.createQuery(hql);
			
			query.setParameter("suit", suit);
			query.setParameter("suitGivDate", suitGivDate);
			query.setParameter("userSuit", userSuit);
			query.setParameter("roleSuit", roleSuit);
			query.setParameter("dateTimeSuit", dateTimeSuit);
			query.setParameter("StoreNum", sStoreNumber);
			query.setParameter("fileNum", fileNum);
			query.setParameter("tranCond",transCond);
			
			
			query.executeUpdate();
			
		}else if(suit.matches("Not Suitable")) {
			
			String hql1 = "UPDATE DetailsOfLpAndSiOrders set suitability=:suit,reason=:Reason," +
			"suitgivendate=:suitGivDate, usersuit=:userSuit,rolesuit=:roleSuit,datetimesuit=:dateTimeSuit WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
			
			Query query = sess.createQuery(hql1);
			
			query.setParameter("suit", suit);
			query.setParameter("Reason", reason);
			query.setParameter("suitGivDate", suitGivDate);
			query.setParameter("userSuit", userSuit);
			query.setParameter("roleSuit", roleSuit);
			query.setParameter("dateTimeSuit", dateTimeSuit);
			query.setParameter("StoreNum", sStoreNumber);
			query.setParameter("fileNum", fileNum);
			query.setParameter("tranCond",transCond);
			
			
			query.executeUpdate();
			
		}else if(suit.matches("Short Fall")) {
			
			String hql2 = "UPDATE DetailsOfLpAndSiOrders set suitability=:suit, shortfall=:shortFall," +
					"suitgivendate=:suitGivDate, usersuit=:userSuit,rolesuit=:roleSuit,datetimesuit=:dateTimeSuit WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
			
			
             Query query = sess.createQuery(hql2);
			
			query.setParameter("suit", suit);
			query.setParameter("shortFall",amount);
			query.setParameter("suitGivDate", suitGivDate);
			query.setParameter("userSuit", userSuit);
			query.setParameter("roleSuit", roleSuit);
			query.setParameter("dateTimeSuit", dateTimeSuit);
			query.setParameter("StoreNum", sStoreNumber);
			query.setParameter("fileNum", fileNum);
			query.setParameter("tranCond",transCond);
			
			
			query.executeUpdate();
			
		}else {
			
			String hql3 = "UPDATE DetailsOfLpAndSiOrders set suitability=:suit, damaged=:Dam," +
					"suitgivendate=:suitGivDate, usersuit=:userSuit,rolesuit=:roleSuit,datetimesuit=:dateTimeSuit WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
			
			
            Query query = sess.createQuery(hql3);
			
			query.setParameter("suit", suit);
			query.setParameter("Dam",damamount);
			query.setParameter("suitGivDate", suitGivDate);
			query.setParameter("userSuit", userSuit);
			query.setParameter("roleSuit", roleSuit);
			query.setParameter("dateTimeSuit", dateTimeSuit);
			query.setParameter("StoreNum", sStoreNumber);
			query.setParameter("fileNum", fileNum);
			query.setParameter("tranCond",transCond);
			
			
			query.executeUpdate();
		}
		
		
		
		tx.commit();
		session.close();
		
	}

@SuppressWarnings("unchecked")
public List<DetailsOfLpAndSiOrders> getDetailList(String fileNum, String sStoreNumber) {
	
	List<DetailsOfLpAndSiOrders>  getDetailList = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	Transaction  tx = sess.beginTransaction();
	
	String tranCond = "false";
	
	getDetailList = sess.createQuery("from DetailsOfLpAndSiOrders d where d.filenum='" + fileNum + "' and d.storenumber='" + sStoreNumber + "' and d.transcondition='" + tranCond + "'").list();
	

	
	tx.commit();
	session.close(); 
	
	return getDetailList;
	
	
}

@SuppressWarnings("unchecked")
public List<DetailsOfLpAndSiOrders> getFilteredList(String section,
		String startdate, String enddate) {
	
	List<DetailsOfLpAndSiOrders>  getFilteredList = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	Transaction  tx = sess.beginTransaction();
	
	String tranCond = "false";
	
	//String date1 = "17-Mar-2013";
	//String date2 = "19-Mar-2013";
	
    //SimpleDateFormat  sdt = new SimpleDateFormat("yyyy-MM-dd");
	//String date1 = sdt.format(startdate);
	//String date2 = sdt.format(enddate);
	
	//String hsql = "FROM DetailsOfLpAndSiOrders WHERE section like :section AND filedate BETWEEN :date1 AND :date2";
	String hsql = "FROM DetailsOfLpAndSiOrders WHERE newfiledate BETWEEN :startdate AND :enddate AND section=:section AND transcondition=:tranCond";
	
	Query  query = sess.createQuery(hsql);
	query.setParameter("section", section);
	
	query.setParameter("startdate", startdate);
	query.setParameter("enddate", enddate);
	query.setParameter("tranCond", tranCond);
	getFilteredList = query.list();
	
	
	
	
	//getFilteredList = sess.createQuery("from DetailsOfLpAndSiOrders d where d.section='" + section + "' and d.filedate between '" + date1 + "' and '" + date2 + "'").list();
	

	
	tx.commit();
	session.close();
	
	return getFilteredList;
}

@SuppressWarnings("unchecked")
public List<DetailsOfLpAndSiOrders> getFileList(String storeNum, String section) {
     
	List<DetailsOfLpAndSiOrders>  getList = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	Transaction  tx = sess.beginTransaction();
	
	getList = sess.createQuery("FROM DetailsOfLpAndSiOrders  WHERE section LIKE '%" + section + "%' AND storenumber LIKE '%" + storeNum + "%'").list();
	
	


	tx.commit();
	
	session.close();
	
	return getList;
}

public void updatePcmQuantityApproval(String fileNum, String sStoreNumber,
		String pcmApp,String pcmGuides, String userPcmApp, String rolePcmApp,
		String dateTimPcmApp, double orderQuan,
		double pcmQuantity, String pcmUnits, double totalValue, String quanPcmDecision) {
	// TODO Auto-generated method stub
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	Transaction  tx = sess.beginTransaction();
	
	String tranCond = "false";
	
	String hql = "UPDATE DetailsOfLpAndSiOrders set pcmapp=:pcmApp,pcmguidelines=:pcmGuideLine,userpcmapp=:userPcmApp,rolepcmapp=:rolePcmApp," +
			"datetimepcmapp=:dateTimePcmApp,quanoldtec=:orderQuan,orderedquantity=:pcmQuantity, orderedunit=:pcmUnits,quanpcmdecision=:quanPcmDecision,totalvalue=:totalValue WHERE storenumber=:StoreNum AND filenum=:fileNum AND transcondition=:tranCond";
			
			Query query = sess.createQuery(hql);
			
			
			query.setParameter("pcmApp", pcmApp);
			query.setParameter("pcmGuideLine", pcmGuides);
			query.setParameter("userPcmApp", userPcmApp);
			query.setParameter("rolePcmApp",rolePcmApp);
			query.setParameter("dateTimePcmApp",dateTimPcmApp);
			query.setParameter("orderQuan", orderQuan);
			query.setParameter("pcmQuantity", pcmQuantity);
			query.setParameter("pcmUnits", pcmUnits);
			query.setParameter("quanPcmDecision", quanPcmDecision);
			query.setParameter("totalValue", totalValue);
			query.setParameter("StoreNum", sStoreNumber);
			query.setParameter("fileNum", fileNum);
			query.setParameter("tranCond", tranCond);
			
			
		
			
			
			query.executeUpdate();
			
		
			tx.commit();
			session.close();
			
	
}

@Override
public void updateSrsClarifications(String StoreNumber, String FileNumber,
		String tecRec, int tecRound, String tecRecGivDate, String tecRecUser,
		String tecRecRole, String tecRecDaTime, String tranCond,
		String nameSuccBidder, String upRemarks, double quantity, String units,
		double orderQuan, String orderUnit, double uPrice, String currency,
		double totalvalue) {
	// TODO Auto-generated method stub
	
	SessionFactory   session  =  HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	Transaction  tx  = sess.beginTransaction();
	
	String hsql = "Update DetailsOfLpAndSiOrders set tecrecommend=:tecRecommend,tecround=:tecround,tecrecgivendate=:givenDate," +
		"usertecrec=:userTecRec,roletecrec=:roleTecRec,datetimetecrec=:dateTimeTecRec, transcondition=:tranCond,namesuccbider=:nameSuccBidder,remarks=:upRemarks," + 
				"quantity=:Quan, units=:units,orderedquantity=:orderQuan,orderedunit=:orderUnit, unitprice=:uPrice, currency=:Curr, totalvalue=:toValue WHERE storenumber=:StoreNum AND filenum=:fileNum"; 
	
	Query query = sess.createQuery(hsql);
	
	query.setParameter("tecRecommend", tecRec);
	query.setParameter("tecround", tecRound);
	query.setParameter("givenDate",tecRecGivDate);
	query.setParameter("userTecRec", tecRecUser);
	query.setParameter("roleTecRec", tecRecRole);
	query.setParameter("dateTimeTecRec", tecRecDaTime);
	query.setParameter("tranCond", tranCond);
	query.setParameter("nameSuccBidder", nameSuccBidder);
	query.setParameter("upRemarks", upRemarks);
	query.setParameter("Quan", quantity);
	query.setParameter("units", units);
	query.setParameter("orderQuan", orderQuan);
	query.setParameter("orderUnit", orderUnit);
	query.setParameter("uPrice", uPrice);
	query.setParameter("Curr", currency);
	query.setParameter("toValue", totalvalue);
	query.setParameter("StoreNum", StoreNumber);
	query.setParameter("fileNum", FileNumber);
	
	
	
	query.executeUpdate();
	
	
	
	tx.commit();
	session.close();
	
}

@Override
public void thirdPartyInspectionReportUpdate(String StoreNum, String FileNum,String tecRecommend,
		String ThirdParyInspec, String userThirdEnterd, String RoleEntered,
		String datetime) {
	// TODO Auto-generated method stub
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session sess = session.getCurrentSession();
	Transaction tx = sess.beginTransaction();
	
	
	String hsql = "Update DetailsOfLpAndSiOrders set condthirdpartyinspect=:conditionThirdPartyIns,tecrecommend=:tecRec,userthirdpartyentered=:userThirdPEntered," +
			"rolethirdpartyentered=:roleThirdPEntered,timestampentered=:timeStamp where storenumber=:storeNum and filenum=:fileNum";
	
	
	Query  query = sess.createQuery(hsql);
	
	query.setParameter("tecRec", tecRecommend);
	query.setParameter("conditionThirdPartyIns",ThirdParyInspec);
	query.setParameter("userThirdPEntered", userThirdEnterd);
	query.setParameter("roleThirdPEntered", RoleEntered);
	query.setParameter("timeStamp", datetime);
	query.setParameter("storeNum", StoreNum);
	query.setParameter("fileNum", FileNum);
	
	query.executeUpdate();
	

	
	tx.commit();
	session.close();
	
}



@SuppressWarnings("unchecked")
public List<DetailsOfLpAndSiOrders> getFilesDetails(String filenum, String startDate, String endDate) {
	// TODO Auto-generated method stub
	
	String trancomp = "F";
	
	List<DetailsOfLpAndSiOrders>  getFilesList = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	Transaction  tx = sess.beginTransaction();
	
	//getFilesList = sess.createSQLQuery("SELECT * FROM details_lp_si_table WHERE file_num LIKE '%" + filenum + "%' AND new_file_date BETWEEN '" + startDate + "' AND '" + endDate + "'").list();
	
	//getFilesList = sess.createQuery("FROM DetailsOfLpAndSiOrders WHERE filenum LIKE '%" + filenum + "%' AND newfiledate BETWEEN '" + startDate + "' AND '" + endDate + "' group by filenum").list();
	
    Query query = sess.createQuery("from DetailsOfLpAndSiOrders where (filenum like :fileNum) and (newfiledate between :start and :end and transcomplete='" + trancomp + "') group by filenum");
	
	
	query.setParameter("fileNum","%" + filenum + "%");
	query.setParameter("start",startDate);
	query.setParameter("end",endDate);
	
	getFilesList = query.list();
	
	
	
    tx.commit();
    session.close();
	
	return getFilesList;
	
	
	
	
	
}

@SuppressWarnings("unchecked")
@Override
public List<DetailsOfLpAndSiOrders> getStockItemsOfFiles(String fileNum) {
	// TODO Auto-generated method stub
	
	String trancomp = "F";
	
	List<DetailsOfLpAndSiOrders>  getStockItemsDetails = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory   session =  HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	Transaction  tx  =  sess.beginTransaction();
	
	getStockItemsDetails = sess.createQuery("FROM DetailsOfLpAndSiOrders WHERE filenum LIKE '%" + fileNum + "%' AND transcomplete='" + trancomp + "' GROUP BY filenum").list();
	
	
	tx.commit();
	session.close();
	
	return getStockItemsDetails;
}

@SuppressWarnings("unchecked")
@Override
public List<DetailsOfLpAndSiOrders> getAllDetailsOfStockItemsOfFiles(
		String storeNum, String fileNum) {
	// TODO Auto-generated method stub
	
	String trancomp = "F";
	
	List<DetailsOfLpAndSiOrders>  getAllDetails = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory  session  =  HibernateUtil.getSessionFactory();
	Session  sess  =  session.getCurrentSession();
	
	Transaction  tx  =  sess.beginTransaction();
	
	
	getAllDetails = sess.createQuery("FROM DetailsOfLpAndSiOrders WHERE storenumber='" + storeNum + "' AND filenum='" + fileNum + "' AND transcomplete='" + trancomp + "'").list();
	
	
	

	tx.commit();
	session.close();
	
	return getAllDetails;
}

@SuppressWarnings("unchecked")
public List<InventoryItemsMainLedger> getInventItemsDetails(String storeNum) {
	// TODO Auto-generated method stub
	
	List<InventoryItemsMainLedger>  getItemDes = new ArrayList<InventoryItemsMainLedger>();
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	Transaction  tx  =  sess.beginTransaction();
	
	getItemDes = sess.createQuery("FROM InventoryItemsMainLedger WHERE storenumber='" + storeNum + "'").list();
	
	

	tx.commit();
	session.close();
	
	return getItemDes;
}

@SuppressWarnings("unchecked")
@Override
public List<DetailsOfLpAndSiOrders> getOutstanding(String storeNum) {
	// TODO Auto-generated method stub
	String trancomp = "F";
	
	List<DetailsOfLpAndSiOrders> getOutStands = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	
	Session  sess = session.getCurrentSession();
	
	Transaction  tx = sess.beginTransaction();
	
	getOutStands = sess.createQuery("from DetailsOfLpAndSiOrders where storenumber like '%" +  storeNum + "%' and transcomplete='" + trancomp + "' group by filenum").list();
	
	tx.commit();
	session.close();
	
	return getOutStands;
}

@SuppressWarnings("unchecked")
@Override
public List<DetailsOfLpAndSiOrders> getStockItemDetailsInFiles(String fileNum) {
	// TODO Auto-generated method stub
	
	String trancomp = "F";
	
List<DetailsOfLpAndSiOrders> getStocDetInFiles = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	
	Session  sess = session.getCurrentSession();
	
	Transaction  tx = sess.beginTransaction();
	
	getStocDetInFiles = sess.createQuery("from DetailsOfLpAndSiOrders where filenum='" +  fileNum + "' and transcomplete='" + trancomp + "'").list();
	
	tx.commit();
	session.close();
	
	return getStocDetInFiles;
}

@SuppressWarnings("unchecked")
@Override
public List<DetailsOfLpAndSiOrders> getOutstandingKnownStockItem(String storeNum) {
	// TODO Auto-generated method stub
	
	String trancomp = "F";
	
List<DetailsOfLpAndSiOrders> getOutStandsKnownItems = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	
	Session  sess = session.getCurrentSession();
	
	Transaction  tx = sess.beginTransaction();
	
	getOutStandsKnownItems = sess.createQuery("from DetailsOfLpAndSiOrders where storenumber='" +  storeNum + "' and transcomplete='" + trancomp + "' group by filenum").list();
	
	tx.commit();
	session.close();
	return getOutStandsKnownItems;
}



@SuppressWarnings("unchecked")
@Override
public List<DetailsOfLpAndSiOrders> getSearchStockItemDetails(String fileNum,
		String StoreNum) {
	
	String trancomp = "F";
	
	List<DetailsOfLpAndSiOrders> getDetails = new ArrayList<DetailsOfLpAndSiOrders>();
		

	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session sess = session.getCurrentSession();
	
	Transaction tx = sess.beginTransaction();
	
	getDetails = sess.createQuery("from DetailsOfLpAndSiOrders where filenum='" +  fileNum + "' and storenumber='" + StoreNum + "' and transcomplete='" + trancomp + "'").list();
	
	
	tx.commit();
	session.close();
	
	return getDetails;
}


@SuppressWarnings("unchecked")
public List<DetailsOfLpAndSiOrders> getSiLpConditionDetailsTec(
		String startSearch, String endSearch, String type, String condtec) {
	
	List<DetailsOfLpAndSiOrders> getList = new ArrayList<DetailsOfLpAndSiOrders>();
	
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	Transaction tx = sess.beginTransaction();
	
	
	/*String sql = "select d.section, d.file_num, d.file_date, d.tec_date from details_lp_si_table d where d.store_num like :storeNum and d.tec_cond=:tec and d.file_date between :start and :end";
	
	Query query = sess.createSQLQuery(sql)
	.addEntity(DetailsOfLpAndSiOrders.class)
	.setParameter("storeNum",type)
	.setParameter("tec", condtec)
	.setParameter("start", startSearch)
	.setParameter("end", endSearch);
	
	getList = query.list();*/
	String trancomp = "F";
	
	// = sess.createSQLQuery("SELECT section, file_num, file_date, tec_date FROM details_lp_si_table WHERE store_num LIKE '" + type + "%'  AND tec_cond='" + condtec + "' AND file_date BETWEEN '" + startSearch + "' AND '" + endSearch + "'").list();
	//getList = sess.createSQLQuery("SELECT section, file_num, file_date, tec_date FROM details_lp_si_table").list();
	String hsql = "from DetailsOfLpAndSiOrders  where (filenum like :fileNum and teccond=:tecCond) and (newfiledate between :start and :end)  and (transcomplete='" + trancomp + "') group by filenum";
	
	Query query = sess.createQuery(hsql);

	
	query.setParameter("fileNum", type + "%");
	query.setParameter("tecCond", condtec);
	query.setParameter("start", startSearch);
	query.setParameter("end", endSearch);
	
	getList = query.list();
	
	
	
	tx.commit();
	session.close();
	
	return getList;
}

@SuppressWarnings("unchecked")
@Override
public List<DetailsOfLpAndSiOrders> getSiLpConditionDetailsPcm(
		String startSearch, String endSearch, String type, String condpcm) {
	
List<DetailsOfLpAndSiOrders> getList = new ArrayList<DetailsOfLpAndSiOrders>();
	
     String trancomp = "F";	
     String cond = "T";

	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	Transaction tx = sess.beginTransaction();
	
	String hsql = "from DetailsOfLpAndSiOrders  where (filenum like :fileNum and pcmapp=:pcmCond) and (newfiledate between :start and :end)  and (transcomplete='" + trancomp + "' and teccond='" + cond + "') group by filenum";
	
	Query query = sess.createQuery(hsql);
	
	query.setParameter("fileNum", type + "%");
	query.setParameter("pcmCond", condpcm);
	query.setParameter("start", startSearch);
	query.setParameter("end", endSearch);
	
	getList = query.list();


	/*Criteria  ctr = sess.createCriteria(DetailsOfLpAndSiOrders.class);
	
	Criterion filename = Restrictions.like("filenum", type + "%");
	Criterion pcmcon = Restrictions.eq("pcmapp",condpcm);
	
	Criterion filedate = Restrictions.between("filedate",startSearch , endSearch);
	Criterion trancom = Restrictions.eq("transcomplete", trancomp);
	
   
	LogicalExpression andEx1 = Restrictions.and(filename, pcmcon);
	
	
	LogicalExpression andEx2 = Restrictions.and(filedate, trancom);
	

	
	
	
	    ProjectionList projList = Projections.projectionList(); 
		
	    projList.add(Projections.groupProperty("filenum"));
	    ctr.add(Restrictions.and(andEx1, andEx2)).setProjection(projList);
	
	getList = ctr.list();*/
	
	tx.commit();
	session.close();
	
	return getList;
	
	
}

@Override
public List<DetailsOfLpAndSiOrders> getSiLpConditionDetailsLc(
		String startSearch, String endSearch, String type, String condlc) {
	List<DetailsOfLpAndSiOrders> getList = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	String trancomp = "F";	
    String cond = "T";
    String condpcm = "Approved";
	
	Transaction tx = sess.beginTransaction();
	
	String hsql = "from DetailsOfLpAndSiOrders  where (filenum like :fileNum and pcmapp='" + condpcm + "') and (lcopen=:lcOpen and newfiledate between :start and :end)  and (transcomplete='" + trancomp + "' and teccond='" + cond + "') group by filenum";
	
	Query query = sess.createQuery(hsql);
	
	query.setParameter("fileNum", type + "%");
	query.setParameter("lcOpen", condlc);
	query.setParameter("start", startSearch);
	query.setParameter("end", endSearch);
	
	getList = query.list();
	
	tx.commit();
	session.close();
	
	return getList;
}

@Override
public List<DetailsOfLpAndSiOrders> getSiLpConditionDetailsAll(
		String startSearch, String endSearch, String type) {
List<DetailsOfLpAndSiOrders> getList = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	Transaction tx = sess.beginTransaction();
	
	String hsql = "from DetailsOfLpAndSiOrders  where (filenum like :fileNum) and (newfiledate between :start and :end) group by filenum";
	
	Query query = sess.createQuery(hsql);
	
	query.setParameter("fileNum", type + "%");
	query.setParameter("start", startSearch);
	query.setParameter("end", endSearch);
	
	getList = query.list();
	
	tx.commit();
	session.close();
	
	return getList;
}

@Override
public List<DetailsOfLpAndSiOrders> getSiLpConditionDetailsSuit(
		String startSearch, String endSearch, String type, String condsuit) {
	
List<DetailsOfLpAndSiOrders> getList = new ArrayList<DetailsOfLpAndSiOrders>();
	
	SessionFactory  session = HibernateUtil.getSessionFactory();
	Session  sess = session.getCurrentSession();
	
	String trancomp = "F";	
    String cond = "T";
    String condpcm = "Approved";
    String lcOpen = "T";
	
	Transaction tx = sess.beginTransaction();
	
	String hsql = "FROM DetailsOfLpAndSiOrders WHERE (filenum like :fileNum AND pcmapp='" + condpcm + "') AND (teccond='" + cond + "' AND newfiledate BETWEEN :end AND :start) AND (lcopen=:lcOpen AND transcomplete='" + trancomp + "' AND suitability=:suit) GROUP BY filenum";
	
	Query query = sess.createQuery(hsql);
	
	query.setParameter("storeNum", type + "%");
	query.setParameter("suit", condsuit);
	query.setParameter("start", startSearch);
	query.setParameter("end", endSearch);
	
	getList = query.list();
	
	tx.commit();
	session.close();
	
	return getList;
}



	
}
