/**
 * 
 */
package hibernate.service.tempdevelopcme;

import hibernate.domain.tempdevelopcme.SiLpSheetGenerated;
import hibernate.utility.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * @author A.P.Kasthoori
 *
 */
public class SiLpSheetGeneratedDAOImpl implements SiLpSheetGeneratedDAO {

	@SuppressWarnings("unchecked")
	public List<SiLpSheetGenerated> getSiLpSheetItems(String date) {
		
		List<SiLpSheetGenerated>  getItemList = new ArrayList<SiLpSheetGenerated>();
		// TODO Auto-generated method stub
		SessionFactory  session = HibernateUtil.getSessionFactory();		
		Session sess = session.getCurrentSession();
		Transaction tx = sess.beginTransaction();
		
		getItemList = sess.createQuery("from SiLpSheetGenerated where timestamp like '" + date + "%'").list();
		
		tx.commit();
		session.close();
		
		return getItemList;
	}

	@SuppressWarnings("unchecked")
	public List<SiLpSheetGenerated> getDetails(String reportNum) {
		// TODO Auto-generated method stub
		
	
		
		List<SiLpSheetGenerated>  getList = new ArrayList<SiLpSheetGenerated>();
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session sess = session.getCurrentSession();
		
		Transaction  tx  = sess.beginTransaction();
		
		getList = sess.createQuery("from SiLpSheetGenerated where repsheetnum='" + reportNum +  "'").list();
		
		tx.commit();
		session.close();
		
		
		return getList;
	}

	
}
