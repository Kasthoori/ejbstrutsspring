/**
 * 
 */
package hibernate.service.tempdevelopcme;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.tempdevelopcme.TecRecommondHistory;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class TecRecommondHistoryDAOImpl implements TecRecommondHistoryDAO {

	public void saveTecRecommondHistory(TecRecommondHistory tecRecHis) {
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		sess.save(tecRecHis);
		
		sess.flush();
		sess.clear();
		tx.commit();
		session.close();
		
		
		
	}

	

}
