/**
 * 
 */
package hibernate.service.tempdevelopcme;

import hibernate.domain.tempdevelopcme.ConditionOfThirdPartyDecissionHistory;

/**
 * @author A.P.Kasthoori
 *
 */
public interface ConditionOfThirdPartyDecissionHistoryDAO {
	
	public void saveThirdpartyConditionsHistory(ConditionOfThirdPartyDecissionHistory  conditionOfThirdPartyDecissionHistory);

}
