/**
 * 
 */
package hibernate.service.tempdevelopcme;

import java.util.List;

import hibernate.domain.srs.InventoryItemsMainLedger;
import hibernate.domain.tempdevelopcme.DetailsOfLpAndSiOrders;

/**
 * @author A.P.Kasthoori
 *
 */
public interface DetailsOfLpAndSiOrdersDAO {
	
	public void registerLpAndSiOrders(DetailsOfLpAndSiOrders  detailsOfLpAndSiOrders);
	public void updateRegisterFile(String fileNum, String sStoreNumber, String date, String user, String role, String dateTime);
	public void updateTecRecommondation(String fileNum, String sStoreNumber, String tecRec,int tecRound,String teccond, String tecRecGivDate,
			String tecRecUser, String tecRecRole, String tecRecDaTime, String tranCond,String bidSelect, String nameSuccBidder,String upRemarks, double quantity, String units,double orderQuan,String orderUnit, double uPrice, String currency,double totalvalue);
	public void updateNameSuccBidder(String fileNum, String sStoreNumber, String nameSuccBidder, String rem);
	public void updateRemarks(String fileNum, String sStoreNumber, String upRemarks);
	public void updateDates(String fileNum, String sStoreNumber, String selectDate,String dateForEvent, String invoiceNum,
			String userLupDate, String roleLuDate, String datTimLupDate);
	public void updatePcmApproval(String fileNum, String sStoreNumber, String pcmApp,String pcmGuides, String userPcmApp, String rolePcmApp, String dateTimPcmApp);
	public void updatePcmQuantityApproval(String fileNum, String sStoreNumber, String pcmApp,String pcmGuides, String userPcmApp, String rolePcmApp, String dateTimPcmApp, double quanOldTec, double pcmQuantity, String pcmUnits, double totalvalue, String quanPcmDecision);
    public void updateSuitability(String fileNum, String sStoreNumber, String suit,
    		String reason, String shortFall, String damaged, String suitGivDate,String userSuit,
    		String roleSuit, String dateTimeSuit);
    public List<DetailsOfLpAndSiOrders>  getDetailList(String fileNum, String sStoreNumber);
    public List<DetailsOfLpAndSiOrders>  getFilteredList(String section, String startdate, String enddate);
    public List<DetailsOfLpAndSiOrders>  getFileList(String storeNum, String section);
    public void updateSrsClarifications(String StoreNumber, String FileNumber,String tecRec,int tecRound, String tecRecGivDate,
			String tecRecUser, String tecRecRole, String tecRecDaTime, String tranCond, String nameSuccBidder,String upRemarks, double quantity, String units,double orderQuan,String orderUnit, double uPrice, String currency,double totalvalue);
    public void thirdPartyInspectionReportUpdate(String StoreNum, String FileNum, String tecRecommend,String ThirdParyInspec, String userThirdEnterd, String RoleEntered, String datetime);
    public List<DetailsOfLpAndSiOrders> getFilesDetails(String filenum,String startDate,String endDate);
    
    /*
     * Report Generation
     */
    
    public List<DetailsOfLpAndSiOrders> getStockItemsOfFiles(String fileNum);
    public List<DetailsOfLpAndSiOrders>  getAllDetailsOfStockItemsOfFiles(String storeNum, String fileNum);
	public List<InventoryItemsMainLedger> getInventItemsDetails(String storeNum);
	public List<DetailsOfLpAndSiOrders>  getOutstanding(String storeNum);
	public List<DetailsOfLpAndSiOrders> getOutstandingKnownStockItem(String storeNum);
	public List<DetailsOfLpAndSiOrders> getStockItemDetailsInFiles(String fileNum);
	

	public List<DetailsOfLpAndSiOrders> getSearchStockItemDetails(String fileNum,String StoreNum);
	
	//Procurement Searching Details Reports
	
	public List<DetailsOfLpAndSiOrders> getSiLpConditionDetailsTec(String startSearch, String endSearch,String type, String condtec);
	public List<DetailsOfLpAndSiOrders> getSiLpConditionDetailsPcm(String startSearch, String endSearch,String type, String condpcm);
	public List<DetailsOfLpAndSiOrders> getSiLpConditionDetailsLc(String startSearch, String endSearch, String type, String condlc);
	public List<DetailsOfLpAndSiOrders> getSiLpConditionDetailsAll(String startSearch, String endSearch, String type);
	public List<DetailsOfLpAndSiOrders> getSiLpConditionDetailsSuit(String startSearch, String endSearch,String type, String condsuit);
}
