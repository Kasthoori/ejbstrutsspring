package hibernate.service;


import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import hibernate.domain.InventorySuppliers;
import hibernate.utility.HibernateUtil;

public  class InventorySuppliersDAOImpl implements InventorySuppliersDAO {
	
	public void saveInventorySuppliersDAO(InventorySuppliers  inventorySuppliers){
		
		  SessionFactory  session = HibernateUtil.getSessionFactory();
		  Session  sess = session.getCurrentSession();
		  
		  Transaction  tx = sess.beginTransaction();
		  
		  try{
				sess.save(inventorySuppliers);
				tx.commit();
				session.close();
				
			}catch(ConstraintViolationException  ex){
				
				System.out.println("You have entered same store number");
				
				
			}
				
	}
	
	public void updateInventorySuppliersDAO1(InventorySuppliers  inventorySuppliers){
		
		 SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		 
		 Transaction  tx = sess.beginTransaction();
		
		 
		
		
		
		 try{
			 
			 sess.update(inventorySuppliers.getStorenumber(),inventorySuppliers);
			 tx.commit();
			 session.close();
			 
		 }catch(ConstraintViolationException   ex){
			 
			 System.out.println("You have entered same store number");
		 }
		
	}

	public void updateInventorySuppliersDAO(
			InventorySuppliers inventorySuppliers) {
		// TODO Auto-generated method stub
		
	}

	

	

}
