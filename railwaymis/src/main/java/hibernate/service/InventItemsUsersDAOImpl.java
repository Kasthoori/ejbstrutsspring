package hibernate.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import hibernate.domain.InventItemsUsers;
import hibernate.utility.HibernateUtil;

public class InventItemsUsersDAOImpl implements InventItemsUsersDAO {

	public void saveInventItemsUsers(InventItemsUsers  inventItemsUsers) {
		// TODO Auto-generated method stub
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session  sess  = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		try{
			
			sess.save(inventItemsUsers);
			tx.commit();
			session.close();
			
		}catch(ConstraintViolationException  ex){
			
			System.out.println("You have entered Same Store number");
		}
	}

}
