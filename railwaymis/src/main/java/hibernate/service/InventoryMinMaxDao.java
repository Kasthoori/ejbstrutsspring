package hibernate.service;

import java.util.ArrayList;

import hibernate.domain.InventoryMinMax;

public interface InventoryMinMaxDao {
	
	public void saveInventoryMinMax(InventoryMinMax  inventoryMinMax);
	public ArrayList<InventoryMinMax> getInvenMinMaxCriteria(InventoryMinMax  inventoryMinMax);

}
