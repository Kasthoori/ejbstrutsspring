package hibernate.service;

import java.util.ArrayList;
import java.util.List;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import hibernate.domain.DemToApprovalItems;
import hibernate.utility.HibernateUtil;

public class DemToApprovalItemsDAOImpl implements DemToApprovalItemsDAO {
	
	public void saveDemToApprovalItemsDAO(DemToApprovalItems  demToApprovalItems){
		
		SessionFactory  session = HibernateUtil.getSessionFactory();
		Session sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		try{
			sess.save(demToApprovalItems);
			tx.commit();
			session.close();
			
		}catch(ConstraintViolationException  ex){
			
			System.out.println("You have entered same store number");
			
			
		}
		
		
	}

	@SuppressWarnings("unchecked")
	public ArrayList<DemToApprovalItems> getDemToApprovalItemsList(DemToApprovalItems demToApprovalItems) {
		 
		   ArrayList<DemToApprovalItems>  returnDemToApprovalList = new ArrayList();
		   
		   SessionFactory  session = HibernateUtil.getSessionFactory();
		   Session  sess = session.getCurrentSession();
		   
		   Transaction tx = sess.beginTransaction();
		   
		   Criteria  criteria = sess.createCriteria(DemToApprovalItems.class);
		   
		   List<DemToApprovalItems>  demToApprovalList = criteria.list();
		   
		   for(DemToApprovalItems   deAppItems : demToApprovalList){
			   
			   returnDemToApprovalList.add(deAppItems);
		   }
		   
		   tx.commit();
		   session.close();
		  
		return returnDemToApprovalList;
	}

}
