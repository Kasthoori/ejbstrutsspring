/**
 * 
 */
package hibernate.service.gen;

import hibernate.domain.gen.RailwayStationsDetail;

import java.util.List;

/**
 * @author A.P.Kasthoori
 *
 */
public interface RailwayStationsDetailDAO {
	
	public List<String> getStations();

}
