/**
 * 
 */
package hibernate.service.gen;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.gen.ItemIssuingForConsump;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class ItemIssuingForConsumpDAOImpl implements ItemIssuingForConsumpDAO {

	public void saveItemIssuingForConsumption(
			ItemIssuingForConsump itemIssuingForConsump) {
		
		   SessionFactory    session  =  HibernateUtil.getSessionFactory();
		   Session   sess =  session.getCurrentSession();
		   
		   Transaction  tx  =  sess.beginTransaction();
		   
		   sess.save(itemIssuingForConsump);
		   
		   tx.commit();
		   session.close();
		
	}

}
