/**
 * 
 */
package hibernate.service.gen;

import hibernate.domain.gen.AllDepStoreFloor;

import java.util.List;

/**
 * @author A.P.Kasthoori
 *
 */
public interface AllDepStoreFloorDAO {
	
	public List<AllDepStoreFloor>   getListAllDepStoreFloor(String  shop, String subDept);

}
