/**
 * 
 */
package hibernate.service.gen;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.gen.DailyLocoFailureCondition;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class DailyLocoFailureConditionDAOImpl implements DailyLocoFailureConditionDAO{

	/* (non-Javadoc)
	 * @see hibernate.service.gen.DailyLocoFailureConditionDAO#saveDailyLocoFailureCondition(hibernate.domain.gen.DailyLocoFailureCondition)
	 */
	public void saveDailyLocoFailureCondition(
			DailyLocoFailureCondition dailyLocoFailureCondition) {
		
		SessionFactory   session = HibernateUtil.getSessionFactory();
		Session   sess = session.getCurrentSession();
		
		Transaction   tx  = sess.beginTransaction();
		
		sess.save(dailyLocoFailureCondition);
		
		tx.commit();
		session.close();
		
	}

	@SuppressWarnings("unchecked")
	public List<DailyLocoFailureCondition> getList(String locotype,
			String locoid, String currinsert) {
		
		List<DailyLocoFailureCondition>   listDetail = new ArrayList<DailyLocoFailureCondition>();
		
		SessionFactory   session = HibernateUtil.getSessionFactory();
		Session  sess =  session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		 listDetail = sess.createQuery("from DailyLocoFailureCondition d where d.lococlass = '" + locotype + "' and d.locoid='" + locoid + "' and d.currstate='" + currinsert + "'").list();
		
		 tx.commit();
		 session.close();
		 
		return  listDetail;
		
	}

}
