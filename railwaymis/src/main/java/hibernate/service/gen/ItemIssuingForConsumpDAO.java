/**
 * 
 */
package hibernate.service.gen;

import hibernate.domain.gen.ItemIssuingForConsump;

/**
 * @author A.P.Kasthoori
 *
 */
public interface ItemIssuingForConsumpDAO {
	
	public void saveItemIssuingForConsumption(ItemIssuingForConsump    itemIssuingForConsump);

}
