/**
 * 
 */
package hibernate.service.gen;

import java.util.List;

import hibernate.domain.gen.DailyLocoFailureCondition;

/**
 * @author A.P.Kasthoori
 *
 */
public interface DailyLocoFailureConditionDAO {
	
	public void saveDailyLocoFailureCondition(DailyLocoFailureCondition  dailyLocoFailureCondition);
	public List<DailyLocoFailureCondition>  getList(String locotype, String locoid, String currinsert); 
   

}
