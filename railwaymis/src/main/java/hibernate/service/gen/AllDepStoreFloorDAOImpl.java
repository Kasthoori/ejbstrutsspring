/**
 * 
 */
package hibernate.service.gen;

import hibernate.domain.gen.AllDepStoreFloor;
import hibernate.utility.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * @author A.P.Kasthoori
 *
 */
public class AllDepStoreFloorDAOImpl implements AllDepStoreFloorDAO {

	@SuppressWarnings("unchecked")
	public List<AllDepStoreFloor> getListAllDepStoreFloor(String shop, String subDept) {
		
			List<AllDepStoreFloor>    listShopStoreFloor  =  new ArrayList<AllDepStoreFloor>();
			
			SessionFactory   session  =  HibernateUtil.getSessionFactory();
			Session   sess  =  session.getCurrentSession();
			
			Transaction    tx  =  sess.beginTransaction();
			
			listShopStoreFloor  =  sess.createQuery("from AllDepStoreFloor where shop='" + shop + "' and subdept='" + subDept + "'").list();
		
			tx.commit();
			session.close();
			
		return listShopStoreFloor;
	}

}
