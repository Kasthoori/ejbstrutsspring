package hibernate.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import hibernate.domain.*;
import hibernate.utility.HibernateUtil;



public class InventoryMinMaxDaoImpl implements InventoryMinMaxDao {
	

		public void saveInventoryMinMax(InventoryMinMax  inventoryMinMax){
			
			SessionFactory  session = HibernateUtil.getSessionFactory();
			Session  sess = session.getCurrentSession();
	
			Transaction  tx = sess.beginTransaction();
			 try{
			sess.save(inventoryMinMax);
			tx.commit();
			session.close();
			
			
		}catch(ConstraintViolationException  ex){
			
			System.out.println("You have entered Same Store number");
		}
			
			
		  
		}
		

@SuppressWarnings("unchecked")
public ArrayList<InventoryMinMax>  getInvenMinMaxCriteria(InventoryMinMax  inventoryMinMax){
			
			ArrayList<InventoryMinMax> returnInventoryMinMax = new ArrayList();
	
			
			
			SessionFactory  session = HibernateUtil.getSessionFactory();
			Session  sess = session.getCurrentSession();
			
			Transaction  tx = sess.beginTransaction();
			
			//Criteria  criteria = sess.createCriteria("InventItems.class","InventMinMax.class");
			
			Criteria  criteria = sess.createCriteria(InventoryMinMax.class);
			
		
			
			List<InventoryMinMax> inventoryMinMaxList = criteria.list();
			
			
			for(InventoryMinMax  minMax : inventoryMinMaxList){
				
				returnInventoryMinMax.add(minMax);
			}
			
			
			    
			  tx.commit();
			session.close();
			
			return returnInventoryMinMax;
		}
  
			//Session  session = HibernateUtil.getSessionfactory().openSession();
			//Transaction   tr = null;
			
			
    }
		

