package hibernate.service;

import java.util.List;

import hibernate.domain.InventoryItems;

public interface InventoryItemsService {
	
	public void saveInventoryItems(InventoryItems  inventoryItems);
	public void updateInventoryItems(String enterFloor , String storeNumber);
	public List<InventoryItems>  listInventoryItems();
	public List<InventoryItems> listSingleValueInventItem(String storeNum);


}
