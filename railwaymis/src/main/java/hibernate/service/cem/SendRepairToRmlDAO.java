/**
 * 
 */
package hibernate.service.cem;

import java.util.List;

import hibernate.domain.cem.SendRepairToRml;

/**
 * @author A.P.Kasthoori
 *
 */
public interface SendRepairToRmlDAO {
	
	public void saveSendRepairToRmlDAO(SendRepairToRml    sendRepairToRml);
	public List<SendRepairToRml>  listSendRepairToRml(String currInsertion);
	public void updateSendRepairToRmlDAO(String repNum, String currInsertUp);

}
