/**
 * 
 */
package hibernate.service.cem;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernate.domain.cem.SendRepairToRml;
import hibernate.utility.HibernateUtil;

/**
 * @author A.P.Kasthoori
 *
 */
public class SendRepairToRmlDAOImpl implements SendRepairToRmlDAO{

	public void saveSendRepairToRmlDAO(SendRepairToRml sendRepairToRml) {
		
		        SessionFactory  session  =  HibernateUtil.getSessionFactory();
		        Session   sess  =  session.getCurrentSession();
		        
		        Transaction   tx  =  sess.beginTransaction();
		        
		        sess.save(sendRepairToRml);
		        
		        tx.commit();
		        session.close();
		
	}

	@SuppressWarnings("unchecked")
	public List<SendRepairToRml> listSendRepairToRml(String currInsertion) {
		
		List<SendRepairToRml>   getList =  new ArrayList<SendRepairToRml>();
		
		SessionFactory   session  =  HibernateUtil.getSessionFactory();
		Session  sess  =  session.getCurrentSession();
		
		Transaction   tx  =  sess.beginTransaction();
		
		getList = sess.createQuery("from SendRepairToRml where currInsert='" + currInsertion  + "'").list();
		
		tx.commit();
		session.close();
		
		return getList;
	}

	public void updateSendRepairToRmlDAO(String repNum, String currInsertUp) {
		          
		
		SessionFactory   session  =  HibernateUtil.getSessionFactory();
		Session   sess  =  session.getCurrentSession();
		
		Transaction   tx  =  sess.beginTransaction();
		
		SendRepairToRml    srtp = (SendRepairToRml) sess.get(SendRepairToRml.class, repNum);
		srtp.setCurrInsert(currInsertUp);
		
		tx.commit();
		session.close();
		
	}

}
