/**
 * 
 */
package hibernate.service.tender;

import hibernate.domain.tender.Document;

/**
 * @author Anura P. Kasthoori
 *
 */
public interface DocumentDAO {
	
	public void save(Document document);

}
