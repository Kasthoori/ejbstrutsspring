/**
 * 
 */
package hibernate.service.tender;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import hibernate.domain.tender.Document;
import hibernate.utility.HibernateUtil;

/**
 * @author Anura P. Kasthoori
 *
 */
public class DocumentDAOImpl implements DocumentDAO {

	@Override
	public void save(Document document) {
		// TODO Auto-generated method stub
		
		SessionFactory session = HibernateUtil.getSessionFactory();
        Session sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		try{
			sess.save(document);
			tx.commit();
			session.close();
			
		}catch(ConstraintViolationException  ex){
			
			System.out.println("You have entered same File");
			
			
		}
		
	}
	
	
	
		
		
	}


