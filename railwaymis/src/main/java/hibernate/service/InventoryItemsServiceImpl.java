package hibernate.service;

import java.util.ArrayList;
import java.util.List;

import hibernate.domain.InventoryItems;
import hibernate.utility.HibernateUtil;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class InventoryItemsServiceImpl implements InventoryItemsService {
	
	
	  public void saveInventoryItems(InventoryItems  inventoryItems){
		  
		     SessionFactory  session = HibernateUtil.getSessionFactory();
		     //SessionFactory  session = HibernateUtil.this.getSession();
		     Session  sess = session.getCurrentSession();
		    
		  
		     
		     Transaction  tx = sess.beginTransaction();
		    
		     
		     sess.save(inventoryItems);
		     
		     
		     sess.flush();
		     tx.commit();
		    
		     session.close();
	  }
	  
	  
	  

	@SuppressWarnings("unchecked")
	public List<InventoryItems> listInventoryItems(){
		
		SessionFactory   session  =  HibernateUtil.getSessionFactory();
		Session   sess  =  session.getCurrentSession();
		
		String cond = "FALSE";
		
		Transaction  tx  =  sess.beginTransaction();
		
				List<InventoryItems>   inventoryItems =  null;
			
				try{	
					inventoryItems = sess.createQuery("from InventoryItems where enterstorefloor='" + cond + "'").list();
					
					
					sess.flush();
					tx.commit();
					session.close();
					
				}catch(Exception   ex){
					
					ex.printStackTrace();
				}
					
					return inventoryItems;
	  
		  			
	  }
	
	



@SuppressWarnings("unchecked")
public List<InventoryItems>  listSingleValueInventItem(String  storeNum){	
		
		//List<InventoryItems>   inventoryItems1 = null;
	

		List<InventoryItems>  returnInventoryItems  = new ArrayList<InventoryItems>();
		
		SessionFactory   session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction   tx = sess.beginTransaction();
		
		try{
			
			
		
			 returnInventoryItems = sess.createQuery("from InventoryItems I where I.storenumber like '%"+ storeNum +"%'").list();
			
			
		}catch(Exception   ex){
			
			ex.printStackTrace();
		}
		
		     sess.flush();
		     tx.commit();
		     session.close();
		
			return   returnInventoryItems;
			
			
	}


/*
 * (non-Javadoc)
 * @see hibernate.service.InventoryItemsService#updateInventoryItems(java.lang.String, java.lang.String)
 * This is included update method using passing parameter
 */

public void updateInventoryItems(String enterFloor, String storeNumber) {

			SessionFactory   session   =  HibernateUtil.getSessionFactory();
			Session   sess =  session.getCurrentSession();
			
			try{
			Transaction    tx  =  sess.beginTransaction();
			
		    InventoryItems   iv  =  (InventoryItems) sess.get(InventoryItems.class, storeNumber);
		    iv.setEnterstorefloor(enterFloor);
			
			
			sess.flush();
			tx.commit();
			session.close();
			
			}catch(Exception  exe){
				
				exe.printStackTrace();
			
			}finally{
				
				session.close();
			}
}











}



