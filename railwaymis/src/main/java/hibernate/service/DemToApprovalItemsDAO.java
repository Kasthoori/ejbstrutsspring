package hibernate.service;

import java.util.ArrayList;

import hibernate.domain.DemToApprovalItems;

public interface DemToApprovalItemsDAO {
	
	public void saveDemToApprovalItemsDAO(DemToApprovalItems  demToApprovalItems);
	public ArrayList<DemToApprovalItems>  getDemToApprovalItemsList(DemToApprovalItems  demToApprovalItems);

}
