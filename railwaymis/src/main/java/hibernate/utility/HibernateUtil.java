package hibernate.utility;

/**
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;


    public class HibernateUtil {
    	
    	    private static final SessionFactory  sessionFactory  =  buildSessionFactory();
    	        
    	        private static SessionFactory buildSessionFactory(){
    	        	
    	        	try{
                               return new AnnotationConfiguration().configure().buildSessionFactory();
    	        		//return new AnnotationConfiguration().configure().buildSessionFactory();
    	        		
    	        	}catch(Throwable  ex){
    	        		   
    	        		System.err.println("Initial SessionFactory creation failed. "  + ex);
    	        		throw new ExceptionInInitializerError(ex);
    	        		
    	        	}
    	        }
    	        
    	        public static SessionFactory getSessionFactory(){
    	        	
    	        	return sessionFactory;
    	        }
    }	
 */

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


public class HibernateUtil {
    	
	private static SessionFactory sessionFactory;
	private static ServiceRegistry serviceRegistry;

	public static SessionFactory createSessionFactory() {
		
	           Configuration configuration = new Configuration();
	                 configuration.addResource("hibernate.cfg.xml").configure();
	                       serviceRegistry = new StandardServiceRegistryBuilder().
	                       applySettings(configuration.getProperties()).build();
	                      sessionFactory = configuration.configure().buildSessionFactory(serviceRegistry);
	                      
	          return sessionFactory;
	     }

	public static SessionFactory getSessionFactory() {
	    return createSessionFactory();
	} 
	
	
    	
}