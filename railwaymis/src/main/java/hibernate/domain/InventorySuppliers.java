package hibernate.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="inventory_items_suppliers")
public class InventorySuppliers implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public InventorySuppliers(){
		
		
	}
	
	//private int itemnumber;
	private String oem;
	private String oes;
	private String localagent;
	private String registration;
	private String storenumber;
	
	//@Id
	//@Column(name="item_num")
	//@GeneratedValue
	//public int getItemnumber() {
		//return itemnumber;
	//}
	//public void setItemnumber(int itemnumber) {
		//this.itemnumber = itemnumber;
	//}
	
	@Column(name="oem")
	public String getOem() {
		return oem;
	}
	public void setOem(String oem) {
		this.oem = oem;
	}
	
	@Column(name="oes")
	public String getOes() {
		return oes;
	}
	public void setOes(String oes) {
		this.oes = oes;
	}
	
	@Column(name="local_agents")
	public String getLocalagent() {
		return localagent;
	}
	public void setLocalagent(String localagent) {
		this.localagent = localagent;
	}
	
	@Column(name="registration")
	public String getRegistration() {
		return registration;
	}
	public void setRegistration(String registration) {
		this.registration = registration;
	}
	
	@Id
	@Column(name="store_num_fk")
	public String getStorenumber() {
		return storenumber;
	}
	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}
	
	
	

}
