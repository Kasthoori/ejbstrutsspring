package hibernate.domain.Rml;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * @author kasthoori
 */
@Entity
@Table(name="control_oiltanker_master")
public class RegOilTanker implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	public RegOilTanker(){
		
	}
	
	private String date1;
	private String type;
	private String id;
	private String importfrom;
	private String yearofmade;
	private String date2;
	private String tandescription;
	private double tanker_lenght;
	private double tanker_capacity;
	private double tanker_tonnage;
	private String condition;

	@Column(name="import_date")
	public String getDate1() {
		return date1;
	}
	public void setDate1(String date1) {
		this.date1 = date1;
	}
	
	@Column(name="type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Id
	@Column(name="serial_num")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name="import_from")
	public String getImportfrom() {
		return importfrom;
	}
	public void setImportfrom(String importfrom) {
		this.importfrom = importfrom;
	}
	
	
	
	@Column(name="yearof_made")
	public String getYearofmade() {
		return yearofmade;
	}
	public void setYearofmade(String yearofmade) {
		this.yearofmade = yearofmade;
	}
	
	@Column(name="date_commis")
	public String getDate2() {
		return date2;
	}
	public void setDate2(String date2) {
		this.date2 = date2;
	}
	
	
	@Column(name="tan_description")
	public String getTandescription() {
		return tandescription;
	}
	public void setTandescription(String tandescription) {
		this.tandescription = tandescription;
	}
	@Column(name="tank_len")
	public double getTanker_lenght() {
		return tanker_lenght;
	}
	public void setTanker_lenght(double tanlenght) {
		this.tanker_lenght = tanlenght;
	}
	
	@Column(name="tank_cap")
	public double getTanker_capacity() {
		return tanker_capacity;
	}
	public void setTanker_capacity(double tancap) {
		this.tanker_capacity = tancap;
	}
	
	@Column(name="tan_ton")
	public double getTanker_tonnage() {
		return tanker_tonnage;
	}
	public void setTanker_tonnage(double tanton) {
		this.tanker_tonnage = tanton;
	}
	
	@Column(name="tan_condition")
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	
	

	
}
