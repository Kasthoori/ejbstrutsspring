/**
 * 
 */
package hibernate.domain.Rml;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="job_order_within_cme_workshops")
public class JobOrder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int seqnum;
	private String jobordernumber;
	private String repnum;
	private String joborderdate;
	private String expectjobcompletedate;
	private String typeofjob;
	private String type;
	private String serialnumber;
	private String storenum;
	private double quality;
	private String unit;
	private String relshop;
	private String comments;
	private String user;
	private String currinsert;
		
	
	public JobOrder(){
		
	}


	@Column(name="seq_num")
	public int getSeqnum() {
		return seqnum;
	}


	public void setSeqnum(int seqnum) {
		this.seqnum = seqnum;
	}


	@Id
	@Column(name="job_order_num")
	public String getJobordernumber() {
		return jobordernumber;
	}


	public void setJobordernumber(String jobordernumber) {
		this.jobordernumber = jobordernumber;
	}


	@Column(name="rep_num")
	public String getRepnum() {
		return repnum;
	}


	public void setRepnum(String repnum) {
		this.repnum = repnum;
	}


	@Column(name="job_order_date")
	public String getJoborderdate() {
		return joborderdate;
	}


	public void setJoborderdate(String joborderdate) {
		this.joborderdate = joborderdate;
	}


	@Column(name="expect_jobcomplete_date")
	public String getExpectjobcompletedate() {
		return expectjobcompletedate;
	}


	public void setExpectjobcompletedate(String expectjobcompletedate) {
		this.expectjobcompletedate = expectjobcompletedate;
	}


	@Column(name="types_of_job")
	public String getTypeofjob() {
		return typeofjob;
	}


	public void setTypeofjob(String typeofjob) {
		this.typeofjob = typeofjob;
	}


	@Column(name="type")
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	@Column(name="serial_num")
	public String getSerialnumber() {
		return serialnumber;
	}


	public void setSerialnumber(String serialnumber) {
		this.serialnumber = serialnumber;
	}


	@Column(name="store_num")
	public String getStorenum() {
		return storenum;
	}


	public void setStorenum(String storenum) {
		this.storenum = storenum;
	}


	@Column(name="quantity")
	public double getQuality() {
		return quality;
	}


	public void setQuality(double quality) {
		this.quality = quality;
	}


	@Column(name="unit")
	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}


	@Column(name="rel_shop")
	public String getRelshop() {
		return relshop;
	}


	public void setRelshop(String relshop) {
		this.relshop = relshop;
	}


	@Column(name="comments")
	public String getComments() {
		return comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}


	@Column(name="user")
	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	@Column(name="curr_insert")
	public String getCurrinsert() {
	    return this.currinsert;
	}


	public void setCurrinsert(String currinsert) {
	    this.currinsert = currinsert;
	}
	
	

}
