package hibernate.domain.Rml;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * @author A.P.Kasthoori
 */

@Entity
@Table(name="control_coaches_master")
public class RegisterCoaches implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public RegisterCoaches(){
		
	}
	
	private String date1;
	private String type;
	private String serial;
	private String importdate;
	private String yearofmade;
	private String commissiondate;
	private String coachdescription;
	private double coach_lenght;
	private int coach_seat_capacity;
	private double coach_tonnage;
	private String condition;


	@Column(name="import_date_coach")
	public String getDate1() {
		return date1;
	}
	public void setDate1(String date1) {
		this.date1 = date1;
	}
	
	@Column(name="type_coach")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Id
	@Column(name="serial_coach")
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	
	@Column(name="import_from_coach")
	public String getImportdate() {
		return importdate;
	}
	public void setImportdate(String importdate) {
		this.importdate = importdate;
	}
	
	@Column(name="yearof_made_coach")
	public String getYearofmade() {
		return yearofmade;
	}
	public void setYearofmade(String yearofmade) {
		this.yearofmade = yearofmade;
	}
	
	@Column(name="date_commis_coach")
	public String getCommissiondate() {
		return commissiondate;
	}
	public void setCommissiondate(String commissiondate) {
		this.commissiondate = commissiondate;
	}
	
	@Column(name="coach_description")
	public String getCoachdescription() {
		return coachdescription;
	}
	public void setCoachdescription(String coachdescription) {
		this.coachdescription = coachdescription;
	}
	@Column(name="coach_len")
	public double getCoach_lenght() {
		return coach_lenght;
	}
	public void setCoach_lenght(double coach_lenght) {
		this.coach_lenght = coach_lenght;
	}
	
	@Column(name="coach_cap")
	public int getCoach_seat_capacity() {
		return coach_seat_capacity;
	}
	public void setCoach_seat_capacity(int coach_seat_capacity) {
		this.coach_seat_capacity = coach_seat_capacity;
	}
	
	@Column(name="coach_ton")
	public double getCoach_tonnage() {
		return coach_tonnage;
	}
	public void setCoach_tonnage(double coach_tonnage) {
		this.coach_tonnage = coach_tonnage;
	}
	
	@Column(name="coach_cond")
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	
	
	
	
	
}