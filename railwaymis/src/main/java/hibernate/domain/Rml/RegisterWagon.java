package hibernate.domain.Rml;

/*
 * @author A.P.Kasthoori
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="control_wagon_master")
public class RegisterWagon implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String date1;
	private String type;
	private String serial;
	private String importfrom;
	private String yearofmade;
	private String commissiondate;
	private String wagdescription;
	private double wagon_lenght;
	private int wagon_capacity;
	private double wagon_tonnage;
	private String condition;
	
	
	public RegisterWagon(){
		
		
	}

    @Column(name="wagon_date")
	public String getDate1() {
		return date1;
	}


	public void setDate1(String date1) {
		this.date1 = date1;
	}

    @Column(name="wagon_type")
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	@Id
	@Column(name="wagon_serial")
	public String getSerial() {
		return serial;
	}


	public void setSerial(String serial) {
		this.serial = serial;
	}


	@Column(name="wagon_import_from")
	public String getImportfrom() {
		return importfrom;
	}


	public void setImportfrom(String importfrom) {
		this.importfrom = importfrom;
	}


	@Column(name="wagon_year_made")
	public String getYearofmade() {
		return yearofmade;
	}


	public void setYearofmade(String yearofmade) {
		this.yearofmade = yearofmade;
	}


	@Column(name="wagon_commis")
	public String getCommissiondate() {
		return commissiondate;
	}


	public void setCommissiondate(String commissiondate) {
		this.commissiondate = commissiondate;
	}
	
	
	

	@Column(name="wagon_description")
	public String getWagdescription() {
		return wagdescription;
	}

	public void setWagdescription(String wagdescription) {
		this.wagdescription = wagdescription;
	}

	@Column(name="wagon_len")
	public double getWagon_lenght() {
		return wagon_lenght;
	}


	public void setWagon_lenght(double wagon_lenght) {
		this.wagon_lenght = wagon_lenght;
	}


	@Column(name="wagon_cap")
	public int getWagon_capacity() {
		return wagon_capacity;
	}


	public void setWagon_capacity(int wagon_capacity) {
		this.wagon_capacity = wagon_capacity;
	}


	@Column(name="wagon_ton")
	public double getWagon_tonnage() {
		return wagon_tonnage;
	}


	public void setWagon_tonnage(double wagon_tonnage) {
		this.wagon_tonnage = wagon_tonnage;
	}

	@Column(name="wagon_cond")
	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	
	
	
}
