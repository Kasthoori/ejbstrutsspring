package hibernate.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="dem_approval_inventitems")
public class DemToApprovalItems implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public DemToApprovalItems(){
		
		
	}
	
	private int appid;
	private String typeofpurchase;
	private String modeofpurchase;
	private String date1;
	private String storenum;
	private double quantity;
	private String units;
	private String itemuser;
	//private double localquantity;
	//private String localunits;
	private String timetoapproval;
	private String datetoapproval;
	private String cemapproval;
	private String purchaseOk;
	private String dmehandle;
	private String dmeapproval;
	private String cmehandle;
	private String cmebar;
	private double requestedquan;
	private String requestedunit;
	private String editedby;
	private String edited;
	private String secedited;
	private String expectdate;
	
	
	@Id
	@Column(name="app_id")
	@GeneratedValue
	public int getAppid() {
		return appid;
	}
	
	
	public void setAppid(int appid) {
		this.appid = appid;
	}
	
	
	@Column(name="type_ofpuchase")
	public String getTypeofpurchase() {
		return typeofpurchase;
	}


	public void setTypeofpurchase(String typeofpurchase) {
		this.typeofpurchase = typeofpurchase;
	}

	@Column(name="mode_ofpuchase")
	public String getModeofpurchase() {
	    return this.modeofpurchase;
	}


	public void setModeofpurchase(String modeofpurchase) {
	    this.modeofpurchase = modeofpurchase;
	}


	@Column(name="ex_date")
	public String getDate1() {
	    return this.date1;
	}


	public void setDate1(String date1) {
	    this.date1 = date1;
	}


	@Column(name="fk_store_num")
	public String getStorenum() {
		return storenum;
	}
	
	
	public void setStorenum(String storenum) {
		this.storenum = storenum;
	}
	
	@Column(name="quantity")
	public double getQuantity() {
		return quantity;
	}
	
	
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="units")
	public String getUnits() {
		return units;
	}
	
	
	public void setUnits(String units) {
		this.units = units;
	}
	
	@Column(name="item_user")
    public String getItemuser() {
		return itemuser;
	}


	public void setItemuser(String itemuser) {
		this.itemuser = itemuser;
	}


/**
	@Column(name="quantity_local")
	public double getLocalquantity() {
		return localquantity;
	}
	
	
	public void setLocalquantity(double localquantity) {
		this.localquantity = localquantity;
	}
	
	@Column(name="units_local")
	public String getLocalunits() {
		return localunits;
	}
	
	
	public void setLocalunits(String localunits) {
		this.localunits = localunits;
	}

  */ 

    @Column(name="sent_time_approval")
    public String getTimetoapproval() {
		return timetoapproval;
	}

    
	public void setTimetoapproval(String timetoapproval) {
		this.timetoapproval = timetoapproval;
	}


	public void setDatetoapproval(String datetoapproval) {
		this.datetoapproval = datetoapproval;
	}
	


	@Column(name="sent_date_approval")
	public String getDatetoapproval() {
		return datetoapproval;
	}

    @Column(name="cme_approval")
	public String getCemapproval() {
		return cemapproval;
	}


	public void setCemapproval(String cemapproval) {
		this.cemapproval = cemapproval;
	}

    @Column(name="purchase_ok")
	public String getPurchaseOk() {
		return purchaseOk;
	}


	public void setPurchaseOk(String purchaseOk) {
		this.purchaseOk = purchaseOk;
	}

    @Column(name="dme_handle")
	public String getDmehandle() {
		return dmehandle;
	}


	public void setDmehandle(String dmehandle) {
		this.dmehandle = dmehandle;
	}

    @Column(name="dme_approval")
	public String getDmeapproval() {
		return dmeapproval;
	}


	public void setDmeapproval(String dmeapproval) {
		this.dmeapproval = dmeapproval;
	}

    @Column(name="cme_handle")
	public String getCmehandle() {
		return cmehandle;
	}


	public void setCmehandle(String cmehandle) {
		this.cmehandle = cmehandle;
	}

    @Column(name="cme_bar")
	public String getCmebar() {
		return cmebar;
	}


	public void setCmebar(String cmebar) {
		this.cmebar = cmebar;
	}

    
	@Column(name="requested_quan")
	public double getRequestedquan() {
		return requestedquan;
	}


	public void setRequestedquan(double requestedquan) {
		this.requestedquan = requestedquan;
	}


	@Column(name="requested_unit")
	public String getRequestedunit() {
		return requestedunit;
	}


	public void setRequestedunit(String requestedunit) {
		this.requestedunit = requestedunit;
	}


	@Column(name="edited_by")
	public String getEditedby() {
		return editedby;
	}


	public void setEditedby(String editedby) {
		this.editedby = editedby;
	}


	@Column(name="edited")
	public String getEdited() {
		return edited;
	}


	public void setEdited(String edited) {
		this.edited = edited;
	}

    @Column(name="sec_em_edited")
	public String getSecedited() {
		return secedited;
	}


	public void setSecedited(String secedited) {
		this.secedited = secedited;
	}


	@Column(name="expect_date")
	public String getExpectdate() {
	    return this.expectdate;
	}


	public void setExpectdate(String expectdate) {
	    this.expectdate = expectdate;
	}

    
	

	
	

}
