/**
 * 
 */
package hibernate.domain.car.warrents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */

@Entity
@Table(name="car_warrent_dep_code_master_table")
public class EnterDepCodesForWarrents implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String depcode;
	private String department;
	private String dateentered;
	private String user;
	private String role;
	private String subdept;
	private String datestamp;
	
	
	public EnterDepCodesForWarrents(){
		
	}


	@Id
	@Column(name="dep_code")
	public String getDepcode() {
		return depcode;
	}


	public void setDepcode(String depcode) {
		this.depcode = depcode;
	}


	@Column(name="department")
	public String getDepartment() {
		return department;
	}


	public void setDepartment(String department) {
		this.department = department;
	}


	@Column(name="date_entered")
	public String getDateentered() {
		return dateentered;
	}


	public void setDateentered(String dateentered) {
		this.dateentered = dateentered;
	}


	@Column(name="user")
	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	@Column(name="role")
	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	@Column(name="sub_dept")
	public String getSubdept() {
		return subdept;
	}


	public void setSubdept(String subdept) {
		this.subdept = subdept;
	}


	@Column(name="datestamp")
	public String getDatestamp() {
		return datestamp;
	}


	public void setDatestamp(String datestamp) {
		this.datestamp = datestamp;
	}
	
	
	

}
