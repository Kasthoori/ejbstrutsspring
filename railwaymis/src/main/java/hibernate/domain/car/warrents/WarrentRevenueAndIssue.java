/**
 * 
 */
package hibernate.domain.car.warrents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="car_warrent_revenue_table")
public class WarrentRevenueAndIssue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String issuedate;
	private String depcode;
	private String warrantnumber;
	private String fromstation;
	private String tostation;
	private String type;
	private int fulticket;
	private int halfticket;
	private String reservetype;
	private String traveltype;
	private String transit;
	private String tranplace;
	private double transituprice;
	private int numpassenger;
	private double unitprice;
	private double totalprice;
	private String user;
	private String role;
	private String datetime;
	private String subdept;
	private int editturn;
	
	
	@Column(name="issue_date")
	public String getIssuedate() {
		return issuedate;
	}
	public void setIssuedate(String issuedate) {
		this.issuedate = issuedate;
	}
	
	@Column(name="dep_code")
	public String getDepcode() {
		return depcode;
	}
	public void setDepcode(String depcode) {
		this.depcode = depcode;
	}
	
	@Id
	@Column(name="warr_num")
	public String getWarrantnumber() {
		return warrantnumber;
	}
	public void setWarrantnumber(String warrantnumber) {
		this.warrantnumber = warrantnumber;
	}
	
	@Column(name="from_station")
	public String getFromstation() {
		return fromstation;
	}
	public void setFromstation(String fromstation) {
		this.fromstation = fromstation;
	}
	
	@Column(name="to_station")
	public String getTostation() {
		return tostation;
	}
	public void setTostation(String tostation) {
		this.tostation = tostation;
	}
	
	@Column(name="type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column(name="ful_ticket")
	public int getFulticket() {
		return fulticket;
	}
	public void setFulticket(int fulticket) {
		this.fulticket = fulticket;
	}
	@Column(name="half_ticket")
	public int getHalfticket() {
		return halfticket;
	}
	public void setHalfticket(int halfticket) {
		this.halfticket = halfticket;
	}
	@Column(name="res_type")
	public String getReservetype() {
		return reservetype;
	}
	public void setReservetype(String reservetype) {
		this.reservetype = reservetype;
	}
	@Column(name="travel_type")
	public String getTraveltype() {
		return traveltype;
	}
	public void setTraveltype(String traveltype) {
		this.traveltype = traveltype;
	}
	@Column(name="transit")
	public String getTransit() {
		return transit;
	}
	public void setTransit(String transit) {
		this.transit = transit;
	}
	@Column(name="transit_point")
	public String getTranplace() {
		return tranplace;
	}
	public void setTranplace(String tranplace) {
		this.tranplace = tranplace;
	}
	
	@Column(name="transit_uprice")
	public double getTransituprice() {
		return transituprice;
	}
	public void setTransituprice(double transituprice) {
		this.transituprice = transituprice;
	}
	@Column(name="num_passenger")
	public int getNumpassenger() {
		return numpassenger;
	}
	public void setNumpassenger(int numpassenger) {
		this.numpassenger = numpassenger;
	}
	
	@Column(name="unit_price")
	public double getUnitprice() {
		return unitprice;
	}
	public void setUnitprice(double unitprice) {
		this.unitprice = unitprice;
	}
	
	@Column(name="tot_price")
	public double getTotalprice() {
		return totalprice;
	}
	public void setTotalprice(double totalprice) {
		this.totalprice = totalprice;
	}
	
	@Column(name="user")
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	@Column(name="role")
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	@Column(name="datetime")
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	
	@Column(name="subdept")
	public String getSubdept() {
		return subdept;
	}
	public void setSubdept(String subdept) {
		this.subdept = subdept;
	}
	
	@Column(name="edit_turn")
	public int getEditturn() {
		return editturn;
	}
	public void setEditturn(int editturn) {
		this.editturn = editturn;
	}
	
	
	
	

}
