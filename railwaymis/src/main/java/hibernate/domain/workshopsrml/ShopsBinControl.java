/**
 * 
 */
package hibernate.domain.workshopsrml;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="invent_items_shops_bins_control")
public class ShopsBinControl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String shop15;
	private String shop16;
	private String shop27;
	private String shop28;
	
	public ShopsBinControl(){
		
	}

	@Id
	@Column(name="id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="shop_15")
	public String getShop15() {
		return shop15;
	}

	public void setShop15(String shop15) {
		this.shop15 = shop15;
	}

	@Column(name="shop_16")
	public String getShop16() {
		return shop16;
	}

	public void setShop16(String shop16) {
		this.shop16 = shop16;
	}

	@Column(name="shop_27")
	public String getShop27() {
		return shop27;
	}

	public void setShop27(String shop27) {
		this.shop27 = shop27;
	}

	@Column(name="shop_28")
	public String getShop28() {
		return shop28;
	}

	public void setShop28(String shop28) {
		this.shop28 = shop28;
	}
	
	

}
