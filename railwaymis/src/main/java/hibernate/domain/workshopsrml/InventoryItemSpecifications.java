/**
 * 
 */
package hibernate.domain.workshopsrml;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="inventitems_specifications")
public class InventoryItemSpecifications implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InventoryItemSpecifications() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	private String storenumber;
	private String specification;
	private String general;
	private String user;
	private String subdept;
	private String role;
	private String date;
	private String drawings;

	
	@Id
	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}
	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}
	
	@Column(name="specifications")
	public String getSpecification() {
		return specification;
	}
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	
	@Column(name="general")
	public String getGeneral() {
		return general;
	}
	public void setGeneral(String general) {
		this.general = general;
	}
	
	@Column(name="user")
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	@Column(name="subdept")
	public String getSubdept() {
		return subdept;
	}
	public void setSubdept(String subdept) {
		this.subdept = subdept;
	}
	
	@Column(name="role")
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	@Column(name="date")
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	@Column(name="drawings")
	public String getDrawings() {
		return drawings;
	}
	public void setDrawings(String drawings) {
		this.drawings = drawings;
	}
	
	

}
