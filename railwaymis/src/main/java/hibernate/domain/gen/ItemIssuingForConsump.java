/**
 * 
 */
package hibernate.domain.gen;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="invent_items_issuingfrom_shopfloor_forconsumption")
public class ItemIssuingForConsump implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int issueid;
	private String storenumber;
	private String repnumber;
	private double quantity;
	private String unit;
	private String locotype;
	private String locoid;
	private String itemclass;
	private String reptype;
	private String redetails;
	private String date;
	private String time;
	private String shop;
	private String subdept;
	private String depuser;
	
	
	
	public ItemIssuingForConsump(){
		
	}



	@Id
	@GeneratedValue
	@Column(name="issue_id")
	public int getIssueid() {
		return issueid;
	}



	public void setIssueid(int issueid) {
		this.issueid = issueid;
	}



	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}



	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}



	@Column(name="rep_num")
	public String getRepnumber() {
		return repnumber;
	}



	public void setRepnumber(String repnumber) {
		this.repnumber = repnumber;
	}



	@Column(name="quantity")
	public double getQuantity() {
		return quantity;
	}



	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}



	@Column(name="unit")
	public String getUnit() {
		return unit;
	}



	public void setUnit(String unit) {
		this.unit = unit;
	}



	@Column(name="loco_type")
	public String getLocotype() {
		return locotype;
	}



	public void setLocotype(String locotype) {
		this.locotype = locotype;
	}



	@Column(name="loco_id")
	public String getLocoid() {
		return locoid;
	}



	public void setLocoid(String locoid) {
		this.locoid = locoid;
	}
	

	@Column(name="item_class")
	public String getItemclass() {
		return itemclass;
	}



	public void setItemclass(String itemclass) {
		this.itemclass = itemclass;
	}



	@Column(name="type_rep")
	public String getReptype() {
		return reptype;
	}



	public void setReptype(String reptype) {
		this.reptype = reptype;
	}



	@Column(name="detail_rep")
	public String getRedetails() {
		return redetails;
	}



	public void setRedetails(String redetails) {

		this.redetails = redetails;
	}
	
	@Column(name="issue_date")
	public String getDate() {
		return date;
	}



	public void setDate(String date) {
		this.date = date;
	}



	@Column(name="in_time")
	public String getTime() {
		return time;
	}



	public void setTime(String time) {
		this.time = time;
	}



	@Column(name="rel_shop")
	public String getShop() {
		return shop;
	}



	public void setShop(String shop) {
		this.shop = shop;
	}



	@Column(name="sub_dept")
	public String getSubdept() {
		return subdept;
	}



	public void setSubdept(String subdept) {
		this.subdept = subdept;
	}



	@Column(name="user")
	public String getDepuser() {
		return depuser;
	}



	public void setDepuser(String depuser) {
		this.depuser = depuser;
	}
	
	

}
