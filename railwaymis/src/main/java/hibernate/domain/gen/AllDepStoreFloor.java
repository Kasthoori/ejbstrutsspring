/**
 * 
 */
package hibernate.domain.gen;

import hibernate.domain.srs.InventoryItemsMainLedger;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="invent_items_shops_floor_control")
public class AllDepStoreFloor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String storenumber;
	private double quantity;
	private String units;
	private String shop;
	private String subdept;
	private String binid;
	
	private InventoryItemsMainLedger inventoryItemsMainLedger;
	
	
	public AllDepStoreFloor(){
		
		
	}


	@Id
	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}


	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}


	@Column(name="quantity")
	public double getQuantity() {
		return quantity;
	}


	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}


	@Column(name="units")
	public String getUnits() {
		return units;
	}


	public void setUnits(String units) {
		this.units = units;
	}


	@Column(name="shop")
	public String getShop() {
		return shop;
	}


	public void setShop(String shop) {
		this.shop = shop;
	}


	@Column(name="sub_dept")
	public String getSubdept() {
		return subdept;
	}


	public void setSubdept(String subdept) {
		this.subdept = subdept;
	}


	@Column(name="bin_id")
	public String getBinid() {
		return binid;
	}


	public void setBinid(String binid) {
		this.binid = binid;
	}


	@OneToOne
	@PrimaryKeyJoinColumn
	public InventoryItemsMainLedger getInventoryItemsMainLedger() {
		return inventoryItemsMainLedger;
	}


	public void setInventoryItemsMainLedger(
			InventoryItemsMainLedger inventoryItemsMainLedger) {
		this.inventoryItemsMainLedger = inventoryItemsMainLedger;
	}
	
	
	
	

}
