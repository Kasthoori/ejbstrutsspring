/**
 * 
 */
package hibernate.domain.gen;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */

@Entity
@Table(name="loco_failure_condition_daily")
public class DailyLocoFailureCondition implements Serializable {

	
	
	private static final long serialVersionUID = 1L;
	
	
	
	private String date1;
	private String lococlass;
	private String locoid;
	private String locostate;
	private String reason;
	private String justification;
	private String currstate;
	private String enteredtime;
	private String user;
	
	
	public DailyLocoFailureCondition() {
		
	}


	@Column(name="date")
	public String getDate1() {
		return date1;
	}


	public void setDate1(String date1) {
		this.date1 = date1;
	}


	@Column(name="loco_class")
	public String getLococlass() {
		return lococlass;
	}


	public void setLococlass(String lococlass) {
		this.lococlass = lococlass;
	}

	@Column(name="loco_id")
	public String getLocoid() {
		return locoid;
	}


	public void setLocoid(String locoid) {
		this.locoid = locoid;
	}


	@Column(name="loco_state")
	public String getLocostate() {
		return locostate;
	}


	public void setLocostate(String locostate) {
		this.locostate = locostate;
	}


	@Column(name="reason")
	public String getReason() {
		return reason;
	}


	public void setReason(String reason) {
		this.reason = reason;
	}


	@Column(name="justification")
	public String getJustification() {
		return justification;
	}


	public void setJustification(String justification) {
		this.justification = justification;
	}


	@Column(name="curr_state")
	public String getCurrstate() {
		return currstate;
	}


	public void setCurrstate(String currstate) {
		this.currstate = currstate;
	}


    @Id
	@Column(name="entered_time")
	public String getEnteredtime() {
		return enteredtime;
	}


	public void setEnteredtime(String enteredtime) {
		this.enteredtime = enteredtime;
	}


	@Column(name="user")
	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}
	
	

}
