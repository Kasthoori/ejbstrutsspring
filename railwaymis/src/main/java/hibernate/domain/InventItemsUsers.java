package hibernate.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="invent_items_users")
public class InventItemsUsers implements Serializable {
	
	/**
	 *@author A.P.Kasthoori 
	 */
	private static final long serialVersionUID = 1L;





	public InventItemsUsers(){
		
		
	}
	
	
	private String storenumber;	
	private String medrawing;	
	private String medrawingii;
	private String meelectriclocoi;
	private String meelectriclocoii;
	private String mehydraulicloco1;
	private String mehydraulicloco2;
	private String mepowerlocoi;
	private String mepowerlocoii;
	private String mecarriagesi;
	private String mecarriagesii;
	private String meproductioni;
	private String meproductionii;
	private String meplantmachinery;
	private String meroadvehicle;
	private String electricalengrepallcarrlocoi;
	private String electricalengrepallcarrlocoii;
	private String electricalengpower;
	private String electricalelectroniceng;
	private String conditionmf15;
	private String emrs;
	private String emels;
	private String emhls;
	private String emepcs;
	private String empcs;
	private String emc;
	private String emw;
	private String emo;
	
	
	
	
	@Id
	@Column(name="store_num")
	public String getStorenum() {
		return storenumber;
	}
	public void setStorenum(String storenumber) {
		this.storenumber = storenumber;
	}
	
	@Column(name="em_drawings")
	public String getMedrawing() {
		return medrawing;
	}
	public void setMedrawing(String medrawing) {
		this.medrawing = medrawing;
	}
	
	@Column(name="em_drawings_ii")
	public String getMedrawingii() {
		return medrawingii;
	}
	public void setMedrawingii(String medrawingii) {
		this.medrawingii = medrawingii;
	}
	
	@Column(name="mele_i")
	public String getMeelectriclocoi() {
		return meelectriclocoi;
	}
	public void setMeelectriclocoi(String meelectriclocoi) {
		this.meelectriclocoi = meelectriclocoi;
	}
	
	@Column(name="mele_ii")
	public String getMeelectriclocoii() {
		return meelectriclocoii;
	}
	public void setMeelectriclocoii(String meelectriclocoii) {
		this.meelectriclocoii = meelectriclocoii;
	}
	
	@Column(name="melh_i")
	public String getMehydraulicloco1() {
		return mehydraulicloco1;
	}
	public void setMehydraulicloco1(String mehydraulicloco1) {
		this.mehydraulicloco1 = mehydraulicloco1;
	}
	
	@Column(name="melh_ii")
	public String getMehydraulicloco2() {
		return mehydraulicloco2;
	}
	public void setMehydraulicloco2(String mehydraulicloco2) {
		this.mehydraulicloco2 = mehydraulicloco2;
	}
	@Column(name="melp_i")
	public String getMepowerlocoi() {
		return mepowerlocoi;
	}
	public void setMepowerlocoi(String mepowerlocoi) {
		this.mepowerlocoi = mepowerlocoi;
	}
	
	@Column(name="melp_ii")
	public String getMepowerlocoii() {
		return mepowerlocoii;
	}
	public void setMepowerlocoii(String mepowerlocoii) {
		this.mepowerlocoii = mepowerlocoii;
	}
	
	@Column(name="mec")
	public String getMecarriagesi() {
		return mecarriagesi;
	}
	public void setMecarriagesi(String mecarriagesi) {
		this.mecarriagesi = mecarriagesi;
	}
	
	@Column(name="mew")
	public String getMecarriagesii() {
		return mecarriagesii;
	}
	public void setMecarriagesii(String mecarriagesii) {
		this.mecarriagesii = mecarriagesii;
	}
	
	@Column(name="mep_i")
	public String getMeproductioni() {
		return meproductioni;
	}
	public void setMeproductioni(String meproductioni) {
		this.meproductioni = meproductioni;
	}
	
	@Column(name="mep_ii")
	public String getMeproductionii() {
		return meproductionii;
	}
	public void setMeproductionii(String meproductionii) {
		this.meproductionii = meproductionii;
	}
	
	@Column(name="mem")
	public String getMeplantmachinery() {
		return meplantmachinery;
	}
	public void setMeplantmachinery(String meplantmachinery) {
		this.meplantmachinery = meplantmachinery;
	}
	
	@Column(name="mes")
	public String getMeroadvehicle() {
		return meroadvehicle;
	}
	public void setMeroadvehicle(String meroadvehicle) {
		this.meroadvehicle = meroadvehicle;
	}
	
	@Column(name="eet_i")
	public String getElectricalengrepallcarrlocoi() {
		return electricalengrepallcarrlocoi;
	}
	public void setElectricalengrepallcarrlocoi(String electricalengrepallcarrlocoi) {
		this.electricalengrepallcarrlocoi = electricalengrepallcarrlocoi;
	}
	
	@Column(name="eet_ii")
	public String getElectricalengrepallcarrlocoii() {
		return electricalengrepallcarrlocoii;
	}
	public void setElectricalengrepallcarrlocoii(
			String electricalengrepallcarrlocoii) {
		this.electricalengrepallcarrlocoii = electricalengrepallcarrlocoii;
	}
	
	@Column(name="eep")
	public String getElectricalengpower() {
		return electricalengpower;
	}
	
	public void setElectricalengpower(String electricalengpower) {
		this.electricalengpower = electricalengpower;
	}
	
	@Column(name="eee")
	public String getElectricalelectroniceng() {
		return electricalelectroniceng;
	}
	public void setElectricalelectroniceng(String electricalelectroniceng) {
		this.electricalelectroniceng = electricalelectroniceng;
	}
	
	@Column(name="cond_mf_15")
	public String getConditionmf15() {
		return conditionmf15;
	}
	public void setConditionmf15(String conditionmf15) {
		this.conditionmf15 = conditionmf15;
	}
	
	@Column(name="em_rs")
	public String getEmrs() {
		return emrs;
	}
	public void setEmrs(String emrs) {
		this.emrs = emrs;
	}
	
	@Column(name="em_els")
	public String getEmels() {
		return emels;
	}
	public void setEmels(String emels) {
		this.emels = emels;
	}
	
	@Column(name="em_hls")
	public String getEmhls() {
		return emhls;
	}
	public void setEmhls(String emhls) {
		this.emhls = emhls;
	}
	
	@Column(name="em_epcs")
	public String getEmepcs() {
		return emepcs;
	}
	public void setEmepcs(String emepcs) {
		this.emepcs = emepcs;
	}
	
	@Column(name="em_pcs")
	public String getEmpcs() {
		return empcs;
	}
	public void setEmpcs(String empcs) {
		this.empcs = empcs;
	}
	
	@Column(name="em_c")
	public String getEmc() {
		return emc;
	}
	public void setEmc(String emc) {
		this.emc = emc;
	}
	
	@Column(name="em_w")
	public String getEmw() {
		return emw;
	}
	public void setEmw(String emw) {
		this.emw = emw;
	}
	
	@Column(name="em_o")
	public String getEmo() {
		return emo;
	}
	public void setEmo(String emo) {
		this.emo = emo;
	}

	
	
    
	
}
