/**
 * 
 */
package hibernate.domain.cem;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="all_items_to_repiarpool_rml")
public class SendRepairToRml implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int seqNum;
	private String repNum;
	private String type;
	private String serialNum;
	private String despatchedFrom;
	private String despatchedDate;
	private String despatchedDatetoShopByShop33;
	private String despatchedTimetoShopByShop33;
	private String despatchedTo;
	private String detectedFailure;
	private String suggestions;
	private String itemDescription;
	private String noteddateByrelShop;
	private String noteddateByshop33;
	private String notedtimeByrelShop;
	private String notedtimeByshop33;
	private String currInsert;
	private String daterepCompleted;
	private String locoClass;
	private String locoId;
	private String category;
	private String releaserml;
	
	
		public SendRepairToRml(){
		
		
	}



	@Column(name="seq_num")
	public int getSeqNum() {
		return seqNum;
	}



	public void setSeqNum(int seqNum) {
		this.seqNum = seqNum;
	}


    @Id
	@Column(name="rep_num")
	public String getRepNum() {
		return repNum;
	}



	public void setRepNum(String repNum) {
		this.repNum = repNum;
	}



	@Column(name="type")
	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	@Column(name="serial_num")
	public String getSerialNum() {
		return serialNum;
	}



	public void setSerialNum(String serialNum) {
		this.serialNum = serialNum;
	}



	@Column(name="despatched_from")
	public String getDespatchedFrom() {
		return despatchedFrom;
	}



	public void setDespatchedFrom(String despatchedFrom) {
		this.despatchedFrom = despatchedFrom;
	}



	@Column(name="despatched_date")
	public String getDespatchedDate() {
		return despatchedDate;
	}



	public void setDespatchedDate(String despatchedDate) {
		this.despatchedDate = despatchedDate;
	}



	@Column(name="despatched_date_toshop_by33")
	public String getDespatchedDatetoShopByShop33() {
		return despatchedDatetoShopByShop33;
	}



	public void setDespatchedDatetoShopByShop33(String despatchedDatetoShopByShop33) {
		this.despatchedDatetoShopByShop33 = despatchedDatetoShopByShop33;
	}



	@Column(name="despatched_time_toshop_by33")
	public String getDespatchedTimetoShopByShop33() {
		return despatchedTimetoShopByShop33;
	}



	public void setDespatchedTimetoShopByShop33(String despatchedTimetoShopByShop33) {
		this.despatchedTimetoShopByShop33 = despatchedTimetoShopByShop33;
	}



	@Column(name="despatched_to")
	public String getDespatchedTo() {
		return despatchedTo;
	}



	public void setDespatchedTo(String despatchedTo) {
		this.despatchedTo = despatchedTo;
	}



	@Column(name="detected_failure")
	public String getDetectedFailure() {
		return detectedFailure;
	}



	public void setDetectedFailure(String detectedFailure) {
		this.detectedFailure = detectedFailure;
	}



	@Column(name="suggestion")
	public String getSuggestions() {
		return suggestions;
	}



	public void setSuggestions(String suggestions) {
		this.suggestions = suggestions;
	}



	@Column(name="item_descript")
	public String getItemDescription() {
		return itemDescription;
	}



	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}



	@Column(name="noted_date_byrelshop")
	public String getNoteddateByrelShop() {
		return noteddateByrelShop;
	}



	public void setNoteddateByrelShop(String noteddateByrelShop) {
		this.noteddateByrelShop = noteddateByrelShop;
	}



	@Column(name="noted_date_byshop_33")
	public String getNoteddateByshop33() {
		return noteddateByshop33;
	}



	public void setNoteddateByshop33(String noteddateByshop33) {
		this.noteddateByshop33 = noteddateByshop33;
	}



	@Column(name="noted_time_byrelshop")
	public String getNotedtimeByrelShop() {
		return notedtimeByrelShop;
	}



	public void setNotedtimeByrelShop(String notedtimeByrelShop) {
		this.notedtimeByrelShop = notedtimeByrelShop;
	}



	@Column(name="noted_time_byshop_33")
	public String getNotedtimeByshop33() {
		return notedtimeByshop33;
	}



	public void setNotedtimeByshop33(String notedtimeByshop33) {
		this.notedtimeByshop33 = notedtimeByshop33;
	}



	@Column(name="curr_insert")
	public String getCurrInsert() {
		return currInsert;
	}



	public void setCurrInsert(String currInsert) {
		this.currInsert = currInsert;
	}



	@Column(name="date_repair_completed")
	public String getDaterepCompleted() {
		return daterepCompleted;
	}



	public void setDaterepCompleted(String daterepCompleted) {
		this.daterepCompleted = daterepCompleted;
	}
	
	@Column(name="loco_class")
	public String getLocoClass() {
		return locoClass;
	}



	public void setLocoClass(String locoClass) {
		this.locoClass = locoClass;
	}
	
	@Column(name="loco_id")
	public String getLocoId() {
		return locoId;
	}



	public void setLocoId(String locoId) {
		this.locoId = locoId;
	}



	@Column(name="category")
	public String getCategory() {
		return category;
	}



	public void setCategory(String category) {
		this.category = category;
	}



	@Column(name="released_from_rml")
	public String getReleaserml() {
		return releaserml;
	}



	public void setReleaserml(String releaserml) {
		this.releaserml = releaserml;
	}
	
	

}
