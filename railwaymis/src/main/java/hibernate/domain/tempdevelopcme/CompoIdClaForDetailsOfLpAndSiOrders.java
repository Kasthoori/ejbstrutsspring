/**
 * 
 */
package hibernate.domain.tempdevelopcme;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author A.P.Kasthoori
 *
 */
@Embeddable
public class CompoIdClaForDetailsOfLpAndSiOrders implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	protected String storenumber;
	protected String filenum;
	
	public CompoIdClaForDetailsOfLpAndSiOrders() {}
	
	public CompoIdClaForDetailsOfLpAndSiOrders(String storenumber, String filenum) {
		
		this.storenumber = storenumber;
		this.filenum = filenum;
		
	}

	@Column(name="store_num")
	public String getStorenumber() {
		return this.storenumber;
	}

	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}

	@Column(name="file_num")
	public String getFilenum() {
		return this.filenum;
	}

	public void setFilenum(String filenum) {
		this.filenum = filenum;
	}

	
}
