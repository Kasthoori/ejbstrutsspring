/**
 * 
 */
package hibernate.domain.tempdevelopcme;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="details_lp_si_table_copy")
public class DetailsOfLpSiTableCopy implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String section;
	private String storenumber;
	private String ordertype;
	private String silpnum;
	private String lpsidate;
	private String filenum;
	private String filedate;
	private String fileregdate;
	private String filereguser;
	private String fileregrole;
	
	
	public DetailsOfLpSiTableCopy() {
		
	}


	

	@Column(name="section")
	public String getSection() {
		return section;
	}


	public void setSection(String section) {
		this.section = section;
	}


	@Id
	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}


	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}


	@Column(name="order_type")
	public String getOrdertype() {
		return ordertype;
	}


	public void setOrdertype(String ordertype) {
		this.ordertype = ordertype;
	}


	@Column(name="si_lpo_num")
	public String getSilpnum() {
		return silpnum;
	}


	public void setSilpnum(String silpnum) {
		this.silpnum = silpnum;
	}


	@Column(name="lp_si_date")
	public String getLpsidate() {
		return lpsidate;
	}


	public void setLpsidate(String lpsidate) {
		this.lpsidate = lpsidate;
	}


	@Column(name="file_num")
	public String getFilenum() {
		return filenum;
	}


	public void setFilenum(String filenum) {
		this.filenum = filenum;
	}


	@Column(name="file_date")
	public String getFiledate() {
		return filedate;
	}


	public void setFiledate(String filedate) {
		this.filedate = filedate;
	}


	@Column(name="file_reg_date")
	public String getFileregdate() {
		return fileregdate;
	}


	public void setFileregdate(String fileregdate) {
		this.fileregdate = fileregdate;
	}


	@Column(name="file_reg_user")
	public String getFilereguser() {
		return filereguser;
	}


	public void setFilereguser(String filereguser) {
		this.filereguser = filereguser;
	}


	@Column(name="file_reg_role")
	public String getFileregrole() {
		return fileregrole;
	}

	
	public void setFileregrole(String fileregrole) {
		this.fileregrole = fileregrole;
	}
	
	

}
