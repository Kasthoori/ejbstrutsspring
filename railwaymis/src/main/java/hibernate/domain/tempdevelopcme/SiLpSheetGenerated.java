/**
 * 
 */
package hibernate.domain.tempdevelopcme;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="si_lp_sheet_generated")
public class SiLpSheetGenerated implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String repsheetnum;
	private int seqid;
	private String items;
	private String year;
	private String user;
	private String subdept;
	private String itemtype;
	private String timestamp;

	public SiLpSheetGenerated() {
		super();
		// TODO Auto-generated constructor stub
	}


	@Id
	@Column(name="rep_sheet_num")
	public String getRepsheetnum() {
		return repsheetnum;
	}

	public void setRepsheetnum(String repsheetnum) {
		this.repsheetnum = repsheetnum;
	}

	@Column(name="seq_id")
	public int getSeqid() {
		return seqid;
	}

	public void setSeqid(int seqid) {
		this.seqid = seqid;
	}

	@Column(name="items")
	public String getItems() {
		return items;
	}

	public void setItems(String items) {
		this.items = items;
	}

	@Column(name="year")
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Column(name="user")
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@Column(name="subdept")
	public String getSubdept() {
		return subdept;
	}

	public void setSubdept(String subdept) {
		this.subdept = subdept;
	}

	@Column(name="item_type")
	public String getItemtype() {
		return itemtype;
	}

	public void setItemtype(String itemtype) {
		this.itemtype = itemtype;
	}

	@Column(name="time_stamp")
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	
	
	

}
