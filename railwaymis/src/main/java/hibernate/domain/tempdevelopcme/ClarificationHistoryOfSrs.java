/**
 * 
 */
package hibernate.domain.tempdevelopcme;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */

@Entity
@Table(name="details_srs_clarific_history_table")
public class ClarificationHistoryOfSrs implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String clarificid;
	private String storenumber;
	private String filenum;
	private String histecrec;
	private String hisusertecrec;
	private String hisroletecrec;
	private String hissrsclarific;
	private String hisclarificdatesrs;
	private String hisclarificusersrs;
	private String hisclarificdesigsrs;
	private int tecround;

	public ClarificationHistoryOfSrs() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="clarific_id")
	public String getClarificid() {
		return clarificid;
	}

	public void setClarificid(String clarificid) {
		this.clarificid = clarificid;
	}

	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}

	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}

	@Column(name="file_num")
	public String getFilenum() {
		return filenum;
	}

	public void setFilenum(String filenum) {
		this.filenum = filenum;
	}

	@Column(name="his_tec_rec")
	public String getHistecrec() {
		return histecrec;
	}

	public void setHistecrec(String histecrec) {
		this.histecrec = histecrec;
	}

	@Column(name="his_user_tec_rec")
	public String getHisusertecrec() {
		return hisusertecrec;
	}

	public void setHisusertecrec(String hisusertecrec) {
		this.hisusertecrec = hisusertecrec;
	}

	@Column(name="his_role_tec_rec")
	public String getHisroletecrec() {
		return hisroletecrec;
	}

	public void setHisroletecrec(String hisroletecrec) {
		this.hisroletecrec = hisroletecrec;
	}

	@Column(name="his_srs_clarific")
	public String getHissrsclarific() {
		return hissrsclarific;
	}

	public void setHissrsclarific(String hissrsclarific) {
		this.hissrsclarific = hissrsclarific;
	}

	@Column(name="his_clarific_date_srs")
	public String getHisclarificdatesrs() {
		return hisclarificdatesrs;
	}

	public void setHisclarificdatesrs(String hisclarificdatesrs) {
		this.hisclarificdatesrs = hisclarificdatesrs;
	}

	@Column(name="his_clarific_user_srs")
	public String getHisclarificusersrs() {
		return hisclarificusersrs;
	}

	public void setHisclarificusersrs(String hisclarificusersrs) {
		this.hisclarificusersrs = hisclarificusersrs;
	}

	@Column(name="his_clarific_desig_srs")
	public String getHisclarificdesigsrs() {
		return hisclarificdesigsrs;
	}

	public void setHisclarificdesigsrs(String hisclarificdesigsrs) {
		this.hisclarificdesigsrs = hisclarificdesigsrs;
	}

	@Column(name="his_tec_round")
	public int getTecround() {
		return tecround;
	}

	public void setTecround(int tecround) {
		this.tecround = tecround;
	}
	
	
	

}
