/**
 * 
 */
package hibernate.domain.tempdevelopcme;

import hibernate.domain.srs.InventoryItemsMainLedger;
import hibernate.domain.srs.InventoryItemsSpecifications;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.PrimaryKeyJoinColumns;
import javax.persistence.Table;



/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@IdClass(CompoIdClaForDetailsOfLpAndSiOrders.class)
@Table(name="details_lp_si_table")
public class DetailsOfLpAndSiOrders implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//private String id;
	private String section;
	private String typeorder;
	private String storenumber;
	private String silpnum;
	private String silpdate;
	private String silpuser;
	private String silprole;
	private String silpsubdept;
	private String silpdateent;
	private String filenum;
	private String filedate;
	private String newfiledate;
	private String transcondition;
	private String tecdate;
	private Integer tecround;
	private String teccond;
	private String usertecdate;
	private String roletecdate;
	private String datetimetecdate;
	private String tecrecommend;
	private String bidselect;
	private String namesuccbider;
	private String remarks;
	private double quantity;
	private String units;
	private double orderedquantity;
	private String orderedunit;
	private String quanpcmdecision;
	private double quanoldtec;
	private double unitprice;
	private String currency;
	private double totalvalue;
	private String tecrecgivendate;
	private String usertecrec;
	private String roletecrec;
	private String datetimetecrec;
	private String pcmdate;
	private String inspectrepdate;
	private String ordplacedate;
	private String lcopendate;
	private String itemrecievdate;
	private String invoicenum;
	private String suitrepdate;
	private String userdatelupda;
	private String roledatelupda;
	private String datetimelupda;
	private String pcmapp;
	private String pcmguidelines;
	private String userpcmapp;
	private String rolepcmapp;
	private String datetimepcmapp;
	private String suitability;
	private String reason;
	private String shortfall;
	private String damaged;
	private String suitgivendate;
	private String usersuit;
	private String rolesuit;
	private String datetimesuit;
	private String acknowcme;
	private String useracknowcme;
	private String dateacknowcme;
	private String clarificbysrs;
	private String clarifidatesrs;
	private String clarifiusersrs;
	private String clarifydesigusersrs;
	private String condclarificsrs;
	private String positionfile;
	private String lcopen;
	private String lcnumber;
	private Date lcdate;
	private String userlcentered;
	private String rolelcentered;
	private String datelcentered;
	private String transcomplete;
	private String condthirdpartyinspect;
	private String userthirdpartyentered;
	private String rolethirdpartyentered;
	private String timestampentered;
	
	
	public DetailsOfLpAndSiOrders(){
		
	}


	
	
	@Column(name="section")
	public String getSection() {
		return this.section;
	}


	

	public void setSection(String section) {
		this.section = section;
	}


	@Id
	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}


	@Column(name="type_order")
	public String getTypeorder() {
		return this.typeorder;
	}



	public void setTypeorder(String typeorder) {
		this.typeorder = typeorder;
	}



	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}

	@Column(name="si_lpo_num")
	public String getSilpnum() {
		return silpnum;
	}


	public void setSilpnum(String silpnum) {
		this.silpnum = silpnum;
	}


	@Column(name="lp_si_date")
	public String getSilpdate() {
		return silpdate;
	}


	public void setSilpdate(String silpdate) {
		this.silpdate = silpdate;
	}
    

	@Column(name="si_lp_user")
	public String getSilpuser() {
		return this.silpuser;
	}


	public void setSilpuser(String silpuser) {
		this.silpuser = silpuser;
	}


	@Column(name="si_lp_role")
	public String getSilprole() {
		return this.silprole;
	}


	public void setSilprole(String silprole) {
		this.silprole = silprole;
	}
	
	
	

    @Column(name="si_lp_subdept")
	public String getSilpsubdept() {
		return silpsubdept;
	}



	public void setSilpsubdept(String silpsubdept) {
		this.silpsubdept = silpsubdept;
	}



	@Column(name="si_lp_dateenterd")
	public String getSilpdateent() {
		return this.silpdateent;
	}


	public void setSilpdateent(String silpdateent) {
		this.silpdateent = silpdateent;
	}


	@Id
	@Column(name="file_num")
	public String getFilenum() {
		return filenum;
	}


	public void setFilenum(String filenum) {
		this.filenum = filenum;
	}


	@Column(name="file_date")
	public String getFiledate() {
		return filedate;
	}



	public void setFiledate(String filedate) {
		this.filedate = filedate;
	}

	@Column(name="new_file_date")
	public String getNewfiledate() {
		return this.newfiledate;
	}



	public void setNewfiledate(String newfiledate) {
		this.newfiledate = newfiledate;
	}



	@Column(name="transac_cond")
	public String getTranscondition() {
		return transcondition;
	}


	public void setTranscondition(String transcondition) {
		this.transcondition = transcondition;
	}


	@Column(name="tec_date")
	public String getTecdate() {
		return tecdate;
	}


	public void setTecdate(String tecdate) {
		this.tecdate = tecdate;
	}
	
	
	

    @Column(nullable=true,name="tec_round")
	public Integer getTecround() {
		return tecround;
	}



	public void setTecround(Integer tecround) {
		this.tecround = tecround;
	}


	
    @Column(name="tec_cond")
	public String getTeccond() {
		return teccond;
	}




	public void setTeccond(String teccond) {
		this.teccond = teccond;
	}




	@Column(name="user_tec_date")
	public String getUsertecdate() {
		return usertecdate;
	}


	public void setUsertecdate(String usertecdate) {
		this.usertecdate = usertecdate;
	}


	@Column(name="role_tec_date")
	public String getRoletecdate() {
		return roletecdate;
	}


	public void setRoletecdate(String roletecdate) {
		this.roletecdate = roletecdate;
	}


	@Column(name="date_time_tecdate")
	public String getDatetimetecdate() {
		return datetimetecdate;
	}


	public void setDatetimetecdate(String datetimetecdate) {
		this.datetimetecdate = datetimetecdate;
	}


	@Column(name="tec_recommond")
	public String getTecrecommend() {
		return tecrecommend;
	}


	public void setTecrecommend(String tecrecommend) {
		this.tecrecommend = tecrecommend;
	}
	
	

    @Column(name="bidder_selected_by")
	public String getBidselect() {
		return bidselect;
	}




	public void setBidselect(String bidselect) {
		this.bidselect = bidselect;
	}




	@Column(name="name_succ_bidder")
	public String getNamesuccbider() {
		return namesuccbider;
	}


	public void setNamesuccbider(String namesuccbider) {
		this.namesuccbider = namesuccbider;
	}


	@Column(name="remarks")
	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	@Column(name="quantity")
	public double getQuantity() {
		return this.quantity;
	}



	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="ordered_quantity")
	public double getOrderedquantity() {
		return orderedquantity;
	}



	public void setOrderedquantity(double orderedquantity) {
		this.orderedquantity = orderedquantity;
	}


    @Column(name="ordered_units")
	public String getOrderedunit() {
		return orderedunit;
	}



	public void setOrderedunit(String orderedunit) {
		this.orderedunit = orderedunit;
	}



	@Column(name="units")
	public String getUnits() {
		return this.units;
	}



	public void setUnits(String units) {
		this.units = units;
	}
	
	
	


    @Column(name="pcm_decision")
	public String getQuanpcmdecision() {
		return quanpcmdecision;
	}



	public void setQuanpcmdecision(String quanpcmdecision) {
		this.quanpcmdecision = quanpcmdecision;
	}



	@Column(name="quan_old_tec")
	public double getQuanoldtec() {
		return quanoldtec;
	}



	public void setQuanoldtec(double quanoldtec) {
		this.quanoldtec = quanoldtec;
	}



	@Column(name="unit_price")
	public double getUnitprice() {
		return this.unitprice;
	}



	public void setUnitprice(double unitprice) {
		this.unitprice = unitprice;
	}



	@Column(name="currency")
	public String getCurrency() {
		return this.currency;
	}



	public void setCurrency(String currency) {
		this.currency = currency;
	}



	@Column(name="total_value")
	public double getTotalvalue() {
		return this.totalvalue;
	}



	public void setTotalvalue(double totalvalue) {
		this.totalvalue = totalvalue;
	}



	@Column(name="tecrec_given_date")
	public String getTecrecgivendate() {
		return this.tecrecgivendate;
	}


	public void setTecrecgivendate(String tecrecgivendate) {
		this.tecrecgivendate = tecrecgivendate;
	}


	@Column(name="user_tec_rec")
	public String getUsertecrec() {
		return usertecrec;
	}


	public void setUsertecrec(String usertecrec) {
		this.usertecrec = usertecrec;
	}


	@Column(name="role_tec_rec")
	public String getRoletecrec() {
		return roletecrec;
	}


	public void setRoletecrec(String roletecrec) {
		this.roletecrec = roletecrec;
	}


	@Column(name="date_time_tec_rec")
	public String getDatetimetecrec() {
		return datetimetecrec;
	}


	public void setDatetimetecrec(String datetimetecrec) {
		this.datetimetecrec = datetimetecrec;
	}


	@Column(name="pcm_date")
	public String getPcmdate() {
		return pcmdate;
	}


	public void setPcmdate(String pcmdate) {
		this.pcmdate = pcmdate;
	}


	@Column(name="inspect_rep_date")
	public String getInspectrepdate() {
		return inspectrepdate;
	}


	public void setInspectrepdate(String inspectrepdate) {
		this.inspectrepdate = inspectrepdate;
	}


	@Column(name="order_place_date")
	public String getOrdplacedate() {
		return ordplacedate;
	}


	public void setOrdplacedate(String ordplacedate) {
		this.ordplacedate = ordplacedate;
	}


	@Column(name="lc_open_date")
	public String getLcopendate() {
		return lcopendate;
	}


	public void setLcopendate(String lcopendate) {
		this.lcopendate = lcopendate;
	}


	@Column(name="item_rec_date")
	public String getItemrecievdate() {
		return itemrecievdate;
	}


	public void setItemrecievdate(String itemrecievdate) {
		this.itemrecievdate = itemrecievdate;
	}


	@Column(name="invoice_num")
	public String getInvoicenum() {
		return invoicenum;
	}


	public void setInvoicenum(String invoicenum) {
		this.invoicenum = invoicenum;
	}


	@Column(name="suit_rep_date")
	public String getSuitrepdate() {
		return suitrepdate;
	}


	public void setSuitrepdate(String suitrepdate) {
		this.suitrepdate = suitrepdate;
	}


	@Column(name="user_date_lupdate")
	public String getUserdatelupda() {
		return userdatelupda;
	}


	public void setUserdatelupda(String userdatelupda) {
		this.userdatelupda = userdatelupda;
	}


	@Column(name="role_date_lupdate")
	public String getRoledatelupda() {
		return roledatelupda;
	}


	public void setRoledatelupda(String roledatelupda) {
		this.roledatelupda = roledatelupda;
	}


	@Column(name="date_time_lupdate")
	public String getDatetimelupda() {
		return datetimelupda;
	}


	public void setDatetimelupda(String datetimelupda) {
		this.datetimelupda = datetimelupda;
	}


	@Column(name="pcm_app")
	public String getPcmapp() {
		return pcmapp;
	}


	public void setPcmapp(String pcmapp) {
		this.pcmapp = pcmapp;
	}
	
	
	

    @Column(name="pcm_guide_lines")
	public String getPcmguidelines() {
		return pcmguidelines;
	}




	public void setPcmguidelines(String pcmguidelines) {
		this.pcmguidelines = pcmguidelines;
	}




	@Column(name="user_pcm_app")
	public String getUserpcmapp() {
		return userpcmapp;
	}


	public void setUserpcmapp(String userpcmapp) {
		this.userpcmapp = userpcmapp;
	}


	@Column(name="role_pcm_app")
	public String getRolepcmapp() {
		return rolepcmapp;
	}


	public void setRolepcmapp(String rolepcmapp) {
		this.rolepcmapp = rolepcmapp;
	}


	@Column(name="date_time_pcm_app")
	public String getDatetimepcmapp() {
		return datetimepcmapp;
	}


	public void setDatetimepcmapp(String datetimepcmapp) {
		this.datetimepcmapp = datetimepcmapp;
	}


	@Column(name="suitability")
	public String getSuitability() {
		return suitability;
	}


	public void setSuitability(String suitability) {
		this.suitability = suitability;
	}


	@Column(name="reason_not_suit")
	public String getReason() {
		return reason;
	}


	public void setReason(String reason) {
		this.reason = reason;
	}


	@Column(name="short_fall")
	public String getShortfall() {
		return shortfall;
	}


	public void setShortfall(String shortfall) {
		this.shortfall = shortfall;
	}


	@Column(name="damaged")
	public String getDamaged() {
		return damaged;
	}


	public void setDamaged(String damaged) {
		this.damaged = damaged;
	}
	
	@Column(name="suitability_given_date")
	public String getSuitgivendate() {
		return this.suitgivendate;
	}


	public void setSuitgivendate(String suitgivendate) {
		this.suitgivendate = suitgivendate;
	}


	@Column(name="user_suit")
	public String getUsersuit() {
		return usersuit;
	}


	public void setUsersuit(String usersuit) {
		this.usersuit = usersuit;
	}


	@Column(name="role_suit")
	public String getRolesuit() {
		return rolesuit;
	}


	public void setRolesuit(String rolesuit) {
		this.rolesuit = rolesuit;
	}


	@Column(name="date_time_suit")
	public String getDatetimesuit() {
		return datetimesuit;
	}


	public void setDatetimesuit(String datetimesuit) {
		this.datetimesuit = datetimesuit;
	}


    @Column(name="acknow_cme")
	public String getAcknowcme() {
		return acknowcme;
	}



	public void setAcknowcme(String acknowcme) {
		this.acknowcme = acknowcme;
	}



	@Column(name="user_acknow_cme")
	public String getUseracknowcme() {
		return useracknowcme;
	}



	public void setUseracknowcme(String useracknowcme) {
		this.useracknowcme = useracknowcme;
	}



	@Column(name="date_acknow_cme")
	public String getDateacknowcme() {
		return dateacknowcme;
	}



	public void setDateacknowcme(String dateacknowcme) {
		this.dateacknowcme = dateacknowcme;
	}



	@Column(name="clarific_by_srs")
	public String getClarificbysrs() {
		return clarificbysrs;
	}



	public void setClarificbysrs(String clarificbysrs) {
		this.clarificbysrs = clarificbysrs;
	}



	@Column(name="clarifi_date_srs")
	public String getClarifidatesrs() {
		return clarifidatesrs;
	}



	public void setClarifidatesrs(String clarifidatesrs) {
		this.clarifidatesrs = clarifidatesrs;
	}



	@Column(name="clarifi_user_srs")
	public String getClarifiusersrs() {
		return clarifiusersrs;
	}



	public void setClarifiusersrs(String clarifiusersrs) {
		this.clarifiusersrs = clarifiusersrs;
	}



	@Column(name="clarifi_desig_user_srs")
	public String getClarifydesigusersrs() {
		return clarifydesigusersrs;
	}



	public void setClarifydesigusersrs(String clarifydesigusersrs) {
		this.clarifydesigusersrs = clarifydesigusersrs;
	}



	@Column(name="cond_clarific_srs")
	public String getCondclarificsrs() {
		return condclarificsrs;
	}



	public void setCondclarificsrs(String condclarificsrs) {
		this.condclarificsrs = condclarificsrs;
	}



	@Column(name="position_file")
	public String getPositionfile() {
		return positionfile;
	}



	public void setPositionfile(String positionfile) {
		this.positionfile = positionfile;
	}
	

	@Column(name="lc_open")
	public String getLcopen() {
		return lcopen;
	}




	public void setLcopen(String lcopen) {
		this.lcopen = lcopen;
	}




	@Column(name="lc_number")
	public String getLcnumber() {
		return lcnumber;
	}




	public void setLcnumber(String lcnumber) {
		this.lcnumber = lcnumber;
	}



	
    @Column(name="lc_date")
	public Date getLcdate() {
		return lcdate;
	}




	public void setLcdate(Date lcdate) {
		this.lcdate = lcdate;
	}



    @Column(name="lc_user")
	public String getUserlcentered() {
		return userlcentered;
	}




	public void setUserlcentered(String userlcentered) {
		this.userlcentered = userlcentered;
	}



    @Column(name="lc_role")
	public String getRolelcentered() {
		return rolelcentered;
	}




	public void setRolelcentered(String rolelcentered) {
		this.rolelcentered = rolelcentered;
	}



    @Column(name="lc_date_entered")
	public String getDatelcentered() {
		return datelcentered;
	}




	public void setDatelcentered(String datelcentered) {
		this.datelcentered = datelcentered;
	}




	@Column(name="transac_complete")
	public String getTranscomplete() {
		return transcomplete;
	}



	public void setTranscomplete(String transcomplete) {
		this.transcomplete = transcomplete;
	}



	@Column(name="cond_thirdparty_inspection")
	public String getCondthirdpartyinspect() {
		return condthirdpartyinspect;
	}



	public void setCondthirdpartyinspect(String condthirdpartyinspect) {
		this.condthirdpartyinspect = condthirdpartyinspect;
	}



	@Column(name="cond_user_thirdparty_entered")
	public String getUserthirdpartyentered() {
		return userthirdpartyentered;
	}



	public void setUserthirdpartyentered(String userthirdpartyentered) {
		this.userthirdpartyentered = userthirdpartyentered;
	}



	@Column(name="cond_role_thirdparty_entered")
	public String getRolethirdpartyentered() {
		return rolethirdpartyentered;
	}



	public void setRolethirdpartyentered(String rolethirdpartyentered) {
		this.rolethirdpartyentered = rolethirdpartyentered;
	}



	@Column(name="timestamp_cond_entered")
	public String getTimestampentered() {
		return timestampentered;
	}



	public void setTimestampentered(String timestampentered) {
		this.timestampentered = timestampentered;
	}
	
	

}
