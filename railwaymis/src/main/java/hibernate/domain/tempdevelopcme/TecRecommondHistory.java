/**
 * 
 */
package hibernate.domain.tempdevelopcme;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="tec_recommond_history_table")
public class TecRecommondHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String id;
	private String storenumber;
	private String filenum;
	private String silpnum;
	private String tecrec;
	private Integer tecround;
	private String succbidder;
	private String remarks;
	private String usertecrec;
	private String roletecrec;
	private String timedatetecrec;
	
	
	public TecRecommondHistory(){}


	@Id
	@GeneratedValue
	@Column(name="id")
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}


	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}


	@Column(name="file_num")
	public String getFilenum() {
		return filenum;
	}


	public void setFilenum(String filenum) {
		this.filenum = filenum;
	}


	@Column(name="si_lpo_num")
	public String getSilpnum() {
		return this.silpnum;
	}


	public void setSilpnum(String silpnum) {
		this.silpnum = silpnum;
	}


	@Column(name="tec_recommond")
	public String getTecrec() {
		return tecrec;
	}


	public void setTecrec(String tecrec) {
		this.tecrec = tecrec;
	}
	
	

    @Column(name="tec_round")
	public Integer getTecround() {
		return tecround;
	}


	public void setTecround(Integer tecround) {
		this.tecround = tecround;
	}


	@Column(name="name_succ_bidder")
	public String getSuccbidder() {
		return succbidder;
	}


	public void setSuccbidder(String succbidder) {
		this.succbidder = succbidder;
	}


	@Column(name="remarks")
	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	@Column(name="user_tec_rec")
	public String getUsertecrec() {
		return usertecrec;
	}


	public void setUsertecrec(String usertecrec) {
		this.usertecrec = usertecrec;
	}


	@Column(name="role_tec_rec")
	public String getRoletecrec() {
		return roletecrec;
	}


	public void setRoletecrec(String roletecrec) {
		this.roletecrec = roletecrec;
	}


	@Column(name="date_time_tec_rec")
	public String getTimedatetecrec() {
		return timedatetecrec;
	}


	public void setTimedatetecrec(String timedatetecrec) {
		this.timedatetecrec = timedatetecrec;
	}
	
	
	

}
