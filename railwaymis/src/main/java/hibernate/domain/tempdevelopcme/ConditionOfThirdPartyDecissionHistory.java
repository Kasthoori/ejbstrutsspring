/**
 * 
 */
package hibernate.domain.tempdevelopcme;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="details_lp_si_thirdparty_inspect_history_table")
public class ConditionOfThirdPartyDecissionHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String hisid;
	private String storenumber;
	private String filenumber;
	private String histcondthirdpartydec;
	private String userthirdentered;
	private String rolethirdentered;
	private String timethirdentered;

	public ConditionOfThirdPartyDecissionHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="his_id")
	public String getHisid() {
		return hisid;
	}

	public void setHisid(String hisid) {
		this.hisid = hisid;
	}

	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}

	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}

	@Column(name="file_num")
	public String getFilenumber() {
		return filenumber;
	}

	public void setFilenumber(String filenumber) {
		this.filenumber = filenumber;
	}

	@Column(name="his_cond_thirdparty_inspection")
	public String getHistcondthirdpartydec() {
		return histcondthirdpartydec;
	}

	public void setHistcondthirdpartydec(String histcondthirdpartydec) {
		this.histcondthirdpartydec = histcondthirdpartydec;
	}

	@Column(name="his_user_thirdparty_enter")
	public String getUserthirdentered() {
		return userthirdentered;
	}

	public void setUserthirdentered(String userthirdentered) {
		this.userthirdentered = userthirdentered;
	}

	@Column(name="his_role_thirdparty_enter")
	public String getRolethirdentered() {
		return rolethirdentered;
	}

	public void setRolethirdentered(String rolethirdentered) {
		this.rolethirdentered = rolethirdentered;
	}

	@Column(name="his_timestamp")
	public String getTimethirdentered() {
		return timethirdentered;
	}

	public void setTimethirdentered(String timethirdentered) {
		this.timethirdentered = timethirdentered;
	}
	
	
	

}
