package hibernate.domain;

import hibernate.domain.srs.StoreFloorRml;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "new_inventory_items_ledger_control")
public class InventoryItems implements Serializable {
	
	/**
	 *@author kasthoori 
	 */
	private static final long serialVersionUID = 1L;
	public InventoryItems(){
		
	}
	
	
	 private String storenumber;
	
	
	private String itemname;
	
	
	private String descriptions;		
	private Double quantity;	
	private String units;	
	private String date;	
	private String application;	
	private String category;
    private String displaycolor;
    private String enterstorefloor;
   // private StoreFloorRml  storeFloorRml;
   
   

  /*  @OneToOne
	public StoreFloorRml getStoreFloorRml() {
		return storeFloorRml;
	}



	public void setStoreFloorRml(StoreFloorRml storeFloorRml) {
		this.storeFloorRml = storeFloorRml;
	}
*/


	@Id
	@Column(name="store_num")
	public String getStorenumber(){
		return storenumber;
	}

	

	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}
	@Column(name = "part_num")
	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	@Column(name="descriptions")
	public String getDescriptions() {
		return descriptions;
	}
	@Column(name = "quantity")
	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}
	
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	@Column(name = "units")
	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}
	@Column(name = "date")
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	@Column(name = "application")
	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}
	
	@Column(name="category")
	public String getCategory() {
		return category;
	}



	public void setCategory(String category) {
		this.category = category;
	}


    @Column(name="displaycolor")
	public String getDisplaycolor() {
		return displaycolor;
	}



	public void setDisplaycolor(String displaycolor) {
		this.displaycolor = displaycolor;
	}



	@Column(name="enter_store_floor")
	public String getEnterstorefloor() {
		return enterstorefloor;
	}



	public void setEnterstorefloor(String enterstorefloor) {
		this.enterstorefloor = enterstorefloor;
	}


    
   
	

}
