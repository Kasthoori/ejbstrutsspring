/**
 * 
 */
package hibernate.domain.srs;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="common_invent_items_ledger")
public class CommonInventItemsLedger implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String regdate;
    private String commstorenum;
    private String commpartnum;
    private String commapp;
    private String commcategory;
    private String mappstorenum;
    private String commdescript;
   
    
    public CommonInventItemsLedger(){
	
    }


    @Column(name="regis_date")
    public String getRegdate() {
        return this.regdate;
    }

    public void setRegdate(String regdate) {
        this.regdate = regdate;
    }

    @Id
    @Column(name="comm_store_num")
    public String getCommstorenum() {
        return this.commstorenum;
    }

    public void setCommstorenum(String commstorenum) {
        this.commstorenum = commstorenum;
    }

    @Column(name="comm_part_num")
    public String getCommpartnum() {
        return this.commpartnum;
    }

    public void setCommpartnum(String commpartnum) {
        this.commpartnum = commpartnum;
    }

    @Column(name="comm_app")
    public String getCommapp() {
        return this.commapp;
    }

    public void setCommapp(String commapp) {
        this.commapp = commapp;
    }

    @Column(name="common_category")
    public String getCommcategory() {
        return this.commcategory;
    }

    public void setCommcategory(String commcategory) {
        this.commcategory = commcategory;
    }

    @Column(name="store_num")
    public String getMappstorenum() {
        return this.mappstorenum;
    }

    public void setMappstorenum(String mappstorenum) {
        this.mappstorenum = mappstorenum;
    }

    @Column(name="comm_descript")
    public String getCommdescript() {
        return this.commdescript;
    }

    public void setCommdescript(String commdescript) {
        this.commdescript = commdescript;
    }

   
    
}
