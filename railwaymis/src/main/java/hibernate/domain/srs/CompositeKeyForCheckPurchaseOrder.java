/**
 * 
 */
package hibernate.domain.srs;

import java.io.Serializable;

import javax.persistence.Column;

/**
 * @author A.P.Kasthoori
 *
 */
public class CompositeKeyForCheckPurchaseOrder implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String storenumber;
    private String appdate;
    
    @Column(name="store_num")
    public String getStorenumber() {
        return this.storenumber;
    }
    public void setStorenumber(String storenumber) {
        this.storenumber = storenumber;
    }
    
    @Column(name="app_date")
    public String getAppdate() {
        return this.appdate;
    }
    public void setAppdate(String appdate) {
        this.appdate = appdate;
    }
    
    

}
