/**
 * 
 */
package hibernate.domain.srs.itemupload;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anura P. Kasthoori
 *
 */

@Entity
@Table(name="invent_items_uploadbin_descript_table")
public class ItemUploadDescToBin implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String entryid;
	private String issueddate;
	private String issuedtime;
	private String storenumber;
	private double quantityupload;
	private String units;
	private String grnnum;
	private String ordtype;
	private String ordnum;
	
	
	@Id
	@GeneratedValue
	@Column(name="entry_id")
	public String getEntryid() {
		return entryid;
	}
	public void setEntryid(String entryid) {
		this.entryid = entryid;
	}
	
	@Column(name="issued_date")
	public String getIssueddate() {
		return issueddate;
	}
	public void setIssueddate(String issueddate) {
		this.issueddate = issueddate;
	}
	
	@Column(name="issued_time")
	public String getIssuedtime() {
		return issuedtime;
	}
	public void setIssuedtime(String issuedtime) {
		this.issuedtime = issuedtime;
	}
	
	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}
	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}
	
	@Column(name="quantity_upload")
	public double getQuantityupload() {
		return quantityupload;
	}
	public void setQuantityupload(double quantityupload) {
		this.quantityupload = quantityupload;
	}
	
	@Column(name="units")
	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}
	
	@Column(name="grn_num")
	public String getGrnnum() {
		return grnnum;
	}
	public void setGrnnum(String grnnum) {
		this.grnnum = grnnum;
	}
	
	@Column(name="order_type")
	public String getOrdtype() {
		return ordtype;
	}
	public void setOrdtype(String ordtype) {
		this.ordtype = ordtype;
	}
	
	@Column(name="order_num")
	public String getOrdnum() {
		return ordnum;
	}
	public void setOrdnum(String ordnum) {
		this.ordnum = ordnum;
	}
	
	
	
	

}
