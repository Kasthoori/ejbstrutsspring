/**
 * 
 */
package hibernate.domain.srs;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */
@Entity
@Table(name="inventory_stock_num_descript_control")
public class EnterStockItemsDescriptionsAndSpec implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 
	private String date1;
	private String stocknum;
	private String description;
	private String specification;
	private String user;
	private String role;
	private String subdept;
	private String userEdit;
	private String roleEdit;
	private String subdeptEdit;
	private String dateEdit;

	
	public EnterStockItemsDescriptionsAndSpec(){
		
	}

	@Column(name="date")
	public String getDate1() {
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	@Id
	@Column(name="stock_num")
	public String getStocknum() {
		return stocknum;
	}

	public void setStocknum(String stocknum) {
		this.stocknum = stocknum;
	}

	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="specification")
	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	@Column(name="user")
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@Column(name="role")
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Column(name="sub_dept")
	public String getSubdept() {
		return subdept;
	}

	public void setSubdept(String subdept) {
		this.subdept = subdept;
	}

	@Column(name="user_edit")
	public String getUserEdit() {
		return userEdit;
	}

	public void setUserEdit(String userEdit) {
		this.userEdit = userEdit;
	}

	@Column(name="role_edit")
	public String getRoleEdit() {
		return roleEdit;
	}

	public void setRoleEdit(String roleEdit) {
		this.roleEdit = roleEdit;
	}

	@Column(name="sub_dept_edit")
	public String getSubdeptEdit() {
		return subdeptEdit;
	}

	public void setSubdeptEdit(String subdeptEdit) {
		this.subdeptEdit = subdeptEdit;
	}

	@Column(name="date_edit")
	public String getDateEdit() {
		return dateEdit;
	}

	public void setDateEdit(String dateEdit) {
		this.dateEdit = dateEdit;
	}
	
	

}
