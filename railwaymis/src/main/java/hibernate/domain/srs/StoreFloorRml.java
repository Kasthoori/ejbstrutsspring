package hibernate.domain.srs;

import hibernate.domain.InventorySuppliers;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PreUpdate;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@Table(name="invent_items_ledger_rml_control")
public class StoreFloorRml implements Serializable {

	/**
	 *@author A.P.Kasthoori
	 */
	private static final long serialVersionUID = 1L;
	
        private String storenumber;	
	    private double quantity;	
	    private String itemUnits;	
	    private String binId;
	    private InventoryItemsMainLedger inventoryItemsMainLedger;
	    private InventoryItemsSpecifications  inventoryItemsSpecifications;
	    //private InventorySuppliers  inventorySuppliers;
	    
	
	public StoreFloorRml(){
		
	}

	@OneToOne
	@PrimaryKeyJoinColumn
	public InventoryItemsMainLedger getInventoryItemsMainLedger() {
		return inventoryItemsMainLedger;
	}

	public void setInventoryItemsMainLedger(
			InventoryItemsMainLedger inventoryItemsMainLedger) {
		this.inventoryItemsMainLedger = inventoryItemsMainLedger;
	}


	@OneToOne
	@PrimaryKeyJoinColumn
	public InventoryItemsSpecifications getInventoryItemsSpecifications() {
	    return this.inventoryItemsSpecifications;
	}

	public void setInventoryItemsSpecifications(
		InventoryItemsSpecifications inventoryItemsSpecifications) {
	    this.inventoryItemsSpecifications = inventoryItemsSpecifications;
	}
/*
	@OneToOne
	@PrimaryKeyJoinColumn
	public InventorySuppliers getInventorySuppliers() {
	    return this.inventorySuppliers;
	}

	public void setInventorySuppliers(InventorySuppliers inventorySuppliers) {
	    this.inventorySuppliers = inventorySuppliers;
	}*/

	@Id
	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}

	
	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}

	@Column(name="quantity")
	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	@Column(name="units")
	public String getItemUnits() {
		return itemUnits;
	}

	public void setItemUnits(String itemUnits) {
		this.itemUnits = itemUnits;
	}

	@Column(name="bin_id")
	public String getBinId() {
		return binId;
	}

	public void setBinId(String binId) {
		this.binId = binId;
	}

	
	
	

}
