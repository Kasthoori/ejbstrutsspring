/**
 * 
 */
package hibernate.domain.srs.itemissuing;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anura P. Kasthoori
 *
 */

@Entity
@Table(name="invent_items_stores_mainledger_issuing")
public class StockItemsStoresMainLedgIssuing implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	private String ordernumber;
	
	
	private String storenumber;
	
	
	private double quantityissued;
	
	
	private String units;
	
	
	private String issuedby;
	
	
	private String issuedtime;
	
	
	private String issueddate;
	
	
	private String issuedfrom;
	
	
	private String issuedto;
	
	
	private String subdept;
	
	
	private String outbuf;
	
	
	@Id
	@Column(name="order_num")
	public String getOrdernumber() {
		return ordernumber;
	}
	public void setOrdernumber(String ordernumber) {
		this.ordernumber = ordernumber;
	}
	
	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}
	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}
	
	@Column(name="quantity_issued")
	public double getQuantityissued() {
		return quantityissued;
	}
	public void setQuantityissued(double quantityissued) {
		this.quantityissued = quantityissued;
	}
	
	@Column(name="units")
	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}
	
	@Column(name="issued_by")
	public String getIssuedby() {
		return issuedby;
	}
	public void setIssuedby(String issuedby) {
		this.issuedby = issuedby;
	}
	
	@Column(name="issued_time")
	public String getIssuedtime() {
		return issuedtime;
	}
	public void setIssuedtime(String issuedtime) {
		this.issuedtime = issuedtime;
	}
	
	@Column(name="issued_date")
	public String getIssueddate() {
		return issueddate;
	}
	public void setIssueddate(String issueddate) {
		this.issueddate = issueddate;
	}
	
	@Column(name="issued_from")
	public String getIssuedfrom() {
		return issuedfrom;
	}
	public void setIssuedfrom(String issuedfrom) {
		this.issuedfrom = issuedfrom;
	}
	
	@Column(name="issued_to")
	public String getIssuedto() {
		return issuedto;
	}
	public void setIssuedto(String issuedto) {
		this.issuedto = issuedto;
	}
	
	@Column(name="sub_dept")
	public String getSubdept() {
		return subdept;
	}
	public void setSubdept(String subdept) {
		this.subdept = subdept;
	}
	
	@Column(name="out_buffer")
	public String getOutbuf() {
		return outbuf;
	}
	public void setOutbuf(String outbuf) {
		this.outbuf = outbuf;
	}
	
	
	
	

}
