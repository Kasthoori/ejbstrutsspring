package hibernate.domain.srs;

/*
 * @author A.P.Kasthoori
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="invent_items_ledger_mda_control")
public class StoreFloorMda implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String storeNumber;
	private double itemQuantity;
	private String itemUnits;
	private String binId;
	
public StoreFloorMda(){
	
}

@Id
@Column(name="store_num")
public String getStoreNumber() {
	return storeNumber;
}

public void setStoreNumber(String storeNumber) {
	this.storeNumber = storeNumber;
}

@Column(name="quantity")
public double getItemQuantity() {
	return itemQuantity;
}

public void setItemQuantity(double itemQuantity) {
	this.itemQuantity = itemQuantity;
}

@Column(name="units")
public String getItemUnits() {
	return itemUnits;
}

public void setItemUnits(String itemUnits) {
	this.itemUnits = itemUnits;
}

@Column(name="bin_id")
public String getBinId() {
	return binId;
}

public void setBinId(String binId) {
	this.binId = binId;
}



}
