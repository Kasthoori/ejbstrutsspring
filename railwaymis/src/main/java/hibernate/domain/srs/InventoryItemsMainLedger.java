/**
 * 
 */
package hibernate.domain.srs;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */

@Entity
@Table(name="invent_items_legder")
public class InventoryItemsMainLedger implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
        private String storenumber;
	    private String itemname;
	    private String descriptions;		
	    private Double quantity;	
	    private String units;	
	    private String date;	
	    private String application;	
	    private String category;
        private String displaycolor;
        private String ownersubdept;
        private String store;
        private String edited;
        private String entereduser;
        private String editeddate;
        private String editeduser;
        private String role;
        private String roleedited;
	
	public InventoryItemsMainLedger(){
		
	}
	
	
	

	@Id
	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}

	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}


	@Column(name="part_num")
	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	@Column(name="descriptions")
	public String getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}

	@Column(name="quantity")
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	@Column(name="units")
	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	@Column(name="date")
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Column(name="application")
	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	@Column(name="category")
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Column(name="displaycolor")
	public String getDisplaycolor() {
		return displaycolor;
	}

	public void setDisplaycolor(String displaycolor) {
		this.displaycolor = displaycolor;
	}




	@Column(name="owner_sub_dept")
	public String getOwnersubdept() {
		return ownersubdept;
	}




	public void setOwnersubdept(String ownersubdept) {
		this.ownersubdept = ownersubdept;
	}




	@Column(name="store")
	public String getStore() {
	    return this.store;
	}




	public void setStore(String store) {
	    this.store = store;
	}




	@Column(name="edited")
	public String getEdited() {
		return edited;
	}




	public void setEdited(String edited) {
		this.edited = edited;
	}



    @Column(name="user_entered")
	public String getEntereduser() {
		return entereduser;
	}




	public void setEntereduser(String entereduser) {
		this.entereduser = entereduser;
	}



    @Column(name="date_edited")
	public String getEditeddate() {
		return editeddate;
	}




	public void setEditeddate(String editeddate) {
		this.editeddate = editeddate;
	}



    @Column(name="user_edited")
	public String getEditeduser() {
		return editeduser;
	}




	public void setEditeduser(String editeduser) {
		this.editeduser = editeduser;
	}



    @Column(name="role")
	public String getRole() {
		return role;
	}




	public void setRole(String role) {
		this.role = role;
	}



    @Column(name="role_edited")
	public String getRoleedited() {
		return roleedited;
	}




	public void setRoleedited(String roleedited) {
		this.roleedited = roleedited;
	}
	
	

}
