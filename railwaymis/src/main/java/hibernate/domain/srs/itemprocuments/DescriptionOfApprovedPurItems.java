/**
 * 
 */
package hibernate.domain.srs.itemprocuments;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */

@Entity
@Table(name="descriptions_of_purchaseitems_approved")
public class DescriptionOfApprovedPurItems implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DescriptionOfApprovedPurItems(){
		
	}
	
	private String purchaseid;
	private String storenumber;
	private String tender;
	private String indent;
	private String localpurchase;
	private String supplier;
	private String localagent;
	private String country;
	private double fobprice;
	private double cfrprice;
	private double fasprice;
	private double exstockprice;
	private double deliveryprice;
	private double unitprice;
	private double nbt;
	private double vat;
	private double advertisementcharges;
	private double customduty;
	private double portauthcharges;
	private double totalconvalue;
	private int ratemettons;
	private int seqnum;
	private String lastpurchdate;
	private String purchasecompleted;
	private String year;
	private String dateapproved;
	private String seqnumcond;
	private String purchaseType;
	private double reqQuantity = 0.0;
	private String reqUnit;

	@Id
	@Column(name="purchase_id")
	public String getPurchaseid() {
		return purchaseid;
	}
	public void setPurchaseid(String purchaseid) {
		this.purchaseid = purchaseid;
	}
	
	@Column(name="store_num")
	public String getStorenumber() {
		return storenumber;
	}
	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}
	
	@Column(name="tender")
	public String getTender() {
		return tender;
	}
	public void setTender(String tender) {
		this.tender = tender;
	}
	
	@Column(name="indent")
	public String getIndent() {
		return indent;
	}
	public void setIndent(String indent) {
		this.indent = indent;
	}
	
	@Column(name="local_purchase")
	public String getLocalpurchase() {
		return localpurchase;
	}
	public void setLocalpurchase(String localpurchase) {
		this.localpurchase = localpurchase;
	}
	
	@Column(name="supplier")
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	
	@Column(name="local_agent")
	public String getLocalagent() {
		return localagent;
	}
	public void setLocalagent(String localagent) {
		this.localagent = localagent;
	}
	
	@Column(name="country")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column(name="fob_value")
	public double getFobprice() {
		return fobprice;
	}
	public void setFobprice(double fobprice) {
		this.fobprice = fobprice;
	}
	
	@Column(name="cfr_value")
	public double getCfrprice() {
		return cfrprice;
	}
	public void setCfrprice(double cfrprice) {
		this.cfrprice = cfrprice;
	}
	
	@Column(name="fas_value")
	public double getFasprice() {
		return fasprice;
	}
	public void setFasprice(double fasprice) {
		this.fasprice = fasprice;
	}
	
	@Column(name="ex_stock_price")
	public double getExstockprice() {
		return exstockprice;
	}
	public void setExstockprice(double exstockprice) {
		this.exstockprice = exstockprice;
	}
	
	@Column(name="delivery_price")
	public double getDeliveryprice() {
		return deliveryprice;
	}
	public void setDeliveryprice(double deliveryprice) {
		this.deliveryprice = deliveryprice;
	}
	
	@Column(name="unit_price")
	public double getUnitprice() {
		return unitprice;
	}
	public void setUnitprice(double unitprice) {
		this.unitprice = unitprice;
	}
	
	@Column(name="nbt_value")
	public double getNbt() {
		return nbt;
	}
	public void setNbt(double nbt) {
		this.nbt = nbt;
	}
	
	@Column(name="vat_value")
	public double getVat() {
		return vat;
	}
	public void setVat(double vat) {
		this.vat = vat;
	}
	
	@Column(name="advertisement_charges")
	public double getAdvertisementcharges() {
		return advertisementcharges;
	}
	public void setAdvertisementcharges(double advertisementcharges) {
		this.advertisementcharges = advertisementcharges;
	}
	
	@Column(name="custom_duty")
	public double getCustomduty() {
		return customduty;
	}
	public void setCustomduty(double customduty) {
		this.customduty = customduty;
	}
	
	@Column(name="port_auth_charges")
	public double getPortauthcharges() {
		return portauthcharges;
	}
	public void setPortauthcharges(double portauthcharges) {
		this.portauthcharges = portauthcharges;
	}
	
	@Column(name="total_contract_value")
	public double getTotalconvalue() {
		return totalconvalue;
	}
	public void setTotalconvalue(double totalconvalue) {
		this.totalconvalue = totalconvalue;
	}
	
	@Column(name="rate_metric_tons")
	public int getRatemettons() {
		return ratemettons;
	}
	public void setRatemettons(int ratemettons) {
		this.ratemettons = ratemettons;
	}
	
	@Column(name="seq_num")
	public int getSeqnum() {
		return seqnum;
	}
	public void setSeqnum(int seqnum) {
		this.seqnum = seqnum;
	}
	
	@Column(name="last_purchase")
	public String getLastpurchdate() {
		return lastpurchdate;
	}
	public void setLastpurchdate(String lastpurchdate) {
		this.lastpurchdate = lastpurchdate;
	}
	
	@Column(name="purchase_completed")
	public String getPurchasecompleted() {
		return purchasecompleted;
	}
	public void setPurchasecompleted(String purchasecompleted) {
		this.purchasecompleted = purchasecompleted;
	}
	
	@Column(name="year")
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	@Column(name="date_approved")
	public String getDateapproved() {
		return dateapproved;
	}
	public void setDateapproved(String dateapproved) {
		this.dateapproved = dateapproved;
	}
	
	@Column(name="seq_num_cond")
	public String getSeqnumcond() {
		return seqnumcond;
	}
	public void setSeqnumcond(String seqnumcond) {
		this.seqnumcond = seqnumcond;
	}
	
	@Column(name="purchase_type")
	public String getPurchaseType() {
		return purchaseType;
	}
	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}
	
	@Column(name="approved_quantity")
	public double getReqQuantity() {
		return reqQuantity;
	}
	public void setReqQuantity(double reqQuantity) {
		this.reqQuantity = reqQuantity;
	}
	
	@Column(name="approved_units")
	public String getReqUnit() {
		return reqUnit;
	}
	public void setReqUnit(String reqUnit) {
		this.reqUnit = reqUnit;
	}
	
	
	
	
	
	

}
