/**
 * 
 */
package hibernate.domain.srs;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * @author A.P.Kasthoori
 *
 */

@Entity
@IdClass(CompositeKeyForCheckPurchaseOrder.class)
@Table(name="inventory_items_purchasing_srs_main")
public class CheckPurchaseOrdersFromSubDept implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String storenumber;
    private double quantity;
    private String unit;
    private String approvedby;
    private String subdept;
    private String appdate;
    private String apptime;
    private String typeofpurchase;
    private String modeofpurchase;
    private String expecteddate;
    
    public CheckPurchaseOrdersFromSubDept(){
	
    }

    @Id
    @Column(name="store_num")
    public String getStorenumber() {
        return this.storenumber;
    }

    public void setStorenumber(String storenumber) {
        this.storenumber = storenumber;
    }

    @Column(name="quantity")
    public double getQuantity() {
        return this.quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    @Column(name="units")
    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Column(name="approved_by")
    public String getApprovedby() {
        return this.approvedby;
    }

    public void setApprovedby(String approvedby) {
        this.approvedby = approvedby;
    }

    @Column(name="sub_dept")
    public String getSubdept() {
        return this.subdept;
    }

    public void setSubdept(String subdept) {
        this.subdept = subdept;
    }

    @Id
    @Column(name="app_date")
    public String getAppdate() {
        return this.appdate;
    }

    public void setAppdate(String appdate) {
        this.appdate = appdate;
    }

    @Column(name="app_time")
    public String getApptime() {
        return this.apptime;
    }

    public void setApptime(String apptime) {
        this.apptime = apptime;
    }

    @Column(name="type_ofpurchase")
    public String getTypeofpurchase() {
        return this.typeofpurchase;
    }

    public void setTypeofpurchase(String typeofpurchase) {
        this.typeofpurchase = typeofpurchase;
    }

    @Column(name="mode_ofpurchase")
    public String getModeofpurchase() {
        return this.modeofpurchase;
    }

    public void setModeofpurchase(String modeofpurchase) {
        this.modeofpurchase = modeofpurchase;
    }

    @Column(name="ex_date")
    public String getExpecteddate() {
        return this.expecteddate;
    }

    public void setExpecteddate(String expecteddate) {
        this.expecteddate = expecteddate;
    }
    
    

}
