/**
 * 
 */
package strutsaction.tempdevelopcme;

import hibernate.domain.tempdevelopcme.DetailsOfLpAndSiOrders;
import hibernate.service.tempdevelopcme.DetailsOfLpAndSiOrdersDAO;
import hibernate.service.tempdevelopcme.DetailsOfLpAndSiOrdersDAOImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @author Anura P. Kasthoori
 *
 */
public class DisplaySiLpItemConditionAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String verifydate1;
	private String verifydate2;
	private String type;
	private String conditiontec;
	private String conditionpcm;
	private String conditionlc;
	private String message;
	
	private DetailsOfLpAndSiOrders detailsOfLpAndSiOrders = new DetailsOfLpAndSiOrders();
	private List<DetailsOfLpAndSiOrders>   condList = new ArrayList<DetailsOfLpAndSiOrders>();
	private List<DetailsOfLpAndSiOrders>   condList2;
	private DetailsOfLpAndSiOrdersDAO   detailsOfLpAndSiOrdersDAO = new DetailsOfLpAndSiOrdersDAOImpl();
	
	
	private static final String SUCCESSTEC = "successtec";
	private static final String SUCCESSTECNOT = "successtecnot";
	private static final String SUCCESSPCM = "successpcm";
	private static final String SUCCESSPCMNOT = "successpcmnot";
	private static final String SUCCESSLC = "successlc";
	private static final String SUCCESSLCNOT = "successlcnot";
	
	
	public String execute(){
		
		return SUCCESS;
	}


	public String getConditionStatus(){
		
		setVerifydate1(verifydate1);
		setVerifydate2(verifydate2);
		
		
		if((!getConditiontec().equals("")) && (getConditionpcm().equals("")) && (getConditionlc().equals(""))){
			
		 
			 
			condList = detailsOfLpAndSiOrdersDAO.getSiLpConditionDetailsTec(getVerifydate1(), getVerifydate2(), getType(),getConditiontec());
			//setMessage("Tec Recieved");
			
			
			
			if(condList.isEmpty()){
				
				return ERROR;
			}
			
			if(getConditiontec().matches("T")){
				
				return SUCCESSTEC;
			}else{
				
				return SUCCESSTECNOT;
				
			}
			
			
		
		}else if((getConditiontec().equals("")) && (!getConditionpcm().equals("")) && (getConditionlc().equals(""))){
			
			condList = detailsOfLpAndSiOrdersDAO.getSiLpConditionDetailsPcm(getVerifydate1(), getVerifydate2(), getType(),getConditionpcm());
			//setMessage("Pcm Recieved");
            if(condList.isEmpty()){
				
				return ERROR;
			}
			
            if(getConditionpcm().matches("Approved")){
			
            	return SUCCESSPCM;
            	
            }else{
            	
            	return SUCCESSPCMNOT;
            }
			
		}else if((getConditiontec().equals("")) && (getConditionpcm().equals("")) && (!getConditionlc().equals(""))){
			
			
			condList = detailsOfLpAndSiOrdersDAO.getSiLpConditionDetailsLc(getVerifydate1(), getVerifydate2(), getType(),getConditionlc());
			//setMessage("Lc Recieved");
			
			
            if(condList.isEmpty()){
				
				return ERROR;
			}
			
            if(getConditionlc().matches("T")){
            
            		return SUCCESSLC;
			
            }else{
            	
            	return SUCCESSLCNOT;
            }
			
		}else{
			
			
			condList = detailsOfLpAndSiOrdersDAO.getSiLpConditionDetailsAll(getVerifydate1(), getVerifydate2(), getType());
			setMessage("All Recieved");
			
            if(condList.isEmpty()){
				
				return ERROR;
			}
		}
		//setMessage(getVerifydate1() + getVerifydate2() + getType() + getConditiontec() + getConditionpcm() + getConditionlc());
		
		//condList = detailsOfLpAndSiOrdersDAO.getSiLpConditionDetailsTec(getVerifydate1(), getVerifydate2(), getType(),getConditiontec());
				

		/*if(condList.equals(null)){
			
			
			return ERROR;
		}*/
		
		return SUCCESS;
	}


	public String getVerifydate1() {
		return verifydate1;
	}




	public void setVerifydate1(String verifydate1) {
		this.verifydate1 = verifydate1;
	}




	public String getVerifydate2() {
		return verifydate2;
	}




	public void setVerifydate2(String verifydate2) {
		this.verifydate2 = verifydate2;
	}




	public String getConditiontec() {
		return conditiontec;
	}




	public void setConditiontec(String conditiontec) {
		this.conditiontec = conditiontec;
	}




	public String getConditionpcm() {
		return conditionpcm;
	}




	public void setConditionpcm(String conditionpcm) {
		this.conditionpcm = conditionpcm;
	}



	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getConditionlc() {
		return conditionlc;
	}


	public void setConditionlc(String conditionlc) {
		this.conditionlc = conditionlc;
	}


	public List<DetailsOfLpAndSiOrders> getCondList() {
		return condList;
	}




	public void setCondList(List<DetailsOfLpAndSiOrders> condList) {
		this.condList = condList;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public List<DetailsOfLpAndSiOrders> getCondList2() {
		return condList2;
	}


	public void setCondList2(List<DetailsOfLpAndSiOrders> condList2) {
		this.condList2 = condList2;
	}
	
	
	

}
