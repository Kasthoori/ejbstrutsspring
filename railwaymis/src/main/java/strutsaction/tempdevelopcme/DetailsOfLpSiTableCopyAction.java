/**
 * 
 */
package strutsaction.tempdevelopcme;

import java.util.ArrayList;
import java.util.List;

import hibernate.domain.tempdevelopcme.DetailsOfLpSiTableCopy;
import hibernate.service.tempdevelopcme.DetailsOfLpSiTableCopyDAO;
import hibernate.service.tempdevelopcme.DetailsOfLpSiTableCopyDAOImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @author A.P.Kasthoori
 *
 */
public class DetailsOfLpSiTableCopyAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String section;
	private String storenumber;
	private String ordertype;
	private String silponum;
	private String lpsidate;
	private String filenum;
	private String filedate;
	private String fileregdate;
	
	
	
	private DetailsOfLpSiTableCopy   detailsOfLpSiTableCopy = new DetailsOfLpSiTableCopy();
	private DetailsOfLpSiTableCopyDAO  detailsOfLpSiTableCopyDAO  =  new DetailsOfLpSiTableCopyDAOImpl();
	private List<DetailsOfLpSiTableCopy>  getDetailList = new ArrayList<DetailsOfLpSiTableCopy>();
	
	
public DetailsOfLpSiTableCopyAction() {
	
}

public String execute() {
	
	return SUCCESS;
}

public String getDetailCopyList() {
	
    HttpServletRequest  request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
	HttpSession  session = request.getSession();
	
	String user = (String) session.getAttribute("dbUserName");
	String role = (String) session.getAttribute("strRole");
	
	
	getDetailList = detailsOfLpSiTableCopyDAO.getDetailList(user, role);
	
	
	return SUCCESS;
}

public String getSection() {
	return section;
}

public void setSection(String section) {
	this.section = section;
}

public String getStorenumber() {
	return storenumber;
}

public void setStorenumber(String storenumber) {
	this.storenumber = storenumber;
}

public String getOrdertype() {
	return ordertype;
}

public void setOrdertype(String ordertype) {
	this.ordertype = ordertype;
}

public String getSilponum() {
	return silponum;
}

public void setSilponum(String silponum) {
	this.silponum = silponum;
}

public String getLpsidate() {
	return lpsidate;
}

public void setLpsidate(String lpsidate) {
	this.lpsidate = lpsidate;
}

public String getFilenum() {
	return filenum;
}

public void setFilenum(String filenum) {
	this.filenum = filenum;
}

public String getFiledate() {
	return filedate;
}

public void setFiledate(String filedate) {
	this.filedate = filedate;
}

public String getFileregdate() {
	return fileregdate;
}

public void setFileregdate(String fileregdate) {
	this.fileregdate = fileregdate;
}

public DetailsOfLpSiTableCopy getDetailsOfLpSiTableCopy() {
	return detailsOfLpSiTableCopy;
}

public void setDetailsOfLpSiTableCopy(
		DetailsOfLpSiTableCopy detailsOfLpSiTableCopy) {
	this.detailsOfLpSiTableCopy = detailsOfLpSiTableCopy;
}

public List<DetailsOfLpSiTableCopy> getGetDetailList() {
	return getDetailList;
}

public void setGetDetailList(List<DetailsOfLpSiTableCopy> getDetailList) {
	this.getDetailList = getDetailList;
}



}
