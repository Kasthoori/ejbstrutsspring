/**
 * 
 */
package strutsaction.tempdevelopcme;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.exception.ConstraintViolationException;

import pk1.DbBean;
import pk1.TimeAndDate;
import hibernate.domain.srs.InventoryItemsMainLedger;
import hibernate.domain.tempdevelopcme.ClarificationHistoryOfSrs;
import hibernate.domain.tempdevelopcme.ConditionOfThirdPartyDecissionHistory;
import hibernate.domain.tempdevelopcme.DetailsOfLpAndSiOrders;
import hibernate.domain.tempdevelopcme.DetailsOfLpSiTableCopy;
import hibernate.domain.tempdevelopcme.TecRecommondHistory;
import hibernate.service.srs.InventoryItemsMainLedgerDAO;
import hibernate.service.srs.InventoryItemsMainLedgerDAOImpl;
import hibernate.service.tempdevelopcme.ClarificationHistoryOfSrsDAO;
import hibernate.service.tempdevelopcme.ClarificationHistoryOfSrsDAOImpl;
import hibernate.service.tempdevelopcme.ConditionOfThirdPartyDecissionHistoryDAO;
import hibernate.service.tempdevelopcme.ConditionOfThirdPartyDecissionHistoryDAOImpl;
import hibernate.service.tempdevelopcme.DetailsOfLpAndSiOrdersDAO;
import hibernate.service.tempdevelopcme.DetailsOfLpAndSiOrdersDAOImpl;
import hibernate.service.tempdevelopcme.DetailsOfLpSiTableCopyDAO;
import hibernate.service.tempdevelopcme.DetailsOfLpSiTableCopyDAOImpl;
import hibernate.service.tempdevelopcme.TecRecommondHistoryDAO;
import hibernate.service.tempdevelopcme.TecRecommondHistoryDAOImpl;

import java.sql.*;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @author A.P.Kasthoori

 *
 */
public class DetailsOfLpAndSiOrdersAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public static final String ERROR1 = "error1";
	public static final String REPEAT = "repeat";
	public static final String REPEAT1 = "repeat1";
	public static final String REPEAT2 = "repeat2";
	public static final String REPEAT3 = "repeat3";
	public static final String ERROR2 = "error2";
	public static final String CLARIFY = "clarify";
	public static final String THIRDPARTY = "thirdparty";
	public static final String NOTPROC = "notproc";
	public static final String UPDATERROR = "updaterror";
	public static final String RECOMMEND = "recommend";
	public static final String CANCELED = "canceled";
	public static final String OUTSTANDING = "outstanding";


	
	
	
	String sStoreNumber = "";
	String user = "";
	String role1 = "";
	String enteredDate = "";
	String time = "";
	String subDept = "";
	String select = "";
	
	private String type;
	private String typeorder;
	private String storenumber;
	private String filenumber;
	private String storeNum;
	private String storeNumDigit1;
	private String storeNumDigit2;
	private String storeNumDigit3;
	private String storeNumDigit4;
	private String lponum;
	private String shop;
	private String subdept;
	private String depuser;
	private String role;
	private String date;
	private String filenum;
	private String filenum1;
	private String filenum2;
	private String filenumsame;
	private String datefile;
	private String newfiledate;
	private String date1;
	private String tecrecommend;
	private String tecrecdate;
	private int tecround;
	private String succBidder;
	private String selection;
	private String remarks;
	private double quantity;
	private String units;
	private double orderedQuantity;
	private String orderedUnit;
	private double uprice;
	private String currency;
	private String selecteddate;
	private String invoicenum;
	private String datesforevent;
	private String pcmapproval;
	private String pcmguide;
	private String suitabilitycond;
	private String reasons;
	private String amount;
	private String damamount;
	private String suitgivendate;
	private String startdate;
	private String enddate;
	private String messege;
	private String changequanbypcm;
	private double pcmquantity;
	private String pcmunits;
	private String thirdpartyinspectrep;
	private String selectknownitem;
	private String lcopen;
	private String lcnumber;
	private String lcdate;

	
	
	private InventoryItemsMainLedger  inventoryItemsMainLedger = new InventoryItemsMainLedger();
	private DetailsOfLpAndSiOrders  detailsOfLpAndSiOrders =  new DetailsOfLpAndSiOrders();
	private DetailsOfLpSiTableCopy  detailsOfLpSiTableCopy = new DetailsOfLpSiTableCopy();
	private DetailsOfLpAndSiOrdersDAO  detailsOfLpAndSiOrdersDAO = new DetailsOfLpAndSiOrdersDAOImpl();
	private DetailsOfLpSiTableCopyDAO  detailsOfLpSiTableCopyDAO = new DetailsOfLpSiTableCopyDAOImpl();
	private ClarificationHistoryOfSrs  clarificationHistoryOfSrs  =  new ClarificationHistoryOfSrs();
	private ClarificationHistoryOfSrsDAO  clarificationHistoryOfSrsDAO = new ClarificationHistoryOfSrsDAOImpl();
	
	private InventoryItemsMainLedgerDAO  inventoryItemsMainLedgerDAO = new InventoryItemsMainLedgerDAOImpl();
	
	private ConditionOfThirdPartyDecissionHistory conditionOfThirdPartyDecissionHistory  = new ConditionOfThirdPartyDecissionHistory();
	private ConditionOfThirdPartyDecissionHistoryDAO  conditionOfThirdPartyDecissionHistoryDAO  = new  ConditionOfThirdPartyDecissionHistoryDAOImpl();

	private List<DetailsOfLpAndSiOrders>  getList = new ArrayList<DetailsOfLpAndSiOrders>();
	private List<DetailsOfLpAndSiOrders>  getFilteredList = new ArrayList<DetailsOfLpAndSiOrders>();
	private List<DetailsOfLpAndSiOrders>  getFilesList = new ArrayList<DetailsOfLpAndSiOrders>();
	private List<DetailsOfLpAndSiOrders>  getFilesDetails = new ArrayList<DetailsOfLpAndSiOrders>();
	private List<DetailsOfLpAndSiOrders> getStockItemDetailsOfFiles = new ArrayList<DetailsOfLpAndSiOrders>();
	private List<DetailsOfLpAndSiOrders> getAllDetailsOfStockItems = new ArrayList<DetailsOfLpAndSiOrders>();
	private List<InventoryItemsMainLedger>  getDes = new ArrayList<InventoryItemsMainLedger>();
	private List<DetailsOfLpAndSiOrders>  getOutstandsFrom = new ArrayList<DetailsOfLpAndSiOrders>();
	private List<DetailsOfLpAndSiOrders>  getDetOfStoIteInFil = new ArrayList<DetailsOfLpAndSiOrders>();
	
	private TecRecommondHistory   tecRecHis = new TecRecommondHistory();
	private TecRecommondHistoryDAO  tecRecHisDAO = new TecRecommondHistoryDAOImpl();
	
	HttpServletRequest  request =  (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
	HttpServletResponse  response = (HttpServletResponse) ActionContext.getContext().get(ServletActionContext.HTTP_RESPONSE);
	
	DbBean  db = new DbBean();
	

	
	
	
	//String sessStoreNumber = (String) sessionglob.getAttribute("sessStoreNumber");
	
	
	
	
	public DetailsOfLpAndSiOrdersAction() {
		
		
		
	}
	
	public String execute() throws IOException {
		
		
		return SUCCESS;
	}
	
public String saveDetails(){
    	
    	
    	
HttpSession  session = request.getSession();
		
		Object  isLogedIn = session.getAttribute("isLogedIn");
		
		if(isLogedIn == null) {
			
		   return NONE;
		}
		
		session.setMaxInactiveInterval(1000);
		
		
		user = (String)session.getAttribute("str");
		//role = (String)session.getAttribute("strRole");
		subDept = (String)session.getAttribute("subDept");
		
        TimeAndDate   td = new TimeAndDate();
		
		enteredDate = td.Date();
		
		
		
	
		
		
		time = td.Time();
		
		String  dateTime  =  enteredDate  +  time;
		
		
		   String sNumPart3Fil = "";
		   String sNumPart4Fil = "";
		   String sNumPart5Fil = "";
		
		
		   String sNumPart1 = getStoreNum();
		   String fSl1 = " ";
		   String sNumPart2 = getStoreNumDigit1();
		   String fSl2 = "";
		   String sNumPart3 = getStoreNumDigit2();
		   String sNumPart5 =  getStoreNumDigit4();
		   String fS14 = "";
		   
		   if(sNumPart3.isEmpty()){
			   
			   sNumPart3Fil = "";
		   }else{
			   
			   fSl2 = "/";
			   sNumPart3Fil = sNumPart3;
		   }
		   
		   String fSl3 = "";
		   
		   String sNumPart4 = getStoreNumDigit3();
		   
		   if(sNumPart4.isEmpty()){
			   
			   sNumPart4Fil = "";
		   }else{
			   
			   fSl3 = "/";
			   sNumPart4Fil = sNumPart4;
		   }
		   
		   if(sNumPart5.isEmpty()){
			   
			   sNumPart5Fil = "";
			   
		   }else{
			   
			   fS14 = "/";
			   sNumPart5Fil = sNumPart5;
		   }
		   
	    //This file number check it comes from new registration or update same file
		   
		   String finFileNumber = "";
		   
		   
        sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
	
        //This session object set for update store number to same file
        setsStoreNumber(sStoreNumber);
        
        //session.setAttribute("sessFileNum",getFilenum());
        
        //finish setting session objects
        
        
        //Get file number
        String sameFileNumber = getFilenumsame();
        
        
        String fileNum = getFilenum() + " " + getFilenum1() + "/" + getFilenum2();
        
        
        if(sameFileNumber == null){
        	
        	finFileNumber = fileNum;
        	
        }else{
        	
        	finFileNumber = sameFileNumber;
        }
        
        
        String entLpNum = "";
        
        String lpNum = getLponum();
        
        if(lpNum.isEmpty()){
        	
        	entLpNum = "NOTEN-FILENUM";
        	
        }else{
        	
        	entLpNum = getLponum();
        }
        
        String entTenDate = "";
        
        String tenDate = getDate1();
     
        if(tenDate.isEmpty()){
        	
        	entTenDate = "NOTEN-FILE-DATE";
        }else{
        	
        	entTenDate = getDate1();
        }
        
        
        if((sStoreNumber != null) && (sStoreNumber != "") && (!sStoreNumber.isEmpty()) && (finFileNumber != null) && (sStoreNumber != "" ) && (!finFileNumber.isEmpty())){
    	
    	//detailsOfLpAndSiOrders.setId("");
    	detailsOfLpAndSiOrders.setSection(getType());
    	detailsOfLpAndSiOrders.setStorenumber(sStoreNumber);
    	detailsOfLpAndSiOrders.setTypeorder(getTypeorder());
    	detailsOfLpAndSiOrders.setSilpnum(entLpNum);
    	detailsOfLpAndSiOrders.setSilpdate(entTenDate);
    	detailsOfLpAndSiOrders.setSilpuser(getDepuser());
    	detailsOfLpAndSiOrders.setSilprole(getRole());
    	detailsOfLpAndSiOrders.setSilpsubdept(getSubdept());
    	detailsOfLpAndSiOrders.setSilpdateent(dateTime);
    	detailsOfLpAndSiOrders.setFilenum(finFileNumber);
    	detailsOfLpAndSiOrders.setFiledate(getDatefile());
    	detailsOfLpAndSiOrders.setNewfiledate(getNewfiledate());
    	detailsOfLpAndSiOrders.setTranscondition("false");
    	detailsOfLpAndSiOrders.setTecdate("Not processed");
    	detailsOfLpAndSiOrders.setUsertecdate("Not processed");
    	detailsOfLpAndSiOrders.setRoletecdate("Not processed");
    	detailsOfLpAndSiOrders.setDatetimetecdate("Not processed");
    	detailsOfLpAndSiOrders.setTecround(0);
    	detailsOfLpAndSiOrders.setTeccond("F");
    	detailsOfLpAndSiOrders.setTecrecommend("Not processed");
    	detailsOfLpAndSiOrders.setBidselect("Not processed");
    	detailsOfLpAndSiOrders.setNamesuccbider("Not processed");
    	detailsOfLpAndSiOrders.setRemarks("Not processed");
    	detailsOfLpAndSiOrders.setQuantity(0.0);
    	detailsOfLpAndSiOrders.setUnits("");
    	detailsOfLpAndSiOrders.setOrderedquantity(0.0);
    	detailsOfLpAndSiOrders.setOrderedunit("");
    	detailsOfLpAndSiOrders.setQuanpcmdecision("F");
    	detailsOfLpAndSiOrders.setQuanoldtec(0.0);
    	detailsOfLpAndSiOrders.setUnitprice(0.0);
    	detailsOfLpAndSiOrders.setCurrency("");
    	detailsOfLpAndSiOrders.setTotalvalue(0.0);
    	detailsOfLpAndSiOrders.setTecrecgivendate("Not processed");
    	detailsOfLpAndSiOrders.setUsertecrec("Not processed");
    	detailsOfLpAndSiOrders.setRoletecrec("Not processed");
    	detailsOfLpAndSiOrders.setDatetimetecrec("Not processed");
    	detailsOfLpAndSiOrders.setPcmdate("Not processed");
    	detailsOfLpAndSiOrders.setInspectrepdate("Not processed");
    	detailsOfLpAndSiOrders.setOrdplacedate("Not processed");
    	detailsOfLpAndSiOrders.setLcopendate("Not processed");
    	detailsOfLpAndSiOrders.setItemrecievdate("Not processed");
    	detailsOfLpAndSiOrders.setInvoicenum("Not processed");
    	detailsOfLpAndSiOrders.setSuitrepdate("Not processed");
    	detailsOfLpAndSiOrders.setUserdatelupda("Not processed");
    	detailsOfLpAndSiOrders.setRoledatelupda("Not processed");
    	detailsOfLpAndSiOrders.setDatetimelupda("Not processed");
    	detailsOfLpAndSiOrders.setPcmapp("Not processed");
    	detailsOfLpAndSiOrders.setPcmguidelines("Not processed");
    	detailsOfLpAndSiOrders.setUserpcmapp("Not processed");
    	detailsOfLpAndSiOrders.setRolepcmapp("Not processed");
    	detailsOfLpAndSiOrders.setDatetimepcmapp("Not processed");
    	detailsOfLpAndSiOrders.setSuitability("Not processed");
    	detailsOfLpAndSiOrders.setReason("Not processed");
    	detailsOfLpAndSiOrders.setShortfall("Not processed");
    	detailsOfLpAndSiOrders.setDamaged("Not processed");
    	detailsOfLpAndSiOrders.setSuitgivendate("Not processed");
    	detailsOfLpAndSiOrders.setUsersuit("Not processed");
    	detailsOfLpAndSiOrders.setRolesuit("Not processed");
    	detailsOfLpAndSiOrders.setDatetimesuit("Not processed");
    	detailsOfLpAndSiOrders.setAcknowcme("F");
    	detailsOfLpAndSiOrders.setUseracknowcme("F");
    	detailsOfLpAndSiOrders.setDateacknowcme("false");
    	detailsOfLpAndSiOrders.setClarificbysrs("Not processed");
    	detailsOfLpAndSiOrders.setClarifidatesrs("false");
    	detailsOfLpAndSiOrders.setClarifiusersrs("false");
    	detailsOfLpAndSiOrders.setClarifydesigusersrs("false");
    	detailsOfLpAndSiOrders.setCondclarificsrs("F");
    	detailsOfLpAndSiOrders.setPositionfile("CME");
    	detailsOfLpAndSiOrders.setLcopen("F");
    	detailsOfLpAndSiOrders.setLcnumber("Not processed");
    	detailsOfLpAndSiOrders.setLcdate(null);
    	detailsOfLpAndSiOrders.setUserlcentered("Not processed");
    	detailsOfLpAndSiOrders.setRolelcentered("Not processed");
    	detailsOfLpAndSiOrders.setDatelcentered("Not processed");
    	detailsOfLpAndSiOrders.setTranscomplete("F");
    	detailsOfLpAndSiOrders.setCondthirdpartyinspect("Not processed");
    	detailsOfLpAndSiOrders.setUserthirdpartyentered("Not processed");
    	detailsOfLpAndSiOrders.setRolethirdpartyentered("Not processed");
    	detailsOfLpAndSiOrders.setTimestampentered("Not processed");
    	
    	
    	
    	
    
    	
    	ResultSet  rs = null;
    	String check = "";
   
    	
    	
  /**
   * 
   * This copy table part is used to if user enter Store items in same file he can enter withou
   * entering other details except store number for his or her convenience. Therefore this values
   * store in details_lp_si_table_copy for retrieve again 
   * 
   * */
    	
   try {
    	db.connect();
    	
    	rs = db.execSql("SELECT store_num FROM details_lp_si_table_copy WHERE file_reg_user='" + getDepuser() + "'");
    	
    	while(rs.next()){
    		
    		 check = rs.getString("store_num");
    		 
    	}
    	
    
    		
    	if((check == "") && (check.isEmpty())) {
    		
    		detailsOfLpSiTableCopy.setSection(getType());
        	detailsOfLpSiTableCopy.setStorenumber(sStoreNumber);
        	detailsOfLpSiTableCopy.setOrdertype(getTypeorder());
        	detailsOfLpSiTableCopy.setSilpnum(getLponum());
        	detailsOfLpSiTableCopy.setLpsidate(getDate1());
        	detailsOfLpSiTableCopy.setFilenum(finFileNumber);
        	detailsOfLpSiTableCopy.setFiledate(getDatefile());
        	detailsOfLpSiTableCopy.setFileregdate(getNewfiledate());
        	detailsOfLpSiTableCopy.setFilereguser(getDepuser());
        	detailsOfLpSiTableCopy.setFileregrole(getRole());
        	
        	detailsOfLpSiTableCopyDAO.saveDetailCopy(detailsOfLpSiTableCopy);
        	
          }else {
    		
    		detailsOfLpSiTableCopyDAO.updateDetailCopy(getType(), sStoreNumber, getTypeorder(), getLponum(), getDate1(), finFileNumber, getDatefile(), getNewfiledate(), getDepuser(), getRole());
    		
    	}
    	
    	db.close();
    	
    	
    	
    	detailsOfLpAndSiOrdersDAO.registerLpAndSiOrders(detailsOfLpAndSiOrders);
    	
    
    	
    	}catch(ConstraintViolationException   cve) {
    		
             return ERROR;
             
    	}catch(HibernateException  he){
    		
    		
            return ERROR2; 
             
    	}catch(Exception  ex){
	
	      ex.printStackTrace();
       }
    
  
    
        } 
        
    	return SUCCESS;
    }
    
//*******************************************************************************************************
//*******************************************************************************************************
//********************************ADD EXTRA SAVE METHOD FOR UPDATE TO SAME FILE *************************


public String saveDetailsFromSameFileResult(){
	
	
	
HttpSession  session = request.getSession();
		
		Object  isLogedIn = session.getAttribute("isLogedIn");
		
		if(isLogedIn == null) {
			
		   return NONE;
		}
		
		session.setMaxInactiveInterval(1000);
		
		
		user = (String)session.getAttribute("str");
		//role = (String)session.getAttribute("strRole");
		subDept = (String)session.getAttribute("subDept");
		
        TimeAndDate   td = new TimeAndDate();
        
        
       /* Date date = new Date();
		SimpleDateFormat  sf = new SimpleDateFormat("yyyy-mm-dd");*/
		
		enteredDate = td.Date();
		time = td.Time();
		
		String  dateTime  =  enteredDate  +  time;
		
		
		   String sNumPart3Fil = "";
		   String sNumPart4Fil = "";
		   String sNumPart5Fil = "";
		
		
		   String sNumPart1 = getStoreNum();
		   String fSl1 = " ";
		   String sNumPart2 = getStoreNumDigit1();
		   String fSl2 = "";
		   String sNumPart3 = getStoreNumDigit2();
		   String sNumPart5 =  getStoreNumDigit4();
		   String fS14 = "";
		   
		   if(sNumPart3.isEmpty()){
			   
			   sNumPart3Fil = "";
		   }else{
			   
			   fSl2 = "/";
			   sNumPart3Fil = sNumPart3;
		   }
		   
		   String fSl3 = "";
		   
		   String sNumPart4 = getStoreNumDigit3();
		   
		   if(sNumPart4.isEmpty()){
			   
			   sNumPart4Fil = "";
		   }else{
			   
			   fSl3 = "/";
			   sNumPart4Fil = sNumPart4;
		   }
		   
		   if(sNumPart5.isEmpty()){
			   
			   sNumPart5Fil = "";
			   
		   }else{
			   
			   fS14 = "/";
			   sNumPart5Fil = sNumPart5;
		   }
		   
	    //This file number check it comes from new registration or update same file
		   
		   String finFileNumber = "";
		   
		   
        sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
	
        //This session object set for update store number to same file
        setsStoreNumber(sStoreNumber);
        
        //session.setAttribute("sessFileNum",getFilenum());
        
        //finish setting session objects
        
        
        //Get file number
        String sameFileNumber = getFilenumsame();
        
        
        String fileNum = getFilenum() + " " + getFilenum1() + "/" + getFilenum2();
        
        
        if(sameFileNumber == null){
        	
        	finFileNumber = fileNum;
        	
        }else{
        	
        	finFileNumber = sameFileNumber;
        }
        
        
        String entLpNum = "";
        
        String lpNum = getLponum();
        
        if(lpNum.isEmpty()){
        	
        	entLpNum = "NOTEN-FILENUM";
        	
        }else{
        	
        	entLpNum = getLponum();
        }
        
        String entTenDate = "";
        
        String tenDate = getDate1();
     
        if(tenDate.isEmpty()){
        	
        	entTenDate = "NOTEN-FILE-DATE";
        }else{
        	
        	entTenDate = getDate1();
        }
        
        
        if((sStoreNumber != null) && (sStoreNumber != "") && (!sStoreNumber.isEmpty()) && (finFileNumber != null) && (sStoreNumber != "" ) && (!finFileNumber.isEmpty())){
    	
    	//detailsOfLpAndSiOrders.setId("");
    	detailsOfLpAndSiOrders.setSection(getType());
    	detailsOfLpAndSiOrders.setStorenumber(sStoreNumber);
    	detailsOfLpAndSiOrders.setTypeorder(getTypeorder());
    	detailsOfLpAndSiOrders.setSilpnum(entLpNum);
    	detailsOfLpAndSiOrders.setSilpdate(entTenDate);
    	detailsOfLpAndSiOrders.setSilpuser(getDepuser());
    	detailsOfLpAndSiOrders.setSilprole(getRole());
    	detailsOfLpAndSiOrders.setSilpsubdept(getSubdept());
    	detailsOfLpAndSiOrders.setSilpdateent(dateTime);
    	detailsOfLpAndSiOrders.setFilenum(finFileNumber);
    	detailsOfLpAndSiOrders.setFiledate(getDatefile());
    	detailsOfLpAndSiOrders.setNewfiledate(getNewfiledate());
    	detailsOfLpAndSiOrders.setTranscondition("false");
    	detailsOfLpAndSiOrders.setTecdate("Not processed");
    	detailsOfLpAndSiOrders.setUsertecdate("Not processed");
    	detailsOfLpAndSiOrders.setRoletecdate("Not processed");
    	detailsOfLpAndSiOrders.setDatetimetecdate("Not processed");
    	detailsOfLpAndSiOrders.setTecround(0);
    	detailsOfLpAndSiOrders.setTeccond("F");
    	detailsOfLpAndSiOrders.setTecrecommend("Not processed");
    	detailsOfLpAndSiOrders.setBidselect("Not processed");
    	detailsOfLpAndSiOrders.setNamesuccbider("Not processed");
    	detailsOfLpAndSiOrders.setRemarks("Not processed");
    	detailsOfLpAndSiOrders.setQuantity(0.0);
    	detailsOfLpAndSiOrders.setUnits("");
    	detailsOfLpAndSiOrders.setOrderedquantity(0.0);
    	detailsOfLpAndSiOrders.setOrderedunit("");
    	detailsOfLpAndSiOrders.setQuanpcmdecision("F");
    	detailsOfLpAndSiOrders.setQuanoldtec(0.0);
    	detailsOfLpAndSiOrders.setUnitprice(0.0);
    	detailsOfLpAndSiOrders.setCurrency("");
    	detailsOfLpAndSiOrders.setTotalvalue(0.0);
    	detailsOfLpAndSiOrders.setTecrecgivendate("Not processed");
    	detailsOfLpAndSiOrders.setUsertecrec("Not processed");
    	detailsOfLpAndSiOrders.setRoletecrec("Not processed");
    	detailsOfLpAndSiOrders.setDatetimetecrec("Not processed");
    	detailsOfLpAndSiOrders.setPcmdate("Not processed");
    	detailsOfLpAndSiOrders.setInspectrepdate("Not processed");
    	detailsOfLpAndSiOrders.setOrdplacedate("Not processed");
    	detailsOfLpAndSiOrders.setLcopendate("Not processed");
    	detailsOfLpAndSiOrders.setItemrecievdate("Not processed");
    	detailsOfLpAndSiOrders.setInvoicenum("Not processed");
    	detailsOfLpAndSiOrders.setSuitrepdate("Not processed");
    	detailsOfLpAndSiOrders.setUserdatelupda("Not processed");
    	detailsOfLpAndSiOrders.setRoledatelupda("Not processed");
    	detailsOfLpAndSiOrders.setDatetimelupda("Not processed");
    	detailsOfLpAndSiOrders.setPcmapp("Not processed");
    	detailsOfLpAndSiOrders.setPcmguidelines("Not processed");
    	detailsOfLpAndSiOrders.setUserpcmapp("Not processed");
    	detailsOfLpAndSiOrders.setRolepcmapp("Not processed");
    	detailsOfLpAndSiOrders.setDatetimepcmapp("Not processed");
    	detailsOfLpAndSiOrders.setSuitability("Not processed");
    	detailsOfLpAndSiOrders.setReason("Not processed");
    	detailsOfLpAndSiOrders.setShortfall("Not processed");
    	detailsOfLpAndSiOrders.setDamaged("Not processed");
    	detailsOfLpAndSiOrders.setSuitgivendate("Not processed");
    	detailsOfLpAndSiOrders.setUsersuit("Not processed");
    	detailsOfLpAndSiOrders.setRolesuit("Not processed");
    	detailsOfLpAndSiOrders.setDatetimesuit("Not processed");
    	detailsOfLpAndSiOrders.setAcknowcme("F");
    	detailsOfLpAndSiOrders.setUseracknowcme("F");
    	detailsOfLpAndSiOrders.setDateacknowcme("false");
    	detailsOfLpAndSiOrders.setClarificbysrs("Not processed");
    	detailsOfLpAndSiOrders.setClarifidatesrs("false");
    	detailsOfLpAndSiOrders.setClarifiusersrs("false");
    	detailsOfLpAndSiOrders.setClarifydesigusersrs("false");
    	detailsOfLpAndSiOrders.setCondclarificsrs("F");
    	detailsOfLpAndSiOrders.setPositionfile("CME");
    	detailsOfLpAndSiOrders.setLcopen("F");
    	detailsOfLpAndSiOrders.setLcnumber("Not processed");
    	detailsOfLpAndSiOrders.setLcdate(null);
    	detailsOfLpAndSiOrders.setUserlcentered("Not processed");
    	detailsOfLpAndSiOrders.setRolelcentered("Not processed");
    	detailsOfLpAndSiOrders.setDatelcentered("Not processed");
    	detailsOfLpAndSiOrders.setTranscomplete("F");
    	detailsOfLpAndSiOrders.setCondthirdpartyinspect("Not processed");
    	detailsOfLpAndSiOrders.setUserthirdpartyentered("Not processed");
    	detailsOfLpAndSiOrders.setRolethirdpartyentered("Not processed");
    	detailsOfLpAndSiOrders.setTimestampentered("Not processed");
    	
    	
    	
    	
    
    	
    	ResultSet  rs = null;
    	String check = "";
   
    	
    	
  /**
   * 
   * This copy table part is used to if user enter Store items in same file he can enter withou
   * entering other details except store number for his or her convenience. Therefore this values
   * store in details_lp_si_table_copy for retrieve again 
   * 
   * */
    	
    try {
    	db.connect();
    	
    	rs = db.execSql("SELECT store_num FROM details_lp_si_table_copy WHERE file_reg_user='" + getDepuser() + "'");
    	
    	while(rs.next()){
    		
    		 check = rs.getString("store_num");
    		 
    	}
    	
    
    		
    	if((check == "") && (check.isEmpty())) {
    		
    		detailsOfLpSiTableCopy.setSection(getType());
        	detailsOfLpSiTableCopy.setStorenumber(sStoreNumber);
        	detailsOfLpSiTableCopy.setOrdertype(getTypeorder());
        	detailsOfLpSiTableCopy.setSilpnum(getLponum());
        	detailsOfLpSiTableCopy.setLpsidate(getDate1());
        	detailsOfLpSiTableCopy.setFilenum(finFileNumber);
        	detailsOfLpSiTableCopy.setFiledate(getDatefile());
        	detailsOfLpSiTableCopy.setFileregdate(getNewfiledate());
        	detailsOfLpSiTableCopy.setFilereguser(getDepuser());
        	detailsOfLpSiTableCopy.setFileregrole(getRole());
        	
        	detailsOfLpSiTableCopyDAO.saveDetailCopy(detailsOfLpSiTableCopy);
        	
          }else {
    		
    		detailsOfLpSiTableCopyDAO.updateDetailCopy(getType(), sStoreNumber, getTypeorder(), getLponum(), getDate1(), finFileNumber, getDatefile(), getNewfiledate(), getDepuser(), getRole());
    		
    	}
    	
    	db.close();
    	
    	
    	detailsOfLpAndSiOrdersDAO.registerLpAndSiOrders(detailsOfLpAndSiOrders);
    	
    	
    
    	
    	}catch(ConstraintViolationException   cve) {
    		
             return ERROR;
             
             
    	}catch(Exception  ex){
	
	      ex.printStackTrace();
       }
    
    try{
    
       db.connect();
       
       String testEnt = "";
    		   
       
       ResultSet checkEntered = db.execSql("SELECT store_num FROM details_lp_si_table WHERE store_num='" + sStoreNumber + "'");
       
       while(checkEntered.next()){
    	   
    	   testEnt = checkEntered.getString("store_num");
    	   
    	   if((testEnt == "") && (testEnt.isEmpty())){
    		   
    		   return ERROR1;
    		   
    	   }
    	   
       }
       
    }catch(Exception  ex){
    	
    	ex.printStackTrace();
    }
    
        }else{
        	
        	
        	return ERROR2;
        }
       
       
    	
    	return SUCCESS;
    }
    


//*******************************************************************************************************
public String updateFileRegister() {
	
                  HttpSession  session = request.getSession();

                      user = (String)session.getAttribute("str");
                      role1 = (String)session.getAttribute("strRole");
                      subDept = (String)session.getAttribute("subDept");

    	
            Object  isLogedIn = session.getAttribute("isLogedIn");
		
		             if(isLogedIn == null) {
			
		                 return NONE;
		      }
		
		          session.setMaxInactiveInterval(1000);
		
		
        TimeAndDate   td = new TimeAndDate();
		
		enteredDate = td.Date();
		time = td.Time();
		
		String  dateTime  =  enteredDate  +  time;
		
		
		   String sNumPart3Fil = "";
		   String sNumPart4Fil = "";
		   String sNumPart5Fil = "";
		
		
		   String sNumPart1 = getStoreNum();
		   String fSl1 = " ";
		   String sNumPart2 = getStoreNumDigit1();
		   String fSl2 = "";
		   String sNumPart3 = getStoreNumDigit2();
		   String sNumPart5 =  getStoreNumDigit4();
		   String fS14 = "";
		   
		   if(sNumPart3.isEmpty()){
			   
			   sNumPart3Fil = "";
		   }else{
			   
			   fSl2 = "/";
			   sNumPart3Fil = sNumPart3;
		   }
		   
		   String fSl3 = "";
		   
		   String sNumPart4 = getStoreNumDigit3();
		   
		   if(sNumPart4.isEmpty()){
			   
			   sNumPart4Fil = "";
		   }else{
			   
			   fSl3 = "/";
			   sNumPart4Fil = sNumPart4;
		   }
		   
		   if(sNumPart5.isEmpty()){
			   
			   sNumPart5Fil = "";
			   
		   }else{
			   
			   fS14 = "/";
			   sNumPart5Fil = sNumPart5;
		   }		   
		   
        sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
        
        String fileNum = getFilenum() + " " + getFilenum1() + "/" + getFilenum2();
		  	 
       try { 
        detailsOfLpAndSiOrdersDAO.updateRegisterFile(fileNum, sStoreNumber, getDate1(), getDepuser(), getRole(),dateTime);
    	
       }catch(ConstraintViolationException   cve) {
   		
           return ERROR;
  	    }
        
    	return SUCCESS;
    }

public String TecRecommendationTypeSelection(){
	
	HttpSession  session = request.getSession();
	
	
	Object  isLogedIn = session.getAttribute("isLogedIn");
	
	if(isLogedIn == null) {
		
	   return NONE;
	}
	
	session.setMaxInactiveInterval(1000);
	
	
	
	   String sNumPart3Fil = "";
	   String sNumPart4Fil = "";
	   String sNumPart5Fil = "";
	
	
	   String sNumPart1 = getStoreNum();
	   String fSl1 = " ";
	   String sNumPart2 = getStoreNumDigit1();
	   String fSl2 = "";
	   String sNumPart3 = getStoreNumDigit2();
	   String sNumPart5 =  getStoreNumDigit4();
	   String fS14 = "";
	   
	   if(sNumPart3.isEmpty()){
		   
		   sNumPart3Fil = "";
	   }else{
		   
		   fSl2 = "/";
		   sNumPart3Fil = sNumPart3;
	   }
	   
	   String fSl3 = "";
	   
	   String sNumPart4 = getStoreNumDigit3();
	   
	   if(sNumPart4.isEmpty()){
		   
		   sNumPart4Fil = "";
	   }else{
		   
		   fSl3 = "/";
		   sNumPart4Fil = sNumPart4;
	   }
	   
	   if(sNumPart5.isEmpty()){
		   
		   sNumPart5Fil = "";
		   
	   }else{
		   
		   fS14 = "/";
		   sNumPart5Fil = sNumPart5;
	   }		   
	   
    sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
    
    String fileNum = getFilenum() + " " + getFilenum1() + "/" + getFilenum2();
    	
	
	
	getList = detailsOfLpAndSiOrdersDAO.getDetailList(fileNum, sStoreNumber);
	
    
     
     for(DetailsOfLpAndSiOrders  list : getList){
    	 
    	    String tecRecommendation = list.getTecrecommend();
    	 
    /*
     * 
     * Select relevant type depend on recommend type
     * 
     * 	 
     */
    	 
    	 switch(tecRecommendation){
    	 
    	       case "Clarification":
    	 
    	             return CLARIFY;
    	             
    	       case "Recommended With Third Party Inspection":
    	    	   
    	    	     return THIRDPARTY;
    	    	     
    	       case "Not processed":
    	    	   
    	    	     return NOTPROC;
    	    	     
    	       case "Recommended":
    	    	   
    	    	     return RECOMMEND;
    	    	     
    	       case "Recommended Further Clarifications":
    	    	     
    	    	     return CLARIFY;
    	    	     
    	       case "Recommended Further Third Party Inspection":
    	    	   
    	    	     return THIRDPARTY;
    	    	     
    	       case "Cancelled":
    	    	   
    	    	    return CANCELED;
    	                         
    	 }
    	 
     }
	
	
	
	return SUCCESS;
}


/*
 * This method is used to update details_srs_clarific_history_table when enter data by cme TEC
 * after get clarifications from srs. New status recorded in original table and past data recorded
 * in this table. This method invoked by EnterDataAfterClarification.jsp
 */

public String addClarificationDetailsToHistoryBySrs(){
	
	
	/*
	 * Call session to in activate session after logging expired.
	 */
	
	HttpSession  session = request.getSession();

	user = (String)session.getAttribute("str");
	role1 = (String)session.getAttribute("strRole");
	subDept = (String)session.getAttribute("subDept");
	
	
	
	TimeAndDate   td = new TimeAndDate();
	
	enteredDate = td.Date();
	time = td.Time();
	
	String  dateTime  =  enteredDate  +  time;
	

	    	
	Object  isLogedIn = session.getAttribute("isLogedIn");
			
			if(isLogedIn == null) {
				
			   return NONE;
			}
			
			session.setMaxInactiveInterval(1000);
			
    String filenumber = getFilenumber(); 
    String storenumber = getStorenumber();
    
    
    
    String testSelect = getSelection();
       	
    	
   /* if(testSelect.equals("SELBYSRS")){
    	
    	select = "Selected the most economical supplier by SRS";
    	
    }else if(testSelect.equals("SELBYCME")){
    	
    	select = getSuccBidder();
    	
    }*/
    if(testSelect == null){
    	
    	select = "Refer History";
    
    }else if(testSelect.matches("SELBYCME")){
    	
    	select = getSuccBidder();
    	
    }else if(testSelect.matches("SELBYSRS")){
    	
    	select = "Selected the most economical supplier by SRS";
    	
    }
    
    //Calculation
    double toValue = getOrderedQuantity() * getUprice();
	
    String tranCond = "";
    
    if(getTecrecommend().matches("Fresh quatation")) {
    	
    	tranCond = "true";
    	
    }else {
    	
    	
    	tranCond = "false";
    }
    
    
    //Check values of Selection
    
    
    double entQuan =  getQuantity();
     double desidedQuan = 0.0;
     
     if(entQuan == 0){
  	  
  	   desidedQuan = 0.0;
  	   
     }else{
  	   
  	   desidedQuan = getQuantity();
     }
     
     String enUnits = getUnits();
     String decidedUnit = "";
     
     if(enUnits == ""){
  	   
  	   decidedUnit = "";
  	   
     }else{
  	   
  	   decidedUnit = getUnits();
     }
     
    double entOrdQuan = getOrderedQuantity();
    double decidedOrdQuan = 0.0;
    
    if(entOrdQuan == 0){
  	  
  	  decidedOrdQuan = 0.0;
  	  
    }else{
  	  
  	  decidedOrdQuan = getOrderedQuantity();
    }
    
   String entOrdUnits = getOrderedUnit();
   String decidedOrdUnit = "";
   
   if(entOrdUnits == ""){
  	 
  	decidedOrdUnit = "";
  	 
   }else{
  	 
  	decidedOrdUnit = getOrderedUnit();
   }
   
  double entUPrice = getUprice();
  double decidedUPrice = 0.0;
  
  if(entUPrice == 0){
  	
  	decidedUPrice = 0.0;
  }else{
  	
  	decidedUPrice = getUprice();
  }
  
  
 String entCurrency = getCurrency();
 String decidedCurr = "";
 
 if(entCurrency == ""){
	   
	  decidedCurr = "";
 }else{
	   
	  decidedCurr = getCurrency();
 }
         
	
	getList = detailsOfLpAndSiOrdersDAO.getDetailList(filenumber, storenumber);	
	
	for(DetailsOfLpAndSiOrders  list : getList){
		
		
		clarificationHistoryOfSrs.setStorenumber(storenumber);
		clarificationHistoryOfSrs.setFilenum(filenumber);
		clarificationHistoryOfSrs.setHistecrec(list.getTecrecommend());
		clarificationHistoryOfSrs.setHisusertecrec(list.getUsertecrec());
		clarificationHistoryOfSrs.setHisroletecrec(list.getRoletecrec());
		clarificationHistoryOfSrs.setHissrsclarific(list.getClarificbysrs());
		clarificationHistoryOfSrs.setHisclarificdatesrs(list.getClarifidatesrs());
		clarificationHistoryOfSrs.setHisclarificusersrs(list.getClarifiusersrs());
		clarificationHistoryOfSrs.setHisclarificdesigsrs(list.getClarifydesigusersrs());
		clarificationHistoryOfSrs.setTecround(list.getTecround());
		
		//try{
		
		clarificationHistoryOfSrsDAO.saveHIstoryData(clarificationHistoryOfSrs);
		
		//}catch(HibernateException e){
			
		    // return UPDATERROR;
			
		//}
		
	}
	
	try{
	
	detailsOfLpAndSiOrdersDAO.updateSrsClarifications(storenumber, filenumber, getTecrecommend(),getTecround(),getTecrecdate(), getDepuser(), getRole(),dateTime, tranCond,select,getRemarks(),desidedQuan,decidedUnit,decidedOrdQuan,decidedOrdUnit,decidedUPrice,decidedCurr,toValue);
				
	}catch(HibernateException e){
		
	     return UPDATERROR;
		
	}
	
	return SUCCESS;
	
	
}


/*
 * This method call user going to update TEC after third party inspection report
 * received. Here user decide weather re order or what decision has to be taken.
 */


public String ConditionOfThirdPartyReport(){
	
	HttpSession  session = request.getSession();

	user = (String)session.getAttribute("str");
	role1 = (String)session.getAttribute("strRole");
	subDept = (String)session.getAttribute("subDept");

	    	
	Object  isLogedIn = session.getAttribute("isLogedIn");
			
			if(isLogedIn == null) {
				
			   return NONE;
			}
			
			session.setMaxInactiveInterval(1000);
			
			
	        TimeAndDate   td = new TimeAndDate();
			
			enteredDate = td.Date();
			time = td.Time();
			
			String timestamp = enteredDate +":" +time;
			
			
			String filenumber = getFilenumber(); 
		    String storenumber = getStorenumber();
		    
			
			
			getList = detailsOfLpAndSiOrdersDAO.getDetailList(filenumber, storenumber);	
			
			for(DetailsOfLpAndSiOrders  list : getList){
				
				conditionOfThirdPartyDecissionHistory.setStorenumber(storenumber);
				conditionOfThirdPartyDecissionHistory.setFilenumber(filenumber);
				
				
				conditionOfThirdPartyDecissionHistoryDAO.saveThirdpartyConditionsHistory(conditionOfThirdPartyDecissionHistory);
			}
			
	
			try{
			
			detailsOfLpAndSiOrdersDAO.thirdPartyInspectionReportUpdate(storenumber, filenumber, getTecrecommend(), getThirdpartyinspectrep(), getUser(), getRole(), timestamp);
	
        }catch(HibernateException e){
	
                   return UPDATERROR;
	
}

	
	
	return SUCCESS;
}


public String updateTecRecommendation() {
	
	HttpSession  session = request.getSession();

	user = (String)session.getAttribute("str");
	role1 = (String)session.getAttribute("strRole");
	subDept = (String)session.getAttribute("subDept");
	
	
	String sucBidderSelec = getSelection();

	    	
	Object  isLogedIn = session.getAttribute("isLogedIn");
			
			if(isLogedIn == null) {
				
			   return NONE;
			}
			
			session.setMaxInactiveInterval(1000);
			
			
	        TimeAndDate   td = new TimeAndDate();
			
			enteredDate = td.Date();
			time = td.Time();
			
			String  dateTime  =  enteredDate  +  time;
			
			
			   String sNumPart3Fil = "";
			   String sNumPart4Fil = "";
			   String sNumPart5Fil = "";
			
			
			   String sNumPart1 = getStoreNum();
			   String fSl1 = " ";
			   String sNumPart2 = getStoreNumDigit1();
			   String fSl2 = "";
			   String sNumPart3 = getStoreNumDigit2();
			   String sNumPart5 =  getStoreNumDigit4();
			   String fS14 = "";
			   
			   if(sNumPart3.isEmpty()){
				   
				   sNumPart3Fil = "";
			   }else{
				   
				   fSl2 = "/";
				   sNumPart3Fil = sNumPart3;
			   }
			   
			   String fSl3 = "";
			   
			   String sNumPart4 = getStoreNumDigit3();
			   
			   if(sNumPart4.isEmpty()){
				   
				   sNumPart4Fil = "";
			   }else{
				   
				   fSl3 = "/";
				   sNumPart4Fil = sNumPart4;
			   }
			   
			   if(sNumPart5.isEmpty()){
				   
				   sNumPart5Fil = "";
				   
			   }else{
				   
				   fS14 = "/";
				   sNumPart5Fil = sNumPart5;
			   }		   
			   
	        sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
	        
	     
		        
	        
            String testSelect = getSelection();
            
            
            
            String fileNum = getFilenum() + " " + getFilenum1() + "/" + getFilenum2();
            	
            	
            	
           /* if(testSelect.equals("SELBYSRS")){
            	
            	select = "Selected the most economical supplier by SRS";
            	
            }else if(testSelect.equals("SELBYCME")){
            	
            	select = getSuccBidder();
            	
            }*/
            if(testSelect == null){
            	
            	select = "Refer History";
	        
            }else if(testSelect.matches("SELBYCME")){
	        	
	        	select = getSuccBidder();
	        	
	        }else if(testSelect.matches("SELBYSRS")){
	        	
	        	select = "Selected the most economical supplier by SRS";
	        	
	        }
	        
	        //Calculation
	        double toValue = getOrderedQuantity() * getUprice();
			
	        String tranCond = "";
	        
	        if(getTecrecommend().matches("Fresh quatation")) {
	        	
	        	tranCond = "true";
	        	
	        }else {
	        	
	        	
	        	tranCond = "false";
	        }
	        
	        
	        //Check values of Selection
	        
	       
	        
	        
	
	        try {
	        	
	        	String tecRec = "";
	        	int tecRound = 0;
	        	
	        	 getList = detailsOfLpAndSiOrdersDAO.getDetailList(fileNum, sStoreNumber);
		 	        
		 	        for(DetailsOfLpAndSiOrders  list : getList){
		 	        	
		 	        	
		 	        	String storeNum = list.getStorenumber();
		 	        	String fileNumFromList = list.getFilenum();
		 	        	String siLpoNum = list.getSilpnum();
		 	        	tecRec = list.getTecrecommend();
		 	        	tecRound = list.getTecround();
		 	        	String remarksEnt = list.getRemarks();
		 	        	String succBidderEnt = list.getNamesuccbider();
		 	        	String userEnt = list.getUsertecrec();
		 	        	String roleEnt = list.getRoletecrec();
		 	        	String dateEnt = list.getDatetimetecrec();
		 	        	
		 	        	
		 	        
		 	        	tecRecHis.setStorenumber(storeNum);
		 	        	tecRecHis.setFilenum(fileNumFromList);
		 	        	tecRecHis.setSilpnum(siLpoNum);
		 	        	tecRecHis.setTecrec(tecRec);
		 	        	tecRecHis.setTecround(list.getTecround());
		 	        	tecRecHis.setRemarks(remarksEnt);
		 	        	tecRecHis.setSuccbidder(succBidderEnt);
		 	        	tecRecHis.setUsertecrec(userEnt);
		 	        	tecRecHis.setRoletecrec(roleEnt);
		 	        	tecRecHis.setTimedatetecrec(dateEnt);
		 	        	
		 	        	
		 	        	tecRecHisDAO.saveTecRecommondHistory(tecRecHis);
		 	        	
		 	        	
		 	        }
		 	        
		 	      //Check values "" or not
		 	        
		 	       double entQuan =  getQuantity();
		 	       double desidedQuan = 0.0;
		 	       
		 	       if(entQuan == 0){
		 	    	  
		 	    	   desidedQuan = 0.0;
		 	    	   
		 	       }else{
		 	    	   
		 	    	   desidedQuan = getQuantity();
		 	       }
		 	       
		 	       String enUnits = getUnits();
		 	       String decidedUnit = "";
		 	       
		 	       if(enUnits == ""){
		 	    	   
		 	    	   decidedUnit = "";
		 	    	   
		 	       }else{
		 	    	   
		 	    	   decidedUnit = getUnits();
		 	       }
		 	       
		 	      double entOrdQuan = getOrderedQuantity();
		 	      double decidedOrdQuan = 0.0;
		 	      
		 	      if(entOrdQuan == 0){
		 	    	  
		 	    	  decidedOrdQuan = 0.0;
		 	    	  
		 	      }else{
		 	    	  
		 	    	  decidedOrdQuan = getOrderedQuantity();
		 	      }
		 	      
		 	     String entOrdUnits = getOrderedUnit();
		 	     String decidedOrdUnit = "";
		 	     
		 	     if(entOrdUnits == ""){
		 	    	 
		 	    	decidedOrdUnit = "";
		 	    	 
		 	     }else{
		 	    	 
		 	    	decidedOrdUnit = getOrderedUnit();
		 	     }
		 	     
		 	    double entUPrice = getUprice();
		 	    double decidedUPrice = 0.0;
		 	    
		 	    if(entUPrice == 0){
		 	    	
		 	    	decidedUPrice = 0.0;
		 	    }else{
		 	    	
		 	    	decidedUPrice = getUprice();
		 	    }
		 	    
		 	    
		 	   String entCurrency = getCurrency();
		 	   String decidedCurr = "";
		 	   
		 	   if(entCurrency == ""){
		 		   
		 		  decidedCurr = "";
		 	   }else{
		 		   
		 		  decidedCurr = getCurrency();
		 	   }
		 	   
		 	   String tecCond = "T";
		 	       
		 	  //To check null value
		 	   
	       /* if((fileNum == "") || (fileNum == null) || (fileNum.isEmpty()) || (sStoreNumber == "") || (sStoreNumber == null) || (sStoreNumber.isEmpty()) || (getTecrecommend() == "") || (getTecrecommend() == null ) || (getTecrecommend().isEmpty())){
			 		
			 		
			 		return REPEAT;*/
			 		
			 		
	       /* }else if(getTecrecommend().matches("Recommended With Third Party Inspection")){
			 		
	        	
	        	    return THIRDPARTY;
	        	    
			 		
	        }else if(getTecrecommend().matches("Clarification")){
				
				    
				    return CLARIFY;
				*/
				
			//}else{
				
	       
	               detailsOfLpAndSiOrdersDAO.updateTecRecommondation(fileNum, sStoreNumber, getTecrecommend(),getTecround(),tecCond,getTecrecdate(), getDepuser(), getRole(),dateTime,tranCond,getSelection(),select,getRemarks(),desidedQuan,decidedUnit,decidedOrdQuan,decidedOrdUnit,decidedUPrice,decidedCurr,toValue );
	        
	        
		 	//}
	        
	       
	             
	    	  }catch(HibernateException e){
	    		
	    		
	    		return REPEAT;
	   }
	        
	        
	        
	        
	       /* try{
	            
	            db.connect();
	            
	            String testEnt = "";
	         		   
	            
	           ResultSet checkEntered = db.execSql("SELECT tec_recommond FROM details_lp_si_table WHERE store_num='" + sStoreNumber + "' AND file_num='" + fileNum + "'");
	            
	            while(checkEntered.next()){
	         	   
	         	   testEnt = checkEntered.getString("tec_recommond");
	         	   
	         	   if(testEnt.matches("Not processed")){
	         		   
	         		   return REPEAT;
	         		   
	         	   }
	         	   
	            }
	            
	         }catch(Exception  ex){
	         	
	         	ex.printStackTrace();
	     }
	            
	              */
	        
	        
	      return SUCCESS;
	      
	        
}


/** 
 * 
 * This method integrated to TecRecommondation method
 * 
 * */
public String updateSuccesfulBidder() {
	
	
	HttpSession  session = request.getSession();

	user = (String)session.getAttribute("str");
	role1 = (String)session.getAttribute("strRole");
	subDept = (String)session.getAttribute("subDept");

	    	
	/*Object  isLogedIn = session.getAttribute("isLogedIn");
			
			if(isLogedIn == null) {
				
			   return NONE;
			}
			
			session.setMaxInactiveInterval(1000);*/
			
			
	        TimeAndDate   td = new TimeAndDate();
			
			enteredDate = td.Date();
			time = td.Time();
			
			String  dateTime  =  enteredDate  +  time;
			
			
			   String sNumPart3Fil = "";
			   String sNumPart4Fil = "";
			   String sNumPart5Fil = "";
			
			
			   String sNumPart1 = getStoreNum();
			   String fSl1 = " ";
			   String sNumPart2 = getStoreNumDigit1();
			   String fSl2 = "";
			   String sNumPart3 = getStoreNumDigit2();
			   String sNumPart5 =  getStoreNumDigit4();
			   String fS14 = "";
			   
			   if(sNumPart3.isEmpty()){
				   
				   sNumPart3Fil = "";
			   }else{
				   
				   fSl2 = "/";
				   sNumPart3Fil = sNumPart3;
			   }
			   
			   String fSl3 = "";
			   
			   String sNumPart4 = getStoreNumDigit3();
			   
			   if(sNumPart4.isEmpty()){
				   
				   sNumPart4Fil = "";
				   
			   }else{
				   
				   fSl3 = "/";
				   sNumPart4Fil = sNumPart4;
			   }
			   
			   if(sNumPart5.isEmpty()){
				   
				   sNumPart5Fil = "";
				   
			   }else{
				   
				   fS14 = "/";
				   sNumPart5Fil = sNumPart5;
			   }		   
			   
	        sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
	        
	        
	        String testSelect = getSelection();
	        
	        if(testSelect.equals("SELBYCME")){
	        	
	        	select = getSuccBidder();
	        	
	        }else{
	        	
	        	select = "Selected the most economical supplier by SRS";
	        }
	        
			  	    	
	        try { 
	        	
	        	
	            getList = detailsOfLpAndSiOrdersDAO.getDetailList(getFilenum(), sStoreNumber);
	 	        
	 	        for(DetailsOfLpAndSiOrders  list : getList){
	 	        	
	 	        	
	 	        	String storeNum = list.getStorenumber();
	 	        	String fileNum = list.getFilenum();
	 	        	String siLpoNum = list.getSilpnum();
	 	        	String tecRec = list.getTecrecommend();
	 	        	String remarksEnt = list.getRemarks();
	 	        	String succBidderEnt = list.getNamesuccbider();
	 	        	String userEnt = list.getUsertecrec();
	 	        	String roleEnt = list.getRoletecrec();
	 	        	String dateEnt = list.getDatetimetecrec();
	 	        	
	 	        	
	 	        
	 	        	tecRecHis.setStorenumber(storeNum);
	 	        	tecRecHis.setFilenum(fileNum);
	 	        	tecRecHis.setSilpnum(siLpoNum);
	 	        	tecRecHis.setTecrec(tecRec);
	 	        	tecRecHis.setRemarks(remarksEnt);
	 	        	tecRecHis.setSuccbidder(succBidderEnt);
	 	        	tecRecHis.setUsertecrec(userEnt);
	 	        	tecRecHis.setRoletecrec(roleEnt);
	 	        	tecRecHis.setTimedatetecrec(dateEnt);
	 	        	
	 	        	
	 	        	tecRecHisDAO.saveTecRecommondHistory(tecRecHis);
	 	        	
	 	        	
	 	        }
	 	        	        	
	        
	        detailsOfLpAndSiOrdersDAO.updateNameSuccBidder(getFilenum(), sStoreNumber, select, getRemarks());
	
	        }catch(ConstraintViolationException   cve) {
	    		
	             return ERROR;
	    	}
	        
	return SUCCESS;
}

public String updateRemarks() {
	
	HttpSession  session = request.getSession();

	user = (String)session.getAttribute("str");
	role1 = (String)session.getAttribute("strRole");
	subDept = (String)session.getAttribute("subDept");

	    	
	/*Object  isLogedIn = session.getAttribute("isLogedIn");
			
			if(isLogedIn == null) {
				
			   return NONE;
			}
			
			session.setMaxInactiveInterval(1000);*/
			
			
	        TimeAndDate   td = new TimeAndDate();
			
			enteredDate = td.Date();
			time = td.Time();
			
			String  dateTime  =  enteredDate  +  time;
			
			
			   String sNumPart3Fil = "";
			   String sNumPart4Fil = "";
			   String sNumPart5Fil = "";
			
			
			   String sNumPart1 = getStoreNum();
			   String fSl1 = " ";
			   String sNumPart2 = getStoreNumDigit1();
			   String fSl2 = "";
			   String sNumPart3 = getStoreNumDigit2();
			   String sNumPart5 =  getStoreNumDigit4();
			   String fS14 = "";
			   
			   if(sNumPart3.isEmpty()){
				   
				   sNumPart3Fil = "";
			   }else{
				   
				   fSl2 = "/";
				   sNumPart3Fil = sNumPart3;
			   }
			   
			   String fSl3 = "";
			   
			   String sNumPart4 = getStoreNumDigit3();
			   
			   if(sNumPart4.isEmpty()){
				   
				   sNumPart4Fil = "";
			   }else{
				   
				   fSl3 = "/";
				   sNumPart4Fil = sNumPart4;
			   }
			   
			   if(sNumPart5.isEmpty()){
				   
				   sNumPart5Fil = "";
				   
			   }else{
				   
				   fS14 = "/";
				   sNumPart5Fil = sNumPart5;
			   }		   
			   
	        sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
			
	        try {
	        	
	        	
	        	 getList = detailsOfLpAndSiOrdersDAO.getDetailList(getFilenum(), sStoreNumber);
	 	        
	 	        for(DetailsOfLpAndSiOrders  list : getList){
	 	        	
	 	        	
	 	        	String storeNum = list.getStorenumber();
	 	        	String fileNum = list.getFilenum();
	 	        	String siLpoNum = list.getSilpnum();
	 	        	String tecRec = list.getTecrecommend();
	 	        	String remarksEnt = list.getRemarks();
	 	        	String succBidderEnt = list.getNamesuccbider();
	 	        	String userEnt = list.getUsertecrec();
	 	        	String roleEnt = list.getRoletecrec();
	 	        	String dateEnt = list.getDatetimetecrec();
	 	        	
	 	        	
	 	        	//tecRecHis.setId("");
	 	        	tecRecHis.setStorenumber(storeNum);
	 	        	tecRecHis.setFilenum(fileNum);
	 	        	tecRecHis.setSilpnum(siLpoNum);
	 	        	tecRecHis.setTecrec(tecRec);
	 	        	tecRecHis.setRemarks(remarksEnt);
	 	        	tecRecHis.setSuccbidder(succBidderEnt);
	 	        	tecRecHis.setUsertecrec(userEnt);
	 	        	tecRecHis.setRoletecrec(roleEnt);
	 	        	tecRecHis.setTimedatetecrec(dateEnt);
	 	        	
	 	        	
	 	        	tecRecHisDAO.saveTecRecommondHistory(tecRecHis);
	 	        	
	 	        	
	 	        }
	 	        	
	 	        
	        
	        detailsOfLpAndSiOrdersDAO.updateRemarks(getFilenum(), sStoreNumber, getRemarks());
	        
	        }catch(ConstraintViolationException   cve) {
	    		
	             return ERROR;
	    	}
	     
	return SUCCESS;
}

public String updateDateEvents() {
	
	HttpSession  session = request.getSession();

	user = (String)session.getAttribute("str");
	role1 = (String)session.getAttribute("strRole");
	subDept = (String)session.getAttribute("subDept");

	    	
	Object  isLogedIn = session.getAttribute("isLogedIn");
			
			if(isLogedIn == null) {
				
			   return NONE;
			}
			
			session.setMaxInactiveInterval(1000);
			
			
	        TimeAndDate   td = new TimeAndDate();
			
			enteredDate = td.Date();
			time = td.Time();
			
			String  dateTime  =  enteredDate  +  time;
			
			
			   String sNumPart3Fil = "";
			   String sNumPart4Fil = "";
			   String sNumPart5Fil = "";
			
			
			   String sNumPart1 = getStoreNum();
			   String fSl1 = " ";
			   String sNumPart2 = getStoreNumDigit1();
			   String fSl2 = "";
			   String sNumPart3 = getStoreNumDigit2();
			   String sNumPart5 =  getStoreNumDigit4();
			   String fS14 = "";
			   
			   if(sNumPart3.isEmpty()){
				   
				   sNumPart3Fil = "";
			   }else{
				   
				   fSl2 = "/";
				   sNumPart3Fil = sNumPart3;
			   }
			   
			   String fSl3 = "";
			   
			   String sNumPart4 = getStoreNumDigit3();
			   
			   if(sNumPart4.isEmpty()){
				   
				   sNumPart4Fil = "";
			   }else{
				   
				   fSl3 = "/";
				   sNumPart4Fil = sNumPart4;
			   }
			   
			   if(sNumPart5.isEmpty()){
				   
				   sNumPart5Fil = "";
				   
			   }else{
				   
				   fS14 = "/";
				   sNumPart5Fil = sNumPart5;
			   }		   
			   
	        sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
	        
	        
	        String fileNum = getFilenum() + " " + getFilenum1() + "/" + getFilenum2();
			
	       try {
	    	   
	        detailsOfLpAndSiOrdersDAO.updateDates(fileNum, sStoreNumber, getSelecteddate(), getDatesforevent(), getInvoicenum(), getDepuser(), getRole(), dateTime);
	
	       }catch(ConstraintViolationException   cve) {
	    		
	             return ERROR;
	  }
	       
	       
	       try{
	            
	            
	            db.connect();
	            
	            String testEnt = "";
	         		   
	            
	           ResultSet checkEntered = db.execSql("SELECT user_date_lupdate FROM details_lp_si_table WHERE store_num='" + sStoreNumber + "' AND file_num='" + fileNum + "'");
	            
	            while(checkEntered.next()){
	         	   
	         	   testEnt = checkEntered.getString("user_date_lupdate");
	         	   
	         	   if(testEnt.matches("Not processed")){
	         		   
	         		   return REPEAT2;
	         		   
	         	   }
	         	   
	            }
	            
	         }catch(Exception  ex){
	         	
	         	ex.printStackTrace();
	     }
	               
	       
	       
	        
	return SUCCESS;
}

public String updatePcmApproval() {
	
	HttpSession  session = request.getSession();

	user = (String)session.getAttribute("str");
	role1 = (String)session.getAttribute("strRole");
	subDept = (String)session.getAttribute("subDept");
	
	String checkChangeQuanByPcm = getChangequanbypcm();
	double oldTecQuan = 0.0;
	String oldTecUnits = "";
	double unitPrice = 0.0;

	    	
	Object  isLogedIn = session.getAttribute("isLogedIn");
			
			if(isLogedIn == null) {
				
			   return NONE;
			}
			
			session.setMaxInactiveInterval(1000);
			
			
	        TimeAndDate   td = new TimeAndDate();
			
			enteredDate = td.Date();
			time = td.Time();
			
			String  dateTime  =  enteredDate  +  time;
			
			
			   String sNumPart3Fil = "";
			   String sNumPart4Fil = "";
			   String sNumPart5Fil = "";
			
			
			   String sNumPart1 = getStoreNum();
			   String fSl1 = " ";
			   String sNumPart2 = getStoreNumDigit1();
			   String fSl2 = "";
			   String sNumPart3 = getStoreNumDigit2();
			   String sNumPart5 =  getStoreNumDigit4();
			   String fS14 = "";
			   
			   if(sNumPart3.isEmpty()){
				   
				   sNumPart3Fil = "";
			   }else{
				   
				   fSl2 = "/";
				   sNumPart3Fil = sNumPart3;
			   }
			   
			   String fSl3 = "";
			   
			   String sNumPart4 = getStoreNumDigit3();
			   
			   if(sNumPart4.isEmpty()){
				   
				   sNumPart4Fil = "";
			   }else{
				   
				   fSl3 = "/";
				   sNumPart4Fil = sNumPart4;
			   }
			   
			   if(sNumPart5.isEmpty()){
				   
				   sNumPart5Fil = "";
				   
			   }else{
				   
				   fS14 = "/";
				   sNumPart5Fil = sNumPart5;
			   }
			   
			   
	        sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
	        
	        
	        String fileNum = getFilenum() + " " + getFilenum1() + "/" + getFilenum2();
	        
	        
	        if(checkChangeQuanByPcm.equals("true")){
	        	
	        	String check = "T";
	        	
	        	try{
	        		
	        		db.connect();
	        		
	        		ResultSet rsPcm = db.execSql("SELECT ordered_quantity,ordered_units, unit_price FROM details_lp_si_table WHERE store_num='" + sStoreNumber + "' AND file_num='"  + fileNum + "'");
	        		
	        		while(rsPcm.next()){
	        			
	        			oldTecQuan = rsPcm.getDouble("ordered_quantity");
	        			oldTecUnits = rsPcm.getString("ordered_units");
	        			unitPrice = rsPcm.getDouble("unit_price");
	        			
	        		}
	        		
	        		double totalPcmPrice = getPcmquantity() * unitPrice;
	        		
	        		
	        		detailsOfLpAndSiOrdersDAO.updatePcmQuantityApproval(fileNum, sStoreNumber, getPcmapproval(), getPcmguide(), getDepuser(), getRole(), dateTime, oldTecQuan,getPcmquantity(), getPcmunits(), totalPcmPrice, check);
	        		
	        		
	        	}catch(Exception    ex){
	        		
	        		ex.printStackTrace();
	        		
	        	}
	        	
	        	
	       }else{
	        
			
	        try {
	        	
	        detailsOfLpAndSiOrdersDAO.updatePcmApproval(fileNum, sStoreNumber, getPcmapproval(), getPcmguide(), getDepuser(), getRole(), dateTime);
	
	       }catch(ConstraintViolationException   cve) {
	    		
	             return ERROR;
	    	}
	        
	    }  
	        
try{
	            
	            
	            db.connect();
	            
	            String testEnt = "";
	         		   
	            
	           ResultSet checkEntered = db.execSql("SELECT pcm_app FROM details_lp_si_table WHERE store_num='" + sStoreNumber + "' AND file_num='" + fileNum + "'");
	            
	            while(checkEntered.next()){
	         	   
	         	   testEnt = checkEntered.getString("pcm_app");
	         	   
	         	   if(testEnt.matches("Not processed")){
	         		   
	         		   return REPEAT1;
	         		   
	         	   }
	         	   
	         	   db.close();
	            }
	            
	         }catch(Exception  ex){
	         	
	         	ex.printStackTrace();
	     }
	            
	         
	        
	return SUCCESS;
}


public String updateSuitability() {
	
	
	HttpSession  session = request.getSession();

	user = (String)session.getAttribute("str");
	role1 = (String)session.getAttribute("strRole");
	subDept = (String)session.getAttribute("subDept");

	    	
	Object  isLogedIn = session.getAttribute("isLogedIn");
			
			if(isLogedIn == null) {
				
			   return NONE;
			}
			
			session.setMaxInactiveInterval(1000);
			
			
	        TimeAndDate   td = new TimeAndDate();
			
			enteredDate = td.Date();
			time = td.Time();
			
			String  dateTime  =  enteredDate  +  time;
			
			
			   String sNumPart3Fil = "";
			   String sNumPart4Fil = "";
			   String sNumPart5Fil = "";
			
			
			   String sNumPart1 = getStoreNum();
			   String fSl1 = " ";
			   String sNumPart2 = getStoreNumDigit1();
			   String fSl2 = "";
			   String sNumPart3 = getStoreNumDigit2();
			   String sNumPart5 =  getStoreNumDigit4();
			   String fS14 = "";
			   
			   if(sNumPart3.isEmpty()){
				   
				   sNumPart3Fil = "";
			   }else{
				   
				   fSl2 = "/";
				   sNumPart3Fil = sNumPart3;
			   }
			   
			   String fSl3 = "";
			   
			   String sNumPart4 = getStoreNumDigit3();
			   
			   if(sNumPart4.isEmpty()){
				   
				   sNumPart4Fil = "";
			   }else{
				   
				   fSl3 = "/";
				   sNumPart4Fil = sNumPart4;
			   }
			   
			   if(sNumPart5.isEmpty()){
				   
				   sNumPart5Fil = "";
				   
			   }else{
				   
				   fS14 = "/";
				   sNumPart5Fil = sNumPart5;
			   }		   
			   
	        sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
	        
	        String fileNum = getFilenum() + " " + getFilenum1() + "/" + getFilenum2();
			
	       try { 
	        detailsOfLpAndSiOrdersDAO.updateSuitability(fileNum, sStoreNumber, getSuitabilitycond(), getReasons(), getAmount(), getDamamount(), getSuitgivendate(), getDepuser(), getRole(), dateTime);
	
	       }catch(ConstraintViolationException   cve) {
	    		
	             return ERROR;
	    	}
	       
	       
	       try{
	            
	            
	            db.connect();
	            
	            String testEnt = "";
	         		   
	            
	           ResultSet checkEntered = db.execSql("SELECT suitability FROM details_lp_si_table WHERE store_num='" + sStoreNumber + "' AND file_num='" + fileNum + "'");
	            
	            while(checkEntered.next()){
	         	   
	         	   testEnt = checkEntered.getString("suitability");
	         	   
	         	   if(testEnt.matches("Not processed")){
	         		   
	         		   return REPEAT3;
	         		   
	         	   }
	         	   
	            }
	            
	         }catch(Exception  ex){
	         	
	         	ex.printStackTrace();
	     }
	               
	       
	       
	        
	return SUCCESS;
}

public String getFilteredDetails(){
	
	
	HttpSession  session = request.getSession();

	
	    	
	Object  isLogedIn = session.getAttribute("isLogedIn");
			
			if(isLogedIn == null) {
				
			   return NONE;
			}
			
			session.setMaxInactiveInterval(1000);
			
	
	getFilteredList = detailsOfLpAndSiOrdersDAO.getFilteredList(getType(), getStartdate(), getEnddate());
	
	if(getFilteredList.isEmpty()){
		
		return ERROR;
		
	}else{
	
	return SUCCESS;
	
	}
}
	
public String getFileList(){
	
	/*HttpSession  session = request.getSession();

	
	
	Object  isLogedIn = session.getAttribute("isLogedIn");
			
			if(isLogedIn == null) {
				
			   return NONE;
			}
			
			session.setMaxInactiveInterval(1000);
			
			
		*/	
			
			   String sNumPart3Fil = "";
			   String sNumPart4Fil = "";
			   String sNumPart5Fil = "";
			  
			
			   String sNumPart1 = getStoreNum();
			   String fSl1 = " ";
			   String sNumPart2 = getStoreNumDigit1();
			   String fSl2 = "";
			   String sNumPart3 = getStoreNumDigit2();
			   String sNumPart5 =  getStoreNumDigit4();
			   String fS14 = "";
			   
			   if(sNumPart3.isEmpty()){
				   
				   sNumPart3Fil = "";
			   }else{
				   
				   fSl2 = "/";
				   sNumPart3Fil = sNumPart3;
			   }
			   
			   String fSl3 = "";
			   
			   String sNumPart4 = getStoreNumDigit3();
			   
			   if(sNumPart4.isEmpty()){
				   
				   sNumPart4Fil = "";
			   }else{
				   
				   fSl3 = "/";
				   sNumPart4Fil = sNumPart4;
			   }
			   
			   if(sNumPart5.isEmpty()){
				   
				   sNumPart5Fil = "";
				   
			   }else{
				   
				   fS14 = "/";
				   sNumPart5Fil = sNumPart5;
			   }		   
			   
	        sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
			
			
			
			getFilesList =  detailsOfLpAndSiOrdersDAO.getFileList(getType(),getStorenumber());
	
			
			/*if(getFilesList.isEmpty()){
				
				return ERROR;
				
			}else{
			*/
			return SUCCESS;
			
			//}
	
}


public String getFileDetailsList(){
	
	/*Get Store Number from CheckSiLpPositionCme.jsp*/
	
	   String sNumPart3Fil = "";
	   String sNumPart4Fil = "";
	   String sNumPart5Fil = "";
	  
	
	   String sNumPart1 = getStoreNum();
	   String fSl1 = " ";
	   String sNumPart2 = getStoreNumDigit1();
	   String fSl2 = "";
	   String sNumPart3 = getStoreNumDigit2();
	   String sNumPart5 =  getStoreNumDigit4();
	   String fS14 = "";
	   
	   if(sNumPart3.isEmpty()){
		   
		   sNumPart3Fil = "";
	   }else{
		   
		   fSl2 = "/";
		   sNumPart3Fil = sNumPart3;
	   }
	   
	   String fSl3 = "";
	   
	   String sNumPart4 = getStoreNumDigit3();
	   
	   if(sNumPart4.isEmpty()){
		   
		   sNumPart4Fil = "";
	   }else{
		   
		   fSl3 = "/";
		   sNumPart4Fil = sNumPart4;
	   }
	   
	   if(sNumPart5.isEmpty()){
		   
		   sNumPart5Fil = "";
		   
	   }else{
		   
		   fS14 = "/";
		   sNumPart5Fil = sNumPart5;
	   }		   
	   
             sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
	
	
	
	
	     /*Get File number from CheckSiLpPositionCme.jsp*/
	
	
	     String fileNum = "";
	
	     String first = getFilenum();
	     
	     String second = getFilenum1();
	     
	     String third = getFilenum2();
	     
	     
	     if((first.isEmpty()) && (second.isEmpty()) && (third.isEmpty())){
	    	 
	    	 fileNum = "";
	    	 
	     }else if((second.isEmpty()) && (third.isEmpty())){
	    	 
	    	 fileNum = first;
	    	 
	     }else if(third.isEmpty()){
	    	 
	    	 fileNum = first + " " + second;
	    	 
	     }else{
	    	 
	    	 
	    	 fileNum = first + " " + second + "/" + third;
	     }
	
	     //String fileNum = getFilenum() + " " + getFilenum1() + "/" + getFilenum2();
	     
	     
         if(fileNum.isEmpty()){
	    	 
        	 
        	  /*This is return outstanding details*/
        	 	
        	  getOutstandsFrom = detailsOfLpAndSiOrdersDAO.getOutstandingKnownStockItem(sStoreNumber);
        	  
        	  return OUTSTANDING;
      	
	    	 //getFilesDetails = detailsOfLpAndSiOrdersDAO.getStockItemsOfFiles(fileNum);
	    	 
	     }else{
	
	    	   
	    	   getFilesDetails = detailsOfLpAndSiOrdersDAO.getSearchStockItemDetails(fileNum, sStoreNumber);
	    	   
	    	    //getFilesList =  detailsOfLpAndSiOrdersDAO.getFileList(getType(), storeNum);	
	    		getAllDetailsOfStockItems = detailsOfLpAndSiOrdersDAO.getAllDetailsOfStockItemsOfFiles(fileNum, sStoreNumber);
	    		getDes = inventoryItemsMainLedgerDAO.getListInventoryItemsMainLedger(sStoreNumber);
	     
	         //getFilesDetails = detailsOfLpAndSiOrdersDAO.getFilesDetails(fileNum,getStartdate(),getEnddate());
	         
	         
	     }
	     
	     
	     /*if((getStartdate().isEmpty()) && (getEnddate().isEmpty()) ){
	    	 
	    	 
	    	 getFilesDetails = detailsOfLpAndSiOrdersDAO.getStockItemsOfFiles(fileNum);
	    	 
	     }else{
	
	     
	         getFilesDetails = detailsOfLpAndSiOrdersDAO.getFilesDetails(fileNum,getStartdate(),getEnddate());
	     
	     }*/
	
	return SUCCESS;
}


public String getStockItemsDetailsOfFile(){
	
	String fileNumber = getFilenumber();
	
	getStockItemDetailsOfFiles = detailsOfLpAndSiOrdersDAO.getStockItemsOfFiles(fileNumber);
	
	return SUCCESS;
}


public String getAllDetailsOfStockItemsOfFiles(){
	
	String storeNum = getStorenumber();
	String fileNumber = getFilenumber();
	
	getFilesList =  detailsOfLpAndSiOrdersDAO.getFileList(getType(), storeNum);	
	getAllDetailsOfStockItems = detailsOfLpAndSiOrdersDAO.getAllDetailsOfStockItemsOfFiles(storeNum, fileNumber);
	getDes = inventoryItemsMainLedgerDAO.getListInventoryItemsMainLedger(storeNum);
	
	return SUCCESS;
}

public String getOutstandingList(){
	
	String sNumPart3Fil = "";
	   String sNumPart4Fil = "";
	   String sNumPart5Fil = "";
	
	
	   String sNumPart1 = getStoreNum();
	   String fSl1 = " ";
	   String sNumPart2 = getStoreNumDigit1();
	   String fSl2 = "";
	   String sNumPart3 = getStoreNumDigit2();
	   String sNumPart5 =  getStoreNumDigit4();
	   String fS14 = "";
	   
	   if(sNumPart3.isEmpty()){
		   
		   sNumPart3Fil = "";
	   }else{
		   
		   fSl2 = "/";
		   sNumPart3Fil = sNumPart3;
	   }
	   
	   String fSl3 = "";
	   
	   String sNumPart4 = getStoreNumDigit3();
	   
	   if(sNumPart4.isEmpty()){
		   
		   sNumPart4Fil = "";
	   }else{
		   
		   fSl3 = "/";
		   sNumPart4Fil = sNumPart4;
	   }
	   
	   if(sNumPart5.isEmpty()){
		   
		   sNumPart5Fil = "";
		   
	   }else{
		   
		   fS14 = "/";
		   sNumPart5Fil = sNumPart5;
	   }		   
	   
 sStoreNumber = sNumPart1 + fSl1 + sNumPart2 + fSl2 + sNumPart3Fil + fSl3 + sNumPart4Fil + fS14 + sNumPart5Fil;
   
       String GetKnownSelectItem = getSelectknownitem();
       
       if(GetKnownSelectItem.matches("Known Stock Item")){
    	   
    	   getOutstandsFrom = detailsOfLpAndSiOrdersDAO.getOutstandingKnownStockItem(sStoreNumber);
    	   
    	   
       }else{
        
      
        getOutstandsFrom  =  detailsOfLpAndSiOrdersDAO.getOutstanding(sStoreNumber);
	
       }
	
	return SUCCESS;
}


/*
 * This method get details of Stock items in SI/LP files and 
 * its conditions.
 */

public String getDetailsOfStockItemsInFiles(){
	
	
	String fileN = getFilenumber();
	
	
	   getDetOfStoIteInFil = detailsOfLpAndSiOrdersDAO.getStockItemDetailsInFiles(fileN);
	
	
	return SUCCESS;
}


public String getTypeorder() {
	return this.typeorder;
}

public void setTypeorder(String typeorder) {
	this.typeorder = typeorder;
}

	public String getsStoreNumber() {
		return this.sStoreNumber;
	}

	public void setsStoreNumber(String sStoreNumber) {
		this.sStoreNumber = sStoreNumber;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getEnteredDate() {
		return this.enteredDate;
	}

	public void setEnteredDate(String enteredDate) {
		this.enteredDate = enteredDate;
	}

	public String getTime() {
		return this.time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getSubDept() {
		return this.subDept;
	}

	public void setSubDept(String subDept) {
		this.subDept = subDept;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStoreNum() {
		return this.storeNum;
	}

	public void setStoreNum(String storeNum) {
		this.storeNum = storeNum;
	}

	public String getStoreNumDigit1() {
		return this.storeNumDigit1;
	}

	public void setStoreNumDigit1(String storeNumDigit1) {
		this.storeNumDigit1 = storeNumDigit1;
	}

	public String getStoreNumDigit2() {
		return this.storeNumDigit2;
	}

	public void setStoreNumDigit2(String storeNumDigit2) {
		this.storeNumDigit2 = storeNumDigit2;
	}

	public String getStoreNumDigit3() {
		return this.storeNumDigit3;
	}

	public void setStoreNumDigit3(String storeNumDigit3) {
		this.storeNumDigit3 = storeNumDigit3;
	}

	public String getStoreNumDigit4() {
		return this.storeNumDigit4;
	}

	public void setStoreNumDigit4(String storeNumDigit4) {
		this.storeNumDigit4 = storeNumDigit4;
	}

	public String getLponum() {
		return this.lponum;
	}

	public void setLponum(String lponum) {
		this.lponum = lponum;
	}

	public String getShop() {
		return this.shop;
	}

	public void setShop(String shop) {
		this.shop = shop;
	}

	public String getSubdept() {
		return this.subdept;
	}

	public void setSubdept(String subdept) {
		this.subdept = subdept;
	}

	public String getDepuser() {
		return this.depuser;
	}

	public void setDepuser(String depuser) {
		this.depuser = depuser;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getFilenum() {
		return this.filenum;
	}

	public void setFilenum(String filenum) {
		this.filenum = filenum;
	}
	

	public String getFilenum1() {
		return filenum1;
	}

	public void setFilenum1(String filenum1) {
		this.filenum1 = filenum1;
	}

	public String getFilenum2() {
		return filenum2;
	}

	public void setFilenum2(String filenum2) {
		this.filenum2 = filenum2;
	}
	
	

	public String getFilenumsame() {
		return filenumsame;
	}
	
	public void setFilenumsame(String filenumsame) {
		this.filenumsame = filenumsame;
	}

	public String getDatefile() {
		return datefile;
	}

	public void setDatefile(String datefile) {
		this.datefile = datefile;
	}

	public String getNewfiledate() {
		return this.newfiledate;
	}

	public void setNewfiledate(String newfiledate) {
		this.newfiledate = newfiledate;
	}

	public String getDate1() {
		return this.date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}
	
	
	

	public String getTecrecommend() {
		return this.tecrecommend;
	}

	public void setTecrecommend(String tecrecommend) {
		this.tecrecommend = tecrecommend;
	}

	public String getTecrecdate() {
		return this.tecrecdate;
	}

	public void setTecrecdate(String tecrecdate) {
		this.tecrecdate = tecrecdate;
	}
	
	

	public int getTecround() {
		return tecround;
	}

	public void setTecround(int tecround) {
		this.tecround = tecround;
	}

	public String getSuccBidder() {
		return this.succBidder;
	}

	public void setSuccBidder(String succBidder) {
		this.succBidder = succBidder;
	}
	
	

	public String getSelection() {
		return selection;
	}

	public void setSelection(String selection) {
		this.selection = selection;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	

	public double getQuantity() {
		return this.quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	

	public String getUnits() {
		return this.units;
	}

	public void setUnits(String units) {
		this.units = units;
	}
	

	public double getOrderedQuantity() {
		return orderedQuantity;
	}

	public void setOrderedQuantity(double orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}

	public String getOrderedUnit() {
		return orderedUnit;
	}

	public void setOrderedUnit(String orderedUnit) {
		this.orderedUnit = orderedUnit;
	}

	public double getUprice() {
		return this.uprice;
	}

	public void setUprice(double uprice) {
		this.uprice = uprice;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getSelecteddate() {
		return this.selecteddate;
	}

	public void setSelecteddate(String selecteddate) {
		this.selecteddate = selecteddate;
	}

	public String getInvoicenum() {
		return this.invoicenum;
	}

	public void setInvoicenum(String invoicenum) {
		this.invoicenum = invoicenum;
	}
	
	public String getDatesforevent() {
		return this.datesforevent;
	}

	public void setDatesforevent(String datesforevent) {
		this.datesforevent = datesforevent;
	}

	public String getPcmapproval() {
		return this.pcmapproval;
	}

	public void setPcmapproval(String pcmapproval) {
		this.pcmapproval = pcmapproval;
	}
	

	public String getPcmguide() {
		return pcmguide;
	}

	public void setPcmguide(String pcmguide) {
		this.pcmguide = pcmguide;
	}

	public String getSuitabilitycond() {
		return this.suitabilitycond;
	}

	public void setSuitabilitycond(String suitabilitycond) {
		this.suitabilitycond = suitabilitycond;
	}

	public String getReasons() {
		return this.reasons;
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
	}

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDamamount() {
		return this.damamount;
	}

	public void setDamamount(String damamount) {
		this.damamount = damamount;
	}

	public String getSuitgivendate() {
		return this.suitgivendate;
	}

	public void setSuitgivendate(String suitgivendate) {
		this.suitgivendate = suitgivendate;
	}

	

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public DetailsOfLpAndSiOrders getDetailsOfLpAndSiOrders() {
		return this.detailsOfLpAndSiOrders;
	}
    
	public void setDetailsOfLpAndSiOrders(
			DetailsOfLpAndSiOrders detailsOfLpAndSiOrders) {
		this.detailsOfLpAndSiOrders = detailsOfLpAndSiOrders;
	}
	
	public List<DetailsOfLpAndSiOrders> getGetList() {
		return getList;
	}

	public void setGetList(List<DetailsOfLpAndSiOrders> getList) {
		this.getList = getList;
	}
	

	public List<DetailsOfLpAndSiOrders> getGetFilteredList() {
		return getFilteredList;
	}

	public void setGetFilteredList(List<DetailsOfLpAndSiOrders> getFilteredList) {
		this.getFilteredList = getFilteredList;
	}

	public TecRecommondHistory getTecRecHis() {
		return tecRecHis;
	}

	public void setTecRecHis(TecRecommondHistory tecRecHis) {
		this.tecRecHis = tecRecHis;
	}

	public List<DetailsOfLpAndSiOrders> getGetFilesList() {
		return getFilesList;
	}

	public void setGetFilesList(List<DetailsOfLpAndSiOrders> getFilesList) {
		this.getFilesList = getFilesList;
	}

	public String getMessege() {
		return messege;
	}

	public void setMessege(String messege) {
		this.messege = messege;
	}

	public String getChangequanbypcm() {
		return changequanbypcm;
	}

	public void setChangequanbypcm(String changequanbypcm) {
		this.changequanbypcm = changequanbypcm;
	}

	public double getPcmquantity() {
		return pcmquantity;
	}

	public void setPcmquantity(double pcmquantity) {
		this.pcmquantity = pcmquantity;
	}

	public String getPcmunits() {
		return pcmunits;
	}

	public void setPcmunits(String pcmunits) {
		this.pcmunits = pcmunits;
	}

	public String getStorenumber() {
		return storenumber;
	}

	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}

	public String getSelectknownitem() {
		return selectknownitem;
	}

	public void setSelectknownitem(String selectknownitem) {
		this.selectknownitem = selectknownitem;
	}

	public String getFilenumber() {
		return filenumber;
	}

	public void setFilenumber(String filenumber) {
		this.filenumber = filenumber;
	}
	
	public DetailsOfLpSiTableCopy getDetailsOfLpSiTableCopy() {
		return detailsOfLpSiTableCopy;
	}

	public void setDetailsOfLpSiTableCopy(
			DetailsOfLpSiTableCopy detailsOfLpSiTableCopy) {
		this.detailsOfLpSiTableCopy = detailsOfLpSiTableCopy;
	}

	public String getThirdpartyinspectrep() {
		return thirdpartyinspectrep;
	}

	public void setThirdpartyinspectrep(String thirdpartyinspectrep) {
		this.thirdpartyinspectrep = thirdpartyinspectrep;
	}

	public List<DetailsOfLpAndSiOrders> getGetFilesDetails() {
		return getFilesDetails;
	}

	public void setGetFilesDetails(List<DetailsOfLpAndSiOrders> getFilesDetails) {
		this.getFilesDetails = getFilesDetails;
	}

	public List<DetailsOfLpAndSiOrders> getGetStockItemDetailsOfFiles() {
		return getStockItemDetailsOfFiles;
	}

	public void setGetStockItemDetailsOfFiles(
			List<DetailsOfLpAndSiOrders> getStockItemDetailsOfFiles) {
		this.getStockItemDetailsOfFiles = getStockItemDetailsOfFiles;
	}

	public List<DetailsOfLpAndSiOrders> getGetAllDetailsOfStockItems() {
		return getAllDetailsOfStockItems;
	}

	public void setGetAllDetailsOfStockItems(
			List<DetailsOfLpAndSiOrders> getAllDetailsOfStockItems) {
		this.getAllDetailsOfStockItems = getAllDetailsOfStockItems;
	}

	

	public List<InventoryItemsMainLedger> getGetDes() {
		return getDes;
	}

	public void setGetDes(List<InventoryItemsMainLedger> getDes) {
		this.getDes = getDes;
	}

	public InventoryItemsMainLedger getInventoryItemsMainLedger() {
		return inventoryItemsMainLedger;
	}

	public void setInventoryItemsMainLedger(
			InventoryItemsMainLedger inventoryItemsMainLedger) {
		this.inventoryItemsMainLedger = inventoryItemsMainLedger;
	}

	public List<DetailsOfLpAndSiOrders> getGetOutstandsFrom() {
		return getOutstandsFrom;
	}

	public void setGetOutstandsFrom(List<DetailsOfLpAndSiOrders> getOutstandsFrom) {
		this.getOutstandsFrom = getOutstandsFrom;
	}

	public List<DetailsOfLpAndSiOrders> getGetDetOfStoIteInFil() {
		return getDetOfStoIteInFil;
	}

	public void setGetDetOfStoIteInFil(
			List<DetailsOfLpAndSiOrders> getDetOfStoIteInFil) {
		this.getDetOfStoIteInFil = getDetOfStoIteInFil;
	}

	public String getLcopen() {
		return lcopen;
	}

	public void setLcopen(String lcopen) {
		this.lcopen = lcopen;
	}

	public String getLcnumber() {
		return lcnumber;
	}

	public void setLcnumber(String lcnumber) {
		this.lcnumber = lcnumber;
	}

	public String getLcdate() {
		return lcdate;
	}

	public void setLcdate(String lcdate) {
		this.lcdate = lcdate;
	}
	
	
    
    
}
