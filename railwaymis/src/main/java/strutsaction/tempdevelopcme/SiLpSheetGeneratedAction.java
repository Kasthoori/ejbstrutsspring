/**
 * 
 */
package strutsaction.tempdevelopcme;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import pk1.DbBean;

import hibernate.domain.tempdevelopcme.SiLpSheetGenerated;
import hibernate.service.tempdevelopcme.SiLpSheetGeneratedDAO;
import hibernate.service.tempdevelopcme.SiLpSheetGeneratedDAOImpl;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;


/**
 * @author A.P.Kasthoori
 *
 */
public class SiLpSheetGeneratedAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String date;
	private String repsheet;
	
	
	SiLpSheetGenerated    siLpSheetGenerated = new SiLpSheetGenerated();
	SiLpSheetGeneratedDAO  siLpSheetGeneratedDAO = new SiLpSheetGeneratedDAOImpl();
	List<SiLpSheetGenerated>  getItems = new ArrayList<SiLpSheetGenerated>();
	List<SiLpSheetGenerated>  getLists = new ArrayList<SiLpSheetGenerated>();
	
	List<String>  itemdes = new ArrayList<String>();
	
	public String execute(){
		
		
		return SUCCESS;
		
		
	}
	
	
	public String getListItems(){
		
		String date = getDate();
		
		getItems = siLpSheetGeneratedDAO.getSiLpSheetItems(date);
		
		
		return SUCCESS;
	}
	
	public String getPastList(){
		
		String repNum = getRepsheet();
		
		getLists = siLpSheetGeneratedDAO.getDetails(repNum);
		
		
		
		return SUCCESS;
	}
	

	public String getDate() {
		return date;
	}

  


	public void setDate(String date) {
		this.date = date;
	}


	public String getRepsheet() {
		return repsheet;
	}


	public void setRepsheet(String repsheet) {
		this.repsheet = repsheet;
	}


	public SiLpSheetGenerated getSiLpSheetGenerated() {
		return siLpSheetGenerated;
	}




	public void setSiLpSheetGenerated(SiLpSheetGenerated siLpSheetGenerated) {
		this.siLpSheetGenerated = siLpSheetGenerated;
	}




	public List<SiLpSheetGenerated> getGetItems() {
		return getItems;
	}




	public void setGetItems(List<SiLpSheetGenerated> getItems) {
		this.getItems = getItems;
	}


	public List<SiLpSheetGenerated> getGetLists() {
		return getLists;
	}


	public void setGetLists(List<SiLpSheetGenerated> getLists) {
		this.getLists = getLists;
	}


	public List<String> getItemdes() {
		return itemdes;
	}


	public void setItemdes(List<String> itemdes) {
		this.itemdes = itemdes;
	}
	
	
	


}
