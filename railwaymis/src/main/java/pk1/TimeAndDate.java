package pk1;

public class TimeAndDate {
	
	public String Date(){
		
		java.util.Date  dt = new java.util.Date();
        java.text.SimpleDateFormat  sdt = new java.text.SimpleDateFormat("yyyy-MM-dd");
        
        String currentdate = sdt.format(dt);
        
        return currentdate;
	}
	
	public String Time(){
		
		java.util.Date  t = new java.util.Date();
        java.text.SimpleDateFormat  stm = new java.text.SimpleDateFormat("HH:mm:ss");
        
        String currentTime = stm.format(t);
        return currentTime;
	}

}
